<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_unika_site');


/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');


/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');


/** Nome do host do MySQL */
define('DB_HOST', 'localhost');


/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');


/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pZ@!Um98!mG}{&~R1*?:9NSgoA`s8,O|J87a5{QsMKV~#3uP9A^n@^F!FHe%0Z_P');

define('SECURE_AUTH_KEY',  'M ]h`!YWIE[0q6dfA<;W:gq!,7@ )/O`&B]B~9.I~Be(2!q9S9D<8Gz$:~{x/:fq');

define('LOGGED_IN_KEY',    '=UvyPBBZ%-=!g|C-9nqZao.5Gb3X]bgGV>]M<vZby!,%;5}dd<w}p4538?P37@{I');

define('NONCE_KEY',        '-obtgdOCH7qzOlhiTw|eZ.)edjk*_Pa$B+WpE=C7KVmk_oDe,F7:kw(~-Vxa-I+5');

define('AUTH_SALT',        '{+1IOg>MX+!q:S*v}?-@Kg;B3Jr+~VQBx5*{{#9qvdY|b3<U},pSqFq+XsChY`^v');

define('SECURE_AUTH_SALT', 'cK0!k9& k@ G&BwN/6U/M?u4`SG~%m:*Tm!Q<SK4UO/Y5?x]$o|+WMzXD {+fmdP');

define('LOGGED_IN_SALT',   'GU:)& 3^ijaD=77a69WUW15Hf-D_zn0/_!-N00mHzqaxISE,5 1<mCu~{Z{A|0i#');

define('NONCE_SALT',       'a!aN5tLSS$g[~h]VG/[T=]=SkrQY,((5Hwiz[K$3U*-APtg:8ywJkDN;bc,y}zmf');


/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'uk_';


/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
