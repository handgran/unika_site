<?php
/**
 * Template Name: Inicial
 * Description: Página inicial
 *
 * @package Unika
 */

get_header(); ?>
<!-- PÁGINA INICIAL -->
<div class="pg pg-inicial">
	
	<!-- SESSÃO QUEM SOMOS / CARROSSEL DESTAQUE -->
	<section class="sessaoDestaque">
		<h6 class="hidden">Destaque</h6>
		
			<div class="containerFull carosselDestaque">
				<div class="row">
					<div class="col-sm-5">
						<div class="areaQuemSomos">
							<strong><?php echo $configuracao['pg_inicial_Destque_titulo'] ?></strong>
							<p><?php echo $configuracao['pg_inicial_Destque_texto'] ?></p>
							<a href="<?php echo home_url('/quem-somos/#quemsomos'); ?>">Quem somos</a>
						</div>

						<div class="btnsCarrossel">
							<button id="btnCarrosselDestaqueLeft">
								<i class="fa fa-angle-left" aria-hidden="true"></i>
							</button>
							<span>1/6</span>
							<button id="btnCarrosselDestaqueRight">
								<i class="fa fa-angle-right" aria-hidden="true"></i>
							</button>
						</div>
					</div>
					<div class="col-sm-7 areaCarrossel">
						<div class="carrossel">
							<div id="carrosselDestaque" class="owl-Carousel">
								<?php 
									//LOOP DE POST DESTAQUES
									$posDestaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
									while ( $posDestaques->have_posts() ) : $posDestaques->the_post();
										$fotoDestaque = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
										$fotoDestaque = $fotoDestaque[0];
										$destaque_cor = rwmb_meta('Unika_destaque_cor');
										$destaque_cor_circulo = rwmb_meta('Unika_destaque_cor_circulo');
										$destaque_link = rwmb_meta('Unika_destaque_link');
										$destaque_cor_texto = rwmb_meta('Unika_destaque_cor_texto');
									
								 ?>
								<!-- ITEM -->
								<a href="<?php echo $destaque_link ?>" class="item" style="background:<?php echo $destaque_cor ?>">
									
										<figure style="background:<?php echo $destaque_cor_circulo ?>">
											<img class="img-responsive" src="<?php echo $fotoDestaque ?>" alt="<?php echo get_the_title() ?>">
										</figure>
										<h2 style="color:<?php echo $destaque_cor_texto  ?>  "><?php echo get_the_title() ?></h2>
										<p style="color: <?php echo $destaque_cor_texto  ?> "><?php echo get_the_content() ?></p>

										<span style="color:<?php echo $destaque_cor_texto  ?>">Saiba Mais</span>
									
								</a>
								<?php endwhile; wp_reset_query(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		


	</section>

	

	<!-- SESSÃO INFORMAÇÕES -->
	<section class="sessaoInfo carrosselTelaMenor">
		
		<div class="containerFull carosselDestaque">
			<h6></h6>
			<div class="row">
				<div class="col-sm-5">
					<figure class="imagemIlustrativa" style="background: url(<?php echo $configuracao['pg_inicial_info_foto']['url'] ?>)">

					</figure>
				</div>
				<div class="col-sm-7">
					<div class="areaTextoSkell">
						<article>
							<h5><?php echo $configuracao['pg_inicial_info_titulo'] ?></h5>
							<p><?php echo $configuracao['pg_inicial_info_texto'] ?></p>
							<a href="<?php echo home_url('/quem-somos/'); ?>">Saiba mais</a>
						</article>
						<img  src="<?php bloginfo('template_directory'); ?>/img/quadrado_branco.png" alt="Diagonal" class="skewll">
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- SESSÃO DE SERVIÇOS -->
	<section class="listasessaoServicos background">
		<h6><?php echo $configuracao['pg_inicial_problemas_titulo'] ?></h6>
			<p><?php echo $configuracao['pg_inicial_problemas_texto'] ?></p>
		<div class="container">
			
			<ul>
				<?php 
					//LOOP DE POST SERVIÇOS
					$posServicos = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 5) );
					while ( $posServicos->have_posts() ) : $posServicos->the_post();
					$fotoServicos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoServicos = $fotoServicos[0];
					
				 ?>
				<!-- SERVIÇOS -->
				<li>
					<div class="servico">
						<img src="<?php echo $fotoServicos ?>" alt="<?php echo get_the_title() ?>">
						<p><?php echo get_the_content() ?></p>
					</div>
				</li>
				<?php endwhile; wp_reset_query(); ?>

				<!-- SERVIÇOS -->
				<li class="maisPorblemas">
					<a href="<?php echo home_url('/quem-somos/#problemas-que-resolvemos'); ?>" class="linkMaisProblemas">
						<p>+ 11 Problemas</p>
					</a>
				</li>
			</ul>

		</div>

	</section>

	<!-- SESSÃO ONDE ATUAMOS -->
	<section class="sessaoOndeAtuamos background">
		
		<div class="container correcaoPX">
			<div class="row">
				<div class="col-md-6">
					<div class="titulo">
						<h6><?php echo $configuracao['pg_inicial_onde_titulo'] ?></h6>
						<p><?php echo $configuracao['pg_inicial_onde_texto'] ?></p>

						<!-- <a href="<?php echo home_url('/contato/'); ?>">Entre em contato</a> -->
					</div>
				</div>
				<div class="col-md-6">
					<div class="listaPaises" style="display: none;">
						<ul>
							<?php 
								//LOOP DE POST DESTAQUES
								$posPaises = new WP_Query( array( 'post_type' => 'paises', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
								while ( $posPaises->have_posts() ) : $posPaises->the_post();
							 ?>
							<li>
								<span><?php echo get_the_title() ?></span>
							</li>
							<?php endwhile; wp_reset_query(); ?>
						</ul>
					</div>
				</div>
			</div>

			<div class="paises">
				<ul>
					<?php 
						//LOOP DE POST DESTAQUES
						$posPaises = new WP_Query( array( 'post_type' => 'paises', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
						while ( $posPaises->have_posts() ) : $posPaises->the_post();
						$fotoPaises = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoPaises = $fotoPaises[0];
						
					?>
					<li>
						<img src="<?php echo $fotoPaises  ?>" alt="<?php echo get_the_title() ?>">
						<h2><?php echo get_the_title() ?></h2>
						<p><?php echo get_the_content() ?></p>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>

			<div class="titulo">
				<a href="<?php echo home_url('/contato/'); ?>">Entre em contato</a>
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
			</div>

		</div>

	</section>

	<!-- SESSÃO DE DEPOIMENTOS -->
	<section class="sessaoDepoimentos background">
		<div class="container">
			<h6 class="">Depoimentos</h6>
		</div>
		<div id="carrosselDepoimentos" class="owl-Carousel">
			<?php 
				//LOOP DE POST DEPOIMENTOS
				$posDepoimentos = new WP_Query( array( 'post_type' => 'depoimento', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
				while ( $posDepoimentos->have_posts() ) : $posDepoimentos->the_post();
					$fotoDepoimentos = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
					$fotoDepoimentos = $fotoDepoimentos[0];
					$depoimento_cargo = rwmb_meta('Unika_depoimento_cargo');
					$depoimento_empresa = rwmb_meta('Unika_depoimento_empresa');
			 ?>
			<div class="item">
				<img src="<?php bloginfo('template_directory'); ?>/img/aspas.png" alt="Depoimentos">
				<div class="depoimentos">
					<p><?php echo get_the_content() ?></p>
					<figure class="fotoDepoimentos" style="background: url(<?php echo $fotoDepoimentos ?>)"></figure>
				</div>

				<div class="infos">
					<h2><?php echo get_the_title() ?></h2>
					<p><?php echo $depoimento_cargo ?></p>
					<p><?php echo $depoimento_empresa ?></p>
				</div>
			</div>
			<?php endwhile; wp_reset_query(); ?>

		</div>

		<div class="btnCarrossel">
			<button id="btnCarrosselDepoimentosLeft"><span></span></button>
			<button id="btnCarrosselDepoimentosRight"><span></span></button>
		</div>
	</section>

	<!-- SESSÃO  LOGOS PARCEIROS -->
	<section class="sessaoParceiros background">
		<h6 class="hidden">Parceiros</h6>
		<!-- CARROSSEL DE PARCEIROS -->

		<div class="container">
			<div id="carrosselLogos" class="owl-Carousel">
				<?php 
					//LOOP DE POST PARCEIROS
					$posParceiros = new WP_Query( array( 'post_type' => 'parceiros', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => -1) );
					while ( $posParceiros->have_posts() ) : $posParceiros->the_post();
						$fotoParceiro = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoParceiro = $fotoParceiro[0];
				 ?>
				<!-- ITEM -->
				<figure class="item">
					<!-- LOGO PARCEIRO-->
					<img src="<?php echo $fotoParceiro ?>" alt="<?php echo get_the_title() ?>" class="hvr-push">
				</figure>

				<?php endwhile; wp_reset_query(); ?>

			</div>
		</div>
	</section>

	<!-- SESSÃO NEWSLETTER -->
	<section class="sessaoNewsletter background">

		<!--START Scripts : this is the script part you can add to the header of your theme-->
		<script type="text/javascript" src="http://unikapsicologia.com.br/unika/wp-includes/js/jquery/jquery.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/unika/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/unika/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://unikapsicologia.com.br/unika/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://unikapsicologia.com.br/unika/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
			/* ]]> */
		</script><script type="text/javascript" src="http://unikapsicologia.com.br/unika/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<!--END Scripts-->

		<div class="gradeFundo">
			<div class="container">
				<h6><?php echo $configuracao['pg_inicial_new_titulo'] ?></h6>

				<div class="row">
					<div class="col-md-5">
						<p><?php echo $configuracao['pg_inicial_new_texto'] ?></p>
					</div>
					<div class="col-md-6">
						<div class="widget_wysija_cont html_wysija">
							
							<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html59f89b3ce731e-2" class="wysija-msg ajax"></div>
							<form id="form-wysija-html59f89b3ce731e-2" method="post" action="#wysija" class="widget_wysija html_wysija">

								<div class="form">
									<div class="row">
										<div class="col-xs-8">

											<label class="hidden">Email <span class="wysija-required">*</span></label>

											<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="E-mail" placeholder="E-mail" value="" />

											<span class="abs-req">
												<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
											</span>
										</div>
										<div class="col-xs-4">
											<input class="wysija-submit-field" type="submit" value="Enviar" />
											<input type="hidden" name="form_id" value="2" />
											<input type="hidden" name="action" value="save" />
											<input type="hidden" name="controller" value="subscribers" />
											<input type="hidden" value="1" name="wysija-page" />
											<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- SESSÃO BLOG -->
	<section class="sessaoBlog background">
		<div class="container correcaoX">
			<div class="areaTitulo">
				<h6>Blog</h6>
			</div>
			
			<div class="posts">
				<div class="row">
					<?php 
						//LOOP DE POST DESTAQUES
						$posts = new WP_Query( array( 'post_type' => 'post', 'orderby' => 'id', 'order' => 'desc', 'posts_per_page' => 3) );
						while ( $posts->have_posts() ) : $posts->the_post();
						$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
						$fotoPost = $fotoPost[0];
						
					?>
					<div class="col-sm-4">
						<a  href=" <?php echo get_permalink() ?>#conteudo " class="post">
							<figure style="background: url( <?php echo $fotoPost ?>);"></figure>
							<h2><?php echo get_the_title() ?></h2>
							<span><?php the_time('F j, Y '); ?></span>
							<p><?php customExcerpt(170); ?></p>
						</a>
					</div>
					<?php endwhile; wp_reset_query(); ?>
				</div>
			</div>
		</div>
	</section>

</div>
<?php get_footer(); ?>