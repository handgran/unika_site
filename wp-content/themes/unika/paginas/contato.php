<?php
/**
 * Template Name: Contato
 * Description: Contato
 *
 * @package Unika
 */
$foto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$foto = $foto[0];
get_header(); ?>
<!-- PÁGINA DE CONTATO -->
<div class="pg pg-contato">
	<div class="fundo"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="contato">
					<h5><?php echo $configuracao['contato_titulo'] ?></h5>
					<p><?php echo $configuracao['contato_texto'] ?></p>

					<div class="infocontato">
						<span>Nosso e-mail</span>
						<a href="malito:<?php echo $configuracao['contato_email'] ?>"><?php echo $configuracao['contato_email'] ?></a>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="contato">
					<div class="form">
						<strong>Solicite um orçamento</strong>
						<?php echo do_shortcode('[contact-form-7 id="123" title="Formulário de contato"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- SESSÃO NEWSLETTER -->
	<section class="sessaoNewsletter background">

		<!--START Scripts : this is the script part you can add to the header of your theme-->
		<script type="text/javascript" src="http://localhost/projetos/unika/wp-includes/js/jquery/jquery.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://localhost/projetos/unika/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://localhost/projetos/unika/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.14"></script>
		<script type="text/javascript" src="http://localhost/projetos/unika/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<script type="text/javascript">
			/* <![CDATA[ */
			var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://localhost/projetos/unika/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
			/* ]]> */
		</script><script type="text/javascript" src="http://localhost/projetos/unika/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
		<!--END Scripts-->

		<div class="gradeFundo">
			<div class="container">
				<h6><?php echo $configuracao['pg_inicial_new_titulo'] ?></h6>

				<div class="row">
					<div class="col-md-5">
						<p><?php echo $configuracao['pg_inicial_new_texto'] ?></p>
					</div>
					<div class="col-md-6">
						<div class="widget_wysija_cont html_wysija">
							
							<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html59f89b3ce731e-2" class="wysija-msg ajax"></div>
							<form id="form-wysija-html59f89b3ce731e-2" method="post" action="#wysija" class="widget_wysija html_wysija">

								<div class="form">
									<div class="row">
										<div class="col-xs-8">

											<label class="hidden">Email <span class="wysija-required">*</span></label>

											<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="your email..." placeholder="your email..." value="" />

											<span class="abs-req">
												<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
											</span>
										</div>
										<div class="col-xs-4">
											<input class="wysija-submit-field" type="submit" value="Enviar" />
											<input type="hidden" name="form_id" value="2" />
											<input type="hidden" name="action" value="save" />
											<input type="hidden" name="controller" value="subscribers" />
											<input type="hidden" value="1" name="wysija-page" />
											<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
<?php get_footer(); ?>