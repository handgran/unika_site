<?php
/**
 * Template Name: Palestra
 * Description: Palestra
 *
 * @package Unika
 */

get_header(); ?>
<div class="voltarLista" style="background:<?php echo $configuracao['cor_T'] ?>">
	<a href="<?php echo home_url('/solucoes/'); ?>" style="color:<?php echo $configuracao['cor_texto_T'] ?>">voltar à lista</a>
</div>
<!-- PÁGINA DE SERVIÇO -->
<div class="pg pg-servico background">

	<!-- SESSÃO DE SERVIÇOS -->
	<section class="sessaoServico">
		<h6 class="hidden">Serviço</h6>
		
		<div class="servico" style="background:<?php echo $configuracao['cor_T'] ?>">
				<div class="container">
					<div class="row">
						<div class="col-sm-2">
							<figure style="	background-color: rgba(0,0,0,0.1);">
								<img src="<?php echo $configuracao['icone_T']['url'] ?>" alt="<?php echo $configuracao['titulo_T'] ?>">
							</figure>
						</div>
						<div class="col-sm-10">
							<h2 style="color:<?php echo $configuracao['cor_texto_T'] ?>"><?php echo $configuracao['titulo_T'] ?></h2>
							<p style="color:<?php echo $configuracao['cor_texto_T'] ?>"><?php echo $configuracao['subititulo_T'] ?> </p>
						</div>
					</div>
				</div>
			</div>
		
		
			<article class="descricaoLayoutA correcaoLayout">
				
				<div class="comofazemos comofazemosB">
					<div class="container">
						<strong>Os temas mais solicitados são:</strong>
					</div>
					<div class="container correcaoX">
						<?php foreach ($configuracao['quadros_conteudo_T'] as $configuracao['quadros_conteudo_T']):
							$texto_link = explode("|", $configuracao['quadros_conteudo_T']);
							?>
							<a href=" <?php echo $texto_link[1] ?>" class="displayInline">
								<p><?php echo $texto_link[0] ?></p>
								<!-- <span>Saiba mais</span> -->
							</a>
						<?php endforeach; ?>
					</div>
				</div>
			</article>

		<!-- SESSÃO NEWSLETTER -->
		<section class="sessaoNewsletter background">

			<!--START Scripts : this is the script part you can add to the header of your theme-->
			<script type="text/javascript" src="http://unikapsicologia.com.br/wp-includes/js/jquery/jquery.js?ver=2.7.14"></script>
			<script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.7.14"></script>
			<script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.14"></script>
			<script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
			<script type="text/javascript">
				/* <![CDATA[ */
				var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://unikapsicologia.com.br/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
				/* ]]> */
			</script><script type="text/javascript" src="http://unikapsicologia.com.br/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.14"></script>
			<!--END Scripts-->

			<div class="gradeFundo">
				<div class="container">
					<h6><?php echo $configuracao['pg_inicial_new_titulo'] ?></h6>

					<div class="row">
						<div class="col-md-5">
							<p><?php echo $configuracao['pg_inicial_new_texto'] ?></p>
						</div>
						<div class="col-md-6">
							<div class="widget_wysija_cont html_wysija">
								
								<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html59f89b3ce731e-2" class="wysija-msg ajax"></div>
								<form id="form-wysija-html59f89b3ce731e-2" method="post" action="#wysija" class="widget_wysija html_wysija">

									<div class="form">
										<div class="row">
											<div class="col-xs-8">

												<label class="hidden">Email <span class="wysija-required">*</span></label>

												<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="E-mail" placeholder="E-mail" value="" />

												<span class="abs-req">
													<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
												</span>
											</div>
											<div class="col-xs-4">
												<input class="wysija-submit-field" type="submit" value="Enviar" />
												<input type="hidden" name="form_id" value="2" />
												<input type="hidden" name="action" value="save" />
												<input type="hidden" name="controller" value="subscribers" />
												<input type="hidden" value="1" name="wysija-page" />
												<input type="hidden" name="wysija[user_list][list_ids]" value="1" />
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	</section>
</div>

<?php get_footer(); ?>