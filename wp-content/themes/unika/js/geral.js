
$(function(){

	

	$(".navbar-collapse ul").addClass('nav navbar-nav');
	$("#collapse > div").removeClass('nav navbar-nav');
	
	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	$(document).ready(function() {
		var w = window.innerWidth;
		
		//CARROSSEL DE DESTAQUE
		if (w<1920){
		$("#carrosselDestaque").owlCarousel({
			items : 2,
			dots: true,
			loop: true,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 0,
			center:true,
			animateOut: 'fadeOut',
			responsiveClass:true,			    
			        responsive:{
			            320:{
			                items:1
			            },
			            600:{
			                items:1
			            },
			           
			            991:{
			                items:1
			            },
			            1024:{
			                items:2
			            },
			            1440:{
			                items:2
			            },
			            			            
			        }


		});
		$(".sessaoDestaque").removeClass('correcaotela');
		$(".sessaoInfo").removeClass('correcaotela');
		$(".carrosselTelaMenor").show();
		$(".carrosselTelaMaior").hide();

	}else if (w == 1024){

		$("#carrosselDestaque").owlCarousel({
			items : 1,
			dots: true,
			loop: true,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 0,
			center:false,
			animateOut: 'fadeOut',
			responsiveClass:true,			    
			        responsive:{
			            320:{
			                items:1
			            },
			            600:{
			                items:1
			            },
			           
			            991:{
			                items:1
			            },
			            1024:{
			                items:1
			            },
			            1440:{
			                items:1
			            },
			            			            
			        }


		});
	}else{

		$("#carrosselDestaque").owlCarousel({
			items : 3,
			dots: true,
			loop: true,
			lazyLoad: true,
			mouseDrag:true,
			touchDrag  : true,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,
			center:true,
			animateOut: 'fadeOut',
		});
		$(".sessaoDestaque").addClass('correcaotela');
		$(".sessaoInfo").addClass('correcaotela');
		$(".carrosselTelaMenor").hide();
		$(".carrosselTelaMaior").show();

	}
		var carrossel_destaque = $("#carrosselDestaque").data('owlCarousel');
		$('#btnCarrosselDestaqueLeft').click(function(){ carrossel_destaque.prev(); });
		$('#btnCarrosselDestaqueRight').click(function(){ carrossel_destaque.next(); });

		//CARROSSEL DE DESTAQUE
		$("#carrosselDepoimentos").owlCarousel({
			items : 1,
			dots: true,
			loop: false,
			lazyLoad: true,
			mouseDrag:false,
			touchDrag  : false,	       
			autoplayTimeout:5000,
			autoplayHoverPause:true,
			smartSpeed: 450,
			margin: 25,

		});
		var carrossel_depoimentos = $("#carrosselDepoimentos").data('owlCarousel');
		$('#btnCarrosselDepoimentosLeft').click(function(){ carrossel_depoimentos.prev(); });
		$('#btnCarrosselDepoimentosRight').click(function(){ carrossel_depoimentos.next(); });
		

		//CARROSSEL DE DESTAQUE
		$("#carrosselLogos").owlCarousel({
			items : 4,
			dots: true,
			loop: true,
			lazyLoad: true,
	        mouseDrag:false,
	        touchDrag  : false,	       
	        autoplay:true,
	        autoplayTimeout:2000,
	        autoplayHoverPause:true,
		    animateOut: 'fadeOut',
		    smartSpeed: 450,	
			
			responsiveClass:true,			    
			        responsive:{
			            320:{
			                items:1
			            },
			            600:{
			                items:2
			            },
			           
			            991:{
			                items:3
			            },
			            1024:{
			                items:4
			            },
			            1440:{
			                items:4
			            },
			            			            
			        }	

		});
		
	});

	$(document).ready(function(){
		    var speed = 1000;

		    // check for hash and if div exist... scroll to div
		    var hash = window.location.hash;
		    if($(hash).length) scrollToID(hash, speed);

		    // scroll to div on nav click
		    $('a .post').click(function (e) {
		        e.preventDefault();
		        var id = $(this).data('id');
		        var href = $(this).attr('href');
		        if(href === '#'){
		            scrollToID(id, speed);
		        }else{
		            window.location.href = href;
		        }

		    });
	    	
	    	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	    	      var target = $(this.hash);
	    	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	    	      if (target.length) {
	    	        $('html,body').animate({
	    	          scrollTop: target.offset().top
	    	        }, 1000);
	    	        return false;
	    	      }
	    	    }
		})

		function scrollToID(id, speed) {
		    var offSet = 70;
		    var obj = $(id).offset();
		    var targetOffset = obj.top - offSet;
		    $('html,body').animate({ scrollTop: targetOffset }, speed);
		}
	// $('.selecaobtn').click(function(e){ 
	// 	$(".imdSelecionado").fadeOut();
	// 	setTimeout(function(){ 
	// 		$('.idiomasSelect').fadeIn();
	// 	}, 400);
		
	// 	$('.idioma').animate({
	// 	    width:'130px',
	// 	    height:'107px',
	// 		padding: '0px 10px',
	// 	});
	// });

	// $(".lBR").click(function(e){ 
	// 	$(".imdSelecionado").fadeIn();
	// 	$('.idiomasSelect').hide();
	// 	$('.idioma').animate({
	// 	    width:'55px',
	// 	    height:'33px',
	// 		padding: '0px',
	// 	});
	// });

	// $(".lES").click(function(e){ 
	// 	window.location.href="http://unika.handgran.com.br/es/";
	// });


	setTimeout(function(){ 
		$(".voltarLista").slideDown();
	 }, 2000);
});
