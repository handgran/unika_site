<?php
/**
 * Plugin Name: Base Unika
 * Description: Controle base do tema Unika.
 * Version: 0.1
 * Author: Hudson Caronilo
 * Author URI: 
 * Licence: GPL2
 */

	function baseUnika () {

		//TIPOS DE CONTEÚDO
		conteudosUnika();

		//TAXONOMIA
		taxonomiaUnika();

		//META BOXES
		metaboxesUnika();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
		function conteudosUnika (){
			// TIPOS DE DESTQUE
			tipoDestaque();
			
			// SERVIÇOS
			tipoServico();

			//DEPOIMENTOS
			tipoDepoimento();

			//PARCEIROS
			tipoParceiro();

			// EQUIPE
			tipoEquipe();

			tipoPaizes();




			/* ALTERAÇÃO DO TÍTULO PADRÃO */
			add_filter( 'enter_title_here', 'trocarTituloPadrao' );
			function trocarTituloPadrao($titulo){

				switch (get_current_screen()->post_type) {

					case 'equipe':
						$titulo = 'Nome do integrante';
					break;

					default:
					break;
				}
			    return $titulo;
			}

		}
	
	/****************************************************
	* CUSTOM POST TYPE
	*****************************************************/

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {

			$rotulosDestaque = array(
									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);

			$argsDestaque 	= array(
									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('destaque', $argsDestaque);

		}

		// CUSTOM POST TYPE DESTAQUES
		function tipoPaizes() {

			$rotulosPaizes = array(
									'name'               => 'Países',
									'singular_name'      => 'país',
									'menu_name'          => 'Países que atuamos',
									'name_admin_bar'     => 'Países',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo país',
									'new_item'           => 'Novo país',
									'edit_item'          => 'Editar país',
									'view_item'          => 'Ver país',
									'all_items'          => 'Todos os países',
									'search_items'       => 'Buscar país',
									'parent_item_colon'  => 'Dos países',
									'not_found'          => 'Nenhum país cadastrado.',
									'not_found_in_trash' => 'Nenhum país na lixeira.'
								);

			$argsPaizes 	= array(
									'labels'             => $rotulosPaizes,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-admin-site',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'paises' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('paises', $argsPaizes);

		}

		// CUSTOM POST TYPE SERVIÇO
		function tipoServico() {

			$rotulosServico = array(
									'name'               => 'Problemas que resolvemos',
									'singular_name'      => 'Problema',
									'menu_name'          => 'Problemas que resolvemos',
									'name_admin_bar'     => 'Problemas que resolvemos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo Problema',
									'new_item'           => 'Novo Problema',
									'edit_item'          => 'Editar Problema',
									'view_item'          => 'Ver Problema',
									'all_items'          => 'Todos os Problemas que resolvemos',
									'search_items'       => 'Buscar Problemas que resolvemos',
									'parent_item_colon'  => 'Dos Problemas que resolvemos',
									'not_found'          => 'Nenhum Problema cadastrado.',
									'not_found_in_trash' => 'Nenhum Problema na lixeira.'
								);

			$argsServico 	= array(
									'labels'             => $rotulosServico,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-admin-tools',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'problemas-que-resolvemos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array('title','thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('servico', $argsServico);

		}

		// CUSTOM POST TYPE DEPOIMENTO
		function tipoDepoimento() {

			$rotulosDepoimento = array(
									'name'               => 'Depoimentos',
									'singular_name'      => 'Depoimento',
									'menu_name'          => 'Depoimentos',
									'name_admin_bar'     => 'Depoimentos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo depoimento',
									'new_item'           => 'Novo depoimento',
									'edit_item'          => 'Editar depoimento',
									'view_item'          => 'Ver depoimento',
									'all_items'          => 'Todos os depoimentos',
									'search_items'       => 'Buscar depoimentos',
									'parent_item_colon'  => 'Dos depoimentos',
									'not_found'          => 'Nenhum depoimento cadastrado.',
									'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
								);

			$argsDepoimento 	= array(
									'labels'             => $rotulosDepoimento,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-heart',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'depoimento' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail', 'editor' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('depoimento', $argsDepoimento);

		}

		// CUSTOM POST TYPE PARCEIRO
		function tipoParceiro() {

			$rotulosParceiro = array(
									'name'               => 'Parceiros',
									'singular_name'      => 'parceiro',
									'menu_name'          => 'Parceiros',
									'name_admin_bar'     => 'Parceiros',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo parceiro',
									'new_item'           => 'Novo parceiro',
									'edit_item'          => 'Editar parceiro',
									'view_item'          => 'Ver parceiro',
									'all_items'          => 'Todos os parceiros',
									'search_items'       => 'Buscar parceiros',
									'parent_item_colon'  => 'Dos parceiros',
									'not_found'          => 'Nenhum parceiro cadastrado.',
									'not_found_in_trash' => 'Nenhum parceiro na lixeira.'
								);

			$argsParceiro 	= array(
									'labels'             => $rotulosParceiro,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-universal-access',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'parceiros' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('parceiros', $argsParceiro);

		}

		// CUSTOM POST TYPE COLABORADOR
		function tipoEquipe() {

			$rotulosEquipe = array(
									'name'               => 'especialista',
									'singular_name'      => 'especialista',
									'menu_name'          => 'Especialista',
									'name_admin_bar'     => 'especialista',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo membro',
									'new_item'           => 'Novo membro',
									'edit_item'          => 'Editar membro',
									'view_item'          => 'Ver membro',
									'all_items'          => 'Todos os membros',
									'search_items'       => 'Buscar membros',
									'parent_item_colon'  => 'Dos membros',
									'not_found'          => 'Nenhum membro cadastrado.',
									'not_found_in_trash' => 'Nenhum membro na lixeira.'
								);

			$argsEquipe 	= array(
									'labels'             => $rotulosEquipe,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-nametag',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'equipe' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title','editor', 'thumbnail' )
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('equipe', $argsEquipe);

		}

	/****************************************************
	* META BOXES
	*****************************************************/
		function metaboxesUnika(){
			add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );
		}

			function registraMetaboxes( $metaboxes ){

				// PREFIX
				$prefix = 'Unika_';

				// METABOX DEPOIMENTOS
				$metaboxes[] = array(
					'id'			=> 'metaboxDepoimento',
					'title'			=> 'Detalhes do depoimento',
					'pages' 		=> array('depoimento'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Cargo: ',
							'id'    => "{$prefix}depoimento_cargo",
							'desc'  => '',
							'type'  => 'text',
						), 
						array(
							'name'  => 'Empresa: ',
							'id'    => "{$prefix}depoimento_empresa",
							'desc'  => '',
							'type'  => 'text',
						), 
					),
				);

				// METABOX DEPOIMENTOS
				$metaboxes[] = array(
					'id'			=> 'metaboxPosts',
					'title'			=> 'Posts relacionados',
					'pages' 		=> array('post'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Relacionados',
							'id'    	 => "{$prefix}relacionados_post",
							'desc'  	 => '',
							'type'  	 => 'post',
							'post_type'  => 'post',
							'field_type' => 'select',
							'multiple' => true
						), 
					),
				);

				// METABOX DEPOIMENTOS
				$metaboxes[] = array(
					'id'			=> 'metaboxEquipe',
					'title'			=> 'Detalhes do equipe',
					'pages' 		=> array('equipe'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Facebook: ',
							'id'    => "{$prefix}equipe_facebook",
							'desc'  => '',
							'type'  => 'text',
							'placeholder' => 'https://instagram.com.br/perfil'
						), 
						array(
							'name'  => 'Linkedin: ',
							'id'    => "{$prefix}equipe_linkedin",
							'desc'  => '',
							'type'  => 'text',
							'placeholder' => 'https://instagram.com.br/perfil'
						), 
						array(
							'name'  => 'Instagram: ',
							'id'    => "{$prefix}equipe_instagram",
							'desc'  => '',
							'type'  => 'text',
							'placeholder' => 'https://instagram.com.br/perfil'
						), 
					),
				);

				// METABOX COMO DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxDescaques',
					'title'			=> 'Detalhes do destaque',
					'pages' 		=> array('destaque'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Cor quadro linear gradient: ',
							'id'    => "{$prefix}destaque_cor",
							'desc'  => '',
							'type'  => 'text',
						), 
						array(
							'name'  => 'Cor circulo foto: ',
							'id'    => "{$prefix}destaque_cor_circulo",
							'desc'  => '',
							'type'  => 'color',
						),
						array(
							'name'  => 'Cor do texto: ',
							'id'    => "{$prefix}destaque_cor_texto",
							'desc'  => '',
							'type'  => 'color',
						),
						array(
							'name'  => 'Link: ',
							'id'    => "{$prefix}destaque_link",
							'desc'  => '',
							'type'  => 'text',
						), 

					),
				);

				return $metaboxes;
			}

	/****************************************************
	* TAXONOMIA
	*****************************************************/
		function taxonomiaUnika () {
			// taxonomiaCategoriaProjetos();
		}

		// function taxonomiaCategoriaProjetos() {

		// 	$rotulosCategoriaProjetos = array(
		// 										'name'              => 'Categorias de projeto',
		// 										'singular_name'     => 'Categorias de projetos',
		// 										'search_items'      => 'Buscar categoria do projeto',
		// 										'all_items'         => 'Todas as categorias',
		// 										'parent_item'       => 'Categoria pai',
		// 										'parent_item_colon' => 'Categoria pai:',
		// 										'edit_item'         => 'Editar categoria do projeto',
		// 										'update_item'       => 'Atualizar categoria',
		// 										'add_new_item'      => 'Nova categoria',
		// 										'new_item_name'     => 'Nova categoria',
		// 										'menu_name'         => 'Categorias projetos',
		// 									);

		// 	$argsCategoriaProjetos 		= array(
		// 										'hierarchical'      => true,
		// 										'labels'            => $rotulosCategoriaProjetos,
		// 										'show_ui'           => true,
		// 										'show_admin_column' => true,
		// 										'query_var'         => true,
		// 										'rewrite'           => array( 'slug' => 'categoria-projetos' ),
		// 									);

		// 	register_taxonomy( 'categoriaProjetos', array( 'projetos' ), $argsCategoriaProjetos);

		// }
		// 
		function metaboxjs(){



			global $post;

			$template = get_post_meta($post->ID, '_wp_page_template', true);

			$template = explode('/', $template);

			$template = explode('.', $template[1]);

			$template = $template[0];



			if($template != ''){

				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);

			}

		}


  	/****************************************************
	* AÇÕES
	*****************************************************/

		// INICIA A FUNÇÃO PRINCIPAL
		add_action('init', 'baseUnika');

		// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
		add_action( 'add_meta_boxes', 'metaboxjs');

		// FLUSHS
		function rewrite_flush() {

	    	baseUnika();

	   		flush_rewrite_rules();
		}

		register_activation_hook( __FILE__, 'rewrite_flush' );