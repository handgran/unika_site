-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 19-Mar-2018 às 13:20
-- Versão do servidor: 5.6.32-78.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hgcom_unika_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_aiowps_events`
--

CREATE TABLE IF NOT EXISTS `uk_aiowps_events` (
  `id` bigint(20) NOT NULL,
  `event_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `username` varchar(150) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `event_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ip_or_host` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `referer_info` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `country_code` varchar(50) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `event_data` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_aiowps_failed_logins`
--

CREATE TABLE IF NOT EXISTS `uk_aiowps_failed_logins` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `failed_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_attempt_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_aiowps_failed_logins`
--

INSERT INTO `uk_aiowps_failed_logins` (`id`, `user_id`, `user_login`, `failed_login_date`, `login_attempt_ip`) VALUES
(1, 1, 'unika', '2018-01-17 11:16:35', '168.181.49.171'),
(2, 1, 'unika', '2018-01-17 11:26:39', '168.181.49.171'),
(3, 1, 'unika', '2018-01-19 09:38:35', '168.181.49.171'),
(4, 1, 'unika', '2018-01-19 09:38:40', '168.181.49.171'),
(5, 1, 'unika', '2018-01-19 15:15:13', '168.181.49.171'),
(6, 1, 'unika', '2018-01-22 09:56:17', '168.194.161.34'),
(7, 1, 'unika', '2018-01-25 15:01:03', '168.181.49.125');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_aiowps_global_meta`
--

CREATE TABLE IF NOT EXISTS `uk_aiowps_global_meta` (
  `meta_id` bigint(20) NOT NULL,
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta_key1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key2` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key3` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key4` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_key5` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value1` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value2` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value3` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value4` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `meta_value5` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_aiowps_login_activity`
--

CREATE TABLE IF NOT EXISTS `uk_aiowps_login_activity` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logout_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `login_country` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `browser_type` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_aiowps_login_activity`
--

INSERT INTO `uk_aiowps_login_activity` (`id`, `user_id`, `user_login`, `login_date`, `logout_date`, `login_ip`, `login_country`, `browser_type`) VALUES
(1, 1, 'unika', '2017-10-30 12:36:47', '0000-00-00 00:00:00', '::1', '', ''),
(2, 1, 'unika', '2017-10-31 09:48:16', '0000-00-00 00:00:00', '::1', '', ''),
(3, 1, 'unika', '2017-11-01 10:44:21', '0000-00-00 00:00:00', '::1', '', ''),
(4, 1, 'unika', '2017-11-01 17:04:15', '0000-00-00 00:00:00', '168.181.48.3', '', ''),
(5, 1, 'unika', '2017-11-06 12:33:01', '0000-00-00 00:00:00', '168.194.161.212', '', ''),
(6, 1, 'unika', '2017-12-05 09:15:20', '0000-00-00 00:00:00', '187.95.126.39', '', ''),
(7, 1, 'unika', '2017-12-19 19:31:31', '0000-00-00 00:00:00', '191.177.187.117', '', ''),
(8, 1, 'unika', '2017-12-21 10:38:48', '0000-00-00 00:00:00', '201.14.57.44', '', ''),
(9, 1, 'unika', '2018-01-15 15:35:21', '2018-01-15 15:36:07', '177.220.172.231', '', ''),
(10, 1, 'unika', '2018-01-15 15:36:22', '0000-00-00 00:00:00', '177.220.172.231', '', ''),
(11, 1, 'unika', '2018-01-17 11:16:39', '2018-01-17 11:20:53', '168.181.49.171', '', ''),
(12, 1, 'unika', '2018-01-17 11:26:46', '2018-01-17 11:30:55', '168.181.49.171', '', ''),
(13, 1, 'unika', '2018-01-19 09:38:46', '0000-00-00 00:00:00', '168.181.49.171', '', ''),
(14, 1, 'unika', '2018-01-19 15:15:19', '0000-00-00 00:00:00', '168.181.49.171', '', ''),
(15, 1, 'unika', '2018-01-22 09:56:24', '0000-00-00 00:00:00', '168.194.161.34', '', ''),
(16, 1, 'unika', '2018-01-25 15:01:08', '0000-00-00 00:00:00', '168.181.49.125', '', ''),
(17, 1, 'unika', '2018-01-25 15:56:17', '0000-00-00 00:00:00', '168.181.49.125', '', ''),
(18, 1, 'unika', '2018-01-26 15:40:45', '2018-01-26 16:21:07', '168.181.49.179', '', ''),
(19, 1, 'unika', '2018-01-26 16:01:03', '2018-01-26 16:21:07', '168.181.49.179', '', ''),
(20, 2, 'Vanessa', '2018-01-26 16:21:16', '2018-01-26 16:21:26', '168.181.49.179', '', ''),
(21, 1, 'unika', '2018-01-26 16:26:08', '0000-00-00 00:00:00', '168.181.49.179', '', ''),
(22, 1, 'unika', '2018-02-15 16:12:35', '2018-02-15 17:10:59', '168.181.49.144', '', ''),
(23, 1, 'unika', '2018-02-15 17:11:28', '0000-00-00 00:00:00', '168.181.49.144', '', ''),
(24, 1, 'unika', '2018-02-19 15:04:32', '0000-00-00 00:00:00', '168.181.49.181', '', ''),
(25, 1, 'unika', '2018-02-19 15:37:21', '0000-00-00 00:00:00', '186.212.40.226', '', ''),
(26, 2, 'Vanessa', '2018-02-19 16:18:42', '0000-00-00 00:00:00', '168.181.49.181', '', ''),
(27, 1, 'unika', '2018-02-19 16:39:27', '0000-00-00 00:00:00', '168.181.49.181', '', ''),
(28, 2, 'Vanessa', '2018-02-21 10:37:42', '0000-00-00 00:00:00', '168.181.49.158', '', ''),
(29, 1, 'unika', '2018-02-21 16:17:34', '0000-00-00 00:00:00', '168.181.49.158', '', ''),
(30, 1, 'unika', '2018-03-01 14:01:05', '0000-00-00 00:00:00', '187.23.74.32', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_aiowps_login_lockdown`
--

CREATE TABLE IF NOT EXISTS `uk_aiowps_login_lockdown` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_login` varchar(150) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `lockdown_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `release_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `failed_login_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `lock_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `unlock_key` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_aiowps_permanent_block`
--

CREATE TABLE IF NOT EXISTS `uk_aiowps_permanent_block` (
  `id` bigint(20) NOT NULL,
  `blocked_ip` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `block_reason` varchar(128) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `country_origin` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `blocked_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `unblock` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_commentmeta`
--

CREATE TABLE IF NOT EXISTS `uk_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_comments`
--

CREATE TABLE IF NOT EXISTS `uk_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_links`
--

CREATE TABLE IF NOT EXISTS `uk_links` (
  `link_id` bigint(20) unsigned NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_options`
--

CREATE TABLE IF NOT EXISTS `uk_options` (
  `option_id` bigint(20) unsigned NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=MyISAM AUTO_INCREMENT=1193 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_options`
--

INSERT INTO `uk_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://unika.handgran.com.br/', 'yes'),
(2, 'home', 'http://unika.handgran.com.br/', 'yes'),
(3, 'blogname', 'Unika', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'contato@unika.com.br', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:229:{s:19:"sitemap_index\\.xml$";s:19:"index.php?sitemap=1";s:31:"([^/]+?)-sitemap([0-9]+)?\\.xml$";s:51:"index.php?sitemap=$matches[1]&sitemap_n=$matches[2]";s:24:"([a-z]+)?-?sitemap\\.xsl$";s:25:"index.php?xsl=$matches[1]";s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:11:"destaque/?$";s:28:"index.php?post_type=destaque";s:41:"destaque/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=destaque&feed=$matches[1]";s:36:"destaque/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?post_type=destaque&feed=$matches[1]";s:28:"destaque/page/([0-9]{1,})/?$";s:46:"index.php?post_type=destaque&paged=$matches[1]";s:27:"problemas-que-resolvemos/?$";s:27:"index.php?post_type=servico";s:57:"problemas-que-resolvemos/feed/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=servico&feed=$matches[1]";s:52:"problemas-que-resolvemos/(feed|rdf|rss|rss2|atom)/?$";s:44:"index.php?post_type=servico&feed=$matches[1]";s:44:"problemas-que-resolvemos/page/([0-9]{1,})/?$";s:45:"index.php?post_type=servico&paged=$matches[1]";s:13:"depoimento/?$";s:30:"index.php?post_type=depoimento";s:43:"depoimento/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?post_type=depoimento&feed=$matches[1]";s:38:"depoimento/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?post_type=depoimento&feed=$matches[1]";s:30:"depoimento/page/([0-9]{1,})/?$";s:48:"index.php?post_type=depoimento&paged=$matches[1]";s:12:"parceiros/?$";s:29:"index.php?post_type=parceiros";s:42:"parceiros/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=parceiros&feed=$matches[1]";s:37:"parceiros/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?post_type=parceiros&feed=$matches[1]";s:29:"parceiros/page/([0-9]{1,})/?$";s:47:"index.php?post_type=parceiros&paged=$matches[1]";s:9:"equipe/?$";s:26:"index.php?post_type=equipe";s:39:"equipe/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=equipe&feed=$matches[1]";s:34:"equipe/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=equipe&feed=$matches[1]";s:26:"equipe/page/([0-9]{1,})/?$";s:44:"index.php?post_type=equipe&paged=$matches[1]";s:9:"paises/?$";s:26:"index.php?post_type=paises";s:39:"paises/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=paises&feed=$matches[1]";s:34:"paises/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?post_type=paises&feed=$matches[1]";s:26:"paises/page/([0-9]{1,})/?$";s:44:"index.php?post_type=paises&paged=$matches[1]";s:47:"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:42:"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:23:"category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:35:"category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:17:"category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:44:"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:39:"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:20:"tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:32:"tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:14:"tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:45:"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:40:"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:21:"type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:33:"type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:15:"type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:36:"destaque/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:46:"destaque/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:66:"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:61:"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:42:"destaque/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:25:"destaque/([^/]+)/embed/?$";s:41:"index.php?destaque=$matches[1]&embed=true";s:29:"destaque/([^/]+)/trackback/?$";s:35:"index.php?destaque=$matches[1]&tb=1";s:49:"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?destaque=$matches[1]&feed=$matches[2]";s:44:"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?destaque=$matches[1]&feed=$matches[2]";s:37:"destaque/([^/]+)/page/?([0-9]{1,})/?$";s:48:"index.php?destaque=$matches[1]&paged=$matches[2]";s:44:"destaque/([^/]+)/comment-page-([0-9]{1,})/?$";s:48:"index.php?destaque=$matches[1]&cpage=$matches[2]";s:33:"destaque/([^/]+)(?:/([0-9]+))?/?$";s:47:"index.php?destaque=$matches[1]&page=$matches[2]";s:25:"destaque/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:35:"destaque/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:55:"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:50:"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:31:"destaque/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:52:"problemas-que-resolvemos/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:62:"problemas-que-resolvemos/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:82:"problemas-que-resolvemos/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:77:"problemas-que-resolvemos/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:77:"problemas-que-resolvemos/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:58:"problemas-que-resolvemos/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:41:"problemas-que-resolvemos/([^/]+)/embed/?$";s:40:"index.php?servico=$matches[1]&embed=true";s:45:"problemas-que-resolvemos/([^/]+)/trackback/?$";s:34:"index.php?servico=$matches[1]&tb=1";s:65:"problemas-que-resolvemos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?servico=$matches[1]&feed=$matches[2]";s:60:"problemas-que-resolvemos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:46:"index.php?servico=$matches[1]&feed=$matches[2]";s:53:"problemas-que-resolvemos/([^/]+)/page/?([0-9]{1,})/?$";s:47:"index.php?servico=$matches[1]&paged=$matches[2]";s:60:"problemas-que-resolvemos/([^/]+)/comment-page-([0-9]{1,})/?$";s:47:"index.php?servico=$matches[1]&cpage=$matches[2]";s:49:"problemas-que-resolvemos/([^/]+)(?:/([0-9]+))?/?$";s:46:"index.php?servico=$matches[1]&page=$matches[2]";s:41:"problemas-que-resolvemos/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:51:"problemas-que-resolvemos/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:71:"problemas-que-resolvemos/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:66:"problemas-que-resolvemos/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:66:"problemas-que-resolvemos/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:47:"problemas-que-resolvemos/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:38:"depoimento/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:48:"depoimento/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:68:"depoimento/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:63:"depoimento/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:63:"depoimento/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:44:"depoimento/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:27:"depoimento/([^/]+)/embed/?$";s:43:"index.php?depoimento=$matches[1]&embed=true";s:31:"depoimento/([^/]+)/trackback/?$";s:37:"index.php?depoimento=$matches[1]&tb=1";s:51:"depoimento/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?depoimento=$matches[1]&feed=$matches[2]";s:46:"depoimento/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?depoimento=$matches[1]&feed=$matches[2]";s:39:"depoimento/([^/]+)/page/?([0-9]{1,})/?$";s:50:"index.php?depoimento=$matches[1]&paged=$matches[2]";s:46:"depoimento/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?depoimento=$matches[1]&cpage=$matches[2]";s:35:"depoimento/([^/]+)(?:/([0-9]+))?/?$";s:49:"index.php?depoimento=$matches[1]&page=$matches[2]";s:27:"depoimento/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"depoimento/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"depoimento/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"depoimento/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"depoimento/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"depoimento/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:37:"parceiros/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"parceiros/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"parceiros/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"parceiros/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"parceiros/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:43:"parceiros/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:26:"parceiros/([^/]+)/embed/?$";s:42:"index.php?parceiros=$matches[1]&embed=true";s:30:"parceiros/([^/]+)/trackback/?$";s:36:"index.php?parceiros=$matches[1]&tb=1";s:50:"parceiros/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?parceiros=$matches[1]&feed=$matches[2]";s:45:"parceiros/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?parceiros=$matches[1]&feed=$matches[2]";s:38:"parceiros/([^/]+)/page/?([0-9]{1,})/?$";s:49:"index.php?parceiros=$matches[1]&paged=$matches[2]";s:45:"parceiros/([^/]+)/comment-page-([0-9]{1,})/?$";s:49:"index.php?parceiros=$matches[1]&cpage=$matches[2]";s:34:"parceiros/([^/]+)(?:/([0-9]+))?/?$";s:48:"index.php?parceiros=$matches[1]&page=$matches[2]";s:26:"parceiros/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:36:"parceiros/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:56:"parceiros/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"parceiros/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:51:"parceiros/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:32:"parceiros/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"equipe/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"equipe/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"equipe/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"equipe/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"equipe/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"equipe/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:23:"equipe/([^/]+)/embed/?$";s:39:"index.php?equipe=$matches[1]&embed=true";s:27:"equipe/([^/]+)/trackback/?$";s:33:"index.php?equipe=$matches[1]&tb=1";s:47:"equipe/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?equipe=$matches[1]&feed=$matches[2]";s:42:"equipe/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?equipe=$matches[1]&feed=$matches[2]";s:35:"equipe/([^/]+)/page/?([0-9]{1,})/?$";s:46:"index.php?equipe=$matches[1]&paged=$matches[2]";s:42:"equipe/([^/]+)/comment-page-([0-9]{1,})/?$";s:46:"index.php?equipe=$matches[1]&cpage=$matches[2]";s:31:"equipe/([^/]+)(?:/([0-9]+))?/?$";s:45:"index.php?equipe=$matches[1]&page=$matches[2]";s:23:"equipe/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:33:"equipe/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:53:"equipe/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"equipe/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"equipe/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"equipe/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:34:"paises/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:44:"paises/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:64:"paises/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"paises/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:59:"paises/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:40:"paises/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:23:"paises/([^/]+)/embed/?$";s:39:"index.php?paises=$matches[1]&embed=true";s:27:"paises/([^/]+)/trackback/?$";s:33:"index.php?paises=$matches[1]&tb=1";s:47:"paises/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?paises=$matches[1]&feed=$matches[2]";s:42:"paises/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:45:"index.php?paises=$matches[1]&feed=$matches[2]";s:35:"paises/([^/]+)/page/?([0-9]{1,})/?$";s:46:"index.php?paises=$matches[1]&paged=$matches[2]";s:42:"paises/([^/]+)/comment-page-([0-9]{1,})/?$";s:46:"index.php?paises=$matches[1]&cpage=$matches[2]";s:31:"paises/([^/]+)(?:/([0-9]+))?/?$";s:45:"index.php?paises=$matches[1]&page=$matches[2]";s:23:"paises/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:33:"paises/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:53:"paises/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"paises/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:48:"paises/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:29:"paises/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:32:"feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:27:"(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:8:"embed/?$";s:21:"index.php?&embed=true";s:20:"page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:27:"comment-page-([0-9]{1,})/?$";s:39:"index.php?&page_id=83&cpage=$matches[1]";s:41:"comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:36:"comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:17:"comments/embed/?$";s:21:"index.php?&embed=true";s:44:"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:39:"search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:20:"search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:32:"search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:14:"search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:47:"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:42:"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:23:"author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:35:"author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:17:"author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:69:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:64:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:45:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:57:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:39:"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:56:"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:51:"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:32:"([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:44:"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:26:"([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:43:"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:38:"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:19:"([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:31:"([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:13:"([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:27:".?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:".?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:".?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:20:"(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:40:"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:35:"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:28:"(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:35:"(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:24:"(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";s:27:"[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:37:"[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:57:"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:52:"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:33:"[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:16:"([^/]+)/embed/?$";s:37:"index.php?name=$matches[1]&embed=true";s:20:"([^/]+)/trackback/?$";s:31:"index.php?name=$matches[1]&tb=1";s:40:"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:35:"([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?name=$matches[1]&feed=$matches[2]";s:28:"([^/]+)/page/?([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&paged=$matches[2]";s:35:"([^/]+)/comment-page-([0-9]{1,})/?$";s:44:"index.php?name=$matches[1]&cpage=$matches[2]";s:24:"([^/]+)(?:/([0-9]+))?/?$";s:43:"index.php?name=$matches[1]&page=$matches[2]";s:16:"[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:26:"[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:46:"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:41:"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:22:"[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:11:{i:0;s:35:"redux-framework/redux-framework.php";i:1;s:51:"all-in-one-wp-security-and-firewall/wp-security.php";i:2;s:25:"base-unika/base-unika.php";i:3;s:36:"contact-form-7/wp-contact-form-7.php";i:4;s:32:"disqus-comment-system/disqus.php";i:5;s:21:"meta-box/meta-box.php";i:6;s:37:"post-types-order/post-types-order.php";i:7;s:24:"simple-history/index.php";i:8;s:24:"wordpress-seo/wp-seo.php";i:9;s:35:"wp-user-avatars/wp-user-avatars.php";i:10;s:28:"wysija-newsletters/index.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'unika', 'yes'),
(41, 'stylesheet', 'unika', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:"_multiwidget";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '75', 'yes'),
(84, 'page_on_front', '83', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'initial_db_version', '38590', 'yes'),
(92, 'uk_user_roles', 'a:7:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:68:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:20:"wpseo_manage_options";b:1;s:18:"wysija_newsletters";b:1;s:18:"wysija_subscribers";b:1;s:13:"wysija_config";b:1;s:16:"wysija_theme_tab";b:1;s:16:"wysija_style_tab";b:1;s:22:"wysija_stats_dashboard";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:35:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:15:"wpseo_bulk_edit";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:13:"wpseo_manager";a:2:{s:4:"name";s:11:"SEO Manager";s:12:"capabilities";a:37:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:15:"wpseo_bulk_edit";b:1;s:28:"wpseo_edit_advanced_metadata";b:1;s:20:"wpseo_manage_options";b:1;}}s:12:"wpseo_editor";a:2:{s:4:"name";s:10:"SEO Editor";s:12:"capabilities";a:36:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:15:"wpseo_bulk_edit";b:1;s:28:"wpseo_edit_advanced_metadata";b:1;}}}', 'yes'),
(93, 'fresh_site', '0', 'yes'),
(94, 'WPLANG', 'pt_BR', 'yes'),
(95, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(96, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(97, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'sidebars_widgets', 'a:3:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"array_version";i:3;}', 'yes'),
(101, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(103, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'cron', 'a:9:{i:1521477990;a:1:{s:24:"aiowps_hourly_cron_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1521482817;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1521483407;a:1:{s:29:"simple_history/maybe_purge_db";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1521517103;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1521544538;a:1:{s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1521560384;a:1:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1521560790;a:1:{s:23:"aiowps_daily_cron_event";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1521560791;a:1:{s:19:"wpseo-reindex-links";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(110, 'theme_mods_twentyfifteen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1509291641;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(160, 'wpseo_permalinks', 'a:9:{s:15:"cleanpermalinks";b:0;s:24:"cleanpermalink-extravars";s:0:"";s:29:"cleanpermalink-googlecampaign";b:0;s:31:"cleanpermalink-googlesitesearch";b:0;s:15:"cleanreplytocom";b:0;s:10:"cleanslugs";b:1;s:18:"redirectattachment";b:0;s:17:"stripcategorybase";b:0;s:13:"trailingslash";b:0;}', 'yes'),
(161, 'wpseo_titles', 'a:53:{s:10:"title_test";i:0;s:17:"forcerewritetitle";b:0;s:9:"separator";s:7:"sc-dash";s:15:"usemetakeywords";b:0;s:16:"title-home-wpseo";s:42:"%%sitename%% %%page%% %%sep%% %%sitedesc%%";s:18:"title-author-wpseo";s:40:"%%name%%, Autor em %%sitename%% %%page%%";s:19:"title-archive-wpseo";s:38:"%%date%% %%page%% %%sep%% %%sitename%%";s:18:"title-search-wpseo";s:66:"Você pesquisou por %%searchphrase%% %%page%% %%sep%% %%sitename%%";s:15:"title-404-wpseo";s:44:"Página não encontrada %%sep%% %%sitename%%";s:19:"metadesc-home-wpseo";s:0:"";s:21:"metadesc-author-wpseo";s:0:"";s:22:"metadesc-archive-wpseo";s:0:"";s:18:"metakey-home-wpseo";s:0:"";s:20:"metakey-author-wpseo";s:0:"";s:22:"noindex-subpages-wpseo";b:0;s:20:"noindex-author-wpseo";b:0;s:21:"noindex-archive-wpseo";b:1;s:14:"disable-author";b:0;s:12:"disable-date";b:0;s:19:"disable-post_format";b:0;s:10:"title-post";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:13:"metadesc-post";s:0:"";s:12:"metakey-post";s:0:"";s:12:"noindex-post";b:0;s:13:"showdate-post";b:0;s:16:"hideeditbox-post";b:0;s:10:"title-page";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:13:"metadesc-page";s:0:"";s:12:"metakey-page";s:0:"";s:12:"noindex-page";b:0;s:13:"showdate-page";b:0;s:16:"hideeditbox-page";b:0;s:16:"title-attachment";s:39:"%%title%% %%page%% %%sep%% %%sitename%%";s:19:"metadesc-attachment";s:0:"";s:18:"metakey-attachment";s:0:"";s:18:"noindex-attachment";b:0;s:19:"showdate-attachment";b:0;s:22:"hideeditbox-attachment";b:0;s:18:"title-tax-category";s:53:"Arquivos %%term_title%% %%page%% %%sep%% %%sitename%%";s:21:"metadesc-tax-category";s:0:"";s:20:"metakey-tax-category";s:0:"";s:24:"hideeditbox-tax-category";b:0;s:20:"noindex-tax-category";b:0;s:18:"title-tax-post_tag";s:53:"Arquivos %%term_title%% %%page%% %%sep%% %%sitename%%";s:21:"metadesc-tax-post_tag";s:0:"";s:20:"metakey-tax-post_tag";s:0:"";s:24:"hideeditbox-tax-post_tag";b:0;s:20:"noindex-tax-post_tag";b:0;s:21:"title-tax-post_format";s:53:"Arquivos %%term_title%% %%page%% %%sep%% %%sitename%%";s:24:"metadesc-tax-post_format";s:0:"";s:23:"metakey-tax-post_format";s:0:"";s:27:"hideeditbox-tax-post_format";b:0;s:23:"noindex-tax-post_format";b:1;}', 'yes'),
(141, 'theme_switched', '', 'yes'),
(139, 'current_theme', 'Unika', 'yes'),
(140, 'theme_mods_unika', 'a:3:{i:0;b:0;s:18:"custom_css_post_id";i:-1;s:18:"nav_menu_locations";a:0:{}}', 'yes'),
(125, 'can_compress_scripts', '1', 'no'),
(784, 'simple_history_db_version', '5', 'yes'),
(785, 'simple_history_show_as_page', '1', 'yes'),
(786, 'simple_history_show_on_dashboard', '1', 'yes'),
(787, 'simple_history_enable_rss_feed', '0', 'yes'),
(788, 'simple_history_rss_secret', 'qwdmhptewfpeteispjox', 'yes'),
(854, 'simplehistory_AvailableUpdatesLogger_wp_core_version_available', '4.9.4', 'yes'),
(821, 'simplehistory_AvailableUpdatesLogger_plugin_updates_available', 'a:8:{s:36:"contact-form-7/wp-contact-form-7.php";a:1:{s:15:"checked_version";s:5:"5.0.1";}s:21:"meta-box/meta-box.php";a:1:{s:15:"checked_version";s:6:"4.14.0";}s:37:"post-types-order/post-types-order.php";a:1:{s:15:"checked_version";s:7:"1.9.3.6";}s:24:"wordpress-seo/wp-seo.php";a:1:{s:15:"checked_version";s:5:"7.0.3";}s:51:"all-in-one-wp-security-and-firewall/wp-security.php";a:1:{s:15:"checked_version";s:5:"4.3.2";}s:32:"disqus-comment-system/disqus.php";a:1:{s:15:"checked_version";s:6:"3.0.15";}s:35:"redux-framework/redux-framework.php";a:1:{s:15:"checked_version";s:5:"3.6.8";}s:28:"wysija-newsletters/index.php";a:1:{s:15:"checked_version";s:5:"2.8.2";}}', 'yes'),
(138, 'recently_activated', 'a:2:{s:33:"wp-user-avatar/wp-user-avatar.php";i:1518721037;s:39:"simple-author-box/simple-author-box.php";i:1518720185;}', 'yes'),
(162, 'wpseo_social', 'a:20:{s:9:"fb_admins";a:0:{}s:12:"fbconnectkey";s:32:"7287ce4e02761020ae5beaf0baf8342f";s:13:"facebook_site";s:0:"";s:13:"instagram_url";s:0:"";s:12:"linkedin_url";s:0:"";s:11:"myspace_url";s:0:"";s:16:"og_default_image";s:0:"";s:18:"og_frontpage_title";s:0:"";s:17:"og_frontpage_desc";s:0:"";s:18:"og_frontpage_image";s:0:"";s:9:"opengraph";b:1;s:13:"pinterest_url";s:0:"";s:15:"pinterestverify";s:0:"";s:14:"plus-publisher";s:0:"";s:7:"twitter";b:1;s:12:"twitter_site";s:0:"";s:17:"twitter_card_type";s:7:"summary";s:11:"youtube_url";s:0:"";s:15:"google_plus_url";s:0:"";s:10:"fbadminapp";s:0:"";}', 'yes'),
(163, 'wpseo_rss', 'a:2:{s:9:"rssbefore";s:0:"";s:8:"rssafter";s:54:"O post %%POSTLINK%% apareceu primeiro em %%BLOGLINK%%.";}', 'yes'),
(164, 'wpseo_internallinks', 'a:10:{s:20:"breadcrumbs-404crumb";s:33:"Erro 404: Página não encontrada";s:23:"breadcrumbs-blog-remove";b:0;s:20:"breadcrumbs-boldlast";b:0;s:25:"breadcrumbs-archiveprefix";s:13:"Arquivos para";s:18:"breadcrumbs-enable";b:0;s:16:"breadcrumbs-home";s:7:"Início";s:18:"breadcrumbs-prefix";s:0:"";s:24:"breadcrumbs-searchprefix";s:19:"Você pesquisou por";s:15:"breadcrumbs-sep";s:7:"&raquo;";s:23:"post_types-post-maintax";i:0;}', 'yes'),
(165, 'wpseo_xml', 'a:16:{s:22:"disable_author_sitemap";b:1;s:22:"disable_author_noposts";b:1;s:16:"enablexmlsitemap";b:1;s:16:"entries-per-page";i:1000;s:14:"excluded-posts";s:0:"";s:38:"user_role-administrator-not_in_sitemap";b:0;s:31:"user_role-editor-not_in_sitemap";b:0;s:31:"user_role-author-not_in_sitemap";b:0;s:36:"user_role-contributor-not_in_sitemap";b:0;s:35:"user_role-subscriber-not_in_sitemap";b:0;s:30:"post_types-post-not_in_sitemap";b:0;s:30:"post_types-page-not_in_sitemap";b:0;s:36:"post_types-attachment-not_in_sitemap";b:1;s:34:"taxonomies-category-not_in_sitemap";b:0;s:34:"taxonomies-post_tag-not_in_sitemap";b:0;s:37:"taxonomies-post_format-not_in_sitemap";b:0;}', 'yes'),
(166, 'wpseo_flush_rewrite', '1', 'yes'),
(159, 'wpseo', 'a:25:{s:14:"blocking_files";a:0:{}s:15:"ms_defaults_set";b:0;s:7:"version";s:3:"5.9";s:12:"company_logo";s:0:"";s:12:"company_name";s:0:"";s:17:"company_or_person";s:0:"";s:20:"disableadvanced_meta";b:1;s:19:"onpage_indexability";b:1;s:12:"googleverify";s:0:"";s:8:"msverify";s:0:"";s:11:"person_name";s:0:"";s:12:"website_name";s:0:"";s:22:"alternate_website_name";s:0:"";s:12:"yandexverify";s:0:"";s:9:"site_type";s:0:"";s:20:"has_multiple_authors";s:0:"";s:16:"environment_type";s:0:"";s:23:"content_analysis_active";b:1;s:23:"keyword_analysis_active";b:1;s:20:"enable_setting_pages";b:0;s:21:"enable_admin_bar_menu";b:1;s:26:"enable_cornerstone_content";b:1;s:24:"enable_text_link_counter";b:1;s:22:"show_onboarding_notice";b:1;s:18:"first_activated_on";i:1509291991;}', 'yes'),
(155, 'aiowpsec_db_version', '1.9', 'yes'),
(156, 'aio_wp_security_configs', 'a:80:{s:19:"aiowps_enable_debug";s:0:"";s:36:"aiowps_remove_wp_generator_meta_info";s:0:"";s:25:"aiowps_prevent_hotlinking";s:0:"";s:28:"aiowps_enable_login_lockdown";s:0:"";s:28:"aiowps_allow_unlock_requests";s:0:"";s:25:"aiowps_max_login_attempts";s:1:"3";s:24:"aiowps_retry_time_period";s:1:"5";s:26:"aiowps_lockout_time_length";s:2:"60";s:28:"aiowps_set_generic_login_msg";s:0:"";s:26:"aiowps_enable_email_notify";s:0:"";s:20:"aiowps_email_address";s:20:"contato@unika.com.br";s:27:"aiowps_enable_forced_logout";s:0:"";s:25:"aiowps_logout_time_period";s:2:"60";s:39:"aiowps_enable_invalid_username_lockdown";s:0:"";s:43:"aiowps_instantly_lockout_specific_usernames";a:0:{}s:32:"aiowps_unlock_request_secret_key";s:20:"52cgicuwksl2bxcc8waj";s:35:"aiowps_lockdown_enable_whitelisting";s:0:"";s:36:"aiowps_lockdown_allowed_ip_addresses";s:0:"";s:26:"aiowps_enable_whitelisting";s:0:"";s:27:"aiowps_allowed_ip_addresses";s:0:"";s:27:"aiowps_enable_login_captcha";s:0:"";s:34:"aiowps_enable_custom_login_captcha";s:0:"";s:25:"aiowps_captcha_secret_key";s:20:"aa50w66l351mbcyyiapy";s:42:"aiowps_enable_manual_registration_approval";s:0:"";s:39:"aiowps_enable_registration_page_captcha";s:0:"";s:35:"aiowps_enable_registration_honeypot";s:0:"";s:27:"aiowps_enable_random_prefix";s:0:"";s:31:"aiowps_enable_automated_backups";s:0:"";s:26:"aiowps_db_backup_frequency";s:1:"4";s:25:"aiowps_db_backup_interval";s:1:"2";s:26:"aiowps_backup_files_stored";s:1:"2";s:32:"aiowps_send_backup_email_address";s:0:"";s:27:"aiowps_backup_email_address";s:20:"contato@unika.com.br";s:27:"aiowps_disable_file_editing";s:0:"";s:37:"aiowps_prevent_default_wp_file_access";s:0:"";s:22:"aiowps_system_log_file";s:9:"error_log";s:26:"aiowps_enable_blacklisting";s:0:"";s:26:"aiowps_banned_ip_addresses";s:0:"";s:28:"aiowps_enable_basic_firewall";s:0:"";s:31:"aiowps_enable_pingback_firewall";s:0:"";s:38:"aiowps_disable_xmlrpc_pingback_methods";s:0:"";s:34:"aiowps_block_debug_log_file_access";s:0:"";s:26:"aiowps_disable_index_views";s:0:"";s:30:"aiowps_disable_trace_and_track";s:0:"";s:28:"aiowps_forbid_proxy_comments";s:0:"";s:29:"aiowps_deny_bad_query_strings";s:0:"";s:34:"aiowps_advanced_char_string_filter";s:0:"";s:25:"aiowps_enable_5g_firewall";s:0:"";s:25:"aiowps_enable_6g_firewall";s:0:"";s:26:"aiowps_enable_custom_rules";s:0:"";s:19:"aiowps_custom_rules";s:0:"";s:25:"aiowps_enable_404_logging";s:0:"";s:28:"aiowps_enable_404_IP_lockout";s:0:"";s:30:"aiowps_404_lockout_time_length";s:2:"60";s:28:"aiowps_404_lock_redirect_url";s:16:"http://127.0.0.1";s:31:"aiowps_enable_rename_login_page";s:0:"";s:28:"aiowps_enable_login_honeypot";s:0:"";s:43:"aiowps_enable_brute_force_attack_prevention";s:0:"";s:30:"aiowps_brute_force_secret_word";s:0:"";s:24:"aiowps_cookie_brute_test";s:0:"";s:44:"aiowps_cookie_based_brute_force_redirect_url";s:16:"http://127.0.0.1";s:59:"aiowps_brute_force_attack_prevention_pw_protected_exception";s:0:"";s:51:"aiowps_brute_force_attack_prevention_ajax_exception";s:0:"";s:19:"aiowps_site_lockout";s:0:"";s:23:"aiowps_site_lockout_msg";s:0:"";s:30:"aiowps_enable_spambot_blocking";s:0:"";s:29:"aiowps_enable_comment_captcha";s:0:"";s:31:"aiowps_enable_autoblock_spam_ip";s:0:"";s:33:"aiowps_spam_ip_min_comments_block";s:0:"";s:32:"aiowps_enable_automated_fcd_scan";s:0:"";s:25:"aiowps_fcd_scan_frequency";s:1:"4";s:24:"aiowps_fcd_scan_interval";s:1:"2";s:28:"aiowps_fcd_exclude_filetypes";s:0:"";s:24:"aiowps_fcd_exclude_files";s:0:"";s:26:"aiowps_send_fcd_scan_email";s:0:"";s:29:"aiowps_fcd_scan_email_address";s:20:"contato@unika.com.br";s:27:"aiowps_fcds_change_detected";b:0;s:22:"aiowps_copy_protection";s:0:"";s:40:"aiowps_prevent_site_display_inside_frame";s:0:"";s:32:"aiowps_prevent_users_enumeration";s:0:"";}', 'yes'),
(157, 'wpcf7', 'a:2:{s:7:"version";s:5:"4.9.1";s:13:"bulk_validate";a:4:{s:9:"timestamp";d:1509284790;s:7:"version";s:3:"4.9";s:11:"count_valid";i:1;s:13:"count_invalid";i:0;}}', 'yes'),
(205, 'wpseo_sitemap_1_cache_validator', '5Djff', 'no'),
(206, 'wpseo_sitemap_servico_cache_validator', '4Cs8E', 'no'),
(213, 'wpseo_sitemap_destaque_cache_validator', '6m6Ag', 'no');
INSERT INTO `uk_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(182, 'configuracao', 'a:101:{s:8:"last_tab";s:1:"1";s:8:"opt_logo";a:5:{s:3:"url";s:67:"http://unika.handgran.com.br/wp-content/uploads/2017/10/logo@2x.png";s:2:"id";s:1:"5";s:6:"height";s:2:"74";s:5:"width";s:3:"232";s:9:"thumbnail";s:74:"http://unika.handgran.com.br/wp-content/uploads/2017/10/logo@2x-150x74.png";}s:13:"opt_Copyryght";s:45:"© Unika - Todos os direitos reservados 2017.";s:14:"opt_logoRodape";a:5:{s:3:"url";s:65:"http://unika.handgran.com.br/wp-content/uploads/2017/10/Page1.png";s:2:"id";s:1:"6";s:6:"height";s:2:"23";s:5:"width";s:2:"21";s:9:"thumbnail";s:65:"http://unika.handgran.com.br/wp-content/uploads/2017/10/Page1.png";}s:8:"opt_face";s:58:"https://www.facebook.com/Unika-Psicologia-121862845180902/";s:12:"opt_linkedin";s:42:"https://www.linkedin.com/company/22297352/";s:13:"opt_instagram";s:42:"https://www.instagram.com/unikapsicologia/";s:13:"opt_telefone2";s:0:"";s:9:"opt_Email";s:33:" contato@unikapisicologia.com.br ";s:12:"opt_endereco";s:0:"";s:25:"pg_inicial_Destque_titulo";s:17:"Bem vindo a Unika";s:24:"pg_inicial_Destque_texto";s:127:"Oferecemos soluções para preservar a vida nos ambientes de\r\ntrabalho por meio de intervenções em comportamento e cultura.\r\n";s:20:"pg_inicial_info_foto";a:5:{s:3:"url";s:66:"http://unika.handgran.com.br/wp-content/uploads/2017/12/foto-1.jpg";s:2:"id";s:3:"142";s:6:"height";s:3:"487";s:5:"width";s:3:"583";s:9:"thumbnail";s:74:"http://unika.handgran.com.br/wp-content/uploads/2017/12/foto-1-150x150.jpg";}s:22:"pg_inicial_info_titulo";s:39:"Preserve a vida no ambiente de trabalho";s:21:"pg_inicial_info_texto";s:230:"Considerando a realidade de cada cliente a Unika propõem soluções que se encaixam na rotina, utiliza metodologias de inteligência coletiva incluindo o cliente como parte do processo e por isso geram resultados sustentáveis.\r\n";s:20:"pg_inicial_info_link";s:1:"#";s:27:"pg_inicial_problemas_titulo";s:24:"Problemas que resolvemos";s:26:"pg_inicial_problemas_texto";s:73:"Previna acidentes por meio de ações focadas em comportamento e cultura.";s:22:"pg_inicial_onde_titulo";s:12:"Onde Atuamos";s:21:"pg_inicial_onde_texto";s:87:"Entre em contato conosco para verificar a possibilidade de atuação em outros países.";s:21:"pg_inicial_new_titulo";s:10:"Newsletter";s:20:"pg_inicial_new_texto";s:55:"Cadastre-se, entenda mais sobre segurança do trabalho.";s:22:"quem_somos_banner_foto";a:5:{s:3:"url";s:78:"http://unika.handgran.com.br/wp-content/uploads/2018/02/unikapatternbody-1.png";s:2:"id";s:3:"241";s:6:"height";s:3:"171";s:5:"width";s:2:"63";s:9:"thumbnail";s:85:"http://unika.handgran.com.br/wp-content/uploads/2018/02/unikapatternbody-1-63x150.png";}s:24:"quem_somos_banner_titulo";s:124:"Oferecemos soluções para preservar a vida nos ambientes de trabalho por meio de intervenções em comportamento e cultura.";s:22:"quem_somos_banner_logo";a:5:{s:3:"url";s:71:"http://unika.handgran.com.br/wp-content/uploads/2017/10/logoCortada.png";s:2:"id";s:3:"105";s:6:"height";s:4:"1003";s:5:"width";s:4:"1791";s:9:"thumbnail";s:79:"http://unika.handgran.com.br/wp-content/uploads/2017/10/logoCortada-150x150.png";}s:29:"quem_somos_integrantes_titulo";s:39:"Preserve a vida no ambiente de trabalho";s:28:"quem_somos_integrantes_texto";s:228:"Considerando a realidade de cada cliente a Unika propõem soluções que se encaixam na rotina, utiliza metodologias de inteligência coletiva incluindo o cliente como parte do processo e por isso geram resultados sustentáveis.";s:14:"contato_titulo";s:36:"Quer conhecer melhor nosso trabalho?";s:13:"contato_texto";s:34:"Ficaremos honradas com seu contato";s:13:"contato_email";s:33:" contato@unikapisicologia.com.br ";s:12:"titulo_D_C_S";s:37:"Diagnóstico de Cultura de Segurança";s:16:"subititulo_D_C_S";s:104:"Utilizamos como referência da Escada Evolutiva do Hearts&Minds para realizar o diagnóstico de Cultura.";s:11:"icone_D_C_S";a:5:{s:3:"url";s:70:"http://unika.handgran.com.br/wp-content/uploads/2017/10/Capturar-1.png";s:2:"id";s:3:"144";s:6:"height";s:2:"83";s:5:"width";s:2:"70";s:9:"thumbnail";s:70:"http://unika.handgran.com.br/wp-content/uploads/2017/10/Capturar-1.png";}s:9:"cor_D_C_S";s:49:"linear-gradient(135deg, #74688A 0%, #40374E 100%)";s:15:"cor_texto_D_C_S";s:4:"#fff";s:20:"texto_conteudo_D_C_S";s:391:"<strong>Como fazemos?</strong>\r\n\r\nSão avaliadas 7 dimensões da Cultura de Segurança da empresa que vão permitir ao cliente compreender qual o nível de maturidade da organização e quais são os focos de intervenção para evolução da cultura.\r\n\r\nOs métodos para coleta de dados qualitativa são: grupos focais, entrevistas dirigidas, observação participante e análise documental.";s:22:"quadros_conteudo_D_C_S";a:1:{i:0;s:218:"Sabia como a Unika entende a evolução da Cultura de Segurança das empresas. <br><br>Nós podemos ajudar. Solicite um orçamento deste serviço |http://unika.handgran.com.br/evolucao-da-cultura-de-seguranca/#conteudo";}s:8:"titulo_T";s:9:"Palestras";s:12:"subititulo_T";s:197:"As palestras são eventos com duração de 1 a 2 horas em que são trabalhados alguns temas de maneira dialogada, com objetivo de gerar compreensão e sensibilização sobre determinados assuntos. ";s:7:"icone_T";a:5:{s:3:"url";s:70:"http://unika.handgran.com.br/wp-content/uploads/2017/10/palestras2.png";s:2:"id";s:3:"140";s:6:"height";s:2:"61";s:5:"width";s:2:"61";s:9:"thumbnail";s:70:"http://unika.handgran.com.br/wp-content/uploads/2017/10/palestras2.png";}s:5:"cor_T";s:49:"linear-gradient(135deg, #C1947E 0%, #877065 100%)";s:11:"cor_texto_T";s:4:"#fff";s:18:"quadros_conteudo_T";a:8:{i:0;s:36:"Cultura de Segurança Sustentável|#";i:1;s:23:"Comportamento Seguro |#";i:2;s:24:"Percepção de Riscos |#";i:3;s:29:"Liderança para Segurança |#";i:4;s:24:"Treinamentos Eficazes |#";i:5;s:29:"Educação para Segurança |#";i:6;s:28:"Gestão de Consequências |#";i:7;s:56:"Metodologias Colaborativas para Solução de Problemas|#";}s:8:"titulo_W";s:9:"Workshops";s:12:"subititulo_W";s:223:"Os Workshops são eventos com duração de 4 a 8 horas em que são trabalhadas demandas específicas de cada empresa, por meio de exposição teórica e aplicação de técnicas para solução e encaminhamento de questões.";s:7:"icone_W";a:5:{s:3:"url";s:66:"http://unika.handgran.com.br/wp-content/uploads/2017/10/flexas.png";s:2:"id";s:3:"136";s:6:"height";s:2:"61";s:5:"width";s:2:"61";s:9:"thumbnail";s:66:"http://unika.handgran.com.br/wp-content/uploads/2017/10/flexas.png";}s:5:"cor_W";s:49:"linear-gradient(135deg, #dcc3bc 0%, #bca6a0 100%)";s:11:"cor_texto_W";s:4:"#fff";s:27:"quadros_conteudo_esquerdo_W";s:108:"São desenhados de maneira totalmente customizada às temáticas e demandas que o\r\ncliente pretende abordar.";s:26:"quadros_conteudo_direito_W";s:149:"São utilizadas metodologias colaborativas tais como Café Mundial, Espaço Aberto,\r\nInvestigação Apreciativa, Pró-Ação, Aquário, entre outras.";s:18:"quadros_conteudo_W";a:4:{i:0;s:117:"Workshop de Abertura | Realizado para envolvimento da liderança nas estratégias que serão adotadas naquele ano.|  ";i:1;s:155:"Workshop de Percepção de Riscos | Realizado com foco em aprofundar o conhecimento técnico e na prática exercitar a melhoria da percepção de risco.|  ";i:2;s:124:"Workshop de Engajamento de Líderes | Realizado para envolver os líderes no compromisso com as questões de segurança. |  ";i:3;s:177:"Workshop Construindo Novas Soluções para Problemas Antigos | Realizado para envolver profissionais na inovação para solução de problemas bem conhecidos na organização.| ";}s:9:"titulo_TT";s:12:"Treinamentos";s:13:"subititulo_TT";s:177:"Os treinamentos desenvolvidos pela Unika têm o objetivo de aprimorar competências dos participantes em temas relacionados à segurança comportamental e cultura de segurança.";s:8:"icone_TT";a:5:{s:3:"url";s:69:"http://unika.handgran.com.br/wp-content/uploads/2017/10/palestras.png";s:2:"id";s:3:"139";s:6:"height";s:2:"61";s:5:"width";s:2:"61";s:9:"thumbnail";s:69:"http://unika.handgran.com.br/wp-content/uploads/2017/10/palestras.png";}s:6:"cor_TT";s:51:"linear-gradient(133.5deg, #eae1d6 0%, #d7cec3 100%)";s:12:"cor_texto_TT";s:8:"#3E414F;";s:28:"quadros_conteudo_esquerdo_TT";a:2:{i:0;s:109:"Apresentam metodologias apropriadas ao aprendizado de pessoas adultas e técnicas de inteligência coletiva. ";i:1;s:108:"A carga horária dos treinamentos será definida de acordo com os objetivos de aprendizagem de cada módulo.";}s:27:"quadros_conteudo_direito_TT";a:2:{i:0;s:160:"Os objetivos de aprendizagem são relacionados a estratégia do negócio e são estabelecidos indicadores para verificação de aprendizagem dos participantes. ";i:1;s:110:"A Unika oferece possibilidade de realizar estímulos frequentes de aprendizagem por meio de tarefas virtuais. ";}s:19:"quadros_conteudo_TT";a:6:{i:0;s:50:"Entendendo a Evolução da Cultura de Segurança|#";i:1;s:48:"Liderança Visível e Entusiasta em Segurança|#";i:2;s:28:"Educação para Segurança|#";i:3;s:52:"Transformando seu Time de Segurança em Educadores|#";i:4;s:40:"Gestão de Consequências e Segurança|#";i:5;s:37:"Desvendando a Percepção de Riscos|#";}s:10:"titulo_FSC";s:40:"Ferramentas de Segurança Comportamental";s:14:"subititulo_FSC";s:215:"É uma ferramenta para liderança observar, abordar e corrigir comportamentos de risco e reforçar comportamentos seguros. A Unika utiliza a referência do psicólogo norte americano Scott Geller em sua metodologia.";s:9:"icone_FSC";a:5:{s:3:"url";s:64:"http://unika.handgran.com.br/wp-content/uploads/2017/10/mala.png";s:2:"id";s:3:"137";s:6:"height";s:2:"61";s:5:"width";s:2:"61";s:9:"thumbnail";s:64:"http://unika.handgran.com.br/wp-content/uploads/2017/10/mala.png";}s:7:"cor_FSC";s:83:"linear-gradient(133.5deg, #f1eeea 0%, #D9D4CF 76.89%, #D7D2CC 83.25%, #dbd7d1 100%)";s:13:"cor_texto_FSC";s:8:"#3E414F;";s:27:"quadros_titulo_esquerdo_FSC";s:38:"DDS – Diálogo Diário de Segurança";s:29:"quadros_conteudo_esquerdo_FSC";s:158:"É uma reunião com duração de no máximo 15 minutos, que promove diálogo e conhecimento sobre segurança voltada ao público operacional e administrativo.";s:29:"quadros_servicos_esquerdo_FSC";a:4:{i:0;s:40:"Implantação da Ferramenta DDS Completa";i:1;s:36:"Treinamento dos Facilitadores de DDS";i:2;s:31:"Desenvolvimento de temas de DDS";i:3;s:27:"Verificação de Resultados";}s:26:"quadros_titulo_direito_FSC";s:47:"OAC - Observação de Abordagem Comportamental ";s:28:"quadros_conteudo_direito_FSC";s:215:"É uma ferramenta para liderança observar, abordar e corrigir comportamentos de risco e reforçar comportamentos seguros. A Unika utiliza a referência do psicólogo norte americano Scott Geller em sua metodologia.";s:28:"quadros_servicos_direito_FSC";a:4:{i:0;s:40:"Implantação da Ferramenta OAC Completa";i:1;s:40:"Revitalização de Ferramentas Similares";i:2;s:43:"Treinamento teórico e prático de líderes";i:3;s:40:"Gestão de Indicadores da Ferramenta OAC";}s:35:"quadros_titulo_esquerdo_Segundo_FSC";s:36:"Relatos de quase-acidentes e desvios";s:37:"quadros_conteudo_esquerdo_Segundo_FSC";s:235:"Esta é uma ferramenta preventiva importante para que todos os empregados possam contribuir identificando e relatando desvios (comportamentos de risco) e quase-acidentes, de modo que a empresa possa intervir antes de uma ocorrência.\r\n";s:37:"quadros_servicos_esquerdo_Segundo_FSC";a:5:{i:0;s:36:"Implantação da Ferramenta Completa";i:1;s:34:"Treinamento para uso da Ferramenta";i:2;s:44:"Campanha para Sensibilização de Empregados";i:3;s:31:"Gestão dos Dados da Ferramenta";i:4;s:10:"Soluções";}s:34:"quadros_titulo_direito_Segundo_FSC";s:21:"Abordagem entre pares";s:36:"quadros_conteudo_direito_Segundo_FSC";s:150:"É uma ferramenta para empregados operacionais/ administrativos para abordar e corrigir comportamentos de risco e reforçar comportamentos seguros. \r\n";s:36:"quadros_servicos_direito_Segundo_FSC";a:4:{i:0;s:36:"Implantação da Ferramenta Completa";i:1;s:40:"Revitalização de Ferramentas Similares";i:2;s:67:"Sensibilização e treinamento de empregados para uso da ferramenta";i:3;s:36:"Gestão de indicadores da ferramenta";}s:35:"quadros_titulo_esquerdo_Tegundo_FSC";s:43:"Inspeção de segurança da alta liderança";s:37:"quadros_conteudo_esquerdo_Tegundo_FSC";s:261:"É uma ferramenta que evidencia a liderança pelo exemplo, em que os altos líderes dedicam um tempo na sua agenda para realizar uma verificação em campo com foco em condições e comportamentos em segurança, visando atuar nas oportunidades identificadas. \r\n";s:37:"quadros_servicos_esquerdo_Tegundo_FSC";a:5:{i:0;s:36:"Implantação da Ferramenta Completa";i:1;s:38:"Acompanhamento prático da ferramenta ";i:2;s:21:"Gestão da ferramenta";i:3;s:43:"Construção do Funcionamento da Inspeção";i:4;s:42:"Sensibilização e treinamento de líderes";}s:34:"quadros_titulo_direito_Tegundo_FSC";s:24:"Reuniões de  Segurança";s:36:"quadros_conteudo_direito_Tegundo_FSC";s:274:"São reuniões com duração de 1 hora ou mais com a finalidade de compartilhar indicadores e informações relevantes para o conhecimento de diferentes públicos. \r\nA Unika ajuda a tornar estas reuniões mais dinâmicas, participativas e por consequência mais efetivas. \r\n";s:36:"quadros_servicos_direito_Tegundo_FSC";a:3:{i:0;s:27:"Implantação das Reuniões";i:1;s:28:"Revitalização de Reuniões";i:2;s:53:"Implantação de Indicador de Qualidade das Reuniões";}s:34:"quadros_titulo_esquerdo_quarto_FSC";s:52:"Revisão e adaptação de treinamentos de segurança";s:36:"quadros_conteudo_esquerdo_quarto_FSC";s:348:"Todas as empresas têm uma carga de treinamento alta para os temas de segurança. Mas os treinamentos alcançam os resultados esperados? Será que apresentam metodologias e técnicas apropriadas a adultos?\r\nA Unika tem competência para revisar seus treinamentos e torná-los mais efetivos por meio de planejamento, método e recursos adequados. \r\n";s:36:"quadros_servicos_esquerdo_quarto_FSC";a:4:{i:0;s:33:"Análise crítica de treinamentos";i:1;s:54:"Definição de indicadores de eficácia de treinamento";i:2;s:27:"Treinamento de instrutores ";i:3;s:24:"Revisão de treinamentos";}s:33:"quadros_titulo_direito_quarto_FSC";s:23:"Indicadores Preventivos";s:35:"quadros_conteudo_direito_quarto_FSC";s:378:"A implantação destes indicadores auxilia a empresa a dar foco em ações preventivas e a construir um processo cada vez mais seguro. \r\nA Unika realiza um mapeamento das principais responsabilidades dos líderes e desenvolve um indicador para evidenciar o comprometimento de cada líder com as questões de segurança, uma vez que são os protagonistas na busca dos resultados.";s:35:"quadros_servicos_direito_quarto_FSC";a:3:{i:0;s:38:"Implantação de indicador preventivo ";i:1;s:38:"Acompanhamento e feedback aos líderes";i:2;s:82:"Mapeamento e definição de responsabilidades dos líderes por nível hierárquico";}s:34:"quadros_titulo_esquerdo_quinto_FSC";s:46:"Análise e investigação <br> de ocorrências";s:36:"quadros_conteudo_esquerdo_quinto_FSC";s:254:"Uma Análise e Investigação de Ocorrências eficiente é o segredo para evitar recorrências dentro das organizações. \r\nA Unika tem competência para capacitar os investigadores na análise dos Fatores Humanos que contribuíram para a ocorrência. \r\n";s:36:"quadros_servicos_esquerdo_quinto_FSC";a:3:{i:0;s:48:"Treinamento de investigadores em Fatores Humanos";i:1;s:58:"Construção de Planos de Ação para Evitar Recorrências";i:2;s:61:"Estruturação de Estratégia de Abrangência de Ocorrências";}s:33:"quadros_titulo_direito_quinto_FSC";s:39:"Gestão de consequências em segurança";s:35:"quadros_conteudo_direito_quinto_FSC";s:139:"A Unika auxilia a empresa a definir a melhor estratégia para intervir em comportamentos de riscos e reforçar os comportamentos seguros.\r\n";s:35:"quadros_servicos_direito_quinto_FSC";a:4:{i:0;s:54:"Workshop sobre Gestão de Consequências em Segurança";i:1;s:59:"Suporte técnico para Comitês de Gestão de Consequências";i:2;s:73:"Implantação de uma Política de Gestão de Consequências em Segurança";i:3;s:75:"Revitalização de uma Política de Gestão de Consequências em Segurança";}s:10:"titulo_PES";s:39:"Planejamento Estratégico de Segurança";s:14:"subititulo_PES";s:164:"O planejamento estratégico tem a finalidade de definir onde a empresa pretende chegar em termos de cultura de segurança e como fará para alcançar este objetivo.";s:9:"icone_PES";a:5:{s:3:"url";s:69:"http://unika.handgran.com.br/wp-content/uploads/2017/10/malaChave.png";s:2:"id";s:3:"138";s:6:"height";s:2:"61";s:5:"width";s:2:"61";s:9:"thumbnail";s:69:"http://unika.handgran.com.br/wp-content/uploads/2017/10/malaChave.png";}s:7:"cor_PES";s:52:"linear-gradient(133.5deg, #F3F3F3 0%, #D8D8D8 100%);";s:13:"cor_texto_PES";s:7:"#3e414f";s:20:"quadros_conteudo_PES";a:3:{i:0;s:75:"Análise das políticas, estrutura organizacional e estratégia do negócio";i:1;s:37:"Workshop de Planejamento Estratégico";i:2;s:42:"Comunicação da Estratégia de Segurança";}s:19:"opt-customizer-only";s:1:"2";}', 'yes'),
(183, 'configuracao-transients', 'a:2:{s:14:"changed_values";a:1:{s:34:"quadros_titulo_direito_Tegundo_FSC";s:28:"Reuniões de <br> Segurança";}s:9:"last_save";i:1519240872;}', 'yes'),
(337, 'category_children', 'a:0:{}', 'yes'),
(216, 'wpseo_sitemap_category_cache_validator', '4Ggr2', 'no'),
(325, 'wpseo_sitemap_author_cache_validator', '5Djfh', 'no'),
(328, 'wpseo_taxonomy_meta', 'a:1:{s:8:"category";a:1:{i:1;a:2:{s:13:"wpseo_linkdex";s:2:"-9";s:19:"wpseo_content_score";s:2:"30";}}}', 'yes'),
(340, 'wpseo_sitemap_63_cache_validator', '5E52w', 'no'),
(339, 'wpseo_sitemap_60_cache_validator', '5E52x', 'no'),
(344, 'wpseo_sitemap_119_cache_validator', '5E52t', 'no'),
(346, 'wpseo_sitemap_121_cache_validator', '5E52q', 'no'),
(293, 'auto_core_update_notified', 'a:4:{s:4:"type";s:7:"success";s:5:"email";s:20:"contato@unika.com.br";s:7:"version";s:5:"4.9.4";s:9:"timestamp";i:1518115220;}', 'no'),
(296, 'wysija_last_php_cron_call', '1521475979', 'yes'),
(297, 'wysija_queries', '', 'no'),
(298, 'wysija_queries_errors', '', 'no'),
(278, 'widget_wysija', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(279, 'wysija_post_type_updated', '1509458677', 'yes'),
(280, 'wysija_post_type_created', '1509458677', 'yes'),
(311, 'wpseo_sitemap_equipe_cache_validator', '5thSe', 'no'),
(209, 'wpseo_sitemap_parceiros_cache_validator', 'mb75', 'no'),
(211, 'wpseo_sitemap_depoimento_cache_validator', '6ecPY', 'no'),
(173, 'redux_version_upgraded_from', '3.6.7.7', 'yes'),
(174, '_transient_timeout__redux_activation_redirect', '1521476017', 'no'),
(175, '_transient__redux_activation_redirect', '1', 'no'),
(228, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:"auto_add";a:0:{}}', 'yes'),
(229, 'wpseo_sitemap_page_cache_validator', '5Aapp', 'no'),
(217, 'wpseo_sitemap_post_cache_validator', '5qVrR', 'no'),
(264, 'wpseo_sitemap_paises_cache_validator', '4tvxw', 'no'),
(281, 'installation_step', '16', 'yes'),
(282, 'wysija', 'YToxODp7czo5OiJmcm9tX25hbWUiO3M6NToidW5pa2EiO3M6MTI6InJlcGx5dG9fbmFtZSI7czo1OiJ1bmlrYSI7czoxNToiZW1haWxzX25vdGlmaWVkIjtzOjIwOiJjb250YXRvQHVuaWthLmNvbS5iciI7czoxMDoiZnJvbV9lbWFpbCI7czoxNDoiaW5mb0Bsb2NhbGhvc3QiO3M6MTM6InJlcGx5dG9fZW1haWwiO3M6MTQ6ImluZm9AbG9jYWxob3N0IjtzOjE1OiJkZWZhdWx0X2xpc3RfaWQiO2k6MTtzOjE3OiJ0b3RhbF9zdWJzY3JpYmVycyI7czoxOiIzIjtzOjE2OiJpbXBvcnR3cF9saXN0X2lkIjtpOjI7czoxODoiY29uZmlybV9lbWFpbF9saW5rIjtpOjEwMztzOjEyOiJ1cGxvYWRmb2xkZXIiO3M6NTM6IkM6XHdhbXBcd3d3XHByb2pldG9zXHVuaWthL3dwLWNvbnRlbnQvdXBsb2Fkc1x3eXNpamFcIjtzOjk6InVwbG9hZHVybCI7czo1ODoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy91bmlrYS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphLyI7czoxNjoiY29uZmlybV9lbWFpbF9pZCI7aToyO3M6OToiaW5zdGFsbGVkIjtiOjE7czoyMDoibWFuYWdlX3N1YnNjcmlwdGlvbnMiO2I6MTtzOjE0OiJpbnN0YWxsZWRfdGltZSI7aToxNTA5NDU4Njg0O3M6MTc6Ind5c2lqYV9kYl92ZXJzaW9uIjtzOjY6IjIuNy4xNCI7czoxMToiZGtpbV9kb21haW4iO3M6OToibG9jYWxob3N0IjtzOjE2OiJ3eXNpamFfd2hhdHNfbmV3IjtzOjY6IjIuNy4xNCI7fQ==', 'yes'),
(299, 'wysija_msg', '', 'no'),
(283, 'wysija_reinstall', '0', 'no'),
(284, 'wysija_schedules', 'a:5:{s:5:"queue";a:3:{s:13:"next_schedule";i:1520631958;s:13:"prev_schedule";b:0;s:7:"running";b:0;}s:6:"bounce";a:3:{s:13:"next_schedule";i:1509545098;s:13:"prev_schedule";i:0;s:7:"running";b:0;}s:5:"daily";a:3:{s:13:"next_schedule";i:1520948856;s:13:"prev_schedule";b:0;s:7:"running";b:0;}s:6:"weekly";a:3:{s:13:"next_schedule";i:1520960236;s:13:"prev_schedule";b:0;s:7:"running";b:0;}s:7:"monthly";a:3:{s:13:"next_schedule";i:1523895179;s:13:"prev_schedule";b:0;s:7:"running";b:0;}}', 'yes'),
(294, 'wysija_check_pn', '1521475979.73', 'yes'),
(295, 'wysija_last_scheduled_check', '1521475979', 'yes'),
(421, 'wpseo_sitemap_cache_validator_global', '3VI3A', 'no'),
(1191, '_site_transient_update_themes', 'O:8:"stdClass":4:{s:12:"last_checked";i:1521475981;s:7:"checked";a:2:{s:13:"twentyfifteen";s:3:"1.8";s:5:"unika";s:5:"1.0.0";}s:8:"response";a:1:{s:13:"twentyfifteen";a:4:{s:5:"theme";s:13:"twentyfifteen";s:11:"new_version";s:3:"1.9";s:3:"url";s:43:"https://wordpress.org/themes/twentyfifteen/";s:7:"package";s:59:"https://downloads.wordpress.org/theme/twentyfifteen.1.9.zip";}}s:12:"translations";a:0:{}}', 'no'),
(416, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(824, 'simplehistory_AvailableUpdatesLogger_theme_updates_available', 'a:1:{s:13:"twentyfifteen";a:1:{s:15:"checked_version";s:3:"1.9";}}', 'yes'),
(1053, 'wpseo_sitemap_218_cache_validator', '4l7kY', 'no'),
(1105, '_site_transient_update_core', 'O:8:"stdClass":4:{s:7:"updates";a:1:{i:0;O:8:"stdClass":10:{s:8:"response";s:6:"latest";s:8:"download";s:65:"https://downloads.wordpress.org/release/pt_BR/wordpress-4.9.4.zip";s:6:"locale";s:5:"pt_BR";s:8:"packages";O:8:"stdClass":5:{s:4:"full";s:65:"https://downloads.wordpress.org/release/pt_BR/wordpress-4.9.4.zip";s:10:"no_content";b:0;s:11:"new_bundled";b:0;s:7:"partial";b:0;s:8:"rollback";b:0;}s:7:"current";s:5:"4.9.4";s:7:"version";s:5:"4.9.4";s:11:"php_version";s:5:"5.2.4";s:13:"mysql_version";s:3:"5.0";s:11:"new_bundled";s:3:"4.7";s:15:"partial_version";s:0:"";}}s:12:"last_checked";i:1521475980;s:15:"version_checked";s:5:"4.9.4";s:12:"translations";a:0:{}}', 'no'),
(917, 'widget_wp_user_avatar_profile', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(934, 'wpua_hash_gravatar', 's:74:"a:1:{s:32:"3d385ef985333f3ee656349a48613f71";a:1:{s:10:"02-15-2018";b:0;}}";', 'yes'),
(949, 'wpseo_sitemap_115_cache_validator', '5E52v', 'no'),
(1189, '_site_transient_timeout_theme_roots', '1521477781', 'no'),
(1190, '_site_transient_theme_roots', 'a:2:{s:13:"twentyfifteen";s:7:"/themes";s:5:"unika";s:7:"/themes";}', 'no'),
(1192, '_site_transient_update_plugins', 'O:8:"stdClass":5:{s:12:"last_checked";i:1521475982;s:7:"checked";a:11:{s:51:"all-in-one-wp-security-and-firewall/wp-security.php";s:5:"4.3.1";s:25:"base-unika/base-unika.php";s:3:"0.1";s:36:"contact-form-7/wp-contact-form-7.php";s:5:"4.9.1";s:32:"disqus-comment-system/disqus.php";s:4:"2.87";s:28:"wysija-newsletters/index.php";s:5:"2.8.1";s:21:"meta-box/meta-box.php";s:6:"4.12.6";s:37:"post-types-order/post-types-order.php";s:7:"1.9.3.5";s:35:"redux-framework/redux-framework.php";s:7:"3.6.7.7";s:24:"simple-history/index.php";s:4:"2.20";s:35:"wp-user-avatars/wp-user-avatars.php";s:5:"1.3.0";s:24:"wordpress-seo/wp-seo.php";s:3:"5.9";}s:8:"response";a:8:{s:51:"all-in-one-wp-security-and-firewall/wp-security.php";O:8:"stdClass":11:{s:2:"id";s:49:"w.org/plugins/all-in-one-wp-security-and-firewall";s:4:"slug";s:35:"all-in-one-wp-security-and-firewall";s:6:"plugin";s:51:"all-in-one-wp-security-and-firewall/wp-security.php";s:11:"new_version";s:5:"4.3.2";s:3:"url";s:66:"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/";s:7:"package";s:78:"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip";s:5:"icons";a:2:{s:2:"1x";s:88:"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826";s:7:"default";s:88:"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826";}s:7:"banners";a:2:{s:2:"1x";s:90:"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1232826";s:7:"default";s:90:"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1232826";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.2";s:13:"compatibility";O:8:"stdClass":0:{}}s:36:"contact-form-7/wp-contact-form-7.php";O:8:"stdClass":11:{s:2:"id";s:28:"w.org/plugins/contact-form-7";s:4:"slug";s:14:"contact-form-7";s:6:"plugin";s:36:"contact-form-7/wp-contact-form-7.php";s:11:"new_version";s:5:"5.0.1";s:3:"url";s:45:"https://wordpress.org/plugins/contact-form-7/";s:7:"package";s:63:"https://downloads.wordpress.org/plugin/contact-form-7.5.0.1.zip";s:5:"icons";a:3:{s:2:"1x";s:66:"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007";s:2:"2x";s:66:"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007";s:7:"default";s:66:"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007";}s:7:"banners";a:3:{s:2:"2x";s:69:"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901";s:2:"1x";s:68:"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427";s:7:"default";s:69:"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.4";s:13:"compatibility";O:8:"stdClass":0:{}}s:32:"disqus-comment-system/disqus.php";O:8:"stdClass":11:{s:2:"id";s:35:"w.org/plugins/disqus-comment-system";s:4:"slug";s:21:"disqus-comment-system";s:6:"plugin";s:32:"disqus-comment-system/disqus.php";s:11:"new_version";s:6:"3.0.15";s:3:"url";s:52:"https://wordpress.org/plugins/disqus-comment-system/";s:7:"package";s:71:"https://downloads.wordpress.org/plugin/disqus-comment-system.3.0.15.zip";s:5:"icons";a:3:{s:2:"2x";s:74:"https://ps.w.org/disqus-comment-system/assets/icon-256x256.png?rev=1012448";s:3:"svg";s:66:"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350";s:7:"default";s:66:"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350";}s:7:"banners";a:2:{s:2:"1x";s:76:"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350";s:7:"default";s:76:"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.4";s:13:"compatibility";O:8:"stdClass":0:{}}s:28:"wysija-newsletters/index.php";O:8:"stdClass":11:{s:2:"id";s:32:"w.org/plugins/wysija-newsletters";s:4:"slug";s:18:"wysija-newsletters";s:6:"plugin";s:28:"wysija-newsletters/index.php";s:11:"new_version";s:5:"2.8.2";s:3:"url";s:49:"https://wordpress.org/plugins/wysija-newsletters/";s:7:"package";s:67:"https://downloads.wordpress.org/plugin/wysija-newsletters.2.8.2.zip";s:5:"icons";a:4:{s:2:"1x";s:71:"https://ps.w.org/wysija-newsletters/assets/icon-128x128.png?rev=1703780";s:2:"2x";s:71:"https://ps.w.org/wysija-newsletters/assets/icon-256x256.png?rev=1703780";s:3:"svg";s:63:"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234";s:7:"default";s:63:"https://ps.w.org/wysija-newsletters/assets/icon.svg?rev=1390234";}s:7:"banners";a:3:{s:2:"2x";s:74:"https://ps.w.org/wysija-newsletters/assets/banner-1544x500.png?rev=1703780";s:2:"1x";s:73:"https://ps.w.org/wysija-newsletters/assets/banner-772x250.jpg?rev=1703780";s:7:"default";s:74:"https://ps.w.org/wysija-newsletters/assets/banner-1544x500.png?rev=1703780";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.4";s:13:"compatibility";O:8:"stdClass":0:{}}s:21:"meta-box/meta-box.php";O:8:"stdClass":11:{s:2:"id";s:22:"w.org/plugins/meta-box";s:4:"slug";s:8:"meta-box";s:6:"plugin";s:21:"meta-box/meta-box.php";s:11:"new_version";s:6:"4.14.0";s:3:"url";s:39:"https://wordpress.org/plugins/meta-box/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/meta-box.4.14.0.zip";s:5:"icons";a:2:{s:2:"1x";s:61:"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915";s:7:"default";s:61:"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915";}s:7:"banners";a:2:{s:2:"1x";s:63:"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1626382";s:7:"default";s:63:"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1626382";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.4";s:13:"compatibility";O:8:"stdClass":0:{}}s:37:"post-types-order/post-types-order.php";O:8:"stdClass":11:{s:2:"id";s:30:"w.org/plugins/post-types-order";s:4:"slug";s:16:"post-types-order";s:6:"plugin";s:37:"post-types-order/post-types-order.php";s:11:"new_version";s:7:"1.9.3.6";s:3:"url";s:47:"https://wordpress.org/plugins/post-types-order/";s:7:"package";s:67:"https://downloads.wordpress.org/plugin/post-types-order.1.9.3.6.zip";s:5:"icons";a:2:{s:2:"1x";s:69:"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428";s:7:"default";s:69:"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428";}s:7:"banners";a:3:{s:2:"2x";s:72:"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574";s:2:"1x";s:71:"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949";s:7:"default";s:72:"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.1";s:13:"compatibility";O:8:"stdClass":0:{}}s:35:"redux-framework/redux-framework.php";O:8:"stdClass":11:{s:2:"id";s:29:"w.org/plugins/redux-framework";s:4:"slug";s:15:"redux-framework";s:6:"plugin";s:35:"redux-framework/redux-framework.php";s:11:"new_version";s:5:"3.6.8";s:3:"url";s:46:"https://wordpress.org/plugins/redux-framework/";s:7:"package";s:64:"https://downloads.wordpress.org/plugin/redux-framework.3.6.8.zip";s:5:"icons";a:4:{s:2:"1x";s:67:"https://ps.w.org/redux-framework/assets/icon-128x128.png?rev=995554";s:2:"2x";s:67:"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554";s:3:"svg";s:59:"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554";s:7:"default";s:59:"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554";}s:7:"banners";a:2:{s:2:"1x";s:69:"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165";s:7:"default";s:69:"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165";}s:11:"banners_rtl";a:0:{}s:6:"tested";s:5:"4.9.4";s:13:"compatibility";O:8:"stdClass":0:{}}s:24:"wordpress-seo/wp-seo.php";O:8:"stdClass":11:{s:2:"id";s:27:"w.org/plugins/wordpress-seo";s:4:"slug";s:13:"wordpress-seo";s:6:"plugin";s:24:"wordpress-seo/wp-seo.php";s:11:"new_version";s:5:"7.0.3";s:3:"url";s:44:"https://wordpress.org/plugins/wordpress-seo/";s:7:"package";s:62:"https://downloads.wordpress.org/plugin/wordpress-seo.7.0.3.zip";s:5:"icons";a:4:{s:2:"1x";s:66:"https://ps.w.org/wordpress-seo/assets/icon-128x128.png?rev=1834347";s:2:"2x";s:66:"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347";s:3:"svg";s:58:"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1834347";s:7:"default";s:58:"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1834347";}s:7:"banners";a:3:{s:2:"2x";s:69:"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1695112";s:2:"1x";s:68:"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1695112";s:7:"default";s:69:"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1695112";}s:11:"banners_rtl";a:3:{s:2:"2x";s:73:"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1695112";s:2:"1x";s:72:"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1695112";s:7:"default";s:73:"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1695112";}s:6:"tested";s:5:"4.9.4";s:13:"compatibility";O:8:"stdClass":0:{}}}s:12:"translations";a:0:{}s:9:"no_update";a:2:{s:24:"simple-history/index.php";O:8:"stdClass":9:{s:2:"id";s:28:"w.org/plugins/simple-history";s:4:"slug";s:14:"simple-history";s:6:"plugin";s:24:"simple-history/index.php";s:11:"new_version";s:4:"2.20";s:3:"url";s:45:"https://wordpress.org/plugins/simple-history/";s:7:"package";s:62:"https://downloads.wordpress.org/plugin/simple-history.2.20.zip";s:5:"icons";a:3:{s:0:"";s:63:"https://ps.w.org/simple-history/assets/icon-512.png?rev=1045504";s:3:"svg";s:59:"https://ps.w.org/simple-history/assets/icon.svg?rev=1044051";s:7:"default";s:59:"https://ps.w.org/simple-history/assets/icon.svg?rev=1044051";}s:7:"banners";a:2:{s:2:"1x";s:69:"https://ps.w.org/simple-history/assets/banner-772x250.png?rev=1045523";s:7:"default";s:69:"https://ps.w.org/simple-history/assets/banner-772x250.png?rev=1045523";}s:11:"banners_rtl";a:0:{}}s:35:"wp-user-avatars/wp-user-avatars.php";O:8:"stdClass":9:{s:2:"id";s:29:"w.org/plugins/wp-user-avatars";s:4:"slug";s:15:"wp-user-avatars";s:6:"plugin";s:35:"wp-user-avatars/wp-user-avatars.php";s:11:"new_version";s:5:"1.3.0";s:3:"url";s:46:"https://wordpress.org/plugins/wp-user-avatars/";s:7:"package";s:58:"https://downloads.wordpress.org/plugin/wp-user-avatars.zip";s:5:"icons";a:3:{s:2:"1x";s:68:"https://ps.w.org/wp-user-avatars/assets/icon-128x128.png?rev=1266776";s:2:"2x";s:68:"https://ps.w.org/wp-user-avatars/assets/icon-256x256.png?rev=1266776";s:7:"default";s:68:"https://ps.w.org/wp-user-avatars/assets/icon-256x256.png?rev=1266776";}s:7:"banners";a:3:{s:2:"2x";s:71:"https://ps.w.org/wp-user-avatars/assets/banner-1544x500.png?rev=1266775";s:2:"1x";s:70:"https://ps.w.org/wp-user-avatars/assets/banner-772x250.png?rev=1266775";s:7:"default";s:71:"https://ps.w.org/wp-user-avatars/assets/banner-1544x500.png?rev=1266775";}s:11:"banners_rtl";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_postmeta`
--

CREATE TABLE IF NOT EXISTS `uk_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM AUTO_INCREMENT=777 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_postmeta`
--

INSERT INTO `uk_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 4, '_form', '[email* email id:email class:email placeholder "e-mail"][textarea* mesagem id:mesagem class:mesagem placeholder "seu texto aqui…"]\n<div class="submit">\n[submit id:enviar class:enviar "Enviar"]\n</div>'),
(3, 4, '_mail', 'a:9:{s:6:"active";b:1;s:7:"subject";s:22:"Unika "[your-subject]"";s:6:"sender";s:34:"[your-name] <contato@unika.com.br>";s:9:"recipient";s:20:"contato@unika.com.br";s:4:"body";s:190:"De: [your-name] <[your-email]>\nAssunto: [your-subject]\n\nCorpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Unika (http://localhost/projetos/unika)";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(4, 4, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:22:"Unika "[your-subject]"";s:6:"sender";s:28:"Unika <contato@unika.com.br>";s:9:"recipient";s:12:"[your-email]";s:4:"body";s:134:"Corpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Unika (http://localhost/projetos/unika)";s:18:"additional_headers";s:30:"Reply-To: contato@unika.com.br";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(5, 4, '_messages', 'a:23:{s:12:"mail_sent_ok";s:27:"Agradecemos a sua mensagem.";s:12:"mail_sent_ng";s:74:"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.";s:16:"validation_error";s:63:"Um ou mais campos possuem um erro. Verifique e tente novamente.";s:4:"spam";s:74:"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.";s:12:"accept_terms";s:72:"Você deve aceitar os termos e condições antes de enviar sua mensagem.";s:16:"invalid_required";s:24:"O campo é obrigatório.";s:16:"invalid_too_long";s:23:"O campo é muito longo.";s:17:"invalid_too_short";s:23:"O campo é muito curto.";s:12:"invalid_date";s:34:"O formato de data está incorreto.";s:14:"date_too_early";s:44:"A data é anterior à mais antiga permitida.";s:13:"date_too_late";s:44:"A data é posterior à maior data permitida.";s:13:"upload_failed";s:49:"Ocorreu um erro desconhecido ao enviar o arquivo.";s:24:"upload_file_type_invalid";s:59:"Você não tem permissão para enviar esse tipo de arquivo.";s:21:"upload_file_too_large";s:26:"O arquivo é muito grande.";s:23:"upload_failed_php_error";s:36:"Ocorreu um erro ao enviar o arquivo.";s:14:"invalid_number";s:34:"O formato de número é inválido.";s:16:"number_too_small";s:46:"O número é menor do que o mínimo permitido.";s:16:"number_too_large";s:46:"O número é maior do que o máximo permitido.";s:23:"quiz_answer_not_correct";s:39:"A resposta para o quiz está incorreta.";s:17:"captcha_not_match";s:35:"O código digitado está incorreto.";s:13:"invalid_email";s:45:"O endereço de e-mail informado é inválido.";s:11:"invalid_url";s:19:"A URL é inválida.";s:11:"invalid_tel";s:35:"O número de telefone é inválido.";}'),
(6, 4, '_additional_settings', ''),
(7, 4, '_locale', 'pt_BR'),
(8, 5, '_wp_attached_file', '2017/10/logo@2x.png'),
(9, 5, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:232;s:6:"height";i:74;s:4:"file";s:19:"2017/10/logo@2x.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"logo@2x-150x74.png";s:5:"width";i:150;s:6:"height";i:74;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(10, 6, '_wp_attached_file', '2017/10/Page1.png'),
(11, 6, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:21;s:6:"height";i:23;s:4:"file";s:17:"2017/10/Page1.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(12, 7, '_wp_attached_file', '2017/10/Rectangle-2-Copy-2@2x.png'),
(13, 7, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:980;s:4:"file";s:33:"2017/10/Rectangle-2-Copy-2@2x.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:33:"Rectangle-2-Copy-2@2x-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:33:"Rectangle-2-Copy-2@2x-300x184.png";s:5:"width";i:300;s:6:"height";i:184;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:33:"Rectangle-2-Copy-2@2x-768x470.png";s:5:"width";i:768;s:6:"height";i:470;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:34:"Rectangle-2-Copy-2@2x-1024x627.png";s:5:"width";i:1024;s:6:"height";i:627;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(14, 9, '_edit_last', '1'),
(15, 9, '_edit_lock', '1509387002:1'),
(16, 11, '_edit_last', '1'),
(17, 11, '_edit_lock', '1516368491:1'),
(18, 12, '_wp_attached_file', '2017/10/eficacia.png'),
(19, 12, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:20:"2017/10/eficacia.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(20, 11, '_thumbnail_id', '154'),
(21, 11, '_yoast_wpseo_content_score', '60'),
(22, 14, '_wp_attached_file', '2017/10/comunicacao.png'),
(23, 14, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:23:"2017/10/comunicacao.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(24, 13, '_edit_last', '1'),
(25, 13, '_thumbnail_id', '155'),
(26, 13, '_yoast_wpseo_content_score', '30'),
(27, 13, '_edit_lock', '1516368526:1'),
(28, 16, '_wp_attached_file', '2017/10/conhecimento.png'),
(29, 16, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:24:"2017/10/conhecimento.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(30, 15, '_edit_last', '1'),
(31, 15, '_thumbnail_id', '156'),
(32, 15, '_yoast_wpseo_content_score', '60'),
(33, 15, '_edit_lock', '1516368553:1'),
(34, 17, '_edit_last', '1'),
(35, 17, '_edit_lock', '1516368580:1'),
(36, 18, '_wp_attached_file', '2017/10/comprometimento.png'),
(37, 18, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:27:"2017/10/comprometimento.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(38, 17, '_thumbnail_id', '157'),
(39, 17, '_yoast_wpseo_content_score', '30'),
(40, 19, '_edit_last', '1'),
(41, 19, '_edit_lock', '1516368608:1'),
(42, 20, '_wp_attached_file', '2017/10/aprendizagem.png'),
(43, 20, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:24:"2017/10/aprendizagem.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(44, 19, '_thumbnail_id', '158'),
(45, 19, '_yoast_wpseo_content_score', '30'),
(46, 21, '_edit_last', '1'),
(47, 21, '_edit_lock', '1516368637:1'),
(58, 24, '_thumbnail_id', '161'),
(50, 21, '_thumbnail_id', '159'),
(51, 21, '_yoast_wpseo_content_score', '60'),
(52, 23, '_edit_last', '1'),
(53, 23, '_edit_lock', '1516368661:1'),
(54, 23, '_thumbnail_id', '160'),
(55, 23, '_yoast_wpseo_content_score', '60'),
(56, 24, '_edit_last', '1'),
(57, 24, '_edit_lock', '1516368686:1'),
(59, 24, '_yoast_wpseo_content_score', '30'),
(60, 25, '_edit_last', '1'),
(61, 25, '_edit_lock', '1516368709:1'),
(62, 25, '_thumbnail_id', '162'),
(63, 25, '_yoast_wpseo_content_score', '30'),
(64, 26, '_edit_last', '1'),
(65, 26, '_edit_lock', '1516368738:1'),
(66, 26, '_thumbnail_id', '163'),
(67, 26, '_yoast_wpseo_content_score', '60'),
(68, 27, '_edit_last', '1'),
(69, 27, '_edit_lock', '1516368761:1'),
(70, 27, '_thumbnail_id', '164'),
(71, 27, '_yoast_wpseo_content_score', '30'),
(72, 28, '_edit_last', '1'),
(73, 28, '_edit_lock', '1516368789:1'),
(74, 28, '_thumbnail_id', '165'),
(75, 28, '_yoast_wpseo_content_score', '30'),
(76, 29, '_edit_last', '1'),
(77, 29, '_thumbnail_id', '166'),
(78, 29, '_yoast_wpseo_content_score', '60'),
(79, 29, '_edit_lock', '1516368813:1'),
(80, 30, '_edit_last', '1'),
(81, 30, '_thumbnail_id', '167'),
(82, 30, '_yoast_wpseo_content_score', '60'),
(83, 30, '_edit_lock', '1516368839:1'),
(84, 31, '_edit_last', '1'),
(85, 31, '_thumbnail_id', '169'),
(86, 31, '_yoast_wpseo_content_score', '60'),
(87, 31, '_edit_lock', '1516370783:1'),
(88, 32, '_edit_last', '1'),
(89, 32, '_edit_lock', '1516368887:1'),
(90, 32, '_thumbnail_id', '168'),
(91, 32, '_yoast_wpseo_content_score', '30'),
(92, 33, '_edit_last', '1'),
(93, 33, '_edit_lock', '1509387393:1'),
(94, 33, '_thumbnail_id', '18'),
(95, 33, '_yoast_wpseo_content_score', '30'),
(96, 34, '_edit_last', '1'),
(97, 34, '_edit_lock', '1509387463:1'),
(98, 35, '_wp_attached_file', '2017/10/Araupel@3x.png'),
(99, 35, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:387;s:6:"height";i:192;s:4:"file";s:22:"2017/10/Araupel@3x.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"Araupel@3x-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:22:"Araupel@3x-300x149.png";s:5:"width";i:300;s:6:"height";i:149;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(100, 36, '_wp_attached_file', '2017/10/clientes-o-i-azul@3x.png'),
(101, 36, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:243;s:6:"height";i:174;s:4:"file";s:32:"2017/10/clientes-o-i-azul@3x.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:32:"clientes-o-i-azul@3x-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(102, 37, '_wp_attached_file', '2017/10/Klabin@3x.png'),
(103, 37, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:279;s:6:"height";i:198;s:4:"file";s:21:"2017/10/Klabin@3x.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"Klabin@3x-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(104, 38, '_wp_attached_file', '2017/10/nestle@3x.png'),
(105, 38, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:234;s:6:"height";i:243;s:4:"file";s:21:"2017/10/nestle@3x.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"nestle@3x-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(106, 34, '_thumbnail_id', '38'),
(107, 34, '_yoast_wpseo_content_score', '30'),
(108, 39, '_edit_last', '1'),
(109, 39, '_thumbnail_id', '37'),
(110, 39, '_yoast_wpseo_content_score', '30'),
(111, 39, '_edit_lock', '1509387514:1'),
(112, 40, '_edit_last', '1'),
(113, 40, '_thumbnail_id', '36'),
(114, 40, '_yoast_wpseo_content_score', '30'),
(115, 40, '_edit_lock', '1509387535:1'),
(116, 41, '_edit_last', '1'),
(117, 41, '_thumbnail_id', '35'),
(118, 41, '_yoast_wpseo_content_score', '30'),
(119, 41, '_edit_lock', '1509387559:1'),
(120, 43, '_wp_attached_file', '2017/10/face.jpg'),
(121, 43, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:2061;s:6:"height";i:2098;s:4:"file";s:16:"2017/10/face.jpg";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"face-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"face-295x300.jpg";s:5:"width";i:295;s:6:"height";i:300;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:16:"face-768x782.jpg";s:5:"width";i:768;s:6:"height";i:782;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:18:"face-1006x1024.jpg";s:5:"width";i:1006;s:6:"height";i:1024;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:155:"The archetypal male face of beauty according to a new scientific study to mark the launch of the Samsung Galaxy S6.  Embargoed to 00.01hrs 30th March 2015.";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(130, 45, '_wp_attached_file', '2017/10/argentina@2x.png'),
(131, 45, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:64;s:6:"height";i:42;s:4:"file";s:24:"2017/10/argentina@2x.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(132, 46, '_wp_attached_file', '2017/10/bolivia@3x.png'),
(133, 46, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:96;s:6:"height";i:63;s:4:"file";s:22:"2017/10/bolivia@3x.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(134, 47, '_wp_attached_file', '2017/10/brazil@3x.png'),
(135, 47, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:96;s:6:"height";i:63;s:4:"file";s:21:"2017/10/brazil@3x.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(136, 48, '_wp_attached_file', '2017/10/colombia@3x.png'),
(137, 48, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:96;s:6:"height";i:63;s:4:"file";s:23:"2017/10/colombia@3x.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(138, 49, '_wp_attached_file', '2017/10/equador@3x.png'),
(139, 49, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:96;s:6:"height";i:63;s:4:"file";s:22:"2017/10/equador@3x.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(140, 50, '_wp_attached_file', '2017/10/mexico@3x.png'),
(141, 50, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:96;s:6:"height";i:63;s:4:"file";s:21:"2017/10/mexico@3x.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(142, 51, '_wp_attached_file', '2017/10/peru@3x.png'),
(143, 51, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:96;s:6:"height";i:63;s:4:"file";s:19:"2017/10/peru@3x.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(518, 147, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:243;s:6:"height";i:174;s:4:"file";s:16:"2018/01/sesc.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"sesc-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(517, 147, '_wp_attached_file', '2018/01/sesc.png'),
(519, 148, '_wp_attached_file', '2018/01/yara.png'),
(524, 149, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:300;s:4:"file";s:18:"2018/01/yara-1.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"yara-1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(176, 59, '_wp_attached_file', '2017/10/blogSingle.png'),
(177, 59, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1343;s:6:"height";i:467;s:4:"file";s:22:"2017/10/blogSingle.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"blogSingle-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:22:"blogSingle-300x104.png";s:5:"width";i:300;s:6:"height";i:104;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:22:"blogSingle-768x267.png";s:5:"width";i:768;s:6:"height";i:267;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:23:"blogSingle-1024x356.png";s:5:"width";i:1024;s:6:"height";i:356;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(180, 61, '_wp_attached_file', '2017/10/Rectangle-2-Copy-2@2x-1.png'),
(181, 61, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1600;s:6:"height";i:980;s:4:"file";s:35:"2017/10/Rectangle-2-Copy-2@2x-1.png";s:5:"sizes";a:4:{s:9:"thumbnail";a:4:{s:4:"file";s:35:"Rectangle-2-Copy-2@2x-1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:35:"Rectangle-2-Copy-2@2x-1-300x184.png";s:5:"width";i:300;s:6:"height";i:184;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:35:"Rectangle-2-Copy-2@2x-1-768x470.png";s:5:"width";i:768;s:6:"height";i:470;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:36:"Rectangle-2-Copy-2@2x-1-1024x627.png";s:5:"width";i:1024;s:6:"height";i:627;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(190, 64, '_wp_attached_file', '2017/10/bannerBlog.jpg'),
(191, 64, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:669;s:6:"height";i:431;s:4:"file";s:22:"2017/10/bannerBlog.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"bannerBlog-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"bannerBlog-300x193.jpg";s:5:"width";i:300;s:6:"height";i:193;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:15:"Hudson Carolino";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(697, 219, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:126;s:6:"height";i:340;s:4:"file";s:28:"2018/01/unikapatternbody.png";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:28:"unikapatternbody-126x150.png";s:5:"width";i:126;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:28:"unikapatternbody-111x300.png";s:5:"width";i:111;s:6:"height";i:300;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(696, 219, '_wp_attached_file', '2018/01/unikapatternbody.png'),
(528, 151, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:300;s:6:"height";i:300;s:4:"file";s:18:"2018/01/sesc-1.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"sesc-1-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(527, 151, '_wp_attached_file', '2018/01/sesc-1.png'),
(526, 150, '_edit_lock', '1516195106:1'),
(525, 150, '_edit_last', '1'),
(751, 240, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:42;s:6:"height";i:113;s:4:"file";s:24:"2018/02/unikapattern.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(686, 216, '_yoast_wpseo_content_score', '30'),
(750, 240, '_wp_attached_file', '2018/02/unikapattern.png'),
(684, 216, '_edit_lock', '1516901883:1'),
(683, 216, '_edit_last', '1'),
(235, 71, '_edit_last', '1'),
(236, 71, '_yoast_wpseo_content_score', '60'),
(237, 71, '_edit_lock', '1509469338:1'),
(238, 73, '_edit_last', '1'),
(239, 73, '_yoast_wpseo_content_score', '30'),
(240, 73, '_edit_lock', '1509468497:1'),
(241, 75, '_edit_last', '1'),
(242, 75, '_edit_lock', '1509389104:1'),
(243, 77, '_edit_last', '1'),
(244, 77, '_yoast_wpseo_content_score', '30'),
(245, 77, '_edit_lock', '1516044320:1'),
(246, 79, '_menu_item_type', 'post_type'),
(247, 79, '_menu_item_menu_item_parent', '0'),
(248, 79, '_menu_item_object_id', '77'),
(249, 79, '_menu_item_object', 'page'),
(250, 79, '_menu_item_target', ''),
(251, 79, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(252, 79, '_menu_item_xfn', ''),
(253, 79, '_menu_item_url', ''),
(282, 83, '_edit_last', '1'),
(255, 80, '_menu_item_type', 'post_type'),
(256, 80, '_menu_item_menu_item_parent', '0'),
(257, 80, '_menu_item_object_id', '75'),
(258, 80, '_menu_item_object', 'page'),
(259, 80, '_menu_item_target', ''),
(260, 80, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(261, 80, '_menu_item_xfn', ''),
(262, 80, '_menu_item_url', ''),
(283, 83, '_edit_lock', '1509450464:1'),
(264, 81, '_menu_item_type', 'post_type'),
(265, 81, '_menu_item_menu_item_parent', '0'),
(266, 81, '_menu_item_object_id', '73'),
(267, 81, '_menu_item_object', 'page'),
(268, 81, '_menu_item_target', ''),
(269, 81, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(270, 81, '_menu_item_xfn', ''),
(271, 81, '_menu_item_url', ''),
(284, 83, '_wp_page_template', 'paginas/inicial.php'),
(273, 82, '_menu_item_type', 'post_type'),
(274, 82, '_menu_item_menu_item_parent', '0'),
(275, 82, '_menu_item_object_id', '71'),
(276, 82, '_menu_item_object', 'page'),
(277, 82, '_menu_item_target', ''),
(278, 82, '_menu_item_classes', 'a:1:{i:0;s:0:"";}'),
(279, 82, '_menu_item_xfn', ''),
(280, 82, '_menu_item_url', ''),
(285, 83, '_yoast_wpseo_content_score', '30'),
(523, 149, '_wp_attached_file', '2018/01/yara-1.png'),
(522, 146, '_yoast_wpseo_content_score', '30'),
(521, 146, '_thumbnail_id', '149'),
(520, 148, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:243;s:6:"height";i:174;s:4:"file";s:16:"2018/01/yara.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"yara-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(516, 146, '_edit_lock', '1516195082:1'),
(307, 87, '_edit_last', '1'),
(308, 87, '_edit_lock', '1519158769:1'),
(309, 88, '_wp_attached_file', '2017/10/Bitmap@2x.png'),
(310, 88, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:122;s:6:"height";i:122;s:4:"file";s:21:"2017/10/Bitmap@2x.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(311, 89, '_wp_attached_file', '2017/10/iconworkshop@2x.png'),
(312, 89, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:120;s:6:"height";i:120;s:4:"file";s:27:"2017/10/iconworkshop@2x.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(313, 90, '_wp_attached_file', '2017/10/treinamento.png'),
(314, 90, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:120;s:6:"height";i:120;s:4:"file";s:23:"2017/10/treinamento.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(315, 87, '_thumbnail_id', '138'),
(316, 87, 'Unika_destaque_cor', 'linear-gradient(133.5deg, #F3F3F3 0%, #D8D8D8 100%);'),
(317, 87, 'Unika_destaque_cor_circulo', '#d4d4d4'),
(318, 87, '_yoast_wpseo_content_score', '60'),
(319, 91, '_edit_last', '1'),
(320, 91, '_edit_lock', '1519160660:1'),
(321, 91, '_thumbnail_id', '137'),
(322, 91, 'Unika_destaque_cor', 'linear-gradient(133.5deg, #f1eeea 0%, #D9D4CF 76.89%, #D7D2CC 83.25%, #dbd7d1 100%);'),
(323, 91, 'Unika_destaque_cor_circulo', '#d3cfcb'),
(324, 91, '_yoast_wpseo_content_score', '60'),
(325, 92, '_edit_last', '1'),
(326, 92, '_edit_lock', '1519158769:1'),
(327, 92, '_thumbnail_id', '139'),
(328, 92, 'Unika_destaque_cor', 'linear-gradient(133.5deg, #eae1d6 0%, #d7cec3 100%);'),
(329, 92, 'Unika_destaque_cor_circulo', '#cdc4ba'),
(330, 92, '_yoast_wpseo_content_score', '60'),
(331, 93, '_edit_last', '1'),
(332, 93, '_thumbnail_id', '136'),
(333, 93, 'Unika_destaque_cor', 'linear-gradient(135deg, #dcc3bc 0%, #bca6a0 100%)'),
(334, 93, 'Unika_destaque_cor_circulo', '#bba5a0'),
(335, 93, '_yoast_wpseo_content_score', '60'),
(336, 93, '_edit_lock', '1519158769:1'),
(337, 94, '_edit_last', '1'),
(338, 94, '_edit_lock', '1519158770:1'),
(339, 94, '_thumbnail_id', '140'),
(340, 94, 'Unika_destaque_cor', 'linear-gradient(135deg, #C1947E 0%, #877065 100%)'),
(341, 94, 'Unika_destaque_cor_circulo', '#9e7b6a'),
(342, 94, '_yoast_wpseo_content_score', '60'),
(343, 95, '_edit_last', '1'),
(344, 95, '_edit_lock', '1519158768:1'),
(345, 95, '_thumbnail_id', '144'),
(346, 95, 'Unika_destaque_cor', 'linear-gradient(135deg, #74688A 0%, #40374E 100%);'),
(347, 95, 'Unika_destaque_cor_circulo', '#594f6b'),
(348, 95, '_yoast_wpseo_content_score', '60'),
(349, 96, '_edit_last', '1'),
(350, 96, '_thumbnail_id', '49'),
(351, 96, '_yoast_wpseo_content_score', '30'),
(352, 96, '_edit_lock', '1509455475:1'),
(353, 97, '_edit_last', '1'),
(354, 97, '_edit_lock', '1509455503:1'),
(355, 97, '_thumbnail_id', '46'),
(356, 97, '_yoast_wpseo_content_score', '30'),
(357, 98, '_edit_last', '1'),
(358, 98, '_thumbnail_id', '48'),
(359, 98, '_yoast_wpseo_content_score', '30'),
(360, 98, '_edit_lock', '1509455543:1'),
(361, 99, '_edit_last', '1'),
(362, 99, '_thumbnail_id', '45'),
(363, 99, '_yoast_wpseo_content_score', '30'),
(364, 99, '_edit_lock', '1509455928:1'),
(365, 100, '_edit_last', '1'),
(366, 100, '_edit_lock', '1509455636:1'),
(367, 100, '_thumbnail_id', '50'),
(368, 100, '_yoast_wpseo_content_score', '30'),
(369, 101, '_edit_last', '1'),
(370, 101, '_edit_lock', '1509455677:1'),
(371, 101, '_thumbnail_id', '51'),
(372, 101, '_yoast_wpseo_content_score', '30'),
(373, 102, '_edit_last', '1'),
(374, 102, '_thumbnail_id', '47'),
(375, 102, '_yoast_wpseo_content_score', '30'),
(376, 102, '_edit_lock', '1509455842:1'),
(378, 4, '_config_errors', 'a:1:{s:23:"mail.additional_headers";a:1:{i:0;a:2:{s:4:"code";i:102;s:4:"args";a:3:{s:7:"message";s:64:"A sintaxe de caixa de e-mail usada no campo %name% é inválida.";s:6:"params";a:1:{s:4:"name";s:8:"Reply-To";}s:4:"link";s:68:"https://contactform7.com/configuration-errors/invalid-mailbox-syntax";}}}}'),
(379, 73, '_wp_page_template', 'paginas/solucoes.php'),
(380, 92, 'Unika_destaque_cor_texto', '#3e414f'),
(381, 93, 'Unika_destaque_cor_texto', '#ffffff'),
(382, 94, 'Unika_destaque_cor_texto', '#ffffff'),
(383, 71, '_wp_page_template', 'paginas/empresa.php'),
(384, 71, '_thumbnail_id', '61'),
(385, 105, '_wp_attached_file', '2017/10/logoCortada.png'),
(386, 105, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1791;s:6:"height";i:1003;s:4:"file";s:23:"2017/10/logoCortada.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"logoCortada-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"logoCortada-300x168.png";s:5:"width";i:300;s:6:"height";i:168;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:23:"logoCortada-768x430.png";s:5:"width";i:768;s:6:"height";i:430;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"logoCortada-1024x573.png";s:5:"width";i:1024;s:6:"height";i:573;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:23:"logoCortada-600x336.png";s:5:"width";i:600;s:6:"height";i:336;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(514, 91, 'Unika_destaque_cor_texto', '#3e414f'),
(389, 107, '_edit_last', '1'),
(390, 107, '_edit_lock', '1516900046:1'),
(391, 108, '_wp_attached_file', '2017/10/Katshuska@3x.png'),
(392, 108, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1800;s:6:"height";i:1224;s:4:"file";s:24:"2017/10/Katshuska@3x.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"Katshuska@3x-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:24:"Katshuska@3x-300x204.png";s:5:"width";i:300;s:6:"height";i:204;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:24:"Katshuska@3x-768x522.png";s:5:"width";i:768;s:6:"height";i:522;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:25:"Katshuska@3x-1024x696.png";s:5:"width";i:1024;s:6:"height";i:696;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:24:"Katshuska@3x-600x408.png";s:5:"width";i:600;s:6:"height";i:408;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(393, 109, '_wp_attached_file', '2017/10/gabriela@2x.png'),
(394, 109, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:816;s:4:"file";s:23:"2017/10/gabriela@2x.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:23:"gabriela@2x-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"gabriela@2x-300x204.png";s:5:"width";i:300;s:6:"height";i:204;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:23:"gabriela@2x-768x522.png";s:5:"width";i:768;s:6:"height";i:522;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:24:"gabriela@2x-1024x696.png";s:5:"width";i:1024;s:6:"height";i:696;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:23:"gabriela@2x-600x408.png";s:5:"width";i:600;s:6:"height";i:408;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(395, 110, '_wp_attached_file', '2017/10/karla@2x.png'),
(396, 110, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:816;s:4:"file";s:20:"2017/10/karla@2x.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"karla@2x-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"karla@2x-300x204.png";s:5:"width";i:300;s:6:"height";i:204;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:20:"karla@2x-768x522.png";s:5:"width";i:768;s:6:"height";i:522;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:21:"karla@2x-1024x696.png";s:5:"width";i:1024;s:6:"height";i:696;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:20:"karla@2x-600x408.png";s:5:"width";i:600;s:6:"height";i:408;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(397, 111, '_wp_attached_file', '2017/10/Mariane@2x.png'),
(398, 111, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:816;s:4:"file";s:22:"2017/10/Mariane@2x.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"Mariane@2x-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:22:"Mariane@2x-300x204.png";s:5:"width";i:300;s:6:"height";i:204;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:22:"Mariane@2x-768x522.png";s:5:"width";i:768;s:6:"height";i:522;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:23:"Mariane@2x-1024x696.png";s:5:"width";i:1024;s:6:"height";i:696;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:22:"Mariane@2x-600x408.png";s:5:"width";i:600;s:6:"height";i:408;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(399, 107, '_thumbnail_id', '110'),
(400, 107, '_yoast_wpseo_content_score', '60'),
(404, 112, '_edit_last', '1'),
(405, 112, '_edit_lock', '1516900582:1'),
(406, 112, '_thumbnail_id', '109'),
(534, 154, '_wp_attached_file', '2017/10/11.png'),
(410, 112, '_yoast_wpseo_content_score', '60'),
(411, 113, '_edit_last', '1'),
(412, 113, '_edit_lock', '1516363541:1'),
(413, 113, '_thumbnail_id', '108'),
(535, 154, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:14:"2017/10/11.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(417, 113, '_yoast_wpseo_content_score', '30'),
(418, 114, '_edit_last', '1'),
(419, 114, '_edit_lock', '1516363540:1'),
(420, 114, '_thumbnail_id', '111'),
(424, 114, '_yoast_wpseo_content_score', '60'),
(451, 77, '_wp_page_template', 'paginas/contato.php'),
(452, 123, '_form', '[text* NomeCompleto id:nomeCompleto class:nomeCompleto placeholder "Nome Completo"][email* email id:email class:email placeholder "E-mail"][tel* telefone id:telefone class:telefone placeholder "Telefone"][text* Nomedaempresa id:nomedaempresa class:nome class:da class:empresa placeholder "Nome da empresa"][textarea mensagem id:mensagem class:mensagem placeholder "Mensagem..."]\n<div class="submit">\n[submit id:enviar class:enviar "Enviar"]\n</div>'),
(453, 123, '_mail', 'a:9:{s:6:"active";b:1;s:7:"subject";s:22:"Unika "[your-subject]"";s:6:"sender";s:34:"[your-name] <contato@unika.com.br>";s:9:"recipient";s:20:"contato@unika.com.br";s:4:"body";s:190:"De: [your-name] <[your-email]>\nAssunto: [your-subject]\n\nCorpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Unika (http://localhost/projetos/unika)";s:18:"additional_headers";s:22:"Reply-To: [your-email]";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(454, 123, '_mail_2', 'a:9:{s:6:"active";b:0;s:7:"subject";s:22:"Unika "[your-subject]"";s:6:"sender";s:28:"Unika <contato@unika.com.br>";s:9:"recipient";s:12:"[your-email]";s:4:"body";s:134:"Corpo da mensagem:\n[your-message]\n\n-- \nEste e-mail foi enviado de um formulário de contato em Unika (http://localhost/projetos/unika)";s:18:"additional_headers";s:30:"Reply-To: contato@unika.com.br";s:11:"attachments";s:0:"";s:8:"use_html";b:0;s:13:"exclude_blank";b:0;}'),
(455, 123, '_messages', 'a:23:{s:12:"mail_sent_ok";s:27:"Agradecemos a sua mensagem.";s:12:"mail_sent_ng";s:74:"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.";s:16:"validation_error";s:63:"Um ou mais campos possuem um erro. Verifique e tente novamente.";s:4:"spam";s:74:"Ocorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.";s:12:"accept_terms";s:72:"Você deve aceitar os termos e condições antes de enviar sua mensagem.";s:16:"invalid_required";s:24:"O campo é obrigatório.";s:16:"invalid_too_long";s:23:"O campo é muito longo.";s:17:"invalid_too_short";s:23:"O campo é muito curto.";s:12:"invalid_date";s:34:"O formato de data está incorreto.";s:14:"date_too_early";s:44:"A data é anterior à mais antiga permitida.";s:13:"date_too_late";s:44:"A data é posterior à maior data permitida.";s:13:"upload_failed";s:49:"Ocorreu um erro desconhecido ao enviar o arquivo.";s:24:"upload_file_type_invalid";s:59:"Você não tem permissão para enviar esse tipo de arquivo.";s:21:"upload_file_too_large";s:26:"O arquivo é muito grande.";s:23:"upload_failed_php_error";s:36:"Ocorreu um erro ao enviar o arquivo.";s:14:"invalid_number";s:34:"O formato de número é inválido.";s:16:"number_too_small";s:46:"O número é menor do que o mínimo permitido.";s:16:"number_too_large";s:46:"O número é maior do que o máximo permitido.";s:23:"quiz_answer_not_correct";s:39:"A resposta para o quiz está incorreta.";s:17:"captcha_not_match";s:35:"O código digitado está incorreto.";s:13:"invalid_email";s:45:"O endereço de e-mail informado é inválido.";s:11:"invalid_url";s:19:"A URL é inválida.";s:11:"invalid_tel";s:35:"O número de telefone é inválido.";}'),
(456, 123, '_additional_settings', ''),
(457, 123, '_locale', 'pt_BR'),
(489, 123, '_config_errors', 'a:2:{s:11:"mail.sender";a:1:{i:0;a:2:{s:4:"code";i:103;s:4:"args";a:3:{s:7:"message";s:0:"";s:6:"params";a:0:{}s:4:"link";s:70:"https://contactform7.com/configuration-errors/email-not-in-site-domain";}}}s:23:"mail.additional_headers";a:1:{i:0;a:2:{s:4:"code";i:102;s:4:"args";a:3:{s:7:"message";s:64:"A sintaxe de caixa de e-mail usada no campo %name% é inválida.";s:6:"params";a:1:{s:4:"name";s:8:"Reply-To";}s:4:"link";s:68:"https://contactform7.com/configuration-errors/invalid-mailbox-syntax";}}}}'),
(460, 124, '_edit_last', '1'),
(464, 124, '_wp_page_template', 'paginas/DiagnosticodeCulturadeSeguranca.php'),
(462, 124, '_yoast_wpseo_content_score', '30'),
(463, 124, '_edit_lock', '1509546284:1'),
(465, 126, '_edit_last', '1'),
(466, 126, '_edit_lock', '1509561577:1'),
(467, 127, '_edit_last', '1'),
(468, 127, '_edit_lock', '1509545810:1'),
(469, 127, '_wp_page_template', 'paginas/palestra.php'),
(470, 127, '_yoast_wpseo_content_score', '30'),
(471, 129, '_edit_last', '1'),
(472, 129, '_edit_lock', '1509546518:1'),
(475, 129, '_wp_page_template', 'paginas/Workshops.php'),
(474, 129, '_yoast_wpseo_content_score', '30'),
(476, 131, '_edit_last', '1'),
(477, 131, '_edit_lock', '1509554202:1'),
(478, 131, '_wp_page_template', 'paginas/treinamentos.php'),
(479, 131, '_yoast_wpseo_content_score', '30'),
(480, 133, '_edit_last', '1'),
(481, 133, '_wp_page_template', 'paginas/FerramentadeSegurancaCorporal.php'),
(482, 133, '_yoast_wpseo_content_score', '30'),
(483, 133, '_edit_lock', '1516370319:1'),
(484, 95, 'Unika_destaque_link', 'http://unika.handgran.com.br/diagnostico-de-cultura-de-seguranca/'),
(485, 94, 'Unika_destaque_link', 'http://unika.handgran.com.br/palestras/'),
(486, 93, 'Unika_destaque_link', 'http://unika.handgran.com.br/workshops/'),
(487, 92, 'Unika_destaque_link', 'http://unika.handgran.com.br/treinamentos/'),
(488, 91, 'Unika_destaque_link', 'http://unika.handgran.com.br/ferramenta-de-seguranca-comportamental/'),
(515, 146, '_edit_last', '1'),
(513, 87, 'Unika_destaque_cor_texto', '#3e414f'),
(493, 91, '_wp_old_slug', 'ferramenta-de-seguranca-corporal'),
(494, 136, '_wp_attached_file', '2017/10/flexas.png'),
(495, 136, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:61;s:6:"height";i:61;s:4:"file";s:18:"2017/10/flexas.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(496, 137, '_wp_attached_file', '2017/10/mala.png'),
(497, 137, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:61;s:6:"height";i:61;s:4:"file";s:16:"2017/10/mala.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(498, 138, '_wp_attached_file', '2017/10/malaChave.png'),
(499, 138, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:61;s:6:"height";i:61;s:4:"file";s:21:"2017/10/malaChave.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(500, 139, '_wp_attached_file', '2017/10/palestras.png'),
(501, 139, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:61;s:6:"height";i:61;s:4:"file";s:21:"2017/10/palestras.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(502, 140, '_wp_attached_file', '2017/10/palestras2.png'),
(503, 140, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:61;s:6:"height";i:61;s:4:"file";s:22:"2017/10/palestras2.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(504, 141, '_wp_attached_file', '2017/12/foto.jpg'),
(505, 141, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:583;s:6:"height";i:492;s:4:"file";s:16:"2017/12/foto.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:16:"foto-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:16:"foto-300x253.jpg";s:5:"width";i:300;s:6:"height";i:253;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:15:"Hudson Carolino";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1513714533";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(506, 142, '_wp_attached_file', '2017/12/foto-1.jpg'),
(507, 142, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:583;s:6:"height";i:487;s:4:"file";s:18:"2017/12/foto-1.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:18:"foto-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:18:"foto-1-300x251.jpg";s:5:"width";i:300;s:6:"height";i:251;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:15:"Hudson Carolino";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:10:"1513714533";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(508, 143, '_wp_attached_file', '2017/10/Capturar.png'),
(509, 143, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:70;s:6:"height";i:83;s:4:"file";s:20:"2017/10/Capturar.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(510, 144, '_wp_attached_file', '2017/10/Capturar-1.png'),
(511, 144, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:70;s:6:"height";i:83;s:4:"file";s:22:"2017/10/Capturar-1.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(529, 150, '_thumbnail_id', '151'),
(530, 150, '_yoast_wpseo_content_score', '30'),
(536, 155, '_wp_attached_file', '2017/10/15.png'),
(537, 155, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:14:"2017/10/15.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(538, 156, '_wp_attached_file', '2017/10/4.png');
INSERT INTO `uk_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(539, 156, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:13:"2017/10/4.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(540, 157, '_wp_attached_file', '2017/10/13.png'),
(541, 157, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:14:"2017/10/13.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(542, 158, '_wp_attached_file', '2017/10/16.png'),
(543, 158, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:14:"2017/10/16.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(544, 159, '_wp_attached_file', '2017/10/10.png'),
(545, 159, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:14:"2017/10/10.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(546, 160, '_wp_attached_file', '2017/10/7.png'),
(547, 160, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:13:"2017/10/7.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(548, 161, '_wp_attached_file', '2017/10/3.png'),
(549, 161, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:13:"2017/10/3.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(550, 162, '_wp_attached_file', '2017/10/14.png'),
(551, 162, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:14:"2017/10/14.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(552, 163, '_wp_attached_file', '2017/10/2.png'),
(553, 163, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:13:"2017/10/2.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(554, 164, '_wp_attached_file', '2017/10/12.png'),
(555, 164, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:14:"2017/10/12.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(556, 165, '_wp_attached_file', '2017/10/17.png'),
(557, 165, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:14:"2017/10/17.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(558, 166, '_wp_attached_file', '2017/10/18.png'),
(559, 166, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:14:"2017/10/18.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(560, 167, '_wp_attached_file', '2017/10/9.png'),
(561, 167, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:13:"2017/10/9.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(562, 168, '_wp_attached_file', '2017/10/8.png'),
(563, 168, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:13:"2017/10/8.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(564, 169, '_wp_attached_file', '2017/10/19.png'),
(565, 169, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:40;s:6:"height";i:40;s:4:"file";s:14:"2017/10/19.png";s:5:"sizes";a:0:{}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(566, 171, '_edit_last', '1'),
(567, 171, '_wp_page_template', 'paginas/planejamentoEstrategicodeSeguranca.php'),
(568, 171, '_yoast_wpseo_content_score', '30'),
(569, 171, '_edit_lock', '1516370872:1'),
(570, 87, 'Unika_destaque_link', 'http://unika.handgran.com.br/planejamento-estrategico-de-seguranca/'),
(758, 244, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1440;s:6:"height";i:430;s:4:"file";s:33:"2018/01/fatores-humanos-21-04.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:33:"fatores-humanos-21-04-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:32:"fatores-humanos-21-04-300x90.png";s:5:"width";i:300;s:6:"height";i:90;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:33:"fatores-humanos-21-04-768x229.png";s:5:"width";i:768;s:6:"height";i:229;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:34:"fatores-humanos-21-04-1024x306.png";s:5:"width";i:1024;s:6:"height";i:306;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:33:"fatores-humanos-21-04-600x179.png";s:5:"width";i:600;s:6:"height";i:179;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(757, 244, '_wp_attached_file', '2018/01/fatores-humanos-21-04.png'),
(590, 173, '_edit_last', '2'),
(591, 173, '_edit_lock', '1519222017:2'),
(593, 173, '_yoast_wpseo_content_score', '60'),
(594, 173, '_yoast_wpseo_primary_category', ''),
(599, 178, '_wp_attached_file', '2018/01/Untitled-1.jpg'),
(600, 178, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1847;s:6:"height";i:748;s:4:"file";s:22:"2018/01/Untitled-1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"Untitled-1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"Untitled-1-300x121.jpg";s:5:"width";i:300;s:6:"height";i:121;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"Untitled-1-768x311.jpg";s:5:"width";i:768;s:6:"height";i:311;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"Untitled-1-1024x415.jpg";s:5:"width";i:1024;s:6:"height";i:415;s:9:"mime-type";s:10:"image/jpeg";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:22:"Untitled-1-600x243.jpg";s:5:"width";i:600;s:6:"height";i:243;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(603, 173, '_thumbnail_id', '249'),
(615, 191, '_edit_last', '2'),
(616, 191, '_edit_lock', '1519222017:2'),
(623, 194, '_edit_last', '2'),
(621, 191, '_yoast_wpseo_content_score', '30'),
(622, 191, '_yoast_wpseo_primary_category', ''),
(624, 194, '_edit_lock', '1519221314:2'),
(625, 195, '_wp_attached_file', '2018/01/Untitled-2.jpg'),
(626, 195, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1417;s:6:"height";i:773;s:4:"file";s:22:"2018/01/Untitled-2.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"Untitled-2-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"Untitled-2-300x164.jpg";s:5:"width";i:300;s:6:"height";i:164;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:22:"Untitled-2-768x419.jpg";s:5:"width";i:768;s:6:"height";i:419;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:23:"Untitled-2-1024x559.jpg";s:5:"width";i:1024;s:6:"height";i:559;s:9:"mime-type";s:10:"image/jpeg";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:22:"Untitled-2-600x327.jpg";s:5:"width";i:600;s:6:"height";i:327;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(627, 196, '_wp_attached_file', '2018/01/Vector-Concept-of-Growth-482548689_3157x3157.jpeg'),
(628, 196, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:3155;s:6:"height";i:3155;s:4:"file";s:57:"2018/01/Vector-Concept-of-Growth-482548689_3157x3157.jpeg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:24:"Getty Images/iStockphoto";s:6:"camera";s:0:"";s:7:"caption";s:80:"Vector Concept of Growth with arrow and crane hook, isolated on white background";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:9:"dashadima";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:24:"Vector Concept of Growth";s:11:"orientation";s:1:"0";s:8:"keywords";a:34:{i:0;s:11:"Achievement";i:1;s:5:"Arrow";i:2;s:8:"Business";i:3;s:8:"Concepts";i:4;s:12:"Construction";i:5;s:5:"Crane";i:6;s:9:"Decisions";i:7;s:13:"Determination";i:8;s:11:"Development";i:9;s:7:"Finance";i:10;s:5:"Graph";i:11;s:5:"Green";i:12;s:6:"Growth";i:13;s:6:"Hanger";i:14;s:6:"Hawser";i:15;s:4:"Hook";i:16;s:15:"Human Fertility";i:17;s:5:"Ideas";i:18;s:10:"Innovation";i:19;s:4:"Luck";i:20;s:12:"Making Money";i:21;s:9:"Moving Up";i:22;s:10:"Picking Up";i:23;s:8:"Planning";i:24;s:8:"Progress";i:25;s:9:"Promotion";i:26;s:8:"Recovery";i:27;s:4:"Rope";i:28;s:8:"Solution";i:29;s:11:"Steel Cable";i:30;s:7:"Success";i:31;s:7:"Support";i:32;s:15:"The Way Forward";i:33;s:6:"Vector";}}}'),
(634, 199, '_edit_last', '1'),
(631, 194, '_yoast_wpseo_content_score', '60'),
(632, 194, '_yoast_wpseo_primary_category', ''),
(635, 199, '_edit_lock', '1518719044:1'),
(636, 200, '_wp_attached_file', '2018/01/concept-of-security-537842358_3927x2540.jpeg'),
(637, 200, '_wp_attachment_metadata', 'a:4:{s:5:"width";i:3923;s:6:"height";i:2538;s:4:"file";s:52:"2018/01/concept-of-security-537842358_3927x2540.jpeg";s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:24:"Getty Images/iStockphoto";s:6:"camera";s:0:"";s:7:"caption";s:107:"wall with the drawing of dark clouds, rain and one umbrella, concept of protection and security (3d render)";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:6:"lucadp";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:19:"concept of security";s:11:"orientation";s:1:"0";s:8:"keywords";a:19:{i:0;s:9:"No People";i:1;s:12:"Illustration";i:2;s:7:"Weather";i:3;s:10:"Protection";i:4;s:8:"Solution";i:5;s:8:"Strategy";i:6;s:8:"Problems";i:7;s:6:"Safety";i:8;s:8:"Security";i:9;s:8:"Concepts";i:10;s:3:"Red";i:11;s:4:"Gray";i:12;s:5:"Ideas";i:13;s:4:"Drop";i:14;s:11:"Cloud - Sky";i:15;s:4:"Rain";i:16;s:23:"Wall - Building Feature";i:17;s:8:"Umbrella";i:18;s:6:"render";}}}'),
(638, 199, '_thumbnail_id', '224'),
(642, 202, '_edit_last', '2'),
(640, 199, '_yoast_wpseo_content_score', '30'),
(641, 199, '_yoast_wpseo_primary_category', ''),
(648, 205, '_wp_attached_file', '2018/01/Untitled-3.jpg'),
(645, 202, '_yoast_wpseo_content_score', '30'),
(646, 202, '_yoast_wpseo_primary_category', ''),
(647, 202, '_edit_lock', '1519221044:2'),
(649, 205, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:567;s:6:"height";i:350;s:4:"file";s:22:"2018/01/Untitled-3.jpg";s:5:"sizes";a:2:{s:9:"thumbnail";a:4:{s:4:"file";s:22:"Untitled-3-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:22:"Untitled-3-300x185.jpg";s:5:"width";i:300;s:6:"height";i:185;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"1";s:8:"keywords";a:0:{}}}'),
(650, 204, '_edit_last', '2'),
(651, 204, '_edit_lock', '1519220768:2'),
(656, 207, '_wp_attached_file', '2018/01/Fill-1-Fill-3-Mask_preview.png'),
(654, 204, '_yoast_wpseo_content_score', '30'),
(655, 204, '_yoast_wpseo_primary_category', ''),
(657, 207, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1400;s:6:"height";i:430;s:4:"file";s:38:"2018/01/Fill-1-Fill-3-Mask_preview.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:38:"Fill-1-Fill-3-Mask_preview-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:37:"Fill-1-Fill-3-Mask_preview-300x92.png";s:5:"width";i:300;s:6:"height";i:92;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:38:"Fill-1-Fill-3-Mask_preview-768x236.png";s:5:"width";i:768;s:6:"height";i:236;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:39:"Fill-1-Fill-3-Mask_preview-1024x315.png";s:5:"width";i:1024;s:6:"height";i:315;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:38:"Fill-1-Fill-3-Mask_preview-600x184.png";s:5:"width";i:600;s:6:"height";i:184;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(660, 211, '_edit_last', '1'),
(661, 211, '_edit_lock', '1516996780:1'),
(662, 211, 'Unika_depoimento_cargo', 'Gerente de Saúde, Segurança e Meio Ambiente'),
(663, 211, 'Unika_depoimento_empresa', 'CBA - Companhia Brasileira de Alumínio'),
(664, 211, '_yoast_wpseo_content_score', '30'),
(665, 212, '_edit_last', '1'),
(666, 212, '_edit_lock', '1516996902:1'),
(667, 212, 'Unika_depoimento_cargo', 'Gerente da planta de Manufatura da Owens Illinois'),
(668, 212, 'Unika_depoimento_empresa', 'Envigado – Colômbia'),
(669, 212, '_yoast_wpseo_content_score', '30'),
(670, 213, '_edit_last', '1'),
(671, 213, '_edit_lock', '1516996864:1'),
(672, 213, 'Unika_depoimento_cargo', 'Perito Comportamental Especialista de Projetos de Escavação de Shaft e Raise em Mina Subterrânea'),
(673, 213, 'Unika_depoimento_empresa', 'Multinacional no seguimento de mineração'),
(674, 213, '_yoast_wpseo_content_score', '30'),
(675, 214, '_edit_last', '1'),
(676, 214, '_edit_lock', '1516996808:1'),
(677, 214, 'Unika_depoimento_cargo', 'Diretora EHS América Latina'),
(678, 214, 'Unika_depoimento_empresa', 'Líder Mundial em Vidro'),
(679, 214, '_yoast_wpseo_content_score', '30'),
(700, 216, '_wp_page_template', 'paginas/problemasQueresolvemos.php'),
(765, 247, '_wp_attached_file', '2018/01/foto-blog-evolução-da-cultura-nova-e1519221384964.png'),
(766, 247, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1440;s:6:"height";i:430;s:4:"file";s:63:"2018/01/foto-blog-evolução-da-cultura-nova-e1519221384964.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:48:"foto-blog-evolução-da-cultura-nova-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:47:"foto-blog-evolução-da-cultura-nova-300x90.png";s:5:"width";i:300;s:6:"height";i:90;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:48:"foto-blog-evolução-da-cultura-nova-768x229.png";s:5:"width";i:768;s:6:"height";i:229;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:49:"foto-blog-evolução-da-cultura-nova-1024x306.png";s:5:"width";i:1024;s:6:"height";i:306;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:48:"foto-blog-evolução-da-cultura-nova-600x179.png";s:5:"width";i:600;s:6:"height";i:179;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(707, 224, '_wp_attached_file', '2018/01/1.jpg'),
(708, 224, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:3923;s:6:"height";i:1191;s:4:"file";s:13:"2018/01/1.jpg";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:13:"1-150x150.jpg";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:10:"image/jpeg";}s:6:"medium";a:4:{s:4:"file";s:12:"1-300x91.jpg";s:5:"width";i:300;s:6:"height";i:91;s:9:"mime-type";s:10:"image/jpeg";}s:12:"medium_large";a:4:{s:4:"file";s:13:"1-768x233.jpg";s:5:"width";i:768;s:6:"height";i:233;s:9:"mime-type";s:10:"image/jpeg";}s:5:"large";a:4:{s:4:"file";s:14:"1-1024x311.jpg";s:5:"width";i:1024;s:6:"height";i:311;s:9:"mime-type";s:10:"image/jpeg";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:13:"1-600x182.jpg";s:5:"width";i:600;s:6:"height";i:182;s:9:"mime-type";s:10:"image/jpeg";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:24:"Getty Images/iStockphoto";s:6:"camera";s:0:"";s:7:"caption";s:107:"wall with the drawing of dark clouds, rain and one umbrella, concept of protection and security (3d render)";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:6:"lucadp";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:19:"concept of security";s:11:"orientation";s:1:"1";s:8:"keywords";a:19:{i:0;s:9:"No People";i:1;s:12:"Illustration";i:2;s:7:"Weather";i:3;s:10:"Protection";i:4;s:8:"Solution";i:5;s:8:"Strategy";i:6;s:8:"Problems";i:7;s:6:"Safety";i:8;s:8:"Security";i:9;s:8:"Concepts";i:10;s:3:"Red";i:11;s:4:"Gray";i:12;s:5:"Ideas";i:13;s:4:"Drop";i:14;s:11:"Cloud - Sky";i:15;s:4:"Rain";i:16;s:23:"Wall - Building Feature";i:17;s:8:"Umbrella";i:18;s:6:"render";}}}'),
(773, 251, '_wp_attached_file', '2018/01/dialogo-nova.png'),
(774, 251, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1440;s:6:"height";i:430;s:4:"file";s:24:"2018/01/dialogo-nova.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:24:"dialogo-nova-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:23:"dialogo-nova-300x90.png";s:5:"width";i:300;s:6:"height";i:90;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:24:"dialogo-nova-768x229.png";s:5:"width";i:768;s:6:"height";i:229;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:25:"dialogo-nova-1024x306.png";s:5:"width";i:1024;s:6:"height";i:306;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:24:"dialogo-nova-600x179.png";s:5:"width";i:600;s:6:"height";i:179;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(775, 191, '_thumbnail_id', '251'),
(724, 229, '_wp_attached_file', '2018/02/karla@2x.png'),
(725, 229, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1200;s:6:"height";i:816;s:4:"file";s:20:"2018/02/karla@2x.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"karla@2x-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"karla@2x-300x204.png";s:5:"width";i:300;s:6:"height";i:204;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:20:"karla@2x-768x522.png";s:5:"width";i:768;s:6:"height";i:522;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:21:"karla@2x-1024x696.png";s:5:"width";i:1024;s:6:"height";i:696;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:20:"karla@2x-600x408.png";s:5:"width";i:600;s:6:"height";i:408;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(759, 204, '_thumbnail_id', '244'),
(763, 202, '_thumbnail_id', '245'),
(767, 247, '_wp_attachment_backup_sizes', 'a:1:{s:9:"full-orig";a:3:{s:5:"width";i:8525;s:6:"height";i:2546;s:4:"file";s:40:"foto-blog-evolução-da-cultura-nova.png";}}'),
(768, 194, '_thumbnail_id', '247'),
(770, 249, '_wp_attached_file', '2018/01/blog-nova.png'),
(771, 249, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1440;s:6:"height";i:430;s:4:"file";s:21:"2018/01/blog-nova.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:21:"blog-nova-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:20:"blog-nova-300x90.png";s:5:"width";i:300;s:6:"height";i:90;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:21:"blog-nova-768x229.png";s:5:"width";i:768;s:6:"height";i:229;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:22:"blog-nova-1024x306.png";s:5:"width";i:1024;s:6:"height";i:306;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:21:"blog-nova-600x179.png";s:5:"width";i:600;s:6:"height";i:179;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(740, 200, '_edit_lock', '1519067786:2'),
(761, 245, '_wp_attached_file', '2018/01/oac-nova.png'),
(762, 245, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:1440;s:6:"height";i:430;s:4:"file";s:20:"2018/01/oac-nova.png";s:5:"sizes";a:5:{s:9:"thumbnail";a:4:{s:4:"file";s:20:"oac-nova-150x150.png";s:5:"width";i:150;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}s:6:"medium";a:4:{s:4:"file";s:19:"oac-nova-300x90.png";s:5:"width";i:300;s:6:"height";i:90;s:9:"mime-type";s:9:"image/png";}s:12:"medium_large";a:4:{s:4:"file";s:20:"oac-nova-768x229.png";s:5:"width";i:768;s:6:"height";i:229;s:9:"mime-type";s:9:"image/png";}s:5:"large";a:4:{s:4:"file";s:21:"oac-nova-1024x306.png";s:5:"width";i:1024;s:6:"height";i:306;s:9:"mime-type";s:9:"image/png";}s:22:"wysija-newsletters-max";a:4:{s:4:"file";s:20:"oac-nova-600x179.png";s:5:"width";i:600;s:6:"height";i:179;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}'),
(752, 241, '_wp_attached_file', '2018/02/unikapatternbody-1.png'),
(753, 241, '_wp_attachment_metadata', 'a:5:{s:5:"width";i:63;s:6:"height";i:171;s:4:"file";s:30:"2018/02/unikapatternbody-1.png";s:5:"sizes";a:1:{s:9:"thumbnail";a:4:{s:4:"file";s:29:"unikapatternbody-1-63x150.png";s:5:"width";i:63;s:6:"height";i:150;s:9:"mime-type";s:9:"image/png";}}s:10:"image_meta";a:12:{s:8:"aperture";s:1:"0";s:6:"credit";s:0:"";s:6:"camera";s:0:"";s:7:"caption";s:0:"";s:17:"created_timestamp";s:1:"0";s:9:"copyright";s:0:"";s:12:"focal_length";s:1:"0";s:3:"iso";s:1:"0";s:13:"shutter_speed";s:1:"0";s:5:"title";s:0:"";s:11:"orientation";s:1:"0";s:8:"keywords";a:0:{}}}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_posts`
--

CREATE TABLE IF NOT EXISTS `uk_posts` (
  `ID` bigint(20) unsigned NOT NULL,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=254 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_posts`
--

INSERT INTO `uk_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(244, 2, '2018-02-21 10:45:08', '2018-02-21 13:45:08', '', 'fatores-humanos-21-04', '', 'inherit', 'open', 'closed', '', 'fatores-humanos-21-04', '', '', '2018-02-21 10:45:08', '2018-02-21 13:45:08', '', 204, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/fatores-humanos-21-04.png', 0, 'attachment', 'image/png', 0),
(245, 2, '2018-02-21 10:49:01', '2018-02-21 13:49:01', '', 'oac-nova', '', 'inherit', 'open', 'closed', '', 'oac-nova', '', '', '2018-02-21 10:49:01', '2018-02-21 13:49:01', '', 202, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/oac-nova.png', 0, 'attachment', 'image/png', 0),
(246, 2, '2018-02-21 10:49:18', '2018-02-21 13:49:18', 'A <strong>OAC - Observação e Abordagem de Comportamentos em Segurança é</strong> uma ferramenta educativa e amplamente utilizada no mercado com a finalidade de abordar comportamentos que foram observados, por meio de uma metodologia eficaz, que resulta em mudança de comportamentos de risco e manutenção de comportamentos seguros.\r\n\r\nA Unika utiliza uma metodologia com base em conceitos estabelecidos pelo psicólogo norte americano Scott Geller que tem como premissa que o diálogo estabelecido entre líder e liderado durante a realização da ferramenta precisa ter como base a confiança interpessoal.\r\n\r\nEste método exige um preparo robusto por parte daqueles que serão observadores (responsáveis por conduzir a ferramenta) para que o diálogo realmente aconteça. Para tal, é recomendado que sejam figuras de autoridade dentro da organização – líderes formais ou informais – pois são quem teoricamente deveriam estar melhor preparados para trabalhar com comunicação e gestão, e caso ainda não estejam, na OAC terão esta oportunidade de desenvolvimento.\r\n\r\nOs objetivos da OAC - Observação e Abordagem de Comportamentos em Segurança são vários:\r\n<ul>\r\n 	<li>Aprimorar a abordagem e comunicação entre líderes e liderados sobre os comportamentos em segurança do trabalho;</li>\r\n 	<li>Melhorar a percepção de riscos de líderes e liderados;</li>\r\n 	<li>Trabalhar com maior profundidade no entendimento dos ativadores ou causas do comportamento de risco;</li>\r\n 	<li>Criar um ambiente de confiança que favoreça os comportamentos de segurança em área;</li>\r\n 	<li>Utilizar o conhecimento e experiência do próprio empregado a favor da solução dos comportamentos de risco encontrados, tornando-o mais autônomo e responsável;</li>\r\n 	<li>Romper o ciclo de dependência e codependência. (leia mais sobre isso aqui)</li>\r\n</ul>\r\nPromover maior conscientização e auto responsabilização sobre a preservação da vida das pessoas é o maior benefício que esta ferramenta trás. A OAC oferece uma dimensão bastante prática aos líderes sobre o que precisam fazer para atuar no comportamento de seus empregados imediatamente.\r\n\r\n&nbsp;\r\n\r\nQuer conhecer mais sobre esta ferramenta? Entre em contato conosco.\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)\r\n\r\n&nbsp;', 'OAC - Observação e Abordagem de Comportamentos em Segurança – Por que implantar?', '', 'inherit', 'closed', 'closed', '', '202-autosave-v1', '', '', '2018-02-21 10:49:18', '2018-02-21 13:49:18', '', 202, 'http://unika.handgran.com.br/202-autosave-v1/', 0, 'revision', '', 0),
(236, 2, '2018-02-19 16:19:55', '2018-02-19 19:19:55', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone wp-image-205 size-full" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3.jpg" alt="" width="567" height="350" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: <span class="SpellE">Reason</span>, 2002\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.', '', 'inherit', 'closed', 'closed', '', '204-revision-v1', '', '', '2018-02-19 16:19:55', '2018-02-19 19:19:55', '', 204, 'http://unika.handgran.com.br/204-revision-v1/', 0, 'revision', '', 0),
(156, 1, '2018-01-19 11:31:25', '2018-01-19 13:31:25', '', '4', '', 'inherit', 'open', 'closed', '', '4', '', '', '2018-01-19 11:31:25', '2018-01-19 13:31:25', '', 15, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/4.png', 0, 'attachment', 'image/png', 0),
(157, 1, '2018-01-19 11:31:55', '2018-01-19 13:31:55', '', '13', '', 'inherit', 'open', 'closed', '', '13', '', '', '2018-01-19 11:31:55', '2018-01-19 13:31:55', '', 17, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/13.png', 0, 'attachment', 'image/png', 0),
(158, 1, '2018-01-19 11:32:23', '2018-01-19 13:32:23', '', '16', '', 'inherit', 'open', 'closed', '', '16', '', '', '2018-01-19 11:32:23', '2018-01-19 13:32:23', '', 19, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/16.png', 0, 'attachment', 'image/png', 0),
(159, 1, '2018-01-19 11:32:54', '2018-01-19 13:32:54', '', '10', '', 'inherit', 'open', 'closed', '', '10', '', '', '2018-01-19 11:32:54', '2018-01-19 13:32:54', '', 21, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/10.png', 0, 'attachment', 'image/png', 0),
(160, 1, '2018-01-19 11:33:18', '2018-01-19 13:33:18', '', '7', '', 'inherit', 'open', 'closed', '', '7', '', '', '2018-01-19 11:33:18', '2018-01-19 13:33:18', '', 23, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/7.png', 0, 'attachment', 'image/png', 0),
(161, 1, '2018-01-19 11:33:40', '2018-01-19 13:33:40', '', '3', '', 'inherit', 'open', 'closed', '', '3', '', '', '2018-01-19 11:33:40', '2018-01-19 13:33:40', '', 24, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/3.png', 0, 'attachment', 'image/png', 0),
(148, 1, '2018-01-17 11:17:05', '2018-01-17 13:17:05', '', 'yara', '', 'inherit', 'open', 'closed', '', 'yara', '', '', '2018-01-17 11:17:05', '2018-01-17 13:17:05', '', 146, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/yara.png', 0, 'attachment', 'image/png', 0),
(4, 1, '2017-10-29 13:46:30', '2017-10-29 15:46:30', '[email* email id:email class:email placeholder "e-mail"][textarea* mesagem id:mesagem class:mesagem placeholder "seu texto aqui…"]\r\n<div class="submit">\r\n[submit id:enviar class:enviar "Enviar"]\r\n</div>\n1\nUnika "[your-subject]"\n[your-name] <contato@unika.com.br>\ncontato@unika.com.br\nDe: [your-name] <[your-email]>\r\nAssunto: [your-subject]\r\n\r\nCorpo da mensagem:\r\n[your-message]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Unika (http://localhost/projetos/unika)\nReply-To: [your-email]\n\n\n\n\nUnika "[your-subject]"\nUnika <contato@unika.com.br>\n[your-email]\nCorpo da mensagem:\r\n[your-message]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Unika (http://localhost/projetos/unika)\nReply-To: contato@unika.com.br\n\n\n\nAgradecemos a sua mensagem.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nUm ou mais campos possuem um erro. Verifique e tente novamente.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar sua mensagem.\nO campo é obrigatório.\nO campo é muito longo.\nO campo é muito curto.\nO formato de data está incorreto.\nA data é anterior à mais antiga permitida.\nA data é posterior à maior data permitida.\nOcorreu um erro desconhecido ao enviar o arquivo.\nVocê não tem permissão para enviar esse tipo de arquivo.\nO arquivo é muito grande.\nOcorreu um erro ao enviar o arquivo.\nO formato de número é inválido.\nO número é menor do que o mínimo permitido.\nO número é maior do que o máximo permitido.\nA resposta para o quiz está incorreta.\nO código digitado está incorreto.\nO endereço de e-mail informado é inválido.\nA URL é inválida.\nO número de telefone é inválido.', 'Formulário de contato rodapé', '', 'publish', 'closed', 'closed', '', 'formulario-de-contato-1', '', '', '2017-10-31 13:56:07', '2017-10-31 15:56:07', '', 0, 'http://unika.handgran.com.br/?post_type=wpcf7_contact_form&#038;p=4', 0, 'wpcf7_contact_form', '', 0),
(5, 1, '2017-10-30 15:37:03', '2017-10-30 17:37:03', '', 'logo@2x', '', 'inherit', 'open', 'closed', '', 'logo2x', '', '', '2017-10-30 15:37:03', '2017-10-30 17:37:03', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/logo@2x.png', 0, 'attachment', 'image/png', 0),
(6, 1, '2017-10-30 15:38:58', '2017-10-30 17:38:58', '', 'Page1', '', 'inherit', 'open', 'closed', '', 'page1', '', '', '2017-10-30 15:38:58', '2017-10-30 17:38:58', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/Page1.png', 0, 'attachment', 'image/png', 0),
(7, 1, '2017-10-30 15:57:25', '2017-10-30 17:57:25', '', 'Rectangle 2 Copy 2@2x', '', 'inherit', 'open', 'closed', '', 'rectangle-2-copy-22x', '', '', '2017-10-31 18:42:37', '2017-10-31 20:42:37', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/Rectangle-2-Copy-2@2x.png', 0, 'attachment', 'image/png', 0),
(146, 1, '2018-01-17 11:17:10', '2018-01-17 13:17:10', '', 'Yara', '', 'publish', 'closed', 'closed', '', 'sesc', '', '', '2018-01-17 11:20:05', '2018-01-17 13:20:05', '', 0, 'http://unika.handgran.com.br/?post_type=parceiros&#038;p=146', 0, 'parceiros', '', 0),
(9, 1, '2017-10-30 16:10:02', '0000-00-00 00:00:00', 'Alta frequência de acidentes causada por comportamento', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-10-30 16:10:02', '2017-10-30 18:10:02', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=9', 0, 'servico', '', 0),
(229, 1, '2018-02-15 16:47:54', '2018-02-15 18:47:54', '', 'karla@2x', '', 'inherit', 'open', 'closed', '', 'karla2x-2', '', '', '2018-02-15 16:47:54', '2018-02-15 18:47:54', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2018/02/karla@2x.png', 0, 'attachment', 'image/png', 0),
(11, 1, '2017-10-30 16:10:57', '2017-10-30 18:10:57', 'Alta frequência de acidentes causada por comportamento', 'Alta frequência de acidentes causada por comportamento', '', 'publish', 'closed', 'closed', '', 'alta-frequencia-de-acidentes-causada-por-comportamento', '', '', '2018-01-19 11:30:26', '2018-01-19 13:30:26', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=11', 0, 'servico', '', 0),
(12, 1, '2017-10-30 16:10:52', '2017-10-30 18:10:52', '', 'eficacia', '', 'inherit', 'open', 'closed', '', 'eficacia', '', '', '2017-10-30 16:10:52', '2017-10-30 18:10:52', '', 11, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/eficacia.png', 0, 'attachment', 'image/png', 0),
(13, 1, '2017-10-30 16:11:33', '2017-10-30 18:11:33', 'Baixa percepção de riscos', 'Baixa percepção de riscos', '', 'publish', 'closed', 'closed', '', 'baixa-percepcao-de-riscos', '', '', '2018-01-19 11:30:59', '2018-01-19 13:30:59', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=13', 0, 'servico', '', 0),
(14, 1, '2017-10-30 16:11:30', '2017-10-30 18:11:30', '', 'comunicacao', '', 'inherit', 'open', 'closed', '', 'comunicacao', '', '', '2017-10-30 16:11:30', '2017-10-30 18:11:30', '', 13, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/comunicacao.png', 0, 'attachment', 'image/png', 0),
(15, 1, '2017-10-30 16:12:03', '2017-10-30 18:12:03', 'Comunicação ineficiente entre pares e entre níveis hierárquicos', 'Comunicação ineficiente entre pares e entre níveis hierárquicos', '', 'publish', 'closed', 'closed', '', 'comunicacao-ineficiente-entre-pares-e-entre-niveis-hierarquicos', '', '', '2018-01-19 11:31:29', '2018-01-19 13:31:29', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=15', 0, 'servico', '', 0),
(16, 1, '2017-10-30 16:11:58', '2017-10-30 18:11:58', '', 'conhecimento', '', 'inherit', 'open', 'closed', '', 'conhecimento', '', '', '2017-10-30 16:11:58', '2017-10-30 18:11:58', '', 15, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/conhecimento.png', 0, 'attachment', 'image/png', 0),
(17, 1, '2017-10-30 16:12:26', '2017-10-30 18:12:26', 'Hábitos antigos em segurança do trabalho', 'Hábitos antigos em segurança do trabalho', '', 'publish', 'closed', 'closed', '', 'habitos-antigos-em-seguranca-do-trabalho', '', '', '2018-01-19 11:31:59', '2018-01-19 13:31:59', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=17', 0, 'servico', '', 0),
(18, 1, '2017-10-30 16:12:21', '2017-10-30 18:12:21', '', 'comprometimento', '', 'inherit', 'open', 'closed', '', 'comprometimento', '', '', '2017-10-30 16:12:21', '2017-10-30 18:12:21', '', 17, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/comprometimento.png', 0, 'attachment', 'image/png', 0),
(19, 1, '2017-10-30 16:13:00', '2017-10-30 18:13:00', 'Recorrência comportamentos de risco', 'Recorrência comportamentos de risco', '', 'publish', 'closed', 'closed', '', 'recorrencia-comportamentos-de-risco', '', '', '2018-01-19 11:32:26', '2018-01-19 13:32:26', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=19', 0, 'servico', '', 0),
(20, 1, '2017-10-30 16:12:56', '2017-10-30 18:12:56', '', 'aprendizagem', '', 'inherit', 'open', 'closed', '', 'aprendizagem', '', '', '2017-10-30 16:12:56', '2017-10-30 18:12:56', '', 19, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/aprendizagem.png', 0, 'attachment', 'image/png', 0),
(21, 1, '2017-10-30 16:13:50', '2017-10-30 18:13:50', 'Baixo conhecimento de líderes sobre temas de segurança do trabalho', 'Baixo conhecimento de líderes sobre temas de segurança do trabalho', '', 'publish', 'closed', 'closed', '', 'baixo-conhecimento-de-lideres-sobre-temas-de-seguranca-do-trabalho', '', '', '2018-01-19 11:32:57', '2018-01-19 13:32:57', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=21', 0, 'servico', '', 0),
(23, 1, '2017-10-30 16:14:29', '2017-10-30 18:14:29', 'Baixo comprometimento de líderes com segurança comportamental', 'Baixo comprometimento de líderes com segurança comportamental', '', 'publish', 'closed', 'closed', '', 'baixo-comprometimento-de-lideres-com-seguranca-comportamental', '', '', '2018-01-19 11:33:21', '2018-01-19 13:33:21', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=23', 0, 'servico', '', 0),
(24, 1, '2017-10-30 16:14:47', '2017-10-30 18:14:47', 'Baixa relevância estratégica da área de segurança', 'Baixa relevância estratégica da área de segurança', '', 'publish', 'closed', 'closed', '', 'baixa-relevancia-estrategica-da-area-de-seguranca', '', '', '2018-01-19 11:33:44', '2018-01-19 13:33:44', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=24', 0, 'servico', '', 0),
(25, 1, '2017-10-30 16:15:21', '2017-10-30 18:15:21', 'Segurança ainda não é um valor', 'Segurança ainda não é um valor', '', 'publish', 'closed', 'closed', '', 'seguranca-ainda-nao-e-um-valor', '', '', '2018-01-19 11:34:08', '2018-01-19 13:34:08', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=25', 0, 'servico', '', 0),
(26, 1, '2017-10-30 16:15:41', '2017-10-30 18:15:41', 'Dificuldade de interpretar as estatísticas de segurança', 'Dificuldade de interpretar as estatísticas de segurança', '', 'publish', 'closed', 'closed', '', 'dificuldade-de-interpretar-as-estatisticas-de-seguranca', '', '', '2018-01-19 11:34:37', '2018-01-19 13:34:37', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=26', 0, 'servico', '', 0),
(27, 1, '2017-10-30 16:15:58', '2017-10-30 18:15:58', 'Cultura de segurança frágil', 'Cultura de segurança frágil', '', 'publish', 'closed', 'closed', '', 'cultura-de-seguranca-fragil', '', '', '2018-01-19 11:35:00', '2018-01-19 13:35:00', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=27', 0, 'servico', '', 0),
(28, 1, '2017-10-30 16:16:15', '2017-10-30 18:16:15', 'Dificuldade na tratativa dos acidentes', 'Dificuldade na tratativa dos acidentes', '', 'publish', 'closed', 'closed', '', 'dificuldade-na-tratativa-dos-acidentes', '', '', '2018-01-19 11:35:28', '2018-01-19 13:35:28', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=28', 0, 'servico', '', 0),
(29, 1, '2017-10-30 16:16:32', '2017-10-30 18:16:32', 'Dificuldade na captação de desvios e quase-acidentes', 'Dificuldade na captação de desvios e quase-acidentes', '', 'publish', 'closed', 'closed', '', 'dificuldade-na-captacao-de-desvios-e-quase-acidentes', '', '', '2018-01-19 11:35:53', '2018-01-19 13:35:53', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=29', 0, 'servico', '', 0),
(30, 1, '2017-10-30 16:16:52', '2017-10-30 18:16:52', 'Falta de definição de estratégia de Segurança do trabalho', 'Falta de definição de estratégia de segurança do trabalho', '', 'publish', 'closed', 'closed', '', 'falta-de-definicao-de-estrategia-de-seguranca-do-trabalho', '', '', '2018-01-19 11:36:16', '2018-01-19 13:36:16', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=30', 0, 'servico', '', 0),
(31, 1, '2017-10-30 16:17:13', '2017-10-30 18:17:13', 'Baixa eficácia em processos de aprendizagem em temáticas de segurança do trabalho', 'Baixa eficácia em processos de aprendizagem em temáticas de segurança do trabalho', '', 'publish', 'closed', 'closed', '', 'ineficacia-em-processos-de-aprendizagem-em-tematicas-de-seguranca-do-trabalho', '', '', '2018-01-19 11:53:56', '2018-01-19 13:53:56', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=31', 0, 'servico', '', 0),
(32, 1, '2017-10-30 16:17:46', '2017-10-30 18:17:46', 'Ferramentas de segurança sub utilizadas', 'Ferramentas de segurança sub utilizadas', '', 'publish', 'closed', 'closed', '', 'ferramentas-de-seguranca-sub-utilizadas', '', '', '2018-01-19 11:37:07', '2018-01-19 13:37:07', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=32', 0, 'servico', '', 0),
(33, 1, '2017-10-30 16:18:03', '2017-10-30 18:18:03', 'Melhoria de resultados e cultura de segurança', 'Melhoria de resultados e cultura de segurança', '', 'publish', 'closed', 'closed', '', 'melhoria-de-resultados-e-cultura-de-seguranca', '', '', '2017-10-30 16:18:03', '2017-10-30 18:18:03', '', 0, 'http://unika.handgran.com.br/?post_type=servico&#038;p=33', 0, 'servico', '', 0),
(34, 1, '2017-10-30 16:19:57', '2017-10-30 18:19:57', '', 'Nestle', '', 'publish', 'closed', 'closed', '', 'nestle', '', '', '2017-10-30 16:19:57', '2017-10-30 18:19:57', '', 0, 'http://unika.handgran.com.br/?post_type=parceiros&#038;p=34', 0, 'parceiros', '', 0),
(35, 1, '2017-10-30 16:19:48', '2017-10-30 18:19:48', '', 'Araupel@3x', '', 'inherit', 'open', 'closed', '', 'araupel3x', '', '', '2017-10-30 16:19:48', '2017-10-30 18:19:48', '', 34, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/Araupel@3x.png', 0, 'attachment', 'image/png', 0),
(36, 1, '2017-10-30 16:19:49', '2017-10-30 18:19:49', '', 'clientes-o-i-azul@3x', '', 'inherit', 'open', 'closed', '', 'clientes-o-i-azul3x', '', '', '2017-10-30 16:19:49', '2017-10-30 18:19:49', '', 34, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/clientes-o-i-azul@3x.png', 0, 'attachment', 'image/png', 0),
(37, 1, '2017-10-30 16:19:50', '2017-10-30 18:19:50', '', 'Klabin@3x', '', 'inherit', 'open', 'closed', '', 'klabin3x', '', '', '2017-10-30 16:19:50', '2017-10-30 18:19:50', '', 34, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/Klabin@3x.png', 0, 'attachment', 'image/png', 0),
(38, 1, '2017-10-30 16:19:51', '2017-10-30 18:19:51', '', 'nestle@3x', '', 'inherit', 'open', 'closed', '', 'nestle3x', '', '', '2017-10-30 16:19:51', '2017-10-30 18:19:51', '', 34, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/nestle@3x.png', 0, 'attachment', 'image/png', 0),
(39, 1, '2017-10-30 16:20:21', '2017-10-30 18:20:21', '', 'Klabin', '', 'publish', 'closed', 'closed', '', 'klabin', '', '', '2017-10-30 16:20:21', '2017-10-30 18:20:21', '', 0, 'http://unika.handgran.com.br/?post_type=parceiros&#038;p=39', 0, 'parceiros', '', 0),
(40, 1, '2017-10-30 16:21:09', '2017-10-30 18:21:09', '', 'OI', '', 'publish', 'closed', 'closed', '', 'oi', '', '', '2017-10-30 16:21:09', '2017-10-30 18:21:09', '', 0, 'http://unika.handgran.com.br/?post_type=parceiros&#038;p=40', 0, 'parceiros', '', 0),
(41, 1, '2017-10-30 16:21:33', '2017-10-30 18:21:33', '', 'Araupel', '', 'publish', 'closed', 'closed', '', 'araupel', '', '', '2017-10-30 16:21:33', '2017-10-30 18:21:33', '', 0, 'http://unika.handgran.com.br/?post_type=parceiros&#038;p=41', 0, 'parceiros', '', 0),
(43, 1, '2017-10-30 16:22:47', '2017-10-30 18:22:47', '', 'face', 'The archetypal male face of beauty according to a new scientific study to mark the launch of the Samsung Galaxy S6.  Embargoed to 00.01hrs 30th March 2015.', 'inherit', 'open', 'closed', '', 'face', '', '', '2017-10-30 16:22:47', '2017-10-30 18:22:47', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/face.jpg', 0, 'attachment', 'image/jpeg', 0),
(151, 1, '2018-01-17 11:20:36', '2018-01-17 13:20:36', '', 'sesc', '', 'inherit', 'open', 'closed', '', 'sesc-2', '', '', '2018-01-17 11:20:36', '2018-01-17 13:20:36', '', 150, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/sesc-1.png', 0, 'attachment', 'image/png', 0),
(45, 1, '2017-10-30 16:29:11', '2017-10-30 18:29:11', '', 'argentina@2x', '', 'inherit', 'open', 'closed', '', 'argentina2x', '', '', '2017-10-30 16:29:11', '2017-10-30 18:29:11', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/argentina@2x.png', 0, 'attachment', 'image/png', 0),
(46, 1, '2017-10-30 16:29:13', '2017-10-30 18:29:13', '', 'bolivia@3x', '', 'inherit', 'open', 'closed', '', 'bolivia3x', '', '', '2017-10-30 16:29:13', '2017-10-30 18:29:13', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/bolivia@3x.png', 0, 'attachment', 'image/png', 0),
(47, 1, '2017-10-30 16:29:14', '2017-10-30 18:29:14', '', 'brazil@3x', '', 'inherit', 'open', 'closed', '', 'brazil3x', '', '', '2017-10-30 16:29:14', '2017-10-30 18:29:14', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/brazil@3x.png', 0, 'attachment', 'image/png', 0),
(48, 1, '2017-10-30 16:29:17', '2017-10-30 18:29:17', '', 'colombia@3x', '', 'inherit', 'open', 'closed', '', 'colombia3x', '', '', '2017-10-30 16:29:17', '2017-10-30 18:29:17', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/colombia@3x.png', 0, 'attachment', 'image/png', 0),
(49, 1, '2017-10-30 16:29:19', '2017-10-30 18:29:19', '', 'equador@3x', '', 'inherit', 'open', 'closed', '', 'equador3x', '', '', '2017-10-30 16:29:19', '2017-10-30 18:29:19', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/equador@3x.png', 0, 'attachment', 'image/png', 0),
(50, 1, '2017-10-30 16:29:21', '2017-10-30 18:29:21', '', 'mexico@3x', '', 'inherit', 'open', 'closed', '', 'mexico3x', '', '', '2017-10-30 16:29:21', '2017-10-30 18:29:21', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/mexico@3x.png', 0, 'attachment', 'image/png', 0),
(51, 1, '2017-10-30 16:29:23', '2017-10-30 18:29:23', '', 'peru@3x', '', 'inherit', 'open', 'closed', '', 'peru3x', '', '', '2017-10-30 16:29:23', '2017-10-30 18:29:23', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/peru@3x.png', 0, 'attachment', 'image/png', 0),
(152, 1, '2018-01-19 09:58:59', '2018-01-19 11:58:59', 'Psicóloga, formada em psicologia relacional sistêmica. especialista em gestão de pessoas. experiência em gestão de clima, liderança, diversidade e inclusão em psicologia da segurança do trabalho.', 'Gabriela Comper', '', 'inherit', 'closed', 'closed', '', '112-autosave-v1', '', '', '2018-01-19 09:58:59', '2018-01-19 11:58:59', '', 112, 'http://unika.handgran.com.br/112-autosave-v1/', 0, 'revision', '', 0),
(153, 1, '2018-01-19 10:00:56', '2018-01-19 12:00:56', 'Psicóloga, formada em psicologia relacional sistêmica. especialista em gestão de negócios. experiência em psicologia da segurança do trabalho e gestão de negócios.', 'Mariane Mesquita', '', 'inherit', 'closed', 'closed', '', '114-autosave-v1', '', '', '2018-01-19 10:00:56', '2018-01-19 12:00:56', '', 114, 'http://unika.handgran.com.br/114-autosave-v1/', 0, 'revision', '', 0),
(154, 1, '2018-01-19 11:30:22', '2018-01-19 13:30:22', '', '11', '', 'inherit', 'open', 'closed', '', '11', '', '', '2018-01-19 11:30:22', '2018-01-19 13:30:22', '', 11, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/11.png', 0, 'attachment', 'image/png', 0),
(155, 1, '2018-01-19 11:30:55', '2018-01-19 13:30:55', '', '15', '', 'inherit', 'open', 'closed', '', '15', '', '', '2018-01-19 11:30:55', '2018-01-19 13:30:55', '', 13, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/15.png', 0, 'attachment', 'image/png', 0),
(59, 1, '2017-10-30 16:36:24', '2017-10-30 18:36:24', '', 'blogSingle', '', 'inherit', 'open', 'closed', '', 'blogsingle', '', '', '2017-10-30 16:36:24', '2017-10-30 18:36:24', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/blogSingle.png', 0, 'attachment', 'image/png', 0),
(61, 1, '2017-10-30 16:37:17', '2017-10-30 18:37:17', '', 'Rectangle 2 Copy 2@2x', '', 'inherit', 'open', 'closed', '', 'rectangle-2-copy-22x-2', '', '', '2017-10-30 16:37:17', '2017-10-30 18:37:17', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/Rectangle-2-Copy-2@2x-1.png', 0, 'attachment', 'image/png', 0),
(64, 1, '2017-10-30 16:37:52', '2017-10-30 18:37:52', '', 'bannerBlog', '', 'inherit', 'open', 'closed', '', 'bannerblog', '', '', '2017-10-30 16:37:52', '2017-10-30 18:37:52', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/bannerBlog.jpg', 0, 'attachment', 'image/jpeg', 0),
(217, 1, '2018-01-25 15:19:36', '2018-01-25 17:19:36', '', 'Problemas que resolvemos', '', 'inherit', 'closed', 'closed', '', '216-revision-v1', '', '', '2018-01-25 15:19:36', '2018-01-25 17:19:36', '', 216, 'http://unika.handgran.com.br/216-revision-v1/', 0, 'revision', '', 0),
(162, 1, '2018-01-19 11:34:05', '2018-01-19 13:34:05', '', '14', '', 'inherit', 'open', 'closed', '', '14', '', '', '2018-01-19 11:34:05', '2018-01-19 13:34:05', '', 25, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/14.png', 0, 'attachment', 'image/png', 0),
(216, 1, '2018-01-25 15:19:36', '2018-01-25 17:19:36', '', 'Problemas que resolvemos', '', 'publish', 'closed', 'closed', '', 'problemas-qu-e-resolvemos', '', '', '2018-01-25 15:36:02', '2018-01-25 17:36:02', '', 0, 'http://unika.handgran.com.br/?page_id=216', 0, 'page', '', 0),
(163, 1, '2018-01-19 11:34:34', '2018-01-19 13:34:34', '', '2', '', 'inherit', 'open', 'closed', '', '2', '', '', '2018-01-19 11:34:34', '2018-01-19 13:34:34', '', 26, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/2.png', 0, 'attachment', 'image/png', 0),
(164, 1, '2018-01-19 11:34:56', '2018-01-19 13:34:56', '', '12', '', 'inherit', 'open', 'closed', '', '12', '', '', '2018-01-19 11:34:56', '2018-01-19 13:34:56', '', 27, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/12.png', 0, 'attachment', 'image/png', 0),
(165, 1, '2018-01-19 11:35:24', '2018-01-19 13:35:24', '', '17', '', 'inherit', 'open', 'closed', '', '17', '', '', '2018-01-19 11:35:24', '2018-01-19 13:35:24', '', 28, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/17.png', 0, 'attachment', 'image/png', 0),
(166, 1, '2018-01-19 11:35:50', '2018-01-19 13:35:50', '', '18', '', 'inherit', 'open', 'closed', '', '18', '', '', '2018-01-19 11:35:50', '2018-01-19 13:35:50', '', 29, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/18.png', 0, 'attachment', 'image/png', 0),
(167, 1, '2018-01-19 11:36:12', '2018-01-19 13:36:12', '', '9', '', 'inherit', 'open', 'closed', '', '9', '', '', '2018-01-19 11:36:12', '2018-01-19 13:36:12', '', 30, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/9.png', 0, 'attachment', 'image/png', 0),
(168, 1, '2018-01-19 11:37:03', '2018-01-19 13:37:03', '', '8', '', 'inherit', 'open', 'closed', '', '8', '', '', '2018-01-19 11:37:03', '2018-01-19 13:37:03', '', 32, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/8.png', 0, 'attachment', 'image/png', 0),
(169, 1, '2018-01-19 11:37:31', '2018-01-19 13:37:31', '', '19', '', 'inherit', 'open', 'closed', '', '19', '', '', '2018-01-19 11:37:31', '2018-01-19 13:37:31', '', 31, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/19.png', 0, 'attachment', 'image/png', 0),
(71, 1, '2017-10-30 16:47:02', '2017-10-30 18:47:02', 'Oferecemos soluções para preservar a vida nos ambientes de trabalho por meio de intervenções em comportamento e cultura.', 'Quem somos', '', 'publish', 'closed', 'closed', '', 'quem-somos', '', '', '2017-10-31 15:00:02', '2017-10-31 17:00:02', '', 0, 'http://unika.handgran.com.br/?page_id=71', 0, 'page', '', 0),
(72, 1, '2017-10-30 16:47:02', '2017-10-30 18:47:02', '', 'Quem somos', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2017-10-30 16:47:02', '2017-10-30 18:47:02', '', 71, 'http://unika.handgran.com.br/2017/10/30/71-revision-v1/', 0, 'revision', '', 0),
(73, 1, '2017-10-30 16:47:12', '2017-10-30 18:47:12', '', 'Soluções', '', 'publish', 'closed', 'closed', '', 'solucoes', '', '', '2017-10-31 14:13:13', '2017-10-31 16:13:13', '', 0, 'http://unika.handgran.com.br/?page_id=73', 0, 'page', '', 0),
(74, 1, '2017-10-30 16:47:12', '2017-10-30 18:47:12', '', 'Soluções', '', 'inherit', 'closed', 'closed', '', '73-revision-v1', '', '', '2017-10-30 16:47:12', '2017-10-30 18:47:12', '', 73, 'http://unika.handgran.com.br/2017/10/30/73-revision-v1/', 0, 'revision', '', 0),
(75, 1, '2017-10-30 16:47:22', '2017-10-30 18:47:22', '', 'Blog', '', 'publish', 'closed', 'closed', '', 'blog', '', '', '2017-10-30 16:47:22', '2017-10-30 18:47:22', '', 0, 'http://unika.handgran.com.br/?page_id=75', 0, 'page', '', 0),
(76, 1, '2017-10-30 16:47:22', '2017-10-30 18:47:22', '', 'Blog', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2017-10-30 16:47:22', '2017-10-30 18:47:22', '', 75, 'http://unika.handgran.com.br/2017/10/30/75-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2017-10-30 16:47:31', '2017-10-30 18:47:31', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2017-11-01 10:45:05', '2017-11-01 12:45:05', '', 0, 'http://unika.handgran.com.br/?page_id=77', 0, 'page', '', 0),
(78, 1, '2017-10-30 16:47:31', '2017-10-30 18:47:31', '', 'Contato', '', 'inherit', 'closed', 'closed', '', '77-revision-v1', '', '', '2017-10-30 16:47:31', '2017-10-30 18:47:31', '', 77, 'http://unika.handgran.com.br/2017/10/30/77-revision-v1/', 0, 'revision', '', 0),
(79, 1, '2017-10-30 16:47:52', '2017-10-30 18:47:52', ' ', '', '', 'publish', 'closed', 'closed', '', '79', '', '', '2018-02-21 09:48:20', '2018-02-21 12:48:20', '', 0, 'http://unika.handgran.com.br/?p=79', 4, 'nav_menu_item', '', 0),
(80, 1, '2017-10-30 16:47:52', '2017-10-30 18:47:52', ' ', '', '', 'publish', 'closed', 'closed', '', '80', '', '', '2018-02-21 09:48:20', '2018-02-21 12:48:20', '', 0, 'http://unika.handgran.com.br/?p=80', 3, 'nav_menu_item', '', 0),
(81, 1, '2017-10-30 16:47:52', '2017-10-30 18:47:52', ' ', '', '', 'publish', 'closed', 'closed', '', '81', '', '', '2018-02-21 09:48:20', '2018-02-21 12:48:20', '', 0, 'http://unika.handgran.com.br/?p=81', 2, 'nav_menu_item', '', 0),
(82, 1, '2017-10-30 16:47:52', '2017-10-30 18:47:52', ' ', '', '', 'publish', 'closed', 'closed', '', '82', '', '', '2018-02-21 09:48:20', '2018-02-21 12:48:20', '', 0, 'http://unika.handgran.com.br/?p=82', 1, 'nav_menu_item', '', 0),
(83, 1, '2017-10-31 09:49:52', '2017-10-31 11:49:52', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2017-10-31 09:49:52', '2017-10-31 11:49:52', '', 0, 'http://unika.handgran.com.br/?page_id=83', 0, 'page', '', 0),
(84, 1, '2017-10-31 09:49:52', '2017-10-31 11:49:52', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '83-revision-v1', '', '', '2017-10-31 09:49:52', '2017-10-31 11:49:52', '', 83, 'http://unika.handgran.com.br/2017/10/31/83-revision-v1/', 0, 'revision', '', 0),
(149, 1, '2018-01-17 11:20:01', '2018-01-17 13:20:01', '', 'yara', '', 'inherit', 'open', 'closed', '', 'yara-2', '', '', '2018-01-17 11:20:01', '2018-01-17 13:20:01', '', 146, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/yara-1.png', 0, 'attachment', 'image/png', 0),
(87, 1, '2017-10-31 10:32:20', '2017-10-31 12:32:20', 'O planejamento estratégico tem a finalidade de definir onde a empresa pretende chegar em termos de cultura de segurança…', 'Planejamento Estratégico de Segurança', '', 'publish', 'closed', 'closed', '', 'planejamento-estrategico-de-seguranca', '', '', '2018-02-20 17:31:12', '2018-02-20 20:31:12', '', 0, 'http://unika.handgran.com.br/?post_type=destaque&#038;p=87', 1, 'destaque', '', 0),
(88, 1, '2017-10-31 10:29:17', '2017-10-31 12:29:17', '', 'Bitmap@2x', '', 'inherit', 'open', 'closed', '', 'bitmap2x', '', '', '2017-10-31 10:29:17', '2017-10-31 12:29:17', '', 87, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/Bitmap@2x.png', 0, 'attachment', 'image/png', 0),
(89, 1, '2017-10-31 10:29:18', '2017-10-31 12:29:18', '', 'iconworkshop@2x', '', 'inherit', 'open', 'closed', '', 'iconworkshop2x', '', '', '2017-10-31 10:29:18', '2017-10-31 12:29:18', '', 87, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/iconworkshop@2x.png', 0, 'attachment', 'image/png', 0),
(90, 1, '2017-10-31 10:29:24', '2017-10-31 12:29:24', '', 'treinamento', '', 'inherit', 'open', 'closed', '', 'treinamento', '', '', '2017-10-31 10:29:24', '2017-10-31 12:29:24', '', 87, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/treinamento.png', 0, 'attachment', 'image/png', 0),
(91, 1, '2017-10-31 10:33:56', '2017-10-31 12:33:56', 'Conheça os diversos serviços que podemos oferecer para implantar ou aprimorar a execução de ferramentas de segurança comportamental', 'Ferramentas de Segurança Comportamental', '', 'publish', 'closed', 'closed', '', 'ferramenta-de-seguranca-comportamental', '', '', '2018-02-20 17:33:36', '2018-02-20 20:33:36', '', 0, 'http://unika.handgran.com.br/?post_type=destaque&#038;p=91', 2, 'destaque', '', 0),
(92, 1, '2017-10-31 10:35:58', '2017-10-31 12:35:58', 'Utilizamos como referência da escada evolutiva do Hearts&amp;Minds para realizar o diagnóstico de cultura.', 'Treinamentos', '', 'publish', 'closed', 'closed', '', 'treinamentos', '', '', '2018-02-20 17:31:09', '2018-02-20 20:31:09', '', 0, 'http://unika.handgran.com.br/?post_type=destaque&#038;p=92', 3, 'destaque', '', 0),
(93, 1, '2017-10-31 10:37:14', '2017-10-31 12:37:14', 'O planejamento estratégico tem a finalidade de definir onde a empresa pretende chegar em termos de cultura de segurança…', 'Workshops', '', 'publish', 'closed', 'closed', '', 'workshops', '', '', '2018-02-20 17:31:07', '2018-02-20 20:31:07', '', 0, 'http://unika.handgran.com.br/?post_type=destaque&#038;p=93', 4, 'destaque', '', 0),
(94, 1, '2017-10-31 10:38:58', '2017-10-31 12:38:58', 'Oferecemos soluções para preservar a vida nos ambientes de trabalho por meio de intervenções em comportamento e cultura.', 'Palestras', '', 'publish', 'closed', 'closed', '', 'palestras', '', '', '2018-02-20 17:31:05', '2018-02-20 20:31:05', '', 0, 'http://unika.handgran.com.br/?post_type=destaque&#038;p=94', 5, 'destaque', '', 0),
(95, 1, '2017-10-31 10:40:35', '2017-10-31 12:40:35', 'Utilizamos como referência da escada evolutiva do Hearts&amp;Minds para realizar o diagnóstico de cultura.', 'Diagnóstico de Cultura de Segurança', '', 'publish', 'closed', 'closed', '', 'diagnostico-de-cultura-de-seguranca', '', '', '2018-02-20 17:31:15', '2018-02-20 20:31:15', '', 0, 'http://unika.handgran.com.br/?post_type=destaque&#038;p=95', 0, 'destaque', '', 0),
(96, 1, '2017-10-31 11:13:29', '2017-10-31 13:13:29', '12 CIDADES', 'Equador', '', 'publish', 'closed', 'closed', '', 'equador', '', '', '2017-10-31 11:13:29', '2017-10-31 13:13:29', '', 0, 'http://unika.handgran.com.br/?post_type=paises&#038;p=96', 0, 'paises', '', 0),
(97, 1, '2017-10-31 11:13:58', '2017-10-31 13:13:58', '12 CIDADES', 'Bolívia', '', 'publish', 'closed', 'closed', '', 'bolivia', '', '', '2017-10-31 11:13:58', '2017-10-31 13:13:58', '', 0, 'http://unika.handgran.com.br/?post_type=paises&#038;p=97', 0, 'paises', '', 0),
(98, 1, '2017-10-31 11:14:38', '2017-10-31 13:14:38', '12 CIDADES', 'Colômbia', '', 'publish', 'closed', 'closed', '', 'colombia', '', '', '2017-10-31 11:14:38', '2017-10-31 13:14:38', '', 0, 'http://unika.handgran.com.br/?post_type=paises&#038;p=98', 0, 'paises', '', 0),
(99, 1, '2017-10-31 11:15:34', '2017-10-31 13:15:34', '12 cidades', 'Argentina', '', 'publish', 'closed', 'closed', '', 'argentina', '', '', '2017-10-31 11:20:06', '2017-10-31 13:20:06', '', 0, 'http://unika.handgran.com.br/?post_type=paises&#038;p=99', 0, 'paises', '', 0),
(100, 1, '2017-10-31 11:16:12', '2017-10-31 13:16:12', '12 CIDADES', 'México', '', 'publish', 'closed', 'closed', '', 'mexico', '', '', '2017-10-31 11:16:12', '2017-10-31 13:16:12', '', 0, 'http://unika.handgran.com.br/?post_type=paises&#038;p=100', 0, 'paises', '', 0),
(101, 1, '2017-10-31 11:16:55', '2017-10-31 13:16:55', '12 CIDADES', 'Peru', '', 'publish', 'closed', 'closed', '', 'peru', '', '', '2017-10-31 11:16:55', '2017-10-31 13:16:55', '', 0, 'http://unika.handgran.com.br/?post_type=paises&#038;p=101', 0, 'paises', '', 0),
(102, 1, '2017-10-31 11:17:16', '2017-10-31 13:17:16', '12 CIDADES', 'Brasil', '', 'publish', 'closed', 'closed', '', 'brasil', '', '', '2017-10-31 11:17:16', '2017-10-31 13:17:16', '', 0, 'http://unika.handgran.com.br/?post_type=paises&#038;p=102', 0, 'paises', '', 0),
(103, 1, '2017-10-31 12:04:44', '2017-10-31 14:04:44', '[wysija_page]', 'Confirmação de assinatura', '', 'publish', 'closed', 'closed', '', 'subscriptions', '', '', '2017-10-31 12:04:44', '2017-10-31 14:04:44', '', 0, 'http://unika.handgran.com.br/?wysijap=subscriptions', 0, 'wysijap', '', 0);
INSERT INTO `uk_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(104, 1, '2017-10-31 15:00:02', '2017-10-31 17:00:02', 'Oferecemos soluções para preservar a vida nos ambientes de trabalho por meio de intervenções em comportamento e cultura.', 'Quem somos', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2017-10-31 15:00:02', '2017-10-31 17:00:02', '', 71, 'http://unika.handgran.com.br/2017/10/31/71-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2017-10-31 15:09:43', '2017-10-31 17:09:43', '', 'logoCortada', '', 'inherit', 'open', 'closed', '', 'logocortada', '', '', '2017-10-31 15:09:43', '2017-10-31 17:09:43', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/logoCortada.png', 0, 'attachment', 'image/png', 0),
(150, 1, '2018-01-17 11:20:41', '2018-01-17 13:20:41', '', 'Sesc', '', 'publish', 'closed', 'closed', '', 'sesc-2', '', '', '2018-01-17 11:20:41', '2018-01-17 13:20:41', '', 0, 'http://unika.handgran.com.br/?post_type=parceiros&#038;p=150', 0, 'parceiros', '', 0),
(107, 1, '2017-10-31 15:23:13', '2017-10-31 17:23:13', 'Psicóloga, formada em psicologia relacional sistêmica. Especialista em psicologia organizacional e do trabalho. Experiência em psicologia da segurança do trabalho, liderança e gestão de pessoas.', 'Karla Mikoski', '', 'publish', 'closed', 'closed', '', 'karla-mikoski', '', '', '2018-01-25 15:09:45', '2018-01-25 17:09:45', '', 0, 'http://unika.handgran.com.br/?post_type=equipe&#038;p=107', 0, 'equipe', '', 0),
(108, 1, '2017-10-31 15:20:05', '2017-10-31 17:20:05', '', 'Katshuska@3x', '', 'inherit', 'open', 'closed', '', 'katshuska3x', '', '', '2017-10-31 15:20:05', '2017-10-31 17:20:05', '', 107, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/Katshuska@3x.png', 0, 'attachment', 'image/png', 0),
(109, 1, '2017-10-31 15:20:34', '2017-10-31 17:20:34', '', 'gabriela@2x', '', 'inherit', 'open', 'closed', '', 'gabriela2x', '', '', '2017-10-31 15:20:34', '2017-10-31 17:20:34', '', 107, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/gabriela@2x.png', 0, 'attachment', 'image/png', 0),
(110, 1, '2017-10-31 15:20:38', '2017-10-31 17:20:38', '', 'karla@2x', '', 'inherit', 'open', 'closed', '', 'karla2x', '', '', '2017-10-31 15:20:38', '2017-10-31 17:20:38', '', 107, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/karla@2x.png', 0, 'attachment', 'image/png', 0),
(111, 1, '2017-10-31 15:20:42', '2017-10-31 17:20:42', '', 'Mariane@2x', '', 'inherit', 'open', 'closed', '', 'mariane2x', '', '', '2017-10-31 15:20:42', '2017-10-31 17:20:42', '', 107, 'http://unika.handgran.com.br/wp-content/uploads/2017/10/Mariane@2x.png', 0, 'attachment', 'image/png', 0),
(112, 1, '2017-10-31 15:25:36', '2017-10-31 17:25:36', 'Psicóloga, formada em psicologia relacional sistêmica. Especialista em gestão de pessoas. Experiência em gestão de clima, liderança, diversidade e inclusão em psicologia da segurança do trabalho.', 'Gabriela Comper', '', 'publish', 'closed', 'closed', '', 'gabriela-comper', '', '', '2018-01-25 15:10:01', '2018-01-25 17:10:01', '', 0, 'http://unika.handgran.com.br/?post_type=equipe&#038;p=112', 2, 'equipe', '', 0),
(113, 1, '2017-10-31 15:26:15', '2017-10-31 17:26:15', 'Pedagoga, formada em Profissional e Self Coaching. Especialista em Desevolvimento Gerencial. Experiência em Programas de Educação Corporativa e Desenvolvimento de Lideranças.Facilitadora de Programas Sócio - Educacionais de Terceiro Setor, Programas de Coaching e em Psicologia do Trabalho.', 'Karla Katshuska Batistel', '', 'private', 'closed', 'closed', '', 'karla-katshuska-batistel', '', '', '2018-01-19 09:56:41', '2018-01-19 11:56:41', '', 0, 'http://unika.handgran.com.br/?post_type=equipe&#038;p=113', 3, 'equipe', '', 0),
(114, 1, '2017-10-31 15:26:46', '2017-10-31 17:26:46', 'Psicóloga, formada em psicologia relacional sistêmica. Especialista em gestão de negócios. Experiência em psicologia da segurança do trabalho e gestão de negócios.', 'Mariane Mesquita', '', 'publish', 'closed', 'closed', '', 'mariane-mesquita', '', '', '2018-01-19 10:02:09', '2018-01-19 12:02:09', '', 0, 'http://unika.handgran.com.br/?post_type=equipe&#038;p=114', 1, 'equipe', '', 0),
(243, 2, '2018-02-21 10:45:22', '2018-02-21 13:45:22', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone wp-image-205 size-full" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3.jpg" alt="" width="567" height="350" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: Reason, 2002\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.', '', 'inherit', 'closed', 'closed', '', '204-autosave-v1', '', '', '2018-02-21 10:45:22', '2018-02-21 13:45:22', '', 204, 'http://unika.handgran.com.br/204-autosave-v1/', 0, 'revision', '', 0),
(147, 1, '2018-01-17 11:17:04', '2018-01-17 13:17:04', '', 'sesc', '', 'inherit', 'open', 'closed', '', 'sesc', '', '', '2018-01-17 11:17:04', '2018-01-17 13:17:04', '', 146, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/sesc.png', 0, 'attachment', 'image/png', 0),
(238, 1, '2018-02-19 16:39:57', '2018-02-19 19:39:57', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone wp-image-205 size-full" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3.jpg" alt="" width="567" height="350" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: <span class="SpellE">Reason</span>, 2002\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.', '', 'inherit', 'closed', 'closed', '', '204-revision-v1', '', '', '2018-02-19 16:39:57', '2018-02-19 19:39:57', '', 204, 'http://unika.handgran.com.br/204-revision-v1/', 0, 'revision', '', 0),
(239, 1, '2018-02-19 16:40:18', '2018-02-19 19:40:18', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone wp-image-205 size-full" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3.jpg" alt="" width="567" height="350" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: Reason, 2002\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.', '', 'inherit', 'closed', 'closed', '', '204-revision-v1', '', '', '2018-02-19 16:40:18', '2018-02-19 19:40:18', '', 204, 'http://unika.handgran.com.br/204-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2017-11-01 11:10:59', '2017-11-01 13:10:59', '[text* NomeCompleto id:nomeCompleto class:nomeCompleto placeholder "Nome Completo"][email* email id:email class:email placeholder "E-mail"][tel* telefone id:telefone class:telefone placeholder "Telefone"][text* Nomedaempresa id:nomedaempresa class:nome class:da class:empresa placeholder "Nome da empresa"][textarea mensagem id:mensagem class:mensagem placeholder "Mensagem..."]\r\n<div class="submit">\r\n[submit id:enviar class:enviar "Enviar"]\r\n</div>\n1\nUnika "[your-subject]"\n[your-name] <contato@unika.com.br>\ncontato@unika.com.br\nDe: [your-name] <[your-email]>\r\nAssunto: [your-subject]\r\n\r\nCorpo da mensagem:\r\n[your-message]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Unika (http://localhost/projetos/unika)\nReply-To: [your-email]\n\n\n\n\nUnika "[your-subject]"\nUnika <contato@unika.com.br>\n[your-email]\nCorpo da mensagem:\r\n[your-message]\r\n\r\n-- \r\nEste e-mail foi enviado de um formulário de contato em Unika (http://localhost/projetos/unika)\nReply-To: contato@unika.com.br\n\n\n\nAgradecemos a sua mensagem.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nUm ou mais campos possuem um erro. Verifique e tente novamente.\nOcorreu um erro ao tentar enviar sua mensagem. Tente novamente mais tarde.\nVocê deve aceitar os termos e condições antes de enviar sua mensagem.\nO campo é obrigatório.\nO campo é muito longo.\nO campo é muito curto.\nO formato de data está incorreto.\nA data é anterior à mais antiga permitida.\nA data é posterior à maior data permitida.\nOcorreu um erro desconhecido ao enviar o arquivo.\nVocê não tem permissão para enviar esse tipo de arquivo.\nO arquivo é muito grande.\nOcorreu um erro ao enviar o arquivo.\nO formato de número é inválido.\nO número é menor do que o mínimo permitido.\nO número é maior do que o máximo permitido.\nA resposta para o quiz está incorreta.\nO código digitado está incorreto.\nO endereço de e-mail informado é inválido.\nA URL é inválida.\nO número de telefone é inválido.', 'Formulário de contato', '', 'publish', 'closed', 'closed', '', 'formulario-de-contato', '', '', '2017-12-05 09:16:40', '2017-12-05 11:16:40', '', 0, 'http://unika.handgran.com.br/?post_type=wpcf7_contact_form&#038;p=123', 0, 'wpcf7_contact_form', '', 0),
(124, 1, '2017-11-01 11:15:58', '2017-11-01 13:15:58', '', 'Diagnóstico de Cultura de Segurança', '', 'publish', 'closed', 'closed', '', 'diagnostico-de-cultura-de-seguranca', '', '', '2017-11-01 11:16:10', '2017-11-01 13:16:10', '', 0, 'http://unika.handgran.com.br/?page_id=124', 0, 'page', '', 0),
(125, 1, '2017-11-01 11:15:58', '2017-11-01 13:15:58', '', 'Diagnóstico de Cultura de Segurança', '', 'inherit', 'closed', 'closed', '', '124-revision-v1', '', '', '2017-11-01 11:15:58', '2017-11-01 13:15:58', '', 124, 'http://unika.handgran.com.br/124-revision-v1/', 0, 'revision', '', 0),
(126, 1, '2017-11-01 11:52:38', '0000-00-00 00:00:00', '', 'Palestras', '', 'draft', 'closed', 'closed', '', '', '', '', '2017-11-01 11:52:38', '2017-11-01 13:52:38', '', 0, 'http://unika.handgran.com.br/?page_id=126', 0, 'page', '', 0),
(127, 1, '2017-11-01 11:53:58', '2017-11-01 13:53:58', '', 'Palestras', '', 'publish', 'closed', 'closed', '', 'palestras', '', '', '2017-11-01 11:53:58', '2017-11-01 13:53:58', '', 0, 'http://unika.handgran.com.br/?page_id=127', 0, 'page', '', 0),
(128, 1, '2017-11-01 11:53:58', '2017-11-01 13:53:58', '', 'Palestras', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2017-11-01 11:53:58', '2017-11-01 13:53:58', '', 127, 'http://unika.handgran.com.br/127-revision-v1/', 0, 'revision', '', 0),
(129, 1, '2017-11-01 12:28:54', '2017-11-01 14:28:54', '', 'Workshops', '', 'publish', 'closed', 'closed', '', 'workshops', '', '', '2017-11-01 12:29:01', '2017-11-01 14:29:01', '', 0, 'http://unika.handgran.com.br/?page_id=129', 0, 'page', '', 0),
(130, 1, '2017-11-01 12:28:54', '2017-11-01 14:28:54', '', 'Workshops', '', 'inherit', 'closed', 'closed', '', '129-revision-v1', '', '', '2017-11-01 12:28:54', '2017-11-01 14:28:54', '', 129, 'http://unika.handgran.com.br/129-revision-v1/', 0, 'revision', '', 0),
(131, 1, '2017-11-01 13:22:36', '2017-11-01 15:22:36', '', 'Treinamentos', '', 'publish', 'closed', 'closed', '', 'treinamentos', '', '', '2017-11-01 13:22:36', '2017-11-01 15:22:36', '', 0, 'http://unika.handgran.com.br/?page_id=131', 0, 'page', '', 0),
(132, 1, '2017-11-01 13:22:36', '2017-11-01 15:22:36', '', 'Treinamentos', '', 'inherit', 'closed', 'closed', '', '131-revision-v1', '', '', '2017-11-01 13:22:36', '2017-11-01 15:22:36', '', 131, 'http://unika.handgran.com.br/131-revision-v1/', 0, 'revision', '', 0),
(133, 1, '2017-11-01 14:45:03', '2017-11-01 16:45:03', '', 'Ferramenta de Segurança Comportamental', '', 'publish', 'closed', 'closed', '', 'ferramenta-de-seguranca-comportamental', '', '', '2018-01-19 11:46:18', '2018-01-19 13:46:18', '', 0, 'http://unika.handgran.com.br/?page_id=133', 0, 'page', '', 0),
(134, 1, '2017-11-01 14:45:03', '2017-11-01 16:45:03', '', 'Ferramenta de Segurança Corporal', '', 'inherit', 'closed', 'closed', '', '133-revision-v1', '', '', '2017-11-01 14:45:03', '2017-11-01 16:45:03', '', 133, 'http://unika.handgran.com.br/133-revision-v1/', 0, 'revision', '', 0),
(241, 1, '2018-02-21 09:52:58', '2018-02-21 12:52:58', '', 'unikapatternbody-1', '', 'inherit', 'open', 'closed', '', 'unikapatternbody-1', '', '', '2018-02-21 09:52:58', '2018-02-21 12:52:58', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2018/02/unikapatternbody-1.png', 0, 'attachment', 'image/png', 0),
(136, 1, '2017-12-19 19:48:55', '2017-12-19 21:48:55', '', 'flexas', '', 'inherit', 'open', 'closed', '', 'flexas', '', '', '2017-12-19 19:48:55', '2017-12-19 21:48:55', '', 87, 'http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/unika_site/wp-content/uploads/2017/10/flexas.png', 0, 'attachment', 'image/png', 0),
(137, 1, '2017-12-19 19:48:57', '2017-12-19 21:48:57', '', 'mala', '', 'inherit', 'open', 'closed', '', 'mala', '', '', '2017-12-19 19:48:57', '2017-12-19 21:48:57', '', 87, 'http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/unika_site/wp-content/uploads/2017/10/mala.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `uk_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(138, 1, '2017-12-19 19:48:58', '2017-12-19 21:48:58', '', 'malaChave', '', 'inherit', 'open', 'closed', '', 'malachave', '', '', '2017-12-19 19:48:58', '2017-12-19 21:48:58', '', 87, 'http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/unika_site/wp-content/uploads/2017/10/malaChave.png', 0, 'attachment', 'image/png', 0),
(139, 1, '2017-12-19 19:49:00', '2017-12-19 21:49:00', '', 'palestras', '', 'inherit', 'open', 'closed', '', 'palestras-2', '', '', '2017-12-19 19:49:00', '2017-12-19 21:49:00', '', 87, 'http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/unika_site/wp-content/uploads/2017/10/palestras.png', 0, 'attachment', 'image/png', 0),
(140, 1, '2017-12-19 19:53:37', '2017-12-19 21:53:37', '', 'palestras2', '', 'inherit', 'open', 'closed', '', 'palestras2', '', '', '2017-12-19 19:53:37', '2017-12-19 21:53:37', '', 94, 'http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/unika_site/wp-content/uploads/2017/10/palestras2.png', 0, 'attachment', 'image/png', 0),
(141, 1, '2017-12-19 20:15:47', '2017-12-19 22:15:47', '', 'foto', '', 'inherit', 'open', 'closed', '', 'foto', '', '', '2017-12-19 20:15:47', '2017-12-19 22:15:47', '', 0, 'http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/unika_site/wp-content/uploads/2017/12/foto.jpg', 0, 'attachment', 'image/jpeg', 0),
(142, 1, '2017-12-19 20:17:43', '2017-12-19 22:17:43', '', 'foto', '', 'inherit', 'open', 'closed', '', 'foto-2', '', '', '2017-12-19 20:17:43', '2017-12-19 22:17:43', '', 0, 'http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/unika_site/wp-content/uploads/2017/12/foto-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(143, 1, '2017-12-19 20:19:23', '2017-12-19 22:19:23', '', 'Capturar', '', 'inherit', 'open', 'closed', '', 'capturar', '', '', '2017-12-19 20:19:23', '2017-12-19 22:19:23', '', 95, 'http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/unika_site/wp-content/uploads/2017/10/Capturar.png', 0, 'attachment', 'image/png', 0),
(144, 1, '2017-12-19 20:22:33', '2017-12-19 22:22:33', '', 'Capturar', '', 'inherit', 'open', 'closed', '', 'capturar-2', '', '', '2017-12-19 20:22:33', '2017-12-19 22:22:33', '', 95, 'http://hudsoncarolino.carollino.com.br/front-end-projetos/ui/unika_site/wp-content/uploads/2017/10/Capturar-1.png', 0, 'attachment', 'image/png', 0),
(170, 1, '2018-01-19 11:46:18', '2018-01-19 13:46:18', '', 'Ferramenta de Segurança Comportamental', '', 'inherit', 'closed', 'closed', '', '133-revision-v1', '', '', '2018-01-19 11:46:18', '2018-01-19 13:46:18', '', 133, 'http://unika.handgran.com.br/133-revision-v1/', 0, 'revision', '', 0),
(171, 1, '2018-01-19 12:09:55', '2018-01-19 14:09:55', '', 'Planejamento Estratégico de Segurança', '', 'publish', 'closed', 'closed', '', 'planejamento-estrategico-de-seguranca', '', '', '2018-01-19 12:09:55', '2018-01-19 14:09:55', '', 0, 'http://unika.handgran.com.br/?page_id=171', 0, 'page', '', 0),
(172, 1, '2018-01-19 12:09:55', '2018-01-19 14:09:55', '', 'Planejamento Estratégico de Segurança', '', 'inherit', 'closed', 'closed', '', '171-revision-v1', '', '', '2018-01-19 12:09:55', '2018-01-19 14:09:55', '', 171, 'http://unika.handgran.com.br/171-revision-v1/', 0, 'revision', '', 0),
(173, 3, '2018-01-19 15:32:21', '2018-01-19 17:32:21', '<strong>Comportamento humano: O que você precisa saber para evitar acidentes do trabalho</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-As luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Ele tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Consegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Sente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'Comportamento humano: O que você precisa saber para evitar  acidentes do trabalho', '', 'publish', 'open', 'open', '', 'comportamento-humano-o-que-voce-precisa-saber-para-evitar-acidentes-do-trabalho', '', '', '2018-02-21 11:06:17', '2018-02-21 14:06:17', '', 0, 'http://unika.handgran.com.br/?p=173', 5, 'post', '', 0),
(174, 1, '2018-01-19 15:32:21', '2018-01-19 17:32:21', '', 'COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:32:21', '2018-01-19 17:32:21', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(175, 1, '2018-01-19 15:57:37', '2018-01-19 17:57:37', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\n\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\n\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\n\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\n\n&nbsp;\n\n&nbsp;\n\n&nbsp;\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\n<tbody>\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\n</td>\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\n</td>\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\n</td>\n</tr>\n<tr style="mso-yfti-irow: 1;">\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\n</td>\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\nque a pessoa faz</span></p>\n</td>\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\nque acontece com a pessoa depois da ação/ ato.</span></p>\n</td>\n</tr>\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\n-As luvas são pequenas para mão dele</span></p>\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\n-Ele tem alergia ao material da luva</span></p>\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\n-Consegue ter um tato melhor sem luvas</span></p>\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\n-Sente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\n</td>\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\noficina retira as luvas para manusear uma peça</span></p>\n</td>\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\nconfortável</span></p>\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\nalérgico</span></p>\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\nmais precisão e menos tempo</span></p>\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\npara realizar o trabalho</span></p>\n</td>\n</tr>\n</tbody>\n</table>\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\n\n<strong> </strong>\n\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\n\n&nbsp;\n\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\n\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\n<ul>\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\n</ul>\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\n\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\n\n&nbsp;\n\nKarla Mikoski\n\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-autosave-v1', '', '', '2018-01-19 15:57:37', '2018-01-19 17:57:37', '', 173, 'http://unika.handgran.com.br/173-autosave-v1/', 0, 'revision', '', 0),
(178, 1, '2018-01-19 15:36:12', '2018-01-19 17:36:12', '', 'Untitled-1', '', 'inherit', 'open', 'closed', '', 'untitled-1', '', '', '2018-01-19 15:36:12', '2018-01-19 17:36:12', '', 173, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(180, 1, '2018-01-19 15:38:22', '2018-01-19 17:38:22', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class=" wp-image-178 aligncenter" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-1-300x121.jpg" alt="" width="391" height="170" />\r\n\r\n&nbsp;\r\n\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:38:22', '2018-01-19 17:38:22', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(182, 1, '2018-01-19 15:41:29', '2018-01-19 17:41:29', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class=" wp-image-178 aligncenter" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-1-300x121.jpg" alt="" width="462" height="230" />\r\n\r\n&nbsp;\r\n\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:41:29', '2018-01-19 17:41:29', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(181, 1, '2018-01-19 15:39:34', '2018-01-19 17:39:34', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="size-medium wp-image-178 aligncenter" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-1-300x121.jpg" alt="" width="300" height="121" />\r\n\r\n&nbsp;\r\n\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:39:34', '2018-01-19 17:39:34', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(183, 1, '2018-01-19 15:41:46', '2018-01-19 17:41:46', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class=" wp-image-178 aligncenter" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-1-300x121.jpg" alt="" width="335" height="133" />\r\n\r\n&nbsp;\r\n\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:41:46', '2018-01-19 17:41:46', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(184, 1, '2018-01-19 15:44:05', '2018-01-19 17:44:05', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nAs luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nEle tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nConsegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nSente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:44:05', '2018-01-19 17:44:05', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `uk_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(185, 1, '2018-01-19 15:45:25', '2018-01-19 17:45:25', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nAs luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nEle tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nConsegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nSente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:45:25', '2018-01-19 17:45:25', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(186, 1, '2018-01-19 15:53:32', '2018-01-19 17:53:32', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-As luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nEle tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nConsegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nSente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:53:32', '2018-01-19 17:53:32', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(187, 1, '2018-01-19 15:53:48', '2018-01-19 17:53:48', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-As luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-Ele tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nConsegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nSente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:53:48', '2018-01-19 17:53:48', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(188, 1, '2018-01-19 15:53:58', '2018-01-19 17:53:58', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-As luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-Ele tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-Consegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-\r\nSente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:53:58', '2018-01-19 17:53:58', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `uk_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(189, 1, '2018-01-19 15:54:09', '2018-01-19 17:54:09', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-As luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-Ele tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-Consegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">-Sente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:54:09', '2018-01-19 17:54:09', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(190, 1, '2018-01-19 15:54:32', '2018-01-19 17:54:32', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;"><br>-As luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;"><br>-Ele tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;"><br>-Consegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;"><br>-Sente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 15:54:32', '2018-01-19 17:54:32', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(191, 3, '2018-01-19 16:01:14', '2018-01-19 18:01:14', 'Quando se trata de <strong>DDS – Diálogos Diários de Segurança no Trabalho</strong> - uma das principais questões que se observa na circulação de e-mails e fóruns de discussão nos sites voltados à segurança do trabalho é: “Alguém tem um tema para eu trabalhar no DDS?”. Com base nesta repetida dúvida dos profissionais que trabalham nesta área, nasce este texto, com a intenção de refletir sobre onde podemos encontrar os melhores temas para se trabalhar no Diálogos Diários de Segurança no Trabalho.\r\n\r\nÉ comum encontrarmos pessoas diante do computador navegando na internet em busca de temas e textos. Normalmente, atribuímos a qualidade do DDS à qualidade do texto. Mas será que é só isso mesmo? Será que uma simples leitura de um texto diante de um grupo de pessoas pode ser considerado um “DDS – Diálogo diário de segurança”?\r\n\r\nA começar pelo próprio nome desta ferramenta, poderíamos deduzir que uma leitura – em que uma pessoa lê e o restante dos participantes ouve – não pode ser considerado um diálogo. Um diálogo pressupõe que no mínimo duas pessoas conversem ou debatam sobre tema/ assunto.\r\n\r\nA escolha do tema não é algo tão simples quanto se costuma fazer. Um dos princípios da andragogia – parte da pedagogia que estuda o processo de ensino e aprendizagem de adultos – diz que os adultos só aprendem aquilo que eles têm necessidade e/ ou aquilo que terá uma aplicação prática na vida cotidiana.\r\n\r\nO que normalmente ocorre na escolha do tema é que o instrutor ou condutor do DDS faz esta escolha baseado naquilo que <strong>ele</strong> acredita que será mais interessante ou tornará mais interessante a sua fala para o grupo. Aqui já reside uma das barreiras para que o DDS não tenha uma boa efetividade, pois o tema deve ser escolhido não com base no que o instrutor/ condutor quer transmitir, mas sim com base nas necessidades de aprendizagem dos participantes dos Diálogos Diários de Segurança no Trabalho.\r\n\r\nA escolha do tema/ assunto é apenas um dos passos para a realização de um DDS de qualidade. Um bom processo de entrevista e coleta de informações com os próprios participantes costuma ser uma maneira eficaz de trazer temas que de fato agreguem ao público.\r\n\r\nApesar do DDS ser um momento que costuma ter uma curta duração, entre 5 e 15 minutos (recomendável), ele exige um mínimo de planejamento, de preparo, para que surta o resultado esperado: aprendizagem, ou seja, conhecimento adquirido e aplicado no dia a dia! Use estas dicas para escolher seu tema de seu DDS e um requisito bastante importante para gerar aprendizagem terá sido atendido.\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)\r\n\r\n&nbsp;\r\n\r\nQuer receber um artigo completo sobre todos os cuidados para realizar um DDS de qualidade? Clique aqui.', 'Preciso escolher um tema para o DDS - Diálogos Diários de Segurança no Trabalho. O que devo levar em consideração?', '', 'publish', 'open', 'open', '', 'preciso-escolher-um-tema-para-o-dds-dialogos-diarios-de-seguranca-no-trabalho-o-que-devo-levar-em-consideracao', '', '', '2018-02-21 11:08:21', '2018-02-21 14:08:21', '', 0, 'http://unika.handgran.com.br/?p=191', 4, 'post', '', 0),
(193, 1, '2018-01-19 16:01:14', '2018-01-19 18:01:14', 'Quando se trata de <strong>DDS – Diálogos Diários de Segurança no Trabalho</strong> - uma das principais questões que se observa na circulação de e-mails e fóruns de discussão nos sites voltados à segurança do trabalho é: “Alguém tem um tema para eu trabalhar no DDS?”. Com base nesta repetida dúvida dos profissionais que trabalham nesta área, nasce este texto, com a intenção de refletir sobre onde podemos encontrar os melhores temas para se trabalhar no Diálogos Diários de Segurança no Trabalho.\r\n\r\nÉ comum encontrarmos pessoas diante do computador navegando na internet em busca de temas e textos. Normalmente, atribuímos a qualidade do DDS à qualidade do texto. Mas será que é só isso mesmo? Será que uma simples leitura de um texto diante de um grupo de pessoas pode ser considerado um “DDS – Diálogo diário de segurança”?\r\n\r\nA começar pelo próprio nome desta ferramenta, poderíamos deduzir que uma leitura – em que uma pessoa lê e o restante dos participantes ouve – não pode ser considerado um diálogo. Um diálogo pressupõe que no mínimo duas pessoas conversem ou debatam sobre tema/ assunto.\r\n\r\nA escolha do tema não é algo tão simples quanto se costuma fazer. Um dos princípios da andragogia – parte da pedagogia que estuda o processo de ensino e aprendizagem de adultos – diz que os adultos só aprendem aquilo que eles têm necessidade e/ ou aquilo que terá uma aplicação prática na vida cotidiana.\r\n\r\nO que normalmente ocorre na escolha do tema é que o instrutor ou condutor do DDS faz esta escolha baseado naquilo que <strong>ele</strong> acredita que será mais interessante ou tornará mais interessante a sua fala para o grupo. Aqui já reside uma das barreiras para que o DDS não tenha uma boa efetividade, pois o tema deve ser escolhido não com base no que o instrutor/ condutor quer transmitir, mas sim com base nas necessidades de aprendizagem dos participantes dos Diálogos Diários de Segurança no Trabalho.\r\n\r\nA escolha do tema/ assunto é apenas um dos passos para a realização de um DDS de qualidade. Um bom processo de entrevista e coleta de informações com os próprios participantes costuma ser uma maneira eficaz de trazer temas que de fato agreguem ao público.\r\n\r\nApesar do DDS ser um momento que costuma ter uma curta duração, entre 5 e 15 minutos (recomendável), ele exige um mínimo de planejamento, de preparo, para que surta o resultado esperado: aprendizagem, ou seja, conhecimento adquirido e aplicado no dia a dia! Use estas dicas para escolher seu tema de seu DDS e um requisito bastante importante para gerar aprendizagem terá sido atendido.\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)\r\n\r\n&nbsp;\r\n\r\nQuer receber um artigo completo sobre todos os cuidados para realizar um DDS de qualidade? Clique aqui.', 'Preciso escolher um tema para o DDS - Diálogos Diários de Segurança no Trabalho. O que devo levar em consideração?', '', 'inherit', 'closed', 'closed', '', '191-revision-v1', '', '', '2018-01-19 16:01:14', '2018-01-19 18:01:14', '', 191, 'http://unika.handgran.com.br/191-revision-v1/', 0, 'revision', '', 0),
(194, 3, '2018-01-19 16:05:52', '2018-01-19 18:05:52', 'A Unika compreende a evolução da Cultura de Segurança com base  no conceito desenvolvido pelo Energy Institute na construção do Programa Corações e Mentes (Hearts and Minds). O projeto apresenta um estudo detalhado de Cultura de SSMA e seus possíveis níveis de maturidade. Esta análiseestá disponível ao público e pode ser acessada por meio do site: <a href="http://www.energyinst.org/heartsandminds">www.energyinst.org/heartsandminds</a>.\r\n\r\nNo programa, os níveis de maturidade de Cultura são apresentados como uma escada evolutiva, não mais como um gráfico em curva descendente – utilizado na famosa Curva de Bradley -  como pode ser observado na figura a seguir:\r\n\r\n<strong>ESCADA EVOLUTIVA DA CULTURA DE SEGURANÇA DO PROGRAMA CORAÇÕES E MENTES</strong>\r\n\r\n&nbsp;\r\n\r\n<img class="wp-image-195 aligncenter" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-2-300x164.jpg" alt="" width="403" height="214" />\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\nNeste programa, a Cultura Organizacional é apresentada como uma escada evolutiva, em que cada nível possui características distintas e é uma progressão do anterior. Isso quer dizer que, à medida em que a Cultura se consolida em um dos degraus evolutivos, todo o aprendizado e os ganhos conquistados não se perdem e são levados para o próximo estágio. Veja a descrição de cada um dos níveis evolutivos:\r\n\r\n<strong>Patológico</strong>: nível em que as pessoas não se preocupam com a segurança ou com os aspectos preventivos. Se orientam somente para atender a legislação e se empenham em não serem “pegas” descumprindo regras. É comum ouvir afirmações do tipo: “Claro que temos acidentes, este é um negócio perigoso”.\r\n\r\n<strong>Reativo</strong>: é o nível em que a segurança é levada a sério, apesar de só receber atenção suficiente depois que algo deu errado. As seguintes frases são frequentes falas dos líderes: “Se os empregados fizessem como eu falei, não teria acontecido”; “Precisamos fazer as pessoas obedecerem as regras.” Por outro lado, ouve-se dos empregados: “Mas o técnico de segurança passou aqui e não disse nada”; “Foi meu supervisou que falou que tínhamos que agilizar para entregar até as 13 horas”. O que demonstra um padrão de dependência e co-dependência clara neste nível de maturidade cultural.\r\n\r\n<strong>Calculista</strong>: neste nível a organização se sente confortável em relação ao sistema e aos números. O modelo de gestão está bem implantado e os líderes frequentemente conversam com os empregados sobre a importância da segurança, apesar de sua atenção estar normalmente voltada para a conformidade no atendimento de regras simples. Nestes casos, o objetivo é demonstrar que a segurança é levada a sério. Existe uma ênfase na coleta de estatísticas com bônus vinculados a elas. E de fato o bom funcionamento do sistema estimula os comportamentos adequados dentro de empresas com este nível cultural. E as empresas terceirizadas são escolhidas por seus resultados e não somente por serem mais baratas. Muitos dados são coletados e analisados, as pessoas se sentem à vontade para fazer alterações em procedimentos e processos. Há muitas auditorias. Quando algo dá errado, a equipe se surpreende porque considera que o sistema deveria ter funcionado, evitando perdas.\r\n\r\n<strong>Proativo</strong>: neste nível a organização volta a sua atenção para o futuro e não para dados do passado. Os empregados se envolvem na prática com as questões de segurança. O desempenho global em segurança é levado em consideração para promoções e reconhecimentos. Os líderes tratam a segurança como um valor e todas as decisões se baseiam em Saúde, Segurança e Meio Ambiente. A equipe voltada aos processos se pergunta: “Estamos fazendo a coisa certa?”, ao invés de focar nos acidentes.\r\n\r\nNo nível proativo os gestores conhecem a sua organização e sabem onde residem os problemas, bem como os empregados compreendem o que é esperado deles. Gestores e empregados estão em sintonia nas questões de segurança por meio de uma confiança bidirecional desenvolvida ao longo do tempo, o que os permite não recorrer tanto à burocracia e as redundâncias. A divisão de trabalho é menos sofrida porque os empregados estão dispostos a assumir responsabilidades.\r\n\r\n<strong>Generativo</strong>: neste nível se encontram as organizações sustentáveis e de alta confiança. Normalmente são negócios que apresentam um processo de baixa variabilidade, o que confere uma característica de confiabilidade operacional.  São empresas que sabem que o bom resultado de SSMA é um indicador de bom desempenho nos negócios. Elas definem os mais altos padrões ao invés de somente atender à conformidade legal. São extremamente honestas em relação aos fracassos e usam isso para melhorar e não para identificar culpados. Os gestores sabem realmente o que acontece porque a força de trabalho deseja informá-los. O respeito pelas pessoas está implícito em todas as decisões. Os empregados estão sempre atentos, precavidos para o que pode dar de errado, estudando novas possibilidades preventivas e treinando respostas constantemente.\r\n\r\n&nbsp;\r\n\r\nDentro de um processo diagnóstico de cultura são avaliadas 7 dimensões que podem ou não estar presentes na organização:\r\n<ol>\r\n 	<li>Liderança e comprometimento</li>\r\n 	<li>Política e objetivos estratégicos</li>\r\n 	<li>Responsabilidades, recursos e padrões</li>\r\n 	<li>Gestão de perigos e riscos</li>\r\n 	<li>Procedimentos</li>\r\n 	<li>Implantação e monitoramento de ações e iniciativas</li>\r\n 	<li>Auditorias/ Sistema de gestão</li>\r\n</ol>\r\nPara cada uma das dimensões são coletadas percepções de grupos homogêneos por nível hierárquico, além de análise documental e observaçãoativa em campo. Mais do que compreender as ações implantadas em cada dimensão, a coleta de dados visa compreender o padrão de comportamento predominante na prática de cada ação, decisão, iniciativa e/ou ferramenta de gestão.\r\n\r\nOs resultados do diagnóstico indicam como está o nível de maturidade de cultura e sugerem ações para avanço em cada uma das dimensões.\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)\r\n\r\n&nbsp;\r\n\r\nFicou interessado em realizar um processo diagnóstico da Cultura de Segurança da sua empresa? Fale com a gente.\r\n\r\n&nbsp;', 'Evolução da Cultura de Segurança', '', 'publish', 'open', 'open', '', 'evolucao-da-cultura-de-seguranca', '', '', '2018-02-21 10:56:40', '2018-02-21 13:56:40', '', 0, 'http://unika.handgran.com.br/?p=194', 3, 'post', '', 0),
(195, 1, '2018-01-19 16:04:42', '2018-01-19 18:04:42', '', 'Untitled-2', '', 'inherit', 'open', 'closed', '', 'untitled-2', '', '', '2018-01-19 16:04:42', '2018-01-19 18:04:42', '', 194, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(196, 1, '2018-01-19 16:05:20', '2018-01-19 18:05:20', '', 'Vector Concept of Growth', 'Vector Concept of Growth with arrow and crane hook, isolated on white background', 'inherit', 'open', 'closed', '', 'vector-concept-of-growth', '', '', '2018-01-19 16:05:20', '2018-01-19 18:05:20', '', 194, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/Vector-Concept-of-Growth-482548689_3157x3157.jpeg', 0, 'attachment', 'image/jpeg', 0),
(197, 1, '2018-01-19 16:05:52', '2018-01-19 18:05:52', 'A Unika compreende a evolução da Cultura de Segurança com base  no conceito desenvolvido pelo Energy Institute na construção do Programa Corações e Mentes (Hearts and Minds). O projeto apresenta um estudo detalhado de Cultura de SSMA e seus possíveis níveis de maturidade. Esta análiseestá disponível ao público e pode ser acessada por meio do site: <a href="http://www.energyinst.org/heartsandminds">www.energyinst.org/heartsandminds</a>.\r\n\r\nNo programa, os níveis de maturidade de Cultura são apresentados como uma escada evolutiva, não mais como um gráfico em curva descendente – utilizado na famosa Curva de Bradley -  como pode ser observado na figura a seguir:\r\n\r\n<strong>ESCADA EVOLUTIVA DA CULTURA DE SEGURANÇA DO PROGRAMA CORAÇÕES E MENTES</strong>\r\n\r\n<img class="wp-image-195 aligncenter" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-2-300x164.jpg" alt="" width="403" height="214" />\r\n\r\nNeste programa, a Cultura Organizacional é apresentada como uma escada evolutiva, em que cada nível possui características distintas e é uma progressão do anterior. Isso quer dizer que, à medida em que a Cultura se consolida em um dos degraus evolutivos, todo o aprendizado e os ganhos conquistados não se perdem e são levados para o próximo estágio. Veja a descrição de cada um dos níveis evolutivos:\r\n\r\n<strong>Patológico</strong>: nível em que as pessoas não se preocupam com a segurança ou com os aspectos preventivos. Se orientam somente para atender a legislação e se empenham em não serem “pegas” descumprindo regras. É comum ouvir afirmações do tipo: “Claro que temos acidentes, este é um negócio perigoso”.\r\n\r\n<strong>Reativo</strong>: é o nível em que a segurança é levada a sério, apesar de só receber atenção suficiente depois que algo deu errado. As seguintes frases são frequentes falas dos líderes: “Se os empregados fizessem como eu falei, não teria acontecido”; “Precisamos fazer as pessoas obedecerem as regras.” Por outro lado, ouve-se dos empregados: “Mas o técnico de segurança passou aqui e não disse nada”; “Foi meu supervisou que falou que tínhamos que agilizar para entregar até as 13 horas”. O que demonstra um padrão de dependência e co-dependência clara neste nível de maturidade cultural.\r\n\r\n<strong>Calculista</strong>: neste nível a organização se sente confortável em relação ao sistema e aos números. O modelo de gestão está bem implantado e os líderes frequentemente conversam com os empregados sobre a importância da segurança, apesar de sua atenção estar normalmente voltada para a conformidade no atendimento de regras simples. Nestes casos, o objetivo é demonstrar que a segurança é levada a sério. Existe uma ênfase na coleta de estatísticas com bônus vinculados a elas. E de fato o bom funcionamento do sistema estimula os comportamentos adequados dentro de empresas com este nível cultural. E as empresas terceirizadas são escolhidas por seus resultados e não somente por serem mais baratas. Muitos dados são coletados e analisados, as pessoas se sentem à vontade para fazer alterações em procedimentos e processos. Há muitas auditorias. Quando algo dá errado, a equipe se surpreende porque considera que o sistema deveria ter funcionado, evitando perdas.\r\n\r\n<strong>Proativo</strong>: neste nível a organização volta a sua atenção para o futuro e não para dados do passado. Os empregados se envolvem na prática com as questões de segurança. O desempenho global em segurança é levado em consideração para promoções e reconhecimentos. Os líderes tratam a segurança como um valor e todas as decisões se baseiam em Saúde, Segurança e Meio Ambiente. A equipe voltada aos processos se pergunta: “Estamos fazendo a coisa certa?”, ao invés de focar nos acidentes.\r\n\r\nNo nível proativo os gestores conhecem a sua organização e sabem onde residem os problemas, bem como os empregados compreendem o que é esperado deles. Gestores e empregados estão em sintonia nas questões de segurança por meio de uma confiança bidirecional desenvolvida ao longo do tempo, o que os permite não recorrer tanto à burocracia e as redundâncias. A divisão de trabalho é menos sofrida porque os empregados estão dispostos a assumir responsabilidades.\r\n\r\n<strong>Generativo</strong>: neste nível se encontram as organizações sustentáveis e de alta confiança. Normalmente são negócios que apresentam um processo de baixa variabilidade, o que confere uma característica de confiabilidade operacional.  São empresas que sabem que o bom resultado de SSMA é um indicador de bom desempenho nos negócios. Elas definem os mais altos padrões ao invés de somente atender à conformidade legal. São extremamente honestas em relação aos fracassos e usam isso para melhorar e não para identificar culpados. Os gestores sabem realmente o que acontece porque a força de trabalho deseja informá-los. O respeito pelas pessoas está implícito em todas as decisões. Os empregados estão sempre atentos, precavidos para o que pode dar de errado, estudando novas possibilidades preventivas e treinando respostas constantemente.\r\n\r\n&nbsp;\r\n\r\nDentro de um processo diagnóstico de cultura são avaliadas 7 dimensões que podem ou não estar presentes na organização:\r\n<ol>\r\n 	<li>Liderança e comprometimento</li>\r\n 	<li>Política e objetivos estratégicos</li>\r\n 	<li>Responsabilidades, recursos e padrões</li>\r\n 	<li>Gestão de perigos e riscos</li>\r\n 	<li>Procedimentos</li>\r\n 	<li>Implantação e monitoramento de ações e iniciativas</li>\r\n 	<li>Auditorias/ Sistema de gestão</li>\r\n</ol>\r\nPara cada uma das dimensões são coletadas percepções de grupos homogêneos por nível hierárquico, além de análise documental e observaçãoativa em campo. Mais do que compreender as ações implantadas em cada dimensão, a coleta de dados visa compreender o padrão de comportamento predominante na prática de cada ação, decisão, iniciativa e/ou ferramenta de gestão.\r\n\r\nOs resultados do diagnóstico indicam como está o nível de maturidade de cultura e sugerem ações para avanço em cada uma das dimensões.\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)\r\n\r\n&nbsp;\r\n\r\nFicou interessado em realizar um processo diagnóstico da Cultura de Segurança da sua empresa? Fale com a gente.\r\n\r\n&nbsp;', 'Evolução da Cultura de Segurança', '', 'inherit', 'closed', 'closed', '', '194-revision-v1', '', '', '2018-01-19 16:05:52', '2018-01-19 18:05:52', '', 194, 'http://unika.handgran.com.br/194-revision-v1/', 0, 'revision', '', 0),
(198, 1, '2018-01-19 16:06:48', '2018-01-19 18:06:48', 'A Unika compreende a evolução da Cultura de Segurança com base  no conceito desenvolvido pelo Energy Institute na construção do Programa Corações e Mentes (Hearts and Minds). O projeto apresenta um estudo detalhado de Cultura de SSMA e seus possíveis níveis de maturidade. Esta análiseestá disponível ao público e pode ser acessada por meio do site: <a href="http://www.energyinst.org/heartsandminds">www.energyinst.org/heartsandminds</a>.\r\n\r\nNo programa, os níveis de maturidade de Cultura são apresentados como uma escada evolutiva, não mais como um gráfico em curva descendente – utilizado na famosa Curva de Bradley -  como pode ser observado na figura a seguir:\r\n\r\n<strong>ESCADA EVOLUTIVA DA CULTURA DE SEGURANÇA DO PROGRAMA CORAÇÕES E MENTES</strong>\r\n\r\n&nbsp;\r\n\r\n<img class="wp-image-195 aligncenter" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-2-300x164.jpg" alt="" width="403" height="214" />\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\nNeste programa, a Cultura Organizacional é apresentada como uma escada evolutiva, em que cada nível possui características distintas e é uma progressão do anterior. Isso quer dizer que, à medida em que a Cultura se consolida em um dos degraus evolutivos, todo o aprendizado e os ganhos conquistados não se perdem e são levados para o próximo estágio. Veja a descrição de cada um dos níveis evolutivos:\r\n\r\n<strong>Patológico</strong>: nível em que as pessoas não se preocupam com a segurança ou com os aspectos preventivos. Se orientam somente para atender a legislação e se empenham em não serem “pegas” descumprindo regras. É comum ouvir afirmações do tipo: “Claro que temos acidentes, este é um negócio perigoso”.\r\n\r\n<strong>Reativo</strong>: é o nível em que a segurança é levada a sério, apesar de só receber atenção suficiente depois que algo deu errado. As seguintes frases são frequentes falas dos líderes: “Se os empregados fizessem como eu falei, não teria acontecido”; “Precisamos fazer as pessoas obedecerem as regras.” Por outro lado, ouve-se dos empregados: “Mas o técnico de segurança passou aqui e não disse nada”; “Foi meu supervisou que falou que tínhamos que agilizar para entregar até as 13 horas”. O que demonstra um padrão de dependência e co-dependência clara neste nível de maturidade cultural.\r\n\r\n<strong>Calculista</strong>: neste nível a organização se sente confortável em relação ao sistema e aos números. O modelo de gestão está bem implantado e os líderes frequentemente conversam com os empregados sobre a importância da segurança, apesar de sua atenção estar normalmente voltada para a conformidade no atendimento de regras simples. Nestes casos, o objetivo é demonstrar que a segurança é levada a sério. Existe uma ênfase na coleta de estatísticas com bônus vinculados a elas. E de fato o bom funcionamento do sistema estimula os comportamentos adequados dentro de empresas com este nível cultural. E as empresas terceirizadas são escolhidas por seus resultados e não somente por serem mais baratas. Muitos dados são coletados e analisados, as pessoas se sentem à vontade para fazer alterações em procedimentos e processos. Há muitas auditorias. Quando algo dá errado, a equipe se surpreende porque considera que o sistema deveria ter funcionado, evitando perdas.\r\n\r\n<strong>Proativo</strong>: neste nível a organização volta a sua atenção para o futuro e não para dados do passado. Os empregados se envolvem na prática com as questões de segurança. O desempenho global em segurança é levado em consideração para promoções e reconhecimentos. Os líderes tratam a segurança como um valor e todas as decisões se baseiam em Saúde, Segurança e Meio Ambiente. A equipe voltada aos processos se pergunta: “Estamos fazendo a coisa certa?”, ao invés de focar nos acidentes.\r\n\r\nNo nível proativo os gestores conhecem a sua organização e sabem onde residem os problemas, bem como os empregados compreendem o que é esperado deles. Gestores e empregados estão em sintonia nas questões de segurança por meio de uma confiança bidirecional desenvolvida ao longo do tempo, o que os permite não recorrer tanto à burocracia e as redundâncias. A divisão de trabalho é menos sofrida porque os empregados estão dispostos a assumir responsabilidades.\r\n\r\n<strong>Generativo</strong>: neste nível se encontram as organizações sustentáveis e de alta confiança. Normalmente são negócios que apresentam um processo de baixa variabilidade, o que confere uma característica de confiabilidade operacional.  São empresas que sabem que o bom resultado de SSMA é um indicador de bom desempenho nos negócios. Elas definem os mais altos padrões ao invés de somente atender à conformidade legal. São extremamente honestas em relação aos fracassos e usam isso para melhorar e não para identificar culpados. Os gestores sabem realmente o que acontece porque a força de trabalho deseja informá-los. O respeito pelas pessoas está implícito em todas as decisões. Os empregados estão sempre atentos, precavidos para o que pode dar de errado, estudando novas possibilidades preventivas e treinando respostas constantemente.\r\n\r\n&nbsp;\r\n\r\nDentro de um processo diagnóstico de cultura são avaliadas 7 dimensões que podem ou não estar presentes na organização:\r\n<ol>\r\n 	<li>Liderança e comprometimento</li>\r\n 	<li>Política e objetivos estratégicos</li>\r\n 	<li>Responsabilidades, recursos e padrões</li>\r\n 	<li>Gestão de perigos e riscos</li>\r\n 	<li>Procedimentos</li>\r\n 	<li>Implantação e monitoramento de ações e iniciativas</li>\r\n 	<li>Auditorias/ Sistema de gestão</li>\r\n</ol>\r\nPara cada uma das dimensões são coletadas percepções de grupos homogêneos por nível hierárquico, além de análise documental e observaçãoativa em campo. Mais do que compreender as ações implantadas em cada dimensão, a coleta de dados visa compreender o padrão de comportamento predominante na prática de cada ação, decisão, iniciativa e/ou ferramenta de gestão.\r\n\r\nOs resultados do diagnóstico indicam como está o nível de maturidade de cultura e sugerem ações para avanço em cada uma das dimensões.\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)\r\n\r\n&nbsp;\r\n\r\nFicou interessado em realizar um processo diagnóstico da Cultura de Segurança da sua empresa? Fale com a gente.\r\n\r\n&nbsp;', 'Evolução da Cultura de Segurança', '', 'inherit', 'closed', 'closed', '', '194-revision-v1', '', '', '2018-01-19 16:06:48', '2018-01-19 18:06:48', '', 194, 'http://unika.handgran.com.br/194-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `uk_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(199, 3, '2018-01-19 16:10:09', '2018-01-19 18:10:09', 'Você já ouviu alguém dizer: “Olha, é melhor não comemorar quando a empresa alcança zero acidente porque sempre depois de uma comemoração tem um acidente. Será que é coisa do destino, da sorte (ou falta dela), ou ainda uma contingência diversas vezes ensaiada? Ou será que de verdade estamos sendo enganados pelos números?\r\nVamos desvendar este mistério! O que ocorre é que na área de segurança acontece algo cruel: medimos aquilo que não queremos que aconteça, o acidente. E quando a nossa métrica é invertida, fica bastante difícil saber se estamos no caminho correto, pois não temos outros indicadores que nos apontem isso. Por isso, os indicadores preventivos são tão importantes.\r\nExemplificando: se por alguma razão um líder perde as metas de produção nos primeiros 15 dias do mês, ele sabe que terá outros 15 dias para tentar recuperar o que não produziu. Mas com o indicador usual de segurança – taxa de frequência de acidentes – não é isso que ocorre, se acontece um acidente no primeiro dia do mês, não há o que ele possa fazer para recuperar este indicador.\r\nPortanto, quando comemoramos o zero acidente como um resultado alcançado, isso pode ter sido fruto de sorte, do acaso ou do trabalho das pessoas envolvidas, mas infelizmente este indicador não nos diz se nesta empresa o processo é seguro.\r\nOs indicadores preventivos são construidos respeitando a cultura e características de cada organização justamente para tentar mensurar de maneira antecipada se de fato estão sendo tomadas todas as ações necessárias e da maneira adequada para evitar que os acidentes ocorram. Além disso, com tais indicadores, é possível reconhecer as pessoas que estão comprometidas com a segurança e celebrar os progressos e conquistas alcançadas.\r\nPrecisa de ajuda? Nós vamos adorar contribuir na construção dos indicadores preventivos da sua empresa.\r\nKarla Mikoski\r\nCRP (08/09399)', 'Indicadores Preventivos: por que são tão importantes?', '', 'publish', 'open', 'open', '', 'indicadores-preventivos-por-que-sao-tao-importantes', '', '', '2018-02-15 16:23:19', '2018-02-15 18:23:19', '', 0, 'http://unika.handgran.com.br/?p=199', 0, 'post', '', 0),
(200, 1, '2018-01-19 16:09:52', '2018-01-19 18:09:52', '', 'concept of security', 'wall with the drawing of dark clouds, rain and one umbrella, concept of protection and security (3d render)', 'inherit', 'open', 'closed', '', 'concept-of-security', '', '', '2018-01-19 16:09:52', '2018-01-19 18:09:52', '', 199, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/concept-of-security-537842358_3927x2540.jpeg', 0, 'attachment', 'image/jpeg', 0),
(201, 1, '2018-01-19 16:10:09', '2018-01-19 18:10:09', 'Você já ouviu alguém dizer: “Olha, é melhor não comemorar quando a empresa\r\nalcança zero acidente porque sempre depois de uma comemoração tem um\r\nacidente.”acidente. ” Será que é coisa do destino, da sorte (ou falta dela), ou ainda uma\r\ncontingência diversas vezes ensaiada? Ou será que de verdade estamos sendo\r\nenganados pelos números?\r\nVamos desvendar este mistério!\r\nO que ocorre é que na área de segurança acontece algo cruel: medimos aquilo que\r\nnão queremos que aconteça, o acidente. E quando a nossa métrica é invertida, fica\r\nbastante difícil saber se estamos no caminho correto, pois não temos outros indicadores\r\nque nos apontem isso. Por isso, os indicadores preventivos são tão importantes.\r\nExemplificando: se por alguma razão um líder perde as metas de produção nos\r\nprimeiros 15 dias do mês, ele sabe que terá outros 15 dias para tentar recuperar o que não\r\nproduziu. Mas com o indicador usual de segurança – taxa de frequência de acidentes –\r\nnão é isso que ocorre, se acontece um acidente no primeiro dia do mês, não há o que ele\r\npossa fazer para recuperar este indicador.\r\nPortanto, quando comemoramos o zero acidente como um resultado alcançado, isso\r\npode ter sido fruto de sorte, do acaso ou do trabalho das pessoas envolvidas, mas\r\ninfelizmente este indicador não nos diz se nesta empresa o processo é seguro.\r\nOs indicadores preventivos são construidos respeitando a cultura e características de\r\ncada organização justamente para tentar mensurar de maneira antecipada se de fato estão\r\nsendo tomadas todas as ações necessárias e da maneira adequada para evitar que os\r\nacidentes ocorram. Além disso, com tais indicadores, é possível reconhecer as pessoas\r\nque estão comprometidas com a segurança e celebrar os progressos e conquistas\r\nalcançadas.\r\nPrecisa de ajuda? Nós vamos adorar contribuir na construção dos indicadores\r\npreventivos da sua empresa.\r\nKarla Mikoski\r\nCRP (08/09399)', 'Indicadores Preventivos: por que são tão importantes?', '', 'inherit', 'closed', 'closed', '', '199-revision-v1', '', '', '2018-01-19 16:10:09', '2018-01-19 18:10:09', '', 199, 'http://unika.handgran.com.br/199-revision-v1/', 0, 'revision', '', 0),
(202, 3, '2018-01-19 16:11:20', '2018-01-19 18:11:20', 'A <strong>OAC - Observação e Abordagem de Comportamentos em Segurança é</strong> uma ferramenta educativa e amplamente utilizada no mercado com a finalidade de abordar comportamentos que foram observados, por meio de uma metodologia eficaz, que resulta em mudança de comportamentos de risco e manutenção de comportamentos seguros.\r\n\r\nA Unika utiliza uma metodologia com base em conceitos estabelecidos pelo psicólogo norte americano Scott Geller que tem como premissa que o diálogo estabelecido entre líder e liderado durante a realização da ferramenta precisa ter como base a confiança interpessoal.\r\n\r\nEste método exige um preparo robusto por parte daqueles que serão observadores (responsáveis por conduzir a ferramenta) para que o diálogo realmente aconteça. Para tal, é recomendado que sejam figuras de autoridade dentro da organização – líderes formais ou informais – pois são quem teoricamente deveriam estar melhor preparados para trabalhar com comunicação e gestão, e caso ainda não estejam, na OAC terão esta oportunidade de desenvolvimento.\r\n\r\nOs objetivos da OAC - Observação e Abordagem de Comportamentos em Segurança são vários:\r\n<ul>\r\n 	<li>Aprimorar a abordagem e comunicação entre líderes e liderados sobre os comportamentos em segurança do trabalho;</li>\r\n 	<li>Melhorar a percepção de riscos de líderes e liderados;</li>\r\n 	<li>Trabalhar com maior profundidade no entendimento dos ativadores ou causas do comportamento de risco;</li>\r\n 	<li>Criar um ambiente de confiança que favoreça os comportamentos de segurança em área;</li>\r\n 	<li>Utilizar o conhecimento e experiência do próprio empregado a favor da solução dos comportamentos de risco encontrados, tornando-o mais autônomo e responsável;</li>\r\n 	<li>Romper o ciclo de dependência e codependência. (leia mais sobre isso aqui)</li>\r\n</ul>\r\nPromover maior conscientização e auto responsabilização sobre a preservação da vida das pessoas é o maior benefício que esta ferramenta trás. A OAC oferece uma dimensão bastante prática aos líderes sobre o que precisam fazer para atuar no comportamento de seus empregados imediatamente.\r\n\r\n&nbsp;\r\n\r\nQuer conhecer mais sobre esta ferramenta? Entre em contato conosco.\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)\r\n\r\n&nbsp;', 'OAC - Observação e Abordagem de Comportamentos em Segurança – Por que implantar?', '', 'publish', 'open', 'open', '', 'oac-observacao-e-abordagem-de-comportamentos-em-seguranca-por-que-implantar', '', '', '2018-02-21 10:49:12', '2018-02-21 13:49:12', '', 0, 'http://unika.handgran.com.br/?p=202', 2, 'post', '', 0),
(203, 1, '2018-01-19 16:11:20', '2018-01-19 18:11:20', 'A <strong>OAC - Observação e Abordagem de Comportamentos em Segurança é</strong> uma ferramenta educativa e amplamente utilizada no mercado com a finalidade de abordar comportamentos que foram observados, por meio de uma metodologia eficaz, que resulta em mudança de comportamentos de risco e manutenção de comportamentos seguros.\r\n\r\nA Unika utiliza uma metodologia com base em conceitos estabelecidos pelo psicólogo norte americano Scott Geller que tem como premissa que o diálogo estabelecido entre líder e liderado durante a realização da ferramenta precisa ter como base a confiança interpessoal.\r\n\r\nEste método exige um preparo robusto por parte daqueles que serão observadores (responsáveis por conduzir a ferramenta) para que o diálogo realmente aconteça. Para tal, é recomendado que sejam figuras de autoridade dentro da organização – líderes formais ou informais – pois são quem teoricamente deveriam estar melhor preparados para trabalhar com comunicação e gestão, e caso ainda não estejam, na OAC terão esta oportunidade de desenvolvimento.\r\n\r\nOs objetivos da OAC - Observação e Abordagem de Comportamentos em Segurança são vários:\r\n<ul>\r\n 	<li>Aprimorar a abordagem e comunicação entre líderes e liderados sobre os comportamentos em segurança do trabalho;</li>\r\n 	<li>Melhorar a percepção de riscos de líderes e liderados;</li>\r\n 	<li>Trabalhar com maior profundidade no entendimento dos ativadores ou causas do comportamento de risco;</li>\r\n 	<li>Criar um ambiente de confiança que favoreça os comportamentos de segurança em área;</li>\r\n 	<li>Utilizar o conhecimento e experiência do próprio empregado a favor da solução dos comportamentos de risco encontrados, tornando-o mais autônomo e responsável;</li>\r\n 	<li>Romper o ciclo de dependência e codependência. (leia mais sobre isso aqui)</li>\r\n</ul>\r\nPromover maior conscientização e auto responsabilização sobre a preservação da vida das pessoas é o maior benefício que esta ferramenta trás. A OAC oferece uma dimensão bastante prática aos líderes sobre o que precisam fazer para atuar no comportamento de seus empregados imediatamente.\r\n\r\n&nbsp;\r\n\r\nQuer conhecer mais sobre esta ferramenta? Entre em contato conosco.\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)\r\n\r\n&nbsp;', 'OAC - Observação e Abordagem de Comportamentos em Segurança – Por que implantar?', '', 'inherit', 'closed', 'closed', '', '202-revision-v1', '', '', '2018-01-19 16:11:20', '2018-01-19 18:11:20', '', 202, 'http://unika.handgran.com.br/202-revision-v1/', 0, 'revision', '', 0),
(204, 3, '2018-01-19 16:16:16', '2018-01-19 18:16:16', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone wp-image-205 size-full" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3.jpg" alt="" width="567" height="350" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: Reason, 2002\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.', '', 'publish', 'open', 'open', '', 'o-que-ninguem-contou-voce-sobre-influencia-dos-fatores-humanos-nos-acidentes-de-trabalho', '', '', '2018-02-21 10:45:16', '2018-02-21 13:45:16', '', 0, 'http://unika.handgran.com.br/?p=204', 1, 'post', '', 0),
(205, 1, '2018-01-19 16:13:14', '2018-01-19 18:13:14', '', 'Untitled-3', '', 'inherit', 'open', 'closed', '', 'untitled-3', '', '', '2018-01-19 16:13:14', '2018-01-19 18:13:14', '', 204, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3.jpg', 0, 'attachment', 'image/jpeg', 0),
(206, 1, '2018-01-19 16:16:16', '2018-01-19 18:16:16', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone size-medium wp-image-205" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3-300x185.jpg" alt="" width="300" height="185" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: <span class="SpellE">Reason</span>, 2002\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.', '', 'inherit', 'closed', 'closed', '', '204-revision-v1', '', '', '2018-01-19 16:16:16', '2018-01-19 18:16:16', '', 204, 'http://unika.handgran.com.br/204-revision-v1/', 0, 'revision', '', 0),
(207, 1, '2018-01-19 16:20:38', '2018-01-19 18:20:38', '', 'Fill 1 + Fill 3 Mask_preview', '', 'inherit', 'open', 'closed', '', 'fill-1-fill-3-mask_preview', '', '', '2018-01-19 16:20:38', '2018-01-19 18:20:38', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/Fill-1-Fill-3-Mask_preview.png', 0, 'attachment', 'image/png', 0),
(208, 1, '2018-01-19 16:21:28', '2018-01-19 18:21:28', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-As luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Ele tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Consegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Sente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR  ACIDENTES DO TRABALHO', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-19 16:21:28', '2018-01-19 18:21:28', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(209, 1, '2018-01-22 09:57:50', '2018-01-22 11:57:50', 'Você já ouviu alguém dizer: “Olha, é melhor não comemorar quando a empresa alcança zero acidente porque sempre depois de uma comemoração tem um acidente. Será que é coisa do destino, da sorte (ou falta dela), ou ainda uma\ncontingência diversas vezes ensaiada? Ou será que de verdade estamos sendo\nenganados pelos números?\nVamos desvendar este mistério!\nO que ocorre é que na área de segurança acontece algo cruel: medimos aquilo que\nnão queremos que aconteça, o acidente. E quando a nossa métrica é invertida, fica\nbastante difícil saber se estamos no caminho correto, pois não temos outros indicadores\nque nos apontem isso. Por isso, os indicadores preventivos são tão importantes.\nExemplificando: se por alguma razão um líder perde as metas de produção nos\nprimeiros 15 dias do mês, ele sabe que terá outros 15 dias para tentar recuperar o que não\nproduziu. Mas com o indicador usual de segurança – taxa de frequência de acidentes –\nnão é isso que ocorre, se acontece um acidente no primeiro dia do mês, não há o que ele\npossa fazer para recuperar este indicador.\nPortanto, quando comemoramos o zero acidente como um resultado alcançado, isso\npode ter sido fruto de sorte, do acaso ou do trabalho das pessoas envolvidas, mas\ninfelizmente este indicador não nos diz se nesta empresa o processo é seguro.\nOs indicadores preventivos são construidos respeitando a cultura e características de\ncada organização justamente para tentar mensurar de maneira antecipada se de fato estão\nsendo tomadas todas as ações necessárias e da maneira adequada para evitar que os\nacidentes ocorram. Além disso, com tais indicadores, é possível reconhecer as pessoas\nque estão comprometidas com a segurança e celebrar os progressos e conquistas\nalcançadas.\nPrecisa de ajuda? Nós vamos adorar contribuir na construção dos indicadores\npreventivos da sua empresa.\nKarla Mikoski\nCRP (08/09399)', 'Indicadores Preventivos: por que são tão importantes?', '', 'inherit', 'closed', 'closed', '', '199-autosave-v1', '', '', '2018-01-22 09:57:50', '2018-01-22 11:57:50', '', 199, 'http://unika.handgran.com.br/199-autosave-v1/', 0, 'revision', '', 0);
INSERT INTO `uk_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(210, 1, '2018-01-22 09:59:08', '2018-01-22 11:59:08', 'Você já ouviu alguém dizer: “Olha, é melhor não comemorar quando a empresa alcança zero acidente porque sempre depois de uma comemoração tem um acidente. Será que é coisa do destino, da sorte (ou falta dela), ou ainda uma\r\ncontingência diversas vezes ensaiada? Ou será que de verdade estamos sendo enganados pelos números?\r\nVamos desvendar este mistério!\r\nO que ocorre é que na área de segurança acontece algo cruel: medimos aquilo que não queremos que aconteça, o acidente. E quando a nossa métrica é invertida, fica bastante difícil saber se estamos no caminho correto, pois não temos outros indicadores que nos apontem isso. Por isso, os indicadores preventivos são tão importantes.\r\nExemplificando: se por alguma razão um líder perde as metas de produção nos primeiros 15 dias do mês, ele sabe que terá outros 15 dias para tentar recuperar o que não produziu. Mas com o indicador usual de segurança – taxa de frequência de acidentes – não é isso que ocorre, se acontece um acidente no primeiro dia do mês, não há o que ele possa fazer para recuperar este indicador.\r\nPortanto, quando comemoramos o zero acidente como um resultado alcançado, isso pode ter sido fruto de sorte, do acaso ou do trabalho das pessoas envolvidas, mas infelizmente este indicador não nos diz se nesta empresa o processo é seguro.\r\nOs indicadores preventivos são construidos respeitando a cultura e características de cada organização justamente para tentar mensurar de maneira antecipada se de fato estão sendo tomadas todas as ações necessárias e da maneira adequada para evitar que os acidentes ocorram. Além disso, com tais indicadores, é possível reconhecer as pessoas\r\nque estão comprometidas com a segurança e celebrar os progressos e conquistas\r\nalcançadas.\r\nPrecisa de ajuda? Nós vamos adorar contribuir na construção dos indicadores preventivos da sua empresa.\r\nKarla Mikoski\r\nCRP (08/09399)', 'Indicadores Preventivos: por que são tão importantes?', '', 'inherit', 'closed', 'closed', '', '199-revision-v1', '', '', '2018-01-22 09:59:08', '2018-01-22 11:59:08', '', 199, 'http://unika.handgran.com.br/199-revision-v1/', 0, 'revision', '', 0),
(211, 1, '2018-01-22 10:22:12', '2018-01-22 12:22:12', 'A parceria entre a CBA e a Unika tem produzido bons frutos na construção de uma cultura de segurança cada vez mais transparente e sustentável. Um grande diferencial que valorizo nesta parceria é a participação integral da consultoria desde a discussão da estratégia, apoio na elaboração do planejamento e energia na implementação e retroalimentação do nosso programa de comportamento seguro. A experiência de trabalhar com a Unika tem se mostrado enriquecedora, materializando bons resultados tanto na evolução da consciência dos nossos empregados que o comportamento seguro é um pilar forte na busca contínua pela cultura de segurança, quanto na diminuição das taxas de frequência e gravidade dos acidentes. Consideramos o time da Unika muito mais que um parceiro e sim parte fundamental e elementar no nosso processo de construção do comportamento seguro, não medimos esforços pois para nós Segurança é Inegociável.', 'Leandro Campos de Faria', '', 'publish', 'closed', 'closed', '', 'cba-companhia-brasileira-de-aluminio', '', '', '2018-01-26 18:01:42', '2018-01-26 20:01:42', '', 0, 'http://unika.handgran.com.br/?post_type=depoimento&#038;p=211', 0, 'depoimento', '', 0),
(212, 1, '2018-01-22 10:23:19', '2018-01-22 12:23:19', 'A experiência de trabalhar com a Unika tem sido muito enriquecedora, pois dedicam o tempo necessário para conhecer a empresa e o processo de produção, de modo que os conteúdos sejam baseados na realidade do negócio, com isto se garante o impacto e compreensão necessárias na equipe e nos líderes, porque nenhum conteúdo genérico é recebido, e nem planos sem impacto. Além disso, a metodologia é muito dinâmica, o que garante atenção, participação e conscientização de cada um dos participantes. Compreender e se concentrar em questões de comportamentos dentro do processo de produção como uma ferramenta para produzir segurança é fundamental, porque em ambientes de produção que são geralmente dinâmicos é muito difícil, quase impossível eliminar todos os riscos, é aí que entender por que as pessoas fazem o que fazem em certos momentos e ter ferramentas para modificar esses comportamentos quando eles não são seguros, é a única ferramenta confiável que permitirá ter ambientes de trabalho e colaboradores seguros.', 'Andres Zuluaga Suarez', '', 'publish', 'closed', 'closed', '', 'andres-zuluaga-suarez-gerente-da-planta-de-manufatura-da-owens-illinois', '', '', '2018-01-26 18:03:43', '2018-01-26 20:03:43', '', 0, 'http://unika.handgran.com.br/?post_type=depoimento&#038;p=212', 0, 'depoimento', '', 0),
(213, 1, '2018-01-22 10:24:29', '2018-01-22 12:24:29', 'Minha formação como perito na ferramenta de observação e abordagem comportamental, é a parte que faltava para que eu pudesse atingir resultados mais expressivos em segurança com minha equipe. Como sou um multiplicador de conhecimento, percebo que os observadores da unidade estão mais atentos aos detalhes e que trabalham fortemente na identificação das barreiras para mudança dos comportamentos de riscos. De todas as ferramentas de segurança que tenho contato, acredito e posso afirmar que trabalhar o comportamento humano faz todo o diferencial. O profissionalismo e a dedicação da Unika na formação dos peritos, com acompanhamento de campo, a profundidade e qualidade das analises, fez toda diferença para construção e formação de uma base solida em nossa unidade.', 'Delvair Castro', '', 'publish', 'closed', 'closed', '', 'delvair-castro-perito-comportamental-especialista-de-projetos-de-escavacao-de-shaft-e-raise-em-mina-subterranea', '', '', '2018-01-26 18:03:03', '2018-01-26 20:03:03', '', 0, 'http://unika.handgran.com.br/?post_type=depoimento&#038;p=213', 0, 'depoimento', '', 0),
(214, 1, '2018-01-22 10:25:33', '2018-01-22 12:25:33', 'Meu primeiro contato com Segurança Comportamental foi através da Mariane Mesquita, logo a Karla Mikoski somou-se ao projeto. Os resultados obtidos foram excelentes. De lá para cá passaram-se anos e eu sigo aprendendo sobre o tema com estas duas mentoras. Estou certa que esta abordagem é parte fundamental da estratégia para construir e/ou consolidar a Cultura Sustentável em EHS. Trabalhar com a Unika tem sido uma experiência extremamente positiva, pois assegura competência, transparência e confiança ao Programa de Segurança Comportamental implantado nas 18 plantas da OI na América Latina. Temos uma longa jornada pela frente, mas os indicadores mostram que estamos no caminho correto. Estou certa que esta parceria vem capacitando e provendo nossos líderes de recursos para influenciar o Comportamento Seguro dos 12 mil colaboradores que formam a nossa operação na região.', 'Isabela Malpighi', '', 'publish', 'closed', 'closed', '', 'isabela-malpighi', '', '', '2018-01-26 18:02:25', '2018-01-26 20:02:25', '', 0, 'http://unika.handgran.com.br/?post_type=depoimento&#038;p=214', 0, 'depoimento', '', 0),
(240, 1, '2018-02-21 09:51:23', '2018-02-21 12:51:23', '', 'unikapattern', '', 'inherit', 'open', 'closed', '', 'unikapattern', '', '', '2018-02-21 09:51:23', '2018-02-21 12:51:23', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2018/02/unikapattern.png', 0, 'attachment', 'image/png', 0),
(219, 1, '2018-01-25 15:26:31', '2018-01-25 17:26:31', '', 'unikapatternbody', '', 'inherit', 'open', 'closed', '', 'unikapatternbody', '', '', '2018-01-25 15:26:31', '2018-01-25 17:26:31', '', 0, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/unikapatternbody.png', 0, 'attachment', 'image/png', 0),
(220, 1, '2018-01-25 15:30:17', '2018-01-25 17:30:17', '<strong>COMPORTAMENTO HUMANO: O QUE VOCÊ PRECISA SABER PARA EVITAR ACIDENTES DO TRABALHO</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-As luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Ele tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Consegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Sente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'Comportamento humano: O que você precisa saber para evitar  acidentes do trabalho', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-25 15:30:17', '2018-01-25 17:30:17', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(221, 1, '2018-01-25 15:30:47', '2018-01-25 17:30:47', '<strong>Comportamento humano: O que você precisa saber para evitar acidentes do trabalho</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-As luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Ele tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Consegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Sente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'Comportamento humano: O que você precisa saber para evitar  acidentes do trabalho', '', 'inherit', 'closed', 'closed', '', '173-revision-v1', '', '', '2018-01-25 15:30:47', '2018-01-25 17:30:47', '', 173, 'http://unika.handgran.com.br/173-revision-v1/', 0, 'revision', '', 0),
(247, 2, '2018-02-21 10:56:08', '2018-02-21 13:56:08', '', 'foto-blog-evolução-da-cultura-nova', '', 'inherit', 'open', 'closed', '', 'foto-blog-evolucao-da-cultura-nova', '', '', '2018-02-21 10:56:08', '2018-02-21 13:56:08', '', 194, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/foto-blog-evolução-da-cultura-nova.png', 0, 'attachment', 'image/png', 0),
(223, 1, '2018-01-26 09:56:41', '2018-01-26 11:56:41', 'Você já ouviu alguém dizer: “Olha, é melhor não comemorar quando a empresa alcança zero acidente porque sempre depois de uma comemoração tem um acidente. Será que é coisa do destino, da sorte (ou falta dela), ou ainda uma contingência diversas vezes ensaiada? Ou será que de verdade estamos sendo enganados pelos números?\r\nVamos desvendar este mistério! O que ocorre é que na área de segurança acontece algo cruel: medimos aquilo que não queremos que aconteça, o acidente. E quando a nossa métrica é invertida, fica bastante difícil saber se estamos no caminho correto, pois não temos outros indicadores que nos apontem isso. Por isso, os indicadores preventivos são tão importantes.\r\nExemplificando: se por alguma razão um líder perde as metas de produção nos primeiros 15 dias do mês, ele sabe que terá outros 15 dias para tentar recuperar o que não produziu. Mas com o indicador usual de segurança – taxa de frequência de acidentes – não é isso que ocorre, se acontece um acidente no primeiro dia do mês, não há o que ele possa fazer para recuperar este indicador.\r\nPortanto, quando comemoramos o zero acidente como um resultado alcançado, isso pode ter sido fruto de sorte, do acaso ou do trabalho das pessoas envolvidas, mas infelizmente este indicador não nos diz se nesta empresa o processo é seguro.\r\nOs indicadores preventivos são construidos respeitando a cultura e características de cada organização justamente para tentar mensurar de maneira antecipada se de fato estão sendo tomadas todas as ações necessárias e da maneira adequada para evitar que os acidentes ocorram. Além disso, com tais indicadores, é possível reconhecer as pessoas que estão comprometidas com a segurança e celebrar os progressos e conquistas alcançadas.\r\nPrecisa de ajuda? Nós vamos adorar contribuir na construção dos indicadores preventivos da sua empresa.\r\nKarla Mikoski\r\nCRP (08/09399)', 'Indicadores Preventivos: por que são tão importantes?', '', 'inherit', 'closed', 'closed', '', '199-revision-v1', '', '', '2018-01-26 09:56:41', '2018-01-26 11:56:41', '', 199, 'http://unika.handgran.com.br/199-revision-v1/', 0, 'revision', '', 0),
(224, 1, '2018-01-26 10:10:41', '2018-01-26 12:10:41', '', 'concept of security', 'wall with the drawing of dark clouds, rain and one umbrella, concept of protection and security (3d render)', 'inherit', 'open', 'closed', '', 'concept-of-security-2', '', '', '2018-01-26 10:10:41', '2018-01-26 12:10:41', '', 199, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/1.jpg', 0, 'attachment', 'image/jpeg', 0),
(251, 2, '2018-02-21 11:08:11', '2018-02-21 14:08:11', '', 'dialogo-nova', '', 'inherit', 'open', 'closed', '', 'dialogo-nova', '', '', '2018-02-21 11:08:11', '2018-02-21 14:08:11', '', 191, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/dialogo-nova.png', 0, 'attachment', 'image/png', 0),
(252, 2, '2018-02-21 11:08:26', '2018-02-21 14:08:26', 'Quando se trata de <strong>DDS – Diálogos Diários de Segurança no Trabalho</strong> - uma das principais questões que se observa na circulação de e-mails e fóruns de discussão nos sites voltados à segurança do trabalho é: “Alguém tem um tema para eu trabalhar no DDS?”. Com base nesta repetida dúvida dos profissionais que trabalham nesta área, nasce este texto, com a intenção de refletir sobre onde podemos encontrar os melhores temas para se trabalhar no Diálogos Diários de Segurança no Trabalho.\r\n\r\nÉ comum encontrarmos pessoas diante do computador navegando na internet em busca de temas e textos. Normalmente, atribuímos a qualidade do DDS à qualidade do texto. Mas será que é só isso mesmo? Será que uma simples leitura de um texto diante de um grupo de pessoas pode ser considerado um “DDS – Diálogo diário de segurança”?\r\n\r\nA começar pelo próprio nome desta ferramenta, poderíamos deduzir que uma leitura – em que uma pessoa lê e o restante dos participantes ouve – não pode ser considerado um diálogo. Um diálogo pressupõe que no mínimo duas pessoas conversem ou debatam sobre tema/ assunto.\r\n\r\nA escolha do tema não é algo tão simples quanto se costuma fazer. Um dos princípios da andragogia – parte da pedagogia que estuda o processo de ensino e aprendizagem de adultos – diz que os adultos só aprendem aquilo que eles têm necessidade e/ ou aquilo que terá uma aplicação prática na vida cotidiana.\r\n\r\nO que normalmente ocorre na escolha do tema é que o instrutor ou condutor do DDS faz esta escolha baseado naquilo que <strong>ele</strong> acredita que será mais interessante ou tornará mais interessante a sua fala para o grupo. Aqui já reside uma das barreiras para que o DDS não tenha uma boa efetividade, pois o tema deve ser escolhido não com base no que o instrutor/ condutor quer transmitir, mas sim com base nas necessidades de aprendizagem dos participantes dos Diálogos Diários de Segurança no Trabalho.\r\n\r\nA escolha do tema/ assunto é apenas um dos passos para a realização de um DDS de qualidade. Um bom processo de entrevista e coleta de informações com os próprios participantes costuma ser uma maneira eficaz de trazer temas que de fato agreguem ao público.\r\n\r\nApesar do DDS ser um momento que costuma ter uma curta duração, entre 5 e 15 minutos (recomendável), ele exige um mínimo de planejamento, de preparo, para que surta o resultado esperado: aprendizagem, ou seja, conhecimento adquirido e aplicado no dia a dia! Use estas dicas para escolher seu tema de seu DDS e um requisito bastante importante para gerar aprendizagem terá sido atendido.\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)\r\n\r\n&nbsp;\r\n\r\nQuer receber um artigo completo sobre todos os cuidados para realizar um DDS de qualidade? Clique aqui.', 'Preciso escolher um tema para o DDS - Diálogos Diários de Segurança no Trabalho. O que devo levar em consideração?', '', 'inherit', 'closed', 'closed', '', '191-autosave-v1', '', '', '2018-02-21 11:08:26', '2018-02-21 14:08:26', '', 191, 'http://unika.handgran.com.br/191-autosave-v1/', 0, 'revision', '', 0),
(248, 2, '2018-02-21 10:56:46', '2018-02-21 13:56:46', 'A Unika compreende a evolução da Cultura de Segurança com base  no conceito desenvolvido pelo Energy Institute na construção do Programa Corações e Mentes (Hearts and Minds). O projeto apresenta um estudo detalhado de Cultura de SSMA e seus possíveis níveis de maturidade. Esta análiseestá disponível ao público e pode ser acessada por meio do site: <a href="http://www.energyinst.org/heartsandminds">www.energyinst.org/heartsandminds</a>.\r\n\r\nNo programa, os níveis de maturidade de Cultura são apresentados como uma escada evolutiva, não mais como um gráfico em curva descendente – utilizado na famosa Curva de Bradley -  como pode ser observado na figura a seguir:\r\n\r\n<strong>ESCADA EVOLUTIVA DA CULTURA DE SEGURANÇA DO PROGRAMA CORAÇÕES E MENTES</strong>\r\n\r\n&nbsp;\r\n\r\n<img class="wp-image-195 aligncenter" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-2-300x164.jpg" alt="" width="403" height="214" />\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\nNeste programa, a Cultura Organizacional é apresentada como uma escada evolutiva, em que cada nível possui características distintas e é uma progressão do anterior. Isso quer dizer que, à medida em que a Cultura se consolida em um dos degraus evolutivos, todo o aprendizado e os ganhos conquistados não se perdem e são levados para o próximo estágio. Veja a descrição de cada um dos níveis evolutivos:\r\n\r\n<strong>Patológico</strong>: nível em que as pessoas não se preocupam com a segurança ou com os aspectos preventivos. Se orientam somente para atender a legislação e se empenham em não serem “pegas” descumprindo regras. É comum ouvir afirmações do tipo: “Claro que temos acidentes, este é um negócio perigoso”.\r\n\r\n<strong>Reativo</strong>: é o nível em que a segurança é levada a sério, apesar de só receber atenção suficiente depois que algo deu errado. As seguintes frases são frequentes falas dos líderes: “Se os empregados fizessem como eu falei, não teria acontecido”; “Precisamos fazer as pessoas obedecerem as regras.” Por outro lado, ouve-se dos empregados: “Mas o técnico de segurança passou aqui e não disse nada”; “Foi meu supervisou que falou que tínhamos que agilizar para entregar até as 13 horas”. O que demonstra um padrão de dependência e co-dependência clara neste nível de maturidade cultural.\r\n\r\n<strong>Calculista</strong>: neste nível a organização se sente confortável em relação ao sistema e aos números. O modelo de gestão está bem implantado e os líderes frequentemente conversam com os empregados sobre a importância da segurança, apesar de sua atenção estar normalmente voltada para a conformidade no atendimento de regras simples. Nestes casos, o objetivo é demonstrar que a segurança é levada a sério. Existe uma ênfase na coleta de estatísticas com bônus vinculados a elas. E de fato o bom funcionamento do sistema estimula os comportamentos adequados dentro de empresas com este nível cultural. E as empresas terceirizadas são escolhidas por seus resultados e não somente por serem mais baratas. Muitos dados são coletados e analisados, as pessoas se sentem à vontade para fazer alterações em procedimentos e processos. Há muitas auditorias. Quando algo dá errado, a equipe se surpreende porque considera que o sistema deveria ter funcionado, evitando perdas.\r\n\r\n<strong>Proativo</strong>: neste nível a organização volta a sua atenção para o futuro e não para dados do passado. Os empregados se envolvem na prática com as questões de segurança. O desempenho global em segurança é levado em consideração para promoções e reconhecimentos. Os líderes tratam a segurança como um valor e todas as decisões se baseiam em Saúde, Segurança e Meio Ambiente. A equipe voltada aos processos se pergunta: “Estamos fazendo a coisa certa?”, ao invés de focar nos acidentes.\r\n\r\nNo nível proativo os gestores conhecem a sua organização e sabem onde residem os problemas, bem como os empregados compreendem o que é esperado deles. Gestores e empregados estão em sintonia nas questões de segurança por meio de uma confiança bidirecional desenvolvida ao longo do tempo, o que os permite não recorrer tanto à burocracia e as redundâncias. A divisão de trabalho é menos sofrida porque os empregados estão dispostos a assumir responsabilidades.\r\n\r\n<strong>Generativo</strong>: neste nível se encontram as organizações sustentáveis e de alta confiança. Normalmente são negócios que apresentam um processo de baixa variabilidade, o que confere uma característica de confiabilidade operacional.  São empresas que sabem que o bom resultado de SSMA é um indicador de bom desempenho nos negócios. Elas definem os mais altos padrões ao invés de somente atender à conformidade legal. São extremamente honestas em relação aos fracassos e usam isso para melhorar e não para identificar culpados. Os gestores sabem realmente o que acontece porque a força de trabalho deseja informá-los. O respeito pelas pessoas está implícito em todas as decisões. Os empregados estão sempre atentos, precavidos para o que pode dar de errado, estudando novas possibilidades preventivas e treinando respostas constantemente.\r\n\r\n&nbsp;\r\n\r\nDentro de um processo diagnóstico de cultura são avaliadas 7 dimensões que podem ou não estar presentes na organização:\r\n<ol>\r\n 	<li>Liderança e comprometimento</li>\r\n 	<li>Política e objetivos estratégicos</li>\r\n 	<li>Responsabilidades, recursos e padrões</li>\r\n 	<li>Gestão de perigos e riscos</li>\r\n 	<li>Procedimentos</li>\r\n 	<li>Implantação e monitoramento de ações e iniciativas</li>\r\n 	<li>Auditorias/ Sistema de gestão</li>\r\n</ol>\r\nPara cada uma das dimensões são coletadas percepções de grupos homogêneos por nível hierárquico, além de análise documental e observaçãoativa em campo. Mais do que compreender as ações implantadas em cada dimensão, a coleta de dados visa compreender o padrão de comportamento predominante na prática de cada ação, decisão, iniciativa e/ou ferramenta de gestão.\r\n\r\nOs resultados do diagnóstico indicam como está o nível de maturidade de cultura e sugerem ações para avanço em cada uma das dimensões.\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)\r\n\r\n&nbsp;\r\n\r\nFicou interessado em realizar um processo diagnóstico da Cultura de Segurança da sua empresa? Fale com a gente.\r\n\r\n&nbsp;', 'Evolução da Cultura de Segurança', '', 'inherit', 'closed', 'closed', '', '194-autosave-v1', '', '', '2018-02-21 10:56:46', '2018-02-21 13:56:46', '', 194, 'http://unika.handgran.com.br/194-autosave-v1/', 0, 'revision', '', 0),
(249, 2, '2018-02-21 11:06:10', '2018-02-21 14:06:10', '', 'blog-nova', '', 'inherit', 'open', 'closed', '', 'blog-nova', '', '', '2018-02-21 11:06:10', '2018-02-21 14:06:10', '', 173, 'http://unika.handgran.com.br/wp-content/uploads/2018/01/blog-nova.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `uk_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(250, 2, '2018-02-21 11:06:22', '2018-02-21 14:06:22', '<strong>Comportamento humano: O que você precisa saber para evitar acidentes do trabalho</strong>\r\n\r\nUma das constatações mais comuns após um processo de análise e investigação de ocorrências é a de que o comportamento humano é uma das causas dos acidentes de trabalho, o que faz com que as organizações voltem seus olhos aos indivíduos e invistam em treinamentos e abordagens comportamentais. No entanto, ainda hoje há muito equívoco sobre a compreensão do que é comportamento humano e como modificá-lo.\r\n\r\nUm dos principais equívocos é justamente acreditar que quando a causa do acidente de trabalho é comportamental a responsabilidade se localiza somente sobre o indivíduo que sofreu o acidente. Afirmações como: “ele foi treinado, ele tinha anos de experiência, mas escolheu não utilizar a ferramenta adequada”, podem servir de ilustração sobre como o modelo mental dentro de algumas organizações ainda acredita que se o problema é o comportamento o que deve ser trabalhado é o acidentado, quase que numa tentativa de isenção de responsabilidade de qualquer outra esfera.\r\n\r\nNo entanto quando se recorre à ciência especializada neste assunto, a Psicologia, logo se percebe que o comportamento das pessoas é altamente influenciado por situações antecedentes à ação de alguém e pelas consequências que ela pode obter ou evitar agindo de determinada forma, veja exemplo abaixo:\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<table class="MsoNormalTable" style="border-collapse: collapse; border: none; mso-border-alt: solid #B4C6E7 .5pt; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt; mso-border-insideh: .5pt solid #B4C6E7; mso-border-insidev: .5pt solid #B4C6E7;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes; height: 18.85pt;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-bottom: solid #8EAADB 1.5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Situação</span></b></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Ação/ ato</span></b></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: solid #B4C6E7 1.0pt; border-left: none; border-bottom: solid #8EAADB 1.5pt; border-right: solid #B4C6E7 1.0pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; mso-border-bottom-alt: solid #8EAADB 1.5pt; padding: 0cm 5.4pt 0cm 5.4pt; height: 18.85pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><b><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; color: #222a35;">Consequência</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">O que acontece antes ou junto à ação de uma pessoa</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Aquilo\r\nque a pessoa faz</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; background: #DEEAF6; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="201">\r\n<p class="MsoNormal" style="text-align: center; line-height: 115%;" align="center"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">O\r\nque acontece com a pessoa depois da ação/ ato.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2; mso-yfti-lastrow: yes;">\r\n<td style="width: 150.95pt; border: solid #B4C6E7 1.0pt; border-top: none; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-As luvas são pequenas para mão dele</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Ele tem alergia ao material da luva</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Consegue ter um tato melhor sem luvas</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif; mso-bidi-font-weight: bold;">\r\n-Sente-se incômodo ao trabalhar com luvas e ninguém está olhando</span></p>\r\n</td>\r\n<td style="width: 150.95pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">Um mecânico dentro da\r\noficina retira as luvas para manusear uma peça</span></p>\r\n</td>\r\n<td style="width: 151.0pt; border-top: none; border-left: none; border-bottom: solid #B4C6E7 1.0pt; border-right: solid #B4C6E7 1.0pt; mso-border-top-alt: solid #B4C6E7 .5pt; mso-border-left-alt: solid #B4C6E7 .5pt; mso-border-alt: solid #B4C6E7 .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="201">\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Tirando as luvas fica mais\r\nconfortável</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Evita ter processo\r\nalérgico</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Realiza o trabalho com\r\nmais precisão e menos tempo</span></p>\r\n<p class="MsoNormal" style="line-height: 115%;"><span style="font-size: 11.0pt; line-height: 115%; font-family: ''Arial'',sans-serif;">- Sente-se mais confortável\r\npara realizar o trabalho</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<strong>SITUAÇÃO + AÇÃO + CONSEQUÊNCIA = COMPORTAMENTO</strong>\r\n\r\n<strong> </strong>\r\n\r\nA questão é que o operador, naquele momento, influenciado por diversas situações e buscando as consequências positivas ou ganhos, não visualiza ou não dá a devida importância à possibilidade de que ocorra um acidente ou perda. Portanto, os contextos\r\n\r\n&nbsp;\r\n\r\nsocial e cultural possuem alta influência sobre o comportamento de alguém. Observe ao seu redor e avalie: as pessoas tem o mesmo comportamento em todos os contextos/ ambientes? Imagino que você vai chegar a conclusão que não, e isso se deve ao fato de que ambientes diferentes requerem comportamentos diferentes das pessoas. E a verdade é que nós buscamos nos encaixar, ser aceitos, então vamos nos adaptando a estes ambientes.\r\n\r\nNeste sentido, para atuar sobre o comportamento humano de segurança, é necessário que a empresa compreenda que deve estar atenta em como o contexto organizacional está influenciando, por meio de ações ou omissões, o comportamento de seus empregados. Uma empresa atua sobre o comportamento de segurança quando:\r\n<ul>\r\n 	<li>Define um planejamento estratégico de segurança que esteja alinhado aos objetivos do negócio;</li>\r\n 	<li>Estabele metas de produção, qualidade, custo e segurança coerentes e alcançáveis;</li>\r\n 	<li>Deixa clara a responsabilidade de cada um quanto à segurança do trabalho;</li>\r\n 	<li>Ensina líderes e liderados a agirem com foco em segurança;</li>\r\n 	<li>Capacita seus empregados e os corrige nos procedimentos de segurança;</li>\r\n 	<li>Tem um sistema de gestão de segurança que direciona o comportamento dos empregados para que atuem com segurança;</li>\r\n 	<li>Incentiva e reconhece os empregados que atuam e maneira preventiva em segurança;</li>\r\n 	<li>Tem um sistema de consequências desenhado estrategicamente e que funciona bem na prática.</li>\r\n</ul>\r\nEstas são algumas das responsabilidades da empresa para criar um ambiente que estimule comportamentos humanos seguros de modo a evitar acidentes de trabalho, sempre abordando de maneira sistêmica e jamais olhando exclusivamente para tratativas que focam no acidentado.\r\n\r\nPense em como sua empresa tem investido sobre o comportamento em segurança e em quê precisa se adequar!\r\n\r\n&nbsp;\r\n\r\nKarla Mikoski\r\n\r\nCRP (08/09399)', 'Comportamento humano: O que você precisa saber para evitar  acidentes do trabalho', '', 'inherit', 'closed', 'closed', '', '173-autosave-v1', '', '', '2018-02-21 11:06:22', '2018-02-21 14:06:22', '', 173, 'http://unika.handgran.com.br/173-autosave-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_simple_history`
--

CREATE TABLE IF NOT EXISTS `uk_simple_history` (
  `id` bigint(20) NOT NULL,
  `date` datetime NOT NULL,
  `logger` varchar(30) DEFAULT NULL,
  `level` varchar(20) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `occasionsID` varchar(32) DEFAULT NULL,
  `initiator` varchar(16) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=161 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `uk_simple_history`
--

INSERT INTO `uk_simple_history` (`id`, `date`, `logger`, `level`, `message`, `occasionsID`, `initiator`) VALUES
(1, '2018-01-26 18:16:47', 'SimplePluginLogger', 'info', 'Installed plugin "{plugin_name}"', '3b4a4f2c63669a4ec7adaee4a1cb66cf', 'wp_user'),
(2, '2018-01-26 18:16:47', 'SimplePluginLogger', 'info', 'Activated plugin "{plugin_name}"', '39a69b2125c292e9d9abbbc60f375705', 'wp_user'),
(3, '2018-01-26 18:16:47', 'SimpleLogger', 'info', 'Because Simple History was just recently installed, this feed does not contain much events yet. But keep the plugin activated and soon you will see detailed information about page edits, plugin updates, user logins, and much more.', '1c6ed1ff4a97400596b011813faa932f', 'wp'),
(4, '2018-01-26 18:16:47', 'SimpleLogger', 'info', 'Welcome to Simple History!\n\nThis is the main history feed. It will contain events that this plugin has logged.', '0c4babaacbe315745cbb536eaa41278c', 'wp'),
(5, '2018-01-26 18:16:48', 'SimplePluginLogger', 'info', 'Installed plugin "{plugin_name}"', '3b4a4f2c63669a4ec7adaee4a1cb66cf', 'wp'),
(6, '2018-01-26 18:16:48', 'SimplePluginLogger', 'info', 'Activated plugin "{plugin_name}"', '39a69b2125c292e9d9abbbc60f375705', 'wp'),
(7, '2018-01-26 18:17:38', 'SimpleUserLogger', 'info', 'Created user {created_user_login} ({created_user_email}) with role {created_user_role}', 'b7d8139c4109f0071b7f0fd9584de44c', 'wp_user'),
(8, '2018-01-26 18:21:07', 'SimpleUserLogger', 'info', 'Logged out', 'abfee0819f198c2782490c4ccb735367', 'wp_user'),
(9, '2018-01-26 18:21:16', 'SimpleUserLogger', 'info', 'Logged in', '20a168a19c520fcd7a1d5773d5b43577', 'wp_user'),
(10, '2018-01-26 18:21:26', 'SimpleUserLogger', 'info', 'Logged out', 'abfee0819f198c2782490c4ccb735367', 'wp_user'),
(11, '2018-01-26 18:26:08', 'SimpleUserLogger', 'info', 'Logged in', '18536e6407fa9b901f2350fc025b0fa0', 'wp_user'),
(12, '2018-01-26 20:01:42', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'bdba26c36be7c9603ba8f334fcd063c3', 'wp_user'),
(13, '2018-01-26 20:02:14', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'a24c75b7e792f25027f15f10266be5d4', 'wp_user'),
(14, '2018-01-26 20:02:25', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'a24c75b7e792f25027f15f10266be5d4', 'wp_user'),
(15, '2018-01-26 20:03:03', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'a52d4efa8fd26913cbe4fe29a5305678', 'wp_user'),
(16, '2018-01-26 20:03:43', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'b94357ac7634519636382390cd018cd9', 'wp_user'),
(17, '2018-01-29 11:46:17', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '4492cb54e3794c4eedcefe867b181901', 'wp'),
(18, '2018-01-29 11:46:17', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', 'f30c4609d7ca7dd6804b83d629e543d8', 'wp'),
(19, '2018-01-29 11:46:17', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', 'b0e96e74898f5e2d570b496657fb852a', 'wp'),
(20, '2018-01-29 11:46:17', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', 'e0d983b51dfcf017d05473a390936a15', 'wp'),
(21, '2018-01-29 11:46:18', 'AvailableUpdatesLogger', 'notice', 'Found an update to theme "{theme_name}"', 'fce10ce364cb5037b1602f4ae915d202', 'wp'),
(22, '2018-02-05 18:05:11', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '16dca2d45c65b7805de9047f88bdb499', 'wp'),
(23, '2018-02-05 18:05:11', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '26981c78b1c1bb75084873b7ce80fba6', 'wp'),
(24, '2018-02-05 18:05:11', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '1bd5ebe702d09b03936c5b29f411ff44', 'wp'),
(25, '2018-02-08 18:40:08', 'AvailableUpdatesLogger', 'notice', 'Found an update to WordPress.', '74a372b750f0938a5590db8611c44845', 'wp'),
(26, '2018-02-08 18:40:09', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '5cc6f502753b92d128c4649a7a18d121', 'wp'),
(27, '2018-02-08 18:40:09', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '10ed53cbc845163024b2c333047f0468', 'wp'),
(28, '2018-02-08 18:40:09', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '4aaf6a4e1b75dd3ed7310f3934f5ff90', 'wp'),
(29, '2018-02-08 18:40:19', 'SimpleCoreUpdatesLogger', 'notice', 'WordPress auto-updated to {new_version} from {prev_version}', '312fc71abd88c46d277a7d22fb59113d', 'wp'),
(30, '2018-02-15 15:26:49', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '69e67b4aeb17c1ddcd380e2b8ad6d103', 'wp'),
(31, '2018-02-15 15:26:49', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '44172bef4b8ec1181e1849bbf8f18193', 'wp'),
(32, '2018-02-15 18:12:35', 'SimpleUserLogger', 'info', 'Logged in', 'ea0673203f474f142f01a078ca6bcb99', 'wp_user'),
(33, '2018-02-15 18:13:12', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '1a09c853b3e5248413265a447fec79f9', 'wp_user'),
(34, '2018-02-15 18:20:16', 'SimpleUserLogger', 'info', 'Created user {created_user_login} ({created_user_email}) with role {created_user_role}', 'b34aa1a8473cda5abdb56018cd8ee81b', 'wp_user'),
(35, '2018-02-15 18:21:00', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(36, '2018-02-15 18:23:19', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '8bec4fd776d10973890000cca7aec1bf', 'wp_user'),
(37, '2018-02-15 18:23:43', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '435d1153b03714c89df1d94b70ce640d', 'wp_user'),
(38, '2018-02-15 18:23:51', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '1f59c7c5a64ae59cd7e373133572b975', 'wp_user'),
(39, '2018-02-15 18:23:57', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'ad0b2c3b6b388480b4f7f46a86db4a6e', 'wp_user'),
(40, '2018-02-15 18:24:29', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'a7135a86879047f85e090735ee2c2a04', 'wp_user'),
(41, '2018-02-15 18:24:35', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'c822ddcf2dd704c76f73c9d49fbe770a', 'wp_user'),
(42, '2018-02-15 18:26:25', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(43, '2018-02-15 18:26:45', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(44, '2018-02-15 18:27:59', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(45, '2018-02-15 18:28:59', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(46, '2018-02-15 18:30:47', 'SimplePluginLogger', 'info', 'Installed plugin "{plugin_name}"', 'c782cdea07384faa9b9f824b262a0baf', 'wp_user'),
(47, '2018-02-15 18:30:51', 'SimplePluginLogger', 'info', 'Activated plugin "{plugin_name}"', 'ef4943d57792276ddac997bb1afa377c', 'wp_user'),
(48, '2018-02-15 18:31:17', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(49, '2018-02-15 18:32:45', 'SimplePluginLogger', 'info', 'Deactivated plugin "{plugin_name}"', 'bda90680077c5550b33d2a2a97e369a0', 'wp_user'),
(50, '2018-02-15 18:41:53', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(51, '2018-02-15 18:41:58', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(52, '2018-02-15 18:42:08', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(53, '2018-02-15 18:42:24', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(54, '2018-02-15 18:42:36', 'SimplePluginLogger', 'info', 'Deactivated plugin "{plugin_name}"', '088d637e461757f4079eb5d8dd0af498', 'wp_user'),
(55, '2018-02-15 18:42:40', 'SimplePluginLogger', 'info', 'Activated plugin "{plugin_name}"', 'ef4943d57792276ddac997bb1afa377c', 'wp_user'),
(56, '2018-02-15 18:43:04', 'SimplePluginLogger', 'info', 'Deactivated plugin "{plugin_name}"', 'bda90680077c5550b33d2a2a97e369a0', 'wp_user'),
(57, '2018-02-15 18:43:05', 'SimplePluginLogger', 'info', 'Deactivated plugin "{plugin_name}"', 'bda90680077c5550b33d2a2a97e369a0', 'wp_user'),
(58, '2018-02-15 18:43:09', 'SimplePluginLogger', 'info', 'Activated plugin "{plugin_name}"', 'f981bd364d4b494de2107ee6cc084d97', 'wp_user'),
(59, '2018-02-15 18:45:35', 'SimplePluginLogger', 'info', 'Deactivated plugin "{plugin_name}"', '088d637e461757f4079eb5d8dd0af498', 'wp_user'),
(60, '2018-02-15 18:46:05', 'SimplePluginLogger', 'info', 'Installed plugin "{plugin_name}"', '399794aa6378d05c6c4644492e50d2de', 'wp_user'),
(61, '2018-02-15 18:46:08', 'SimplePluginLogger', 'info', 'Activated plugin "{plugin_name}"', 'db1c098d1e6d1b4d0f6a03bfbfda5772', 'wp_user'),
(62, '2018-02-15 18:46:44', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(63, '2018-02-15 18:46:58', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(64, '2018-02-15 18:47:54', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '608e5a4a3f7fa30b1b93b1045ee770ab', 'wp_user'),
(65, '2018-02-15 18:55:20', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(66, '2018-02-15 18:55:40', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(67, '2018-02-15 18:57:17', 'SimplePluginLogger', 'info', 'Deactivated plugin "{plugin_name}"', '957844d3ba52a0d952d16095aa92bb08', 'wp_user'),
(68, '2018-02-15 18:58:00', 'SimplePluginLogger', 'info', 'Installed plugin "{plugin_name}"', 'ab80f9f1f9a9796e81eb52a56ed46b99', 'wp_user'),
(69, '2018-02-15 18:58:03', 'SimplePluginLogger', 'info', 'Activated plugin "{plugin_name}"', 'f981bd364d4b494de2107ee6cc084d97', 'wp_user'),
(70, '2018-02-15 19:01:11', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(71, '2018-02-15 19:06:03', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(72, '2018-02-15 19:06:23', 'SimplePluginLogger', 'info', 'Deactivated plugin "{plugin_name}"', '088d637e461757f4079eb5d8dd0af498', 'wp_user'),
(73, '2018-02-15 19:06:27', 'SimplePluginLogger', 'info', 'Activated plugin "{plugin_name}"', 'f981bd364d4b494de2107ee6cc084d97', 'wp_user'),
(74, '2018-02-15 19:07:20', 'SimplePluginLogger', 'info', 'Deactivated plugin "{plugin_name}"', '088d637e461757f4079eb5d8dd0af498', 'wp_user'),
(75, '2018-02-15 19:07:37', 'SimplePluginLogger', 'info', 'Installed plugin "{plugin_name}"', 'ab80f9f1f9a9796e81eb52a56ed46b99', 'wp_user'),
(76, '2018-02-15 19:07:49', 'SimplePluginLogger', 'info', 'Activated plugin "{plugin_name}"', 'f981bd364d4b494de2107ee6cc084d97', 'wp_user'),
(77, '2018-02-15 19:07:57', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(78, '2018-02-15 19:08:11', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(79, '2018-02-15 19:08:26', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '83aa7c619bcb10e1df5764baf2cf1b76', 'wp_user'),
(80, '2018-02-15 19:09:10', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', 'ba81e296d5a06ea46d0125f561dfb941', 'wp_user'),
(81, '2018-02-15 19:09:55', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '29b24982e97547794311040b739cdacf', 'wp_user'),
(82, '2018-02-15 19:10:59', 'SimpleUserLogger', 'info', 'Logged out', 'abfee0819f198c2782490c4ccb735367', 'wp_user'),
(83, '2018-02-15 19:11:28', 'SimpleUserLogger', 'info', 'Logged in', 'ea0673203f474f142f01a078ca6bcb99', 'wp_user'),
(84, '2018-02-19 17:50:49', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', 'fd07bdb5f8a10304442042260097ea77', 'wp'),
(85, '2018-02-19 17:50:50', 'SimpleCommentsLogger', 'info', 'Deleted a comment to "{comment_post_title}" by {comment_author} ({comment_author_email})', '0aa891435bb3d7db207f29615534f966', 'wp'),
(86, '2018-02-19 18:04:32', 'SimpleUserLogger', 'info', 'Logged in', 'ea0673203f474f142f01a078ca6bcb99', 'wp_user'),
(87, '2018-02-19 18:04:56', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '041904848939c6c473255194800cbd66', 'wp_user'),
(88, '2018-02-19 18:05:42', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', 'f458673d7b8b7ad8c1ecbef1197af69f', 'wp_user'),
(89, '2018-02-19 18:05:53', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '1f59c7c5a64ae59cd7e373133572b975', 'wp_user'),
(90, '2018-02-19 18:06:18', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '435d1153b03714c89df1d94b70ce640d', 'wp_user'),
(91, '2018-02-19 18:12:29', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '0ffbba4b3f54c3ade8a7756aeeca06fa', 'wp_user'),
(92, '2018-02-19 18:12:35', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '1f59c7c5a64ae59cd7e373133572b975', 'wp_user'),
(93, '2018-02-19 18:13:05', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', 'a16cd9ce154764880b9d5aadbeba9ec3', 'wp_user'),
(94, '2018-02-19 18:13:10', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '435d1153b03714c89df1d94b70ce640d', 'wp_user'),
(95, '2018-02-19 18:37:21', 'SimpleUserLogger', 'info', 'Logged in', 'b42fa5e97118db1a9747be0576cba04c', 'wp_user'),
(96, '2018-02-19 19:18:42', 'SimpleUserLogger', 'info', 'Logged in', 'edf263478b5a3cd205f369126c67088d', 'wp_user'),
(97, '2018-02-19 19:19:04', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '176d91dba2ff12028c1b319a5de0a2e4', 'wp_user'),
(98, '2018-02-19 19:19:16', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '1f59c7c5a64ae59cd7e373133572b975', 'wp_user'),
(99, '2018-02-19 19:19:46', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '3d8112af437d7c83dbba133fc1d8eb0c', 'wp_user'),
(100, '2018-02-19 19:19:55', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '435d1153b03714c89df1d94b70ce640d', 'wp_user'),
(101, '2018-02-19 19:20:47', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '435d1153b03714c89df1d94b70ce640d', 'wp_user'),
(102, '2018-02-19 19:39:27', 'SimpleUserLogger', 'info', 'Logged in', 'ea0673203f474f142f01a078ca6bcb99', 'wp_user'),
(103, '2018-02-19 19:39:57', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '435d1153b03714c89df1d94b70ce640d', 'wp_user'),
(104, '2018-02-19 19:40:18', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '435d1153b03714c89df1d94b70ce640d', 'wp_user'),
(105, '2018-02-20 20:31:05', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '68161e32007a7c3c184f1534faca114f', 'wp_user'),
(106, '2018-02-20 20:31:07', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '69b11996c88d54cc0cf2a5c7a0d16a74', 'wp_user'),
(107, '2018-02-20 20:31:09', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '8335272950f6e33b01e25438b1aec8ec', 'wp_user'),
(108, '2018-02-20 20:31:11', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '1a09c853b3e5248413265a447fec79f9', 'wp_user'),
(109, '2018-02-20 20:31:12', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'd612b1b9d654249deca40c42b8d7d349', 'wp_user'),
(110, '2018-02-20 20:31:15', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'c3d3e5c2dcb60d812a9e5aeb7700352c', 'wp_user'),
(111, '2018-02-20 20:33:36', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '1a09c853b3e5248413265a447fec79f9', 'wp_user'),
(112, '2018-02-21 12:48:20', 'SimpleMenuLogger', 'info', 'Edited menu "{menu_name}"', '5f93a4b0f5d6efcaf5cdaa36ea7f63f6', 'wp_user'),
(113, '2018-02-21 12:48:20', 'SimpleCategoriesLogger', 'info', 'Edited term "{to_term_name}" in taxonomy "{to_term_taxonomy}"', '83b6800710725f9964f0675499188e23', 'wp_user'),
(114, '2018-02-21 12:51:23', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '6abf4a6c16aa3c07fccbcbb650ca4ac8', 'wp_user'),
(115, '2018-02-21 12:52:58', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', 'ba5da376930dc59c91e684fbd1500521', 'wp_user'),
(116, '2018-02-21 13:37:42', 'SimpleUserLogger', 'info', 'Logged in', 'edf263478b5a3cd205f369126c67088d', 'wp_user'),
(117, '2018-02-21 13:38:19', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '0bbb5eea8aa11257eb7c962d2e742017', 'wp_user'),
(118, '2018-02-21 13:38:26', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '435d1153b03714c89df1d94b70ce640d', 'wp_user'),
(119, '2018-02-21 13:44:13', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', '8fed62f32c454fed901bcbb6261e40c1', 'wp_user'),
(120, '2018-02-21 13:44:17', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', '73a1604b1d369e35640f608c70878378', 'wp_user'),
(121, '2018-02-21 13:44:31', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', '3f30395ee708a34c4b1709db40b342b3', 'wp_user'),
(122, '2018-02-21 13:44:34', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', '8b884beee6192378f34d0279bd4483bd', 'wp_user'),
(123, '2018-02-21 13:44:37', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', 'b9298dd359339a08abb5dffb57ad423b', 'wp_user'),
(124, '2018-02-21 13:44:40', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', '45a8f9f1c5bce45cbb531a7fa911a497', 'wp_user'),
(125, '2018-02-21 13:45:08', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '351eb3377b6c4ab0d183408555d67fc0', 'wp_user'),
(126, '2018-02-21 13:45:16', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '435d1153b03714c89df1d94b70ce640d', 'wp_user'),
(127, '2018-02-21 13:48:45', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', '29f0cc7d3151b31542f0dc9ad72562c0', 'wp_user'),
(128, '2018-02-21 13:49:01', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '17d8a84a4cce371cf00dc19645c30541', 'wp_user'),
(129, '2018-02-21 13:49:12', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', '1f59c7c5a64ae59cd7e373133572b975', 'wp_user'),
(130, '2018-02-21 13:55:51', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', '45b979ac40791af6d99461e482bccf2b', 'wp_user'),
(131, '2018-02-21 13:56:08', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '678b66ee007caa3331f98ef79124fdb5', 'wp_user'),
(132, '2018-02-21 13:56:41', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'ad0b2c3b6b388480b4f7f46a86db4a6e', 'wp_user'),
(133, '2018-02-21 14:06:10', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '176e0a9bf1f8eef238a513c981d4b355', 'wp_user'),
(134, '2018-02-21 14:06:17', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'c822ddcf2dd704c76f73c9d49fbe770a', 'wp_user'),
(135, '2018-02-21 14:07:54', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', 'c7a55f0269e82ec1957554206532e06d', 'wp_user'),
(136, '2018-02-21 14:07:57', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', '1705a023ab0636cc311129c4c662c3b6', 'wp_user'),
(137, '2018-02-21 14:08:01', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', 'b0f2a17e51f79b546cf65a3d4b394c2d', 'wp_user'),
(138, '2018-02-21 14:08:05', 'SimpleMediaLogger', 'info', 'Deleted {post_type} "{attachment_title}" ("{attachment_filename}")', 'ca6dc1fd4c94fe77dc18673ee4508739', 'wp_user'),
(139, '2018-02-21 14:08:11', 'SimpleMediaLogger', 'info', 'Created {post_type} "{attachment_title}"', '8cb23938b056158e32f371ddfe2171a8', 'wp_user'),
(140, '2018-02-21 14:08:21', 'SimplePostLogger', 'info', 'Updated {post_type} "{post_title}"', 'a7135a86879047f85e090735ee2c2a04', 'wp_user'),
(141, '2018-02-21 19:17:34', 'SimpleUserLogger', 'info', 'Logged in', 'ea0673203f474f142f01a078ca6bcb99', 'wp_user'),
(142, '2018-02-27 12:48:23', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', 'dd4a71b9ddb64a4a1fc09fb36353bd88', 'wp'),
(143, '2018-02-27 12:48:23', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', 'c3e70f947105a2943f20f909a3c9ef00', 'wp'),
(144, '2018-02-28 18:41:06', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '94e22c4eea0e19f4aff3889aef73295b', 'wp'),
(145, '2018-03-01 16:17:52', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '86db2f721d92b7d66fe8e520de6d0fb1', 'wp'),
(146, '2018-03-01 17:01:05', 'SimpleUserLogger', 'info', 'Logged in', '3c8416654afa4d6051ed91c371b9070a', 'wp_user'),
(147, '2018-03-01 17:03:55', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '4fc78201e090ffbe9bcf03fddeeb4ef2', 'wp_user'),
(148, '2018-03-01 17:03:58', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '54a127e72fac059f4c862bbc45a9f035', 'wp_user'),
(149, '2018-03-01 17:04:03', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '458ccbe5d54df0192a48d81264e09256', 'wp_user'),
(150, '2018-03-01 17:45:18', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '794ea179585804515bd4b1160ad6b20e', 'wp_user'),
(151, '2018-03-01 17:45:21', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', 'c9212ee6021fddf2de05ddfb19164a76', 'wp_user'),
(152, '2018-03-01 17:45:23', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '140354f745c5440b71cf6bbefc689e1b', 'wp_user'),
(153, '2018-03-01 17:47:33', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', 'a8fc0f5fa6f1a89eda3f0bd49212cb6c', 'wp_user'),
(154, '2018-03-01 17:47:56', 'SimpleUserLogger', 'info', 'Edited the profile for user {edited_user_login} ({edited_user_email})', '344dc89d94d13521ceeacab4795e7ef1', 'wp_user'),
(155, '2018-03-03 11:04:01', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '35fb8f128cc991db848d87f0e12af17e', 'wp'),
(156, '2018-03-06 16:57:17', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', 'b3531b273ee6f5366528950d2977ea94', 'wp'),
(157, '2018-03-06 16:57:17', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', 'c38941300d35e19a55cb0bcdb59f73ae', 'wp'),
(158, '2018-03-09 19:03:48', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '13eca12444cf2eeb3490c8c276d80f4d', 'wp'),
(159, '2018-03-19 16:13:01', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '3efe26e3f0331e245d82145bb55085cd', 'wp'),
(160, '2018-03-19 16:13:01', 'AvailableUpdatesLogger', 'notice', 'Found an update to plugin "{plugin_name}"', '6f82eac8f8bc5ff8a771746dd5e55e39', 'wp');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_simple_history_contexts`
--

CREATE TABLE IF NOT EXISTS `uk_simple_history_contexts` (
  `context_id` bigint(20) unsigned NOT NULL,
  `history_id` bigint(20) unsigned NOT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` longtext
) ENGINE=MyISAM AUTO_INCREMENT=1729 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `uk_simple_history_contexts`
--

INSERT INTO `uk_simple_history_contexts` (`context_id`, `history_id`, `key`, `value`) VALUES
(1, 1, 'plugin_name', 'Simple History'),
(2, 1, 'plugin_description', 'Plugin that logs various things that occur in WordPress and then presents those events in a very nice GUI.'),
(3, 1, 'plugin_url', 'http://simple-history.com'),
(4, 1, 'plugin_version', '2.20'),
(5, 1, 'plugin_author', 'Pär Thernström'),
(6, 1, '_message_key', 'plugin_installed'),
(7, 1, '_user_id', '1'),
(8, 1, '_user_login', 'unika'),
(9, 1, '_user_email', 'contato@unika.com.br'),
(10, 1, '_server_remote_addr', '168.181.49.179'),
(11, 1, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=Simple+History&tab=search&type=term'),
(12, 2, 'plugin_name', 'Simple History'),
(13, 2, 'plugin_description', 'Plugin that logs various things that occur in WordPress and then presents those events in a very nice GUI.'),
(14, 2, 'plugin_url', 'http://simple-history.com'),
(15, 2, 'plugin_version', '2.20'),
(16, 2, 'plugin_author', 'Pär Thernström'),
(17, 2, 'plugin_slug', 'simple-history'),
(18, 2, 'plugin_title', '<a href="http://simple-history.com/">Simple History</a>'),
(19, 2, '_message_key', 'plugin_activated'),
(20, 2, '_user_id', '1'),
(21, 2, '_user_login', 'unika'),
(22, 2, '_user_email', 'contato@unika.com.br'),
(23, 2, '_server_remote_addr', '168.181.49.179'),
(24, 2, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=Simple+History&tab=search&type=term'),
(25, 3, '_user_id', '1'),
(26, 3, '_user_login', 'unika'),
(27, 3, '_user_email', 'contato@unika.com.br'),
(28, 3, '_server_remote_addr', '168.181.49.179'),
(29, 3, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=Simple+History&tab=search&type=term'),
(30, 4, '_user_id', '1'),
(31, 4, '_user_login', 'unika'),
(32, 4, '_user_email', 'contato@unika.com.br'),
(33, 4, '_server_remote_addr', '168.181.49.179'),
(34, 4, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=Simple+History&tab=search&type=term'),
(35, 5, 'plugin_name', 'Simple History'),
(36, 5, 'plugin_description', 'Plugin that logs various things that occur in WordPress and then presents those events in a very nice GUI.'),
(37, 5, 'plugin_url', 'http://simple-history.com'),
(38, 5, 'plugin_version', '2.20'),
(39, 5, 'plugin_author', 'Pär Thernström'),
(40, 5, '_message_key', 'plugin_installed'),
(41, 5, '_wp_cron_running', 'true'),
(42, 5, '_server_remote_addr', '50.116.86.118'),
(43, 5, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1516990608.0169880390167236328125'),
(44, 6, 'plugin_name', 'Simple History'),
(45, 6, 'plugin_description', 'Plugin that logs various things that occur in WordPress and then presents those events in a very nice GUI.'),
(46, 6, 'plugin_url', 'http://simple-history.com'),
(47, 6, 'plugin_version', '2.20'),
(48, 6, 'plugin_author', 'Pär Thernström'),
(49, 6, 'plugin_slug', 'simple-history'),
(50, 6, 'plugin_title', '<a href="http://simple-history.com/">Simple History</a>'),
(51, 6, '_message_key', 'plugin_activated'),
(52, 6, '_wp_cron_running', 'true'),
(53, 6, '_server_remote_addr', '50.116.86.118'),
(54, 6, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1516990608.0169880390167236328125'),
(55, 7, 'created_user_id', '2'),
(56, 7, 'created_user_email', 'vanessa@handgran.com'),
(57, 7, 'created_user_login', 'Vanessa'),
(58, 7, 'created_user_role', 'administrator'),
(59, 7, 'created_user_first_name', ''),
(60, 7, 'created_user_last_name', ''),
(61, 7, 'created_user_url', ''),
(62, 7, 'send_user_notification', '1'),
(63, 7, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'),
(64, 7, '_message_key', 'user_created'),
(65, 7, '_user_id', '1'),
(66, 7, '_user_login', 'unika'),
(67, 7, '_user_email', 'contato@unika.com.br'),
(68, 7, '_server_remote_addr', '168.181.49.179'),
(69, 7, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-new.php'),
(70, 8, '_message_key', 'user_logged_out'),
(71, 8, '_user_id', '1'),
(72, 8, '_user_login', 'unika'),
(73, 8, '_user_email', 'contato@unika.com.br'),
(74, 8, '_server_remote_addr', '168.181.49.179'),
(75, 8, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=2&wp_http_referer=%2Fwp-admin%2Fusers.php%3Fupdate%3Dadd%26id%3D2'),
(76, 9, 'user_id', '2'),
(77, 9, 'user_email', 'vanessa@handgran.com'),
(78, 9, 'user_login', 'Vanessa'),
(79, 9, '_user_id', '2'),
(80, 9, '_user_login', 'Vanessa'),
(81, 9, '_user_email', 'vanessa@handgran.com'),
(82, 9, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'),
(83, 9, '_message_key', 'user_logged_in'),
(84, 9, '_server_remote_addr', '168.181.49.179'),
(85, 9, '_server_http_referer', 'http://unika.handgran.com.br/wp-login.php?loggedout=true'),
(86, 10, '_message_key', 'user_logged_out'),
(87, 10, '_user_id', '2'),
(88, 10, '_user_login', 'Vanessa'),
(89, 10, '_user_email', 'vanessa@handgran.com'),
(90, 10, '_server_remote_addr', '168.181.49.179'),
(91, 10, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/'),
(92, 11, 'user_id', '1'),
(93, 11, 'user_email', 'contato@unika.com.br'),
(94, 11, 'user_login', 'unika'),
(95, 11, '_user_id', '1'),
(96, 11, '_user_login', 'unika'),
(97, 11, '_user_email', 'contato@unika.com.br'),
(98, 11, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'),
(99, 11, '_message_key', 'user_logged_in'),
(100, 11, '_server_remote_addr', '168.181.49.179'),
(101, 11, '_server_http_referer', 'http://unika.handgran.com.br/wp-login.php?redirect_to=http%3A%2F%2Funika.handgran.com.br%2Fwp-admin%2F&reauth=1'),
(102, 12, 'post_id', '211'),
(103, 12, 'post_type', 'depoimento'),
(104, 12, 'post_title', 'Leandro Campos de Faria'),
(105, 12, 'post_prev_post_title', 'CBA - Companhia Brasileira de Alumínio'),
(106, 12, 'post_new_post_title', 'Leandro Campos de Faria'),
(107, 12, '_message_key', 'post_updated'),
(108, 12, '_user_id', '1'),
(109, 12, '_user_login', 'unika'),
(110, 12, '_user_email', 'contato@unika.com.br'),
(111, 12, '_server_remote_addr', '168.181.49.179'),
(112, 12, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=211&action=edit'),
(113, 13, 'post_id', '214'),
(114, 13, 'post_type', 'depoimento'),
(115, 13, 'post_title', 'Isabela Malpighi'),
(116, 13, '_message_key', 'post_updated'),
(117, 13, '_user_id', '1'),
(118, 13, '_user_login', 'unika'),
(119, 13, '_user_email', 'contato@unika.com.br'),
(120, 13, '_server_remote_addr', '168.181.49.179'),
(121, 13, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=214&action=edit'),
(122, 14, 'post_id', '214'),
(123, 14, 'post_type', 'depoimento'),
(124, 14, 'post_title', 'Isabela Malpighi'),
(125, 14, '_message_key', 'post_updated'),
(126, 14, '_user_id', '1'),
(127, 14, '_user_login', 'unika'),
(128, 14, '_user_email', 'contato@unika.com.br'),
(129, 14, '_server_remote_addr', '168.181.49.179'),
(130, 14, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=214&action=edit'),
(131, 15, 'post_id', '213'),
(132, 15, 'post_type', 'depoimento'),
(133, 15, 'post_title', 'Delvair Castro'),
(134, 15, 'post_prev_post_title', 'Delvair Castro – Perito Comportamental Especialista de Projetos de Escavação de Shaft e Raise em Mina Subterrânea'),
(135, 15, 'post_new_post_title', 'Delvair Castro'),
(136, 15, '_message_key', 'post_updated'),
(137, 15, '_user_id', '1'),
(138, 15, '_user_login', 'unika'),
(139, 15, '_user_email', 'contato@unika.com.br'),
(140, 15, '_server_remote_addr', '168.181.49.179'),
(141, 15, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=213&action=edit'),
(142, 16, 'post_id', '212'),
(143, 16, 'post_type', 'depoimento'),
(144, 16, 'post_title', 'Andres Zuluaga Suarez'),
(145, 16, 'post_prev_post_title', 'Andres Zuluaga Suarez - Gerente da planta de Manufatura da Owens Illinois'),
(146, 16, 'post_new_post_title', 'Andres Zuluaga Suarez'),
(147, 16, '_message_key', 'post_updated'),
(148, 16, '_user_id', '1'),
(149, 16, '_user_login', 'unika'),
(150, 16, '_user_email', 'contato@unika.com.br'),
(151, 16, '_server_remote_addr', '168.181.49.179'),
(152, 16, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=212&action=edit'),
(153, 17, 'plugin_name', 'Contact Form 7'),
(154, 17, 'plugin_current_version', '4.9.1'),
(155, 17, 'plugin_new_version', '4.9.2'),
(156, 17, '_message_key', 'plugin_update_available'),
(157, 17, '_server_remote_addr', '50.116.86.118'),
(158, 17, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1517226375.6628398895263671875000'),
(159, 18, 'plugin_name', 'Meta Box'),
(160, 18, 'plugin_current_version', '4.12.6'),
(161, 18, 'plugin_new_version', '4.13.1'),
(162, 18, '_message_key', 'plugin_update_available'),
(163, 18, '_server_remote_addr', '50.116.86.118'),
(164, 18, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1517226375.6628398895263671875000'),
(165, 19, 'plugin_name', 'Post Types Order'),
(166, 19, 'plugin_current_version', '1.9.3.5'),
(167, 19, 'plugin_new_version', '1.9.3.6'),
(168, 19, '_message_key', 'plugin_update_available'),
(169, 19, '_server_remote_addr', '50.116.86.118'),
(170, 19, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1517226375.6628398895263671875000'),
(171, 20, 'plugin_name', 'Yoast SEO'),
(172, 20, 'plugin_current_version', '5.9'),
(173, 20, 'plugin_new_version', '6.2'),
(174, 20, '_message_key', 'plugin_update_available'),
(175, 20, '_server_remote_addr', '50.116.86.118'),
(176, 20, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1517226375.6628398895263671875000'),
(177, 21, 'theme_name', 'Twenty Fifteen'),
(178, 21, 'theme_current_version', '1.8'),
(179, 21, 'theme_new_version', '1.9'),
(180, 21, '_message_key', 'theme_update_available'),
(181, 21, '_server_remote_addr', '50.116.86.118'),
(182, 21, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1517226375.6628398895263671875000'),
(183, 22, 'plugin_name', 'All In One WP Security'),
(184, 22, 'plugin_current_version', '4.3.1'),
(185, 22, 'plugin_new_version', '4.3.2'),
(186, 22, '_message_key', 'plugin_update_available'),
(187, 22, '_server_remote_addr', '50.116.86.118'),
(188, 22, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1517853908.5689771175384521484375'),
(189, 23, 'plugin_name', 'Contact Form 7'),
(190, 23, 'plugin_current_version', '4.9.1'),
(191, 23, 'plugin_new_version', '5.0'),
(192, 23, '_message_key', 'plugin_update_available'),
(193, 23, '_server_remote_addr', '50.116.86.118'),
(194, 23, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1517853908.5689771175384521484375'),
(195, 24, 'plugin_name', 'Meta Box'),
(196, 24, 'plugin_current_version', '4.12.6'),
(197, 24, 'plugin_new_version', '4.13.2'),
(198, 24, '_message_key', 'plugin_update_available'),
(199, 24, '_server_remote_addr', '50.116.86.118'),
(200, 24, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1517853908.5689771175384521484375'),
(201, 25, 'wp_core_current_version', '4.9.2'),
(202, 25, 'wp_core_new_version', '4.9.4'),
(203, 25, '_message_key', 'core_update_available'),
(204, 25, '_server_remote_addr', '50.116.86.118'),
(205, 25, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1518115207.5915300846099853515625'),
(206, 26, 'plugin_name', 'Disqus Comment System'),
(207, 26, 'plugin_current_version', '2.87'),
(208, 26, 'plugin_new_version', '3.0.9'),
(209, 26, '_message_key', 'plugin_update_available'),
(210, 26, '_server_remote_addr', '50.116.86.118'),
(211, 26, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1518115207.5915300846099853515625'),
(212, 27, 'plugin_name', 'Meta Box'),
(213, 27, 'plugin_current_version', '4.12.6'),
(214, 27, 'plugin_new_version', '4.13.3'),
(215, 27, '_message_key', 'plugin_update_available'),
(216, 27, '_server_remote_addr', '50.116.86.118'),
(217, 27, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1518115207.5915300846099853515625'),
(218, 28, 'plugin_name', 'Redux Framework'),
(219, 28, 'plugin_current_version', '3.6.7.7'),
(220, 28, 'plugin_new_version', '3.6.8'),
(221, 28, '_message_key', 'plugin_update_available'),
(222, 28, '_server_remote_addr', '50.116.86.118'),
(223, 28, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1518115207.5915300846099853515625'),
(224, 29, 'prev_version', '4.9.2'),
(225, 29, 'new_version', '4.9.4'),
(226, 29, '_message_key', 'core_auto_updated'),
(227, 29, '_wp_cron_running', 'true'),
(228, 29, '_server_remote_addr', '50.116.86.118'),
(229, 29, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1518115207.5915300846099853515625'),
(230, 30, 'plugin_name', 'Disqus Comment System'),
(231, 30, 'plugin_current_version', '2.87'),
(232, 30, 'plugin_new_version', '3.0.12'),
(233, 30, '_message_key', 'plugin_update_available'),
(234, 30, '_server_remote_addr', '50.116.86.118'),
(235, 30, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1518708407.2585639953613281250000'),
(236, 31, 'plugin_name', 'Yoast SEO'),
(237, 31, 'plugin_current_version', '5.9'),
(238, 31, 'plugin_new_version', '6.3'),
(239, 31, '_message_key', 'plugin_update_available'),
(240, 31, '_server_remote_addr', '50.116.86.118'),
(241, 31, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1518708407.2585639953613281250000'),
(242, 32, 'user_id', '1'),
(243, 32, 'user_email', 'contato@unika.com.br'),
(244, 32, 'user_login', 'unika'),
(245, 32, '_user_id', '1'),
(246, 32, '_user_login', 'unika'),
(247, 32, '_user_email', 'contato@unika.com.br'),
(248, 32, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(249, 32, '_message_key', 'user_logged_in'),
(250, 32, '_server_remote_addr', '168.181.49.144'),
(251, 32, '_server_http_referer', 'http://unika.handgran.com.br/wp-login.php?redirect_to=http%3A%2F%2Funika.handgran.com.br%2Fwp-admin%2F&reauth=1'),
(252, 33, 'post_id', '91'),
(253, 33, 'post_type', 'destaque'),
(254, 33, 'post_title', 'Ferramentas de Segurança Comportamental'),
(255, 33, 'post_prev_post_title', 'Ferramenta de Segurança Comportamental'),
(256, 33, 'post_new_post_title', 'Ferramentas de Segurança Comportamental'),
(257, 33, '_message_key', 'post_updated'),
(258, 33, '_user_id', '1'),
(259, 33, '_user_login', 'unika'),
(260, 33, '_user_email', 'contato@unika.com.br'),
(261, 33, '_server_remote_addr', '168.181.49.144'),
(262, 33, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=91&action=edit'),
(263, 34, 'created_user_id', '3'),
(264, 34, 'created_user_email', 'karla@unika.com.br'),
(265, 34, 'created_user_login', 'Karla'),
(266, 34, 'created_user_role', 'administrator'),
(267, 34, 'created_user_first_name', 'Karla Mikoski'),
(268, 34, 'created_user_last_name', ''),
(269, 34, 'created_user_url', ''),
(270, 34, 'send_user_notification', '1'),
(271, 34, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(272, 34, '_message_key', 'user_created'),
(273, 34, '_user_id', '1'),
(274, 34, '_user_login', 'unika'),
(275, 34, '_user_email', 'contato@unika.com.br'),
(276, 34, '_server_remote_addr', '168.181.49.144'),
(277, 34, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-new.php'),
(278, 35, 'edited_user_id', '3'),
(279, 35, 'edited_user_email', 'karla@unika.com.br'),
(280, 35, 'edited_user_login', 'Karla'),
(281, 35, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(282, 35, 'user_prev_locale', ''),
(283, 35, 'user_new_locale', 'site-default'),
(284, 35, '_message_key', 'user_updated_profile'),
(285, 35, '_user_id', '1'),
(286, 35, '_user_login', 'unika'),
(287, 35, '_user_email', 'contato@unika.com.br'),
(288, 35, '_server_remote_addr', '168.181.49.144'),
(289, 35, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php%3Fupdate%3Dadd%26id%3D3'),
(290, 36, 'post_id', '199'),
(291, 36, 'post_type', 'post'),
(292, 36, 'post_title', 'Indicadores Preventivos: por que são tão importantes?'),
(293, 36, '_message_key', 'post_updated'),
(294, 36, '_user_id', '1'),
(295, 36, '_user_login', 'unika'),
(296, 36, '_user_email', 'contato@unika.com.br'),
(297, 36, '_server_remote_addr', '168.181.49.144'),
(298, 36, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/edit.php'),
(299, 37, 'post_id', '204'),
(300, 37, 'post_type', 'post'),
(301, 37, 'post_title', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.'),
(302, 37, '_message_key', 'post_updated'),
(303, 37, '_user_id', '1'),
(304, 37, '_user_login', 'unika'),
(305, 37, '_user_email', 'contato@unika.com.br'),
(306, 37, '_server_remote_addr', '168.181.49.144'),
(307, 37, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/edit.php'),
(308, 38, 'post_id', '202'),
(309, 38, 'post_type', 'post'),
(310, 38, 'post_title', 'OAC &#8211; Observação e Abordagem de Comportamentos em Segurança – Por que implantar?'),
(311, 38, '_message_key', 'post_updated'),
(312, 38, '_user_id', '1'),
(313, 38, '_user_login', 'unika'),
(314, 38, '_user_email', 'contato@unika.com.br'),
(315, 38, '_server_remote_addr', '168.181.49.144'),
(316, 38, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/edit.php'),
(317, 39, 'post_id', '194'),
(318, 39, 'post_type', 'post'),
(319, 39, 'post_title', 'Evolução da Cultura de Segurança'),
(320, 39, '_message_key', 'post_updated'),
(321, 39, '_user_id', '1'),
(322, 39, '_user_login', 'unika'),
(323, 39, '_user_email', 'contato@unika.com.br'),
(324, 39, '_server_remote_addr', '168.181.49.144'),
(325, 39, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/edit.php'),
(326, 40, 'post_id', '191'),
(327, 40, 'post_type', 'post'),
(328, 40, 'post_title', 'Preciso escolher um tema para o DDS &#8211; Diálogos Diários de Segurança no Trabalho. O que devo levar em consideração?'),
(329, 40, '_message_key', 'post_updated'),
(330, 40, '_user_id', '1'),
(331, 40, '_user_login', 'unika'),
(332, 40, '_user_email', 'contato@unika.com.br'),
(333, 40, '_server_remote_addr', '168.181.49.144'),
(334, 40, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/edit.php'),
(335, 41, 'post_id', '173'),
(336, 41, 'post_type', 'post'),
(337, 41, 'post_title', 'Comportamento humano: O que você precisa saber para evitar  acidentes do trabalho'),
(338, 41, '_message_key', 'post_updated'),
(339, 41, '_user_id', '1'),
(340, 41, '_user_login', 'unika'),
(341, 41, '_user_email', 'contato@unika.com.br'),
(342, 41, '_server_remote_addr', '168.181.49.144'),
(343, 41, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/edit.php'),
(344, 42, 'edited_user_id', '3'),
(345, 42, 'edited_user_email', 'karla@unika.com.br'),
(346, 42, 'edited_user_login', 'Karla'),
(347, 42, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(348, 42, 'user_prev_locale', ''),
(349, 42, 'user_new_locale', 'site-default'),
(350, 42, '_message_key', 'user_updated_profile'),
(351, 42, '_user_id', '1'),
(352, 42, '_user_login', 'unika'),
(353, 42, '_user_email', 'contato@unika.com.br'),
(354, 42, '_server_remote_addr', '168.181.49.144'),
(355, 42, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(356, 43, 'edited_user_id', '3'),
(357, 43, 'edited_user_email', 'karla@unika.com.br'),
(358, 43, 'edited_user_login', 'Karla'),
(359, 43, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(360, 43, 'user_prev_locale', ''),
(361, 43, 'user_new_locale', 'site-default'),
(362, 43, '_message_key', 'user_updated_profile'),
(363, 43, '_user_id', '1'),
(364, 43, '_user_login', 'unika'),
(365, 43, '_user_email', 'contato@unika.com.br'),
(366, 43, '_server_remote_addr', '168.181.49.144'),
(367, 43, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(368, 44, 'edited_user_id', '3'),
(369, 44, 'edited_user_email', 'karla@unika.com.br'),
(370, 44, 'edited_user_login', 'Karla'),
(371, 44, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(372, 44, 'user_prev_locale', ''),
(373, 44, 'user_new_locale', 'site-default'),
(374, 44, '_message_key', 'user_updated_profile'),
(375, 44, '_user_id', '1'),
(376, 44, '_user_login', 'unika'),
(377, 44, '_user_email', 'contato@unika.com.br'),
(378, 44, '_server_remote_addr', '168.181.49.144'),
(379, 44, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(380, 45, 'edited_user_id', '3'),
(381, 45, 'edited_user_email', 'karla@unika.com.br'),
(382, 45, 'edited_user_login', 'Karla'),
(383, 45, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(384, 45, 'user_prev_locale', ''),
(385, 45, 'user_new_locale', 'site-default'),
(386, 45, '_message_key', 'user_updated_profile'),
(387, 45, '_user_id', '1'),
(388, 45, '_user_login', 'unika'),
(389, 45, '_user_email', 'contato@unika.com.br'),
(390, 45, '_server_remote_addr', '168.181.49.144'),
(391, 45, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(392, 46, 'plugin_slug', 'simple-author-box'),
(393, 46, 'plugin_name', 'Simple Author Box'),
(394, 46, 'plugin_version', '2.0.3'),
(395, 46, 'plugin_author', 'Macho Themes'),
(396, 46, 'plugin_last_updated', ''),
(397, 46, 'plugin_requires', ''),
(398, 46, 'plugin_tested', ''),
(399, 46, 'plugin_rating', ''),
(400, 46, 'plugin_num_ratings', ''),
(401, 46, 'plugin_downloaded', ''),
(402, 46, 'plugin_added', ''),
(403, 46, 'plugin_source_files', '[\n    "readme.txt",\n    "assets",\n    "template",\n    "uninstall.php",\n    "inc",\n    "simple-author-box.php"\n]'),
(404, 46, 'plugin_install_source', 'unknown'),
(405, 46, 'plugin_description', 'Adds a responsive author box with social icons on your posts. <cite>Por <a href="https://www.machothemes.com/">Macho Themes</a>.</cite>'),
(406, 46, 'plugin_url', 'http://wordpress.org/plugins/simple-author-box/'),
(407, 46, '_message_key', 'plugin_installed'),
(408, 46, '_user_id', '1'),
(409, 46, '_user_login', 'unika'),
(410, 46, '_user_email', 'contato@unika.com.br'),
(411, 46, '_server_remote_addr', '168.181.49.144'),
(412, 46, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=Simple+Author+Box&tab=search&type=term'),
(413, 47, 'plugin_name', 'Simple Author Box'),
(414, 47, 'plugin_slug', 'simple-author-box'),
(415, 47, 'plugin_title', '<a href="http://wordpress.org/plugins/simple-author-box/">Simple Author Box</a>'),
(416, 47, 'plugin_description', 'Adds a responsive author box with social icons on your posts. <cite>Por <a href="https://www.machothemes.com/">Macho Themes</a>.</cite>'),
(417, 47, 'plugin_author', '<a href="https://www.machothemes.com/">Macho Themes</a>'),
(418, 47, 'plugin_version', '2.0.3'),
(419, 47, 'plugin_url', 'http://wordpress.org/plugins/simple-author-box/'),
(420, 47, '_message_key', 'plugin_activated'),
(421, 47, '_user_id', '1'),
(422, 47, '_user_login', 'unika'),
(423, 47, '_user_email', 'contato@unika.com.br'),
(424, 47, '_server_remote_addr', '168.181.49.144'),
(425, 47, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=Simple+Author+Box&tab=search&type=term'),
(426, 48, 'edited_user_id', '3'),
(427, 48, 'edited_user_email', 'karla@unika.com.br'),
(428, 48, 'edited_user_login', 'Karla'),
(429, 48, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(430, 48, 'user_prev_locale', ''),
(431, 48, 'user_new_locale', 'site-default'),
(432, 48, '_message_key', 'user_updated_profile'),
(433, 48, '_user_id', '1'),
(434, 48, '_user_login', 'unika'),
(435, 48, '_user_email', 'contato@unika.com.br'),
(436, 48, '_server_remote_addr', '168.181.49.144'),
(437, 48, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(438, 49, 'plugin_name', 'Simple Author Box'),
(439, 49, 'plugin_slug', 'simple-author-box'),
(440, 49, 'plugin_title', '<a href="http://wordpress.org/plugins/simple-author-box/">Simple Author Box</a>'),
(441, 49, 'plugin_description', 'Adds a responsive author box with social icons on your posts. <cite>Por <a href="https://www.machothemes.com/">Macho Themes</a>.</cite>'),
(442, 49, 'plugin_author', '<a href="https://www.machothemes.com/">Macho Themes</a>'),
(443, 49, 'plugin_version', '2.0.3'),
(444, 49, 'plugin_url', 'http://wordpress.org/plugins/simple-author-box/'),
(445, 49, '_message_key', 'plugin_deactivated'),
(446, 49, '_user_id', '1'),
(447, 49, '_user_login', 'unika'),
(448, 49, '_user_email', 'contato@unika.com.br'),
(449, 49, '_server_remote_addr', '168.181.49.144'),
(450, 49, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugins.php'),
(451, 50, 'edited_user_id', '3'),
(452, 50, 'edited_user_email', 'karla@unika.com.br'),
(453, 50, 'edited_user_login', 'Karla'),
(454, 50, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(455, 50, 'user_prev_locale', ''),
(456, 50, 'user_new_locale', 'site-default'),
(457, 50, '_message_key', 'user_updated_profile'),
(458, 50, '_user_id', '1'),
(459, 50, '_user_login', 'unika'),
(460, 50, '_user_email', 'contato@unika.com.br'),
(461, 50, '_server_remote_addr', '168.181.49.144'),
(462, 50, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(463, 51, 'edited_user_id', '3'),
(464, 51, 'edited_user_email', 'karla@unika.com.br'),
(465, 51, 'edited_user_login', 'Karla'),
(466, 51, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(467, 51, 'user_prev_locale', ''),
(468, 51, 'user_new_locale', 'site-default'),
(469, 51, '_message_key', 'user_updated_profile'),
(470, 51, '_user_id', '1'),
(471, 51, '_user_login', 'unika'),
(472, 51, '_user_email', 'contato@unika.com.br'),
(473, 51, '_server_remote_addr', '168.181.49.144'),
(474, 51, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(475, 52, 'edited_user_id', '3'),
(476, 52, 'edited_user_email', 'karla@unika.com.br'),
(477, 52, 'edited_user_login', 'Karla'),
(478, 52, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(479, 52, 'user_prev_locale', ''),
(480, 52, 'user_new_locale', 'site-default'),
(481, 52, '_message_key', 'user_updated_profile'),
(482, 52, '_user_id', '1'),
(483, 52, '_user_login', 'unika'),
(484, 52, '_user_email', 'contato@unika.com.br'),
(485, 52, '_server_remote_addr', '168.181.49.144'),
(486, 52, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(487, 53, 'edited_user_id', '3'),
(488, 53, 'edited_user_email', 'karla@unika.com.br'),
(489, 53, 'edited_user_login', 'Karla'),
(490, 53, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(491, 53, 'user_prev_locale', ''),
(492, 53, 'user_new_locale', 'site-default'),
(493, 53, '_message_key', 'user_updated_profile'),
(494, 53, '_user_id', '1'),
(495, 53, '_user_login', 'unika'),
(496, 53, '_user_email', 'contato@unika.com.br'),
(497, 53, '_server_remote_addr', '168.181.49.144'),
(498, 53, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(499, 54, 'plugin_name', 'WP User Avatars'),
(500, 54, 'plugin_slug', 'wp-user-avatars'),
(501, 54, 'plugin_title', '<a href="https://wordpress.org/plugins/wp-user-avatars/">WP User Avatars</a>'),
(502, 54, 'plugin_description', 'Allow registered users to upload &amp; select their own avatars <cite>Por <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>.</cite>'),
(503, 54, 'plugin_author', '<a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>'),
(504, 54, 'plugin_version', '1.3.0'),
(505, 54, 'plugin_url', 'https://wordpress.org/plugins/wp-user-avatars/'),
(506, 54, '_message_key', 'plugin_deactivated'),
(507, 54, '_user_id', '1'),
(508, 54, '_user_login', 'unika'),
(509, 54, '_user_email', 'contato@unika.com.br'),
(510, 54, '_server_remote_addr', '168.181.49.144'),
(511, 54, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugins.php'),
(512, 55, 'plugin_name', 'Simple Author Box'),
(513, 55, 'plugin_slug', 'simple-author-box'),
(514, 55, 'plugin_title', '<a href="http://wordpress.org/plugins/simple-author-box/">Simple Author Box</a>'),
(515, 55, 'plugin_description', 'Adds a responsive author box with social icons on your posts. <cite>Por <a href="https://www.machothemes.com/">Macho Themes</a>.</cite>'),
(516, 55, 'plugin_author', '<a href="https://www.machothemes.com/">Macho Themes</a>'),
(517, 55, 'plugin_version', '2.0.3'),
(518, 55, 'plugin_url', 'http://wordpress.org/plugins/simple-author-box/'),
(519, 55, '_message_key', 'plugin_activated'),
(520, 55, '_user_id', '1'),
(521, 55, '_user_login', 'unika'),
(522, 55, '_user_email', 'contato@unika.com.br'),
(523, 55, '_server_remote_addr', '168.181.49.144'),
(524, 55, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugins.php?plugin_status=all&paged=1&s'),
(525, 56, 'plugin_name', 'Simple Author Box'),
(526, 56, 'plugin_slug', 'simple-author-box'),
(527, 56, 'plugin_title', '<a href="http://wordpress.org/plugins/simple-author-box/">Simple Author Box</a>'),
(528, 56, 'plugin_description', 'Adds a responsive author box with social icons on your posts. <cite>Por <a href="https://www.machothemes.com/">Macho Themes</a>.</cite>'),
(529, 56, 'plugin_author', '<a href="https://www.machothemes.com/">Macho Themes</a>'),
(530, 56, 'plugin_version', '2.0.3'),
(531, 56, 'plugin_url', 'http://wordpress.org/plugins/simple-author-box/'),
(532, 56, '_message_key', 'plugin_deactivated'),
(533, 56, '_user_id', '1'),
(534, 56, '_user_login', 'unika'),
(535, 56, '_user_email', 'contato@unika.com.br'),
(536, 56, '_server_remote_addr', '168.181.49.144'),
(537, 56, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugins.php?plugin_status=all&paged=1&s'),
(538, 57, 'plugin_name', 'Simple Author Box'),
(539, 57, 'plugin_slug', 'simple-author-box'),
(540, 57, 'plugin_title', '<a href="http://wordpress.org/plugins/simple-author-box/">Simple Author Box</a>'),
(541, 57, 'plugin_description', 'Adds a responsive author box with social icons on your posts. <cite>Por <a href="https://www.machothemes.com/">Macho Themes</a>.</cite>'),
(542, 57, 'plugin_author', '<a href="https://www.machothemes.com/">Macho Themes</a>'),
(543, 57, 'plugin_version', '2.0.3'),
(544, 57, 'plugin_url', 'http://wordpress.org/plugins/simple-author-box/'),
(545, 57, '_message_key', 'plugin_deactivated'),
(546, 57, '_user_id', '1'),
(547, 57, '_user_login', 'unika'),
(548, 57, '_user_email', 'contato@unika.com.br'),
(549, 57, '_server_remote_addr', '168.181.49.144'),
(550, 57, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugins.php?plugin_status=all&paged=1&s'),
(551, 58, 'plugin_name', 'WP User Avatars'),
(552, 58, 'plugin_slug', 'wp-user-avatars'),
(553, 58, 'plugin_title', '<a href="https://wordpress.org/plugins/wp-user-avatars/">WP User Avatars</a>'),
(554, 58, 'plugin_description', 'Allow registered users to upload &amp; select their own avatars <cite>Por <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>.</cite>'),
(555, 58, 'plugin_author', '<a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>'),
(556, 58, 'plugin_version', '1.3.0'),
(557, 58, 'plugin_url', 'https://wordpress.org/plugins/wp-user-avatars/'),
(558, 58, '_message_key', 'plugin_activated'),
(559, 58, '_user_id', '1'),
(560, 58, '_user_login', 'unika'),
(561, 58, '_user_email', 'contato@unika.com.br'),
(562, 58, '_server_remote_addr', '168.181.49.144'),
(563, 58, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugins.php?plugin_status=all&paged=1&s'),
(564, 59, 'plugin_name', 'WP User Avatars'),
(565, 59, 'plugin_slug', 'wp-user-avatars'),
(566, 59, 'plugin_title', '<a href="https://wordpress.org/plugins/wp-user-avatars/">WP User Avatars</a>'),
(567, 59, 'plugin_description', 'Allow registered users to upload &amp; select their own avatars <cite>Por <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>.</cite>'),
(568, 59, 'plugin_author', '<a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>'),
(569, 59, 'plugin_version', '1.3.0'),
(570, 59, 'plugin_url', 'https://wordpress.org/plugins/wp-user-avatars/'),
(571, 59, '_message_key', 'plugin_deactivated'),
(572, 59, '_user_id', '1'),
(573, 59, '_user_login', 'unika'),
(574, 59, '_user_email', 'contato@unika.com.br'),
(575, 59, '_server_remote_addr', '168.181.49.144'),
(576, 59, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugins.php?plugin_status=all&paged=1&s'),
(577, 60, 'plugin_slug', 'wp-user-avatar'),
(578, 60, 'plugin_name', 'WP User Avatar'),
(579, 60, 'plugin_version', '2.0.9'),
(580, 60, 'plugin_author', 'flippercode'),
(581, 60, 'plugin_last_updated', ''),
(582, 60, 'plugin_requires', ''),
(583, 60, 'plugin_tested', ''),
(584, 60, 'plugin_rating', ''),
(585, 60, 'plugin_num_ratings', ''),
(586, 60, 'plugin_downloaded', ''),
(587, 60, 'plugin_added', ''),
(588, 60, 'plugin_source_files', '[\n    "readme.txt",\n    "css",\n    "wp-user-avatar.php",\n    "lang",\n    "index.html",\n    "js",\n    "images",\n    "includes",\n    "uninstall.php"\n]'),
(589, 60, 'plugin_install_source', 'unknown'),
(590, 60, 'plugin_description', 'Use any image from your WordPress Media Library as a custom user avatar. Add your own Default Avatar. <cite>Por <a href="http://www.flippercode.com/">flippercode</a>.</cite>'),
(591, 60, 'plugin_url', 'http://wordpress.org/plugins/wp-user-avatar/'),
(592, 60, '_message_key', 'plugin_installed'),
(593, 60, '_user_id', '1'),
(594, 60, '_user_login', 'unika'),
(595, 60, '_user_email', 'contato@unika.com.br'),
(596, 60, '_server_remote_addr', '168.181.49.144'),
(597, 60, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=WP+User+Avatars&tab=search&type=term'),
(598, 61, 'plugin_name', 'WP User Avatar'),
(599, 61, 'plugin_slug', 'wp-user-avatar'),
(600, 61, 'plugin_title', '<a href="http://wordpress.org/plugins/wp-user-avatar/">WP User Avatar</a>'),
(601, 61, 'plugin_description', 'Use any image from your WordPress Media Library as a custom user avatar. Add your own Default Avatar. <cite>Por <a href="http://www.flippercode.com/">flippercode</a>.</cite>'),
(602, 61, 'plugin_author', '<a href="http://www.flippercode.com/">flippercode</a>'),
(603, 61, 'plugin_version', '2.0.9'),
(604, 61, 'plugin_url', 'http://wordpress.org/plugins/wp-user-avatar/'),
(605, 61, '_message_key', 'plugin_activated'),
(606, 61, '_user_id', '1'),
(607, 61, '_user_login', 'unika'),
(608, 61, '_user_email', 'contato@unika.com.br'),
(609, 61, '_server_remote_addr', '168.181.49.144'),
(610, 61, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=WP+User+Avatars&tab=search&type=term'),
(611, 62, 'edited_user_id', '3'),
(612, 62, 'edited_user_email', 'karla@unika.com.br'),
(613, 62, 'edited_user_login', 'Karla'),
(614, 62, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(615, 62, 'user_prev_locale', ''),
(616, 62, 'user_new_locale', 'site-default'),
(617, 62, '_message_key', 'user_updated_profile'),
(618, 62, '_user_id', '1'),
(619, 62, '_user_login', 'unika'),
(620, 62, '_user_email', 'contato@unika.com.br'),
(621, 62, '_server_remote_addr', '168.181.49.144'),
(622, 62, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(623, 63, 'edited_user_id', '3'),
(624, 63, 'edited_user_email', 'karla@unika.com.br'),
(625, 63, 'edited_user_login', 'Karla'),
(626, 63, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(627, 63, 'user_prev_locale', ''),
(628, 63, 'user_new_locale', 'site-default'),
(629, 63, '_message_key', 'user_updated_profile'),
(630, 63, '_user_id', '1'),
(631, 63, '_user_login', 'unika'),
(632, 63, '_user_email', 'contato@unika.com.br'),
(633, 63, '_server_remote_addr', '168.181.49.144'),
(634, 63, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(635, 64, 'post_type', 'attachment'),
(636, 64, 'attachment_id', '229'),
(637, 64, 'attachment_title', 'karla@2x'),
(638, 64, 'attachment_filename', 'karla@2x.png'),
(639, 64, 'attachment_mime', 'image/png'),
(640, 64, 'attachment_filesize', '1391149'),
(641, 64, '_message_key', 'attachment_created'),
(642, 64, '_user_id', '1'),
(643, 64, '_user_login', 'unika'),
(644, 64, '_user_email', 'contato@unika.com.br'),
(645, 64, '_server_remote_addr', '168.181.49.144'),
(646, 64, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/admin.php?page=wp-user-avatar'),
(647, 65, 'edited_user_id', '3'),
(648, 65, 'edited_user_email', 'karla@unika.com.br'),
(649, 65, 'edited_user_login', 'Karla'),
(650, 65, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(651, 65, 'user_prev_locale', ''),
(652, 65, 'user_new_locale', 'site-default'),
(653, 65, '_message_key', 'user_updated_profile'),
(654, 65, '_user_id', '1'),
(655, 65, '_user_login', 'unika'),
(656, 65, '_user_email', 'contato@unika.com.br'),
(657, 65, '_server_remote_addr', '168.181.49.144'),
(658, 65, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(659, 66, 'edited_user_id', '3'),
(660, 66, 'edited_user_email', 'karla@unika.com.br'),
(661, 66, 'edited_user_login', 'Karla'),
(662, 66, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(663, 66, 'user_prev_locale', ''),
(664, 66, 'user_new_locale', 'site-default'),
(665, 66, '_message_key', 'user_updated_profile'),
(666, 66, '_user_id', '1'),
(667, 66, '_user_login', 'unika'),
(668, 66, '_user_email', 'contato@unika.com.br'),
(669, 66, '_server_remote_addr', '168.181.49.144'),
(670, 66, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(671, 67, 'plugin_name', 'WP User Avatar'),
(672, 67, 'plugin_slug', 'wp-user-avatar'),
(673, 67, 'plugin_title', '<a href="http://wordpress.org/plugins/wp-user-avatar/">WP User Avatar</a>'),
(674, 67, 'plugin_description', 'Use any image from your WordPress Media Library as a custom user avatar. Add your own Default Avatar. <cite>Por <a href="http://www.flippercode.com/">flippercode</a>.</cite>'),
(675, 67, 'plugin_author', '<a href="http://www.flippercode.com/">flippercode</a>'),
(676, 67, 'plugin_version', '2.0.9'),
(677, 67, 'plugin_url', 'http://wordpress.org/plugins/wp-user-avatar/'),
(678, 67, '_message_key', 'plugin_deactivated'),
(679, 67, '_user_id', '1'),
(680, 67, '_user_login', 'unika'),
(681, 67, '_user_email', 'contato@unika.com.br'),
(682, 67, '_server_remote_addr', '168.181.49.144'),
(683, 67, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugins.php'),
(684, 68, 'plugin_slug', 'wp-user-avatars'),
(685, 68, 'plugin_name', 'WP User Avatars'),
(686, 68, 'plugin_version', '1.3.0'),
(687, 68, 'plugin_author', 'John James Jacoby'),
(688, 68, 'plugin_last_updated', ''),
(689, 68, 'plugin_requires', ''),
(690, 68, 'plugin_tested', ''),
(691, 68, 'plugin_rating', ''),
(692, 68, 'plugin_num_ratings', ''),
(693, 68, 'plugin_downloaded', ''),
(694, 68, 'plugin_added', ''),
(695, 68, 'plugin_source_files', '[\n    "readme.txt",\n    "wp-user-avatars.php",\n    "wp-user-avatars"\n]'),
(696, 68, 'plugin_install_source', 'unknown'),
(697, 68, 'plugin_description', 'Allow registered users to upload &amp; select their own avatars <cite>Por <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>.</cite>'),
(698, 68, 'plugin_url', 'https://wordpress.org/plugins/wp-user-avatars/'),
(699, 68, '_message_key', 'plugin_installed'),
(700, 68, '_user_id', '1'),
(701, 68, '_user_login', 'unika'),
(702, 68, '_user_email', 'contato@unika.com.br'),
(703, 68, '_server_remote_addr', '168.181.49.144'),
(704, 68, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=WP+User+Avatars&tab=search&type=term'),
(705, 69, 'plugin_name', 'WP User Avatars'),
(706, 69, 'plugin_slug', 'wp-user-avatars'),
(707, 69, 'plugin_title', '<a href="https://wordpress.org/plugins/wp-user-avatars/">WP User Avatars</a>'),
(708, 69, 'plugin_description', 'Allow registered users to upload &amp; select their own avatars <cite>Por <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>.</cite>'),
(709, 69, 'plugin_author', '<a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>'),
(710, 69, 'plugin_version', '1.3.0'),
(711, 69, 'plugin_url', 'https://wordpress.org/plugins/wp-user-avatars/'),
(712, 69, '_message_key', 'plugin_activated'),
(713, 69, '_user_id', '1'),
(714, 69, '_user_login', 'unika'),
(715, 69, '_user_email', 'contato@unika.com.br'),
(716, 69, '_server_remote_addr', '168.181.49.144'),
(717, 69, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=WP+User+Avatars&tab=search&type=term'),
(718, 70, 'edited_user_id', '3'),
(719, 70, 'edited_user_email', 'karla@unika.com.br'),
(720, 70, 'edited_user_login', 'Karla'),
(721, 70, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(722, 70, 'user_prev_locale', ''),
(723, 70, 'user_new_locale', 'site-default'),
(724, 70, '_message_key', 'user_updated_profile'),
(725, 70, '_user_id', '1'),
(726, 70, '_user_login', 'unika'),
(727, 70, '_user_email', 'contato@unika.com.br'),
(728, 70, '_server_remote_addr', '168.181.49.144'),
(729, 70, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(730, 71, 'edited_user_id', '3'),
(731, 71, 'edited_user_email', 'karla@unika.com.br'),
(732, 71, 'edited_user_login', 'Karla'),
(733, 71, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(734, 71, 'user_prev_locale', ''),
(735, 71, 'user_new_locale', 'site-default'),
(736, 71, '_message_key', 'user_updated_profile'),
(737, 71, '_user_id', '1'),
(738, 71, '_user_login', 'unika'),
(739, 71, '_user_email', 'contato@unika.com.br'),
(740, 71, '_server_remote_addr', '168.181.49.144'),
(741, 71, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(742, 72, 'plugin_name', 'WP User Avatars'),
(743, 72, 'plugin_slug', 'wp-user-avatars'),
(744, 72, 'plugin_title', '<a href="https://wordpress.org/plugins/wp-user-avatars/">WP User Avatars</a>'),
(745, 72, 'plugin_description', 'Allow registered users to upload &amp; select their own avatars <cite>Por <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>.</cite>'),
(746, 72, 'plugin_author', '<a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>'),
(747, 72, 'plugin_version', '1.3.0'),
(748, 72, 'plugin_url', 'https://wordpress.org/plugins/wp-user-avatars/'),
(749, 72, '_message_key', 'plugin_deactivated'),
(750, 72, '_user_id', '1'),
(751, 72, '_user_login', 'unika'),
(752, 72, '_user_email', 'contato@unika.com.br'),
(753, 72, '_server_remote_addr', '168.181.49.144'),
(754, 72, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugins.php?plugin_status=all&paged=1&s'),
(755, 73, 'plugin_name', 'WP User Avatars'),
(756, 73, 'plugin_slug', 'wp-user-avatars'),
(757, 73, 'plugin_title', '<a href="https://wordpress.org/plugins/wp-user-avatars/">WP User Avatars</a>'),
(758, 73, 'plugin_description', 'Allow registered users to upload &amp; select their own avatars <cite>Por <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>.</cite>'),
(759, 73, 'plugin_author', '<a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>'),
(760, 73, 'plugin_version', '1.3.0'),
(761, 73, 'plugin_url', 'https://wordpress.org/plugins/wp-user-avatars/'),
(762, 73, '_message_key', 'plugin_activated'),
(763, 73, '_user_id', '1'),
(764, 73, '_user_login', 'unika'),
(765, 73, '_user_email', 'contato@unika.com.br'),
(766, 73, '_server_remote_addr', '168.181.49.144'),
(767, 73, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugins.php?plugin_status=all&paged=1&s'),
(768, 74, 'plugin_name', 'WP User Avatars'),
(769, 74, 'plugin_slug', 'wp-user-avatars'),
(770, 74, 'plugin_title', '<a href="https://wordpress.org/plugins/wp-user-avatars/">WP User Avatars</a>'),
(771, 74, 'plugin_description', 'Allow registered users to upload &amp; select their own avatars <cite>Por <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>.</cite>'),
(772, 74, 'plugin_author', '<a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>'),
(773, 74, 'plugin_version', '1.3.0'),
(774, 74, 'plugin_url', 'https://wordpress.org/plugins/wp-user-avatars/'),
(775, 74, '_message_key', 'plugin_deactivated'),
(776, 74, '_user_id', '1'),
(777, 74, '_user_login', 'unika'),
(778, 74, '_user_email', 'contato@unika.com.br'),
(779, 74, '_server_remote_addr', '168.181.49.144'),
(780, 74, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugins.php?plugin_status=all&paged=1&s'),
(781, 75, 'plugin_slug', 'wp-user-avatars'),
(782, 75, 'plugin_name', 'WP User Avatars'),
(783, 75, 'plugin_version', '1.3.0'),
(784, 75, 'plugin_author', 'John James Jacoby'),
(785, 75, 'plugin_last_updated', ''),
(786, 75, 'plugin_requires', ''),
(787, 75, 'plugin_tested', ''),
(788, 75, 'plugin_rating', ''),
(789, 75, 'plugin_num_ratings', ''),
(790, 75, 'plugin_downloaded', ''),
(791, 75, 'plugin_added', ''),
(792, 75, 'plugin_source_files', '[\n    "readme.txt",\n    "wp-user-avatars.php",\n    "wp-user-avatars"\n]'),
(793, 75, 'plugin_install_source', 'unknown'),
(794, 75, 'plugin_description', 'Allow registered users to upload &amp; select their own avatars <cite>Por <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>.</cite>'),
(795, 75, 'plugin_url', 'https://wordpress.org/plugins/wp-user-avatars/'),
(796, 75, '_message_key', 'plugin_installed'),
(797, 75, '_user_id', '1'),
(798, 75, '_user_login', 'unika'),
(799, 75, '_user_email', 'contato@unika.com.br'),
(800, 75, '_server_remote_addr', '168.181.49.144'),
(801, 75, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=WP+User+Avatars&tab=search&type=term'),
(802, 76, 'plugin_name', 'WP User Avatars'),
(803, 76, 'plugin_slug', 'wp-user-avatars'),
(804, 76, 'plugin_title', '<a href="https://wordpress.org/plugins/wp-user-avatars/">WP User Avatars</a>'),
(805, 76, 'plugin_description', 'Allow registered users to upload &amp; select their own avatars <cite>Por <a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>.</cite>'),
(806, 76, 'plugin_author', '<a href="https://profiles.wordpress.org/johnjamesjacoby/">John James Jacoby</a>'),
(807, 76, 'plugin_version', '1.3.0'),
(808, 76, 'plugin_url', 'https://wordpress.org/plugins/wp-user-avatars/'),
(809, 76, '_message_key', 'plugin_activated'),
(810, 76, '_user_id', '1'),
(811, 76, '_user_login', 'unika'),
(812, 76, '_user_email', 'contato@unika.com.br'),
(813, 76, '_server_remote_addr', '168.181.49.144'),
(814, 76, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/plugin-install.php?s=WP+User+Avatars&tab=search&type=term'),
(815, 77, 'edited_user_id', '3'),
(816, 77, 'edited_user_email', 'karla@unika.com.br'),
(817, 77, 'edited_user_login', 'Karla'),
(818, 77, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(819, 77, 'user_prev_locale', ''),
(820, 77, 'user_new_locale', 'site-default'),
(821, 77, '_message_key', 'user_updated_profile'),
(822, 77, '_user_id', '1'),
(823, 77, '_user_login', 'unika'),
(824, 77, '_user_email', 'contato@unika.com.br'),
(825, 77, '_server_remote_addr', '168.181.49.144'),
(826, 77, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(827, 78, 'edited_user_id', '3'),
(828, 78, 'edited_user_email', 'karla@unika.com.br'),
(829, 78, 'edited_user_login', 'Karla'),
(830, 78, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(831, 78, 'user_prev_locale', ''),
(832, 78, 'user_new_locale', 'site-default'),
(833, 78, '_message_key', 'user_updated_profile'),
(834, 78, '_user_id', '1'),
(835, 78, '_user_login', 'unika'),
(836, 78, '_user_email', 'contato@unika.com.br'),
(837, 78, '_server_remote_addr', '168.181.49.144'),
(838, 78, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(839, 79, 'edited_user_id', '3'),
(840, 79, 'edited_user_email', 'karla@unika.com.br'),
(841, 79, 'edited_user_login', 'Karla'),
(842, 79, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(843, 79, 'user_prev_locale', ''),
(844, 79, 'user_new_locale', 'site-default'),
(845, 79, '_message_key', 'user_updated_profile'),
(846, 79, '_user_id', '1'),
(847, 79, '_user_login', 'unika');
INSERT INTO `uk_simple_history_contexts` (`context_id`, `history_id`, `key`, `value`) VALUES
(848, 79, '_user_email', 'contato@unika.com.br'),
(849, 79, '_server_remote_addr', '168.181.49.144'),
(850, 79, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(851, 80, 'edited_user_id', '1'),
(852, 80, 'edited_user_email', 'contato@unika.com.br'),
(853, 80, 'edited_user_login', 'unika'),
(854, 80, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(855, 80, 'user_prev_locale', ''),
(856, 80, 'user_new_locale', 'site-default'),
(857, 80, '_message_key', 'user_updated_profile'),
(858, 80, '_user_id', '1'),
(859, 80, '_user_login', 'unika'),
(860, 80, '_user_email', 'contato@unika.com.br'),
(861, 80, '_server_remote_addr', '168.181.49.144'),
(862, 80, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/profile.php?wp_http_referer=%2Fwp-admin%2Fusers.php'),
(863, 81, 'edited_user_id', '2'),
(864, 81, 'edited_user_email', 'vanessa@handgran.com'),
(865, 81, 'edited_user_login', 'Vanessa'),
(866, 81, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(867, 81, 'user_prev_locale', ''),
(868, 81, 'user_new_locale', 'site-default'),
(869, 81, '_message_key', 'user_updated_profile'),
(870, 81, '_user_id', '1'),
(871, 81, '_user_login', 'unika'),
(872, 81, '_user_email', 'contato@unika.com.br'),
(873, 81, '_server_remote_addr', '168.181.49.144'),
(874, 81, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=2&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(875, 82, '_message_key', 'user_logged_out'),
(876, 82, '_user_id', '1'),
(877, 82, '_user_login', 'unika'),
(878, 82, '_user_email', 'contato@unika.com.br'),
(879, 82, '_server_remote_addr', '168.181.49.144'),
(880, 82, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/profile.php'),
(881, 83, 'user_id', '1'),
(882, 83, 'user_email', 'contato@unika.com.br'),
(883, 83, 'user_login', 'unika'),
(884, 83, '_user_id', '1'),
(885, 83, '_user_login', 'unika'),
(886, 83, '_user_email', 'contato@unika.com.br'),
(887, 83, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(888, 83, '_message_key', 'user_logged_in'),
(889, 83, '_server_remote_addr', '168.181.49.144'),
(890, 83, '_server_http_referer', 'http://unika.handgran.com.br/wp-login.php?interim-login=1&wp_lang=pt_BR'),
(891, 84, 'plugin_name', 'Yoast SEO'),
(892, 84, 'plugin_current_version', '5.9'),
(893, 84, 'plugin_new_version', '6.3.1'),
(894, 84, '_message_key', 'plugin_update_available'),
(895, 84, '_server_remote_addr', '50.116.86.118'),
(896, 84, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1519062646.2178969383239746093750'),
(897, 85, 'comment_ID', '1'),
(898, 85, 'comment_author', 'Um comentarista do WordPress'),
(899, 85, 'comment_author_email', 'wapuu@wordpress.example'),
(900, 85, 'comment_author_url', 'https://wordpress.org/'),
(901, 85, 'comment_author_IP', ''),
(902, 85, 'comment_content', 'Olá, isso é um comentário.\nPara começar a moderar, editar e deletar comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href="https://gravatar.com">Gravatar</a>.'),
(903, 85, 'comment_approved', 'post-trashed'),
(904, 85, 'comment_agent', ''),
(905, 85, 'comment_type', 'comment'),
(906, 85, 'comment_parent', '0'),
(907, 85, 'comment_post_ID', '1'),
(908, 85, 'comment_post_title', 'Conhecimento garante comportamento?'),
(909, 85, 'comment_post_type', 'post'),
(910, 85, '_message_key', 'comment_deleted'),
(911, 85, '_wp_cron_running', 'true'),
(912, 85, '_server_remote_addr', '50.116.86.118'),
(913, 85, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1519062646.2178969383239746093750'),
(914, 86, 'user_id', '1'),
(915, 86, 'user_email', 'contato@unika.com.br'),
(916, 86, 'user_login', 'unika'),
(917, 86, '_user_id', '1'),
(918, 86, '_user_login', 'unika'),
(919, 86, '_user_email', 'contato@unika.com.br'),
(920, 86, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(921, 86, '_message_key', 'user_logged_in'),
(922, 86, '_server_remote_addr', '168.181.49.181'),
(923, 86, '_server_http_referer', 'http://unika.handgran.com.br/wp-login.php?redirect_to=http%3A%2F%2Funika.handgran.com.br%2Fwp-admin%2Fedit.php&reauth=1'),
(924, 87, 'post_type', 'attachment'),
(925, 87, 'attachment_id', '230'),
(926, 87, 'attachment_title', 'Sem-Título-1'),
(927, 87, 'attachment_filename', 'Sem-Título-1.jpg'),
(928, 87, 'attachment_mime', 'image/jpeg'),
(929, 87, 'attachment_filesize', '159879'),
(930, 87, '_message_key', 'attachment_created'),
(931, 87, '_user_id', '1'),
(932, 87, '_user_login', 'unika'),
(933, 87, '_user_email', 'contato@unika.com.br'),
(934, 87, '_server_remote_addr', '168.181.49.181'),
(935, 87, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=202&action=edit'),
(936, 88, 'post_type', 'attachment'),
(937, 88, 'attachment_id', '231'),
(938, 88, 'attachment_title', 'Construction deal'),
(939, 88, 'attachment_filename', 'Construction-deal-512903318_5760x3840.jpeg'),
(940, 88, 'attachment_mime', 'image/jpeg'),
(941, 88, 'attachment_filesize', '9477644'),
(942, 88, '_message_key', 'attachment_created'),
(943, 88, '_user_id', '1'),
(944, 88, '_user_login', 'unika'),
(945, 88, '_user_email', 'contato@unika.com.br'),
(946, 88, '_server_remote_addr', '168.181.49.181'),
(947, 88, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=202&action=edit'),
(948, 89, 'post_id', '202'),
(949, 89, 'post_type', 'post'),
(950, 89, 'post_title', 'OAC &#8211; Observação e Abordagem de Comportamentos em Segurança – Por que implantar?'),
(951, 89, 'post_prev_thumb_id', '61'),
(952, 89, 'post_prev_thumb_title', 'Rectangle 2 Copy 2@2x'),
(953, 89, 'post_new_thumb_id', '231'),
(954, 89, 'post_new_thumb_title', 'Construction deal'),
(955, 89, '_message_key', 'post_updated'),
(956, 89, '_user_id', '1'),
(957, 89, '_user_login', 'unika'),
(958, 89, '_user_email', 'contato@unika.com.br'),
(959, 89, '_server_remote_addr', '168.181.49.181'),
(960, 89, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=202&action=edit'),
(961, 90, 'post_id', '204'),
(962, 90, 'post_type', 'post'),
(963, 90, 'post_title', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.'),
(964, 90, 'post_prev_thumb_id', '61'),
(965, 90, 'post_prev_thumb_title', 'Rectangle 2 Copy 2@2x'),
(966, 90, 'post_new_thumb_id', '230'),
(967, 90, 'post_new_thumb_title', 'Sem-Título-1'),
(968, 90, '_message_key', 'post_updated'),
(969, 90, '_user_id', '1'),
(970, 90, '_user_login', 'unika'),
(971, 90, '_user_email', 'contato@unika.com.br'),
(972, 90, '_server_remote_addr', '168.181.49.181'),
(973, 90, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(974, 91, 'post_type', 'attachment'),
(975, 91, 'attachment_id', '232'),
(976, 91, 'attachment_title', 'blog-oac'),
(977, 91, 'attachment_filename', 'blog-oac.jpg'),
(978, 91, 'attachment_mime', 'image/jpeg'),
(979, 91, 'attachment_filesize', '62831'),
(980, 91, '_message_key', 'attachment_created'),
(981, 91, '_user_id', '1'),
(982, 91, '_user_login', 'unika'),
(983, 91, '_user_email', 'contato@unika.com.br'),
(984, 91, '_server_remote_addr', '168.181.49.181'),
(985, 91, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=202&action=edit'),
(986, 92, 'post_id', '202'),
(987, 92, 'post_type', 'post'),
(988, 92, 'post_title', 'OAC &#8211; Observação e Abordagem de Comportamentos em Segurança – Por que implantar?'),
(989, 92, 'post_prev_thumb_id', '231'),
(990, 92, 'post_prev_thumb_title', 'Construction deal'),
(991, 92, 'post_new_thumb_id', '232'),
(992, 92, 'post_new_thumb_title', 'blog-oac'),
(993, 92, '_message_key', 'post_updated'),
(994, 92, '_user_id', '1'),
(995, 92, '_user_login', 'unika'),
(996, 92, '_user_email', 'contato@unika.com.br'),
(997, 92, '_server_remote_addr', '168.181.49.181'),
(998, 92, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=202&action=edit'),
(999, 93, 'post_type', 'attachment'),
(1000, 93, 'attachment_id', '233'),
(1001, 93, 'attachment_title', 'blog-fatores-humanos'),
(1002, 93, 'attachment_filename', 'blog-fatores-humanos.jpg'),
(1003, 93, 'attachment_mime', 'image/jpeg'),
(1004, 93, 'attachment_filesize', '138924'),
(1005, 93, '_message_key', 'attachment_created'),
(1006, 93, '_user_id', '1'),
(1007, 93, '_user_login', 'unika'),
(1008, 93, '_user_email', 'contato@unika.com.br'),
(1009, 93, '_server_remote_addr', '168.181.49.181'),
(1010, 93, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1011, 94, 'post_id', '204'),
(1012, 94, 'post_type', 'post'),
(1013, 94, 'post_title', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.'),
(1014, 94, 'post_prev_thumb_id', '230'),
(1015, 94, 'post_prev_thumb_title', 'Sem-Título-1'),
(1016, 94, 'post_new_thumb_id', '233'),
(1017, 94, 'post_new_thumb_title', 'blog-fatores-humanos'),
(1018, 94, '_message_key', 'post_updated'),
(1019, 94, '_user_id', '1'),
(1020, 94, '_user_login', 'unika'),
(1021, 94, '_user_email', 'contato@unika.com.br'),
(1022, 94, '_server_remote_addr', '168.181.49.181'),
(1023, 94, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1024, 95, 'user_id', '1'),
(1025, 95, 'user_email', 'contato@unika.com.br'),
(1026, 95, 'user_login', 'unika'),
(1027, 95, '_user_id', '1'),
(1028, 95, '_user_login', 'unika'),
(1029, 95, '_user_email', 'contato@unika.com.br'),
(1030, 95, 'server_http_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(1031, 95, '_message_key', 'user_logged_in'),
(1032, 95, '_server_remote_addr', '186.212.40.226'),
(1033, 95, '_server_http_referer', 'http://unika.handgran.com.br/wp-login.php?redirect_to=http%3A%2F%2Funika.handgran.com.br%2Fwp-admin%2F&reauth=1'),
(1034, 96, 'user_id', '2'),
(1035, 96, 'user_email', 'vanessa@handgran.com'),
(1036, 96, 'user_login', 'Vanessa'),
(1037, 96, '_user_id', '2'),
(1038, 96, '_user_login', 'Vanessa'),
(1039, 96, '_user_email', 'vanessa@handgran.com'),
(1040, 96, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(1041, 96, '_message_key', 'user_logged_in'),
(1042, 96, '_server_remote_addr', '168.181.49.181'),
(1043, 96, '_server_http_referer', 'http://unika.handgran.com.br/wp-login.php?redirect_to=http%3A%2F%2Funika.handgran.com.br%2Fwp-admin%2Fpost.php%3Fpost%3D200%26action%3Dedit&reauth=1'),
(1044, 97, 'post_type', 'attachment'),
(1045, 97, 'attachment_id', '234'),
(1046, 97, 'attachment_title', 'blog-oac-1200&#215;430'),
(1047, 97, 'attachment_filename', 'blog-oac-1200x430.jpg'),
(1048, 97, 'attachment_mime', 'image/jpeg'),
(1049, 97, 'attachment_filesize', '43467'),
(1050, 97, '_message_key', 'attachment_created'),
(1051, 97, '_user_id', '2'),
(1052, 97, '_user_login', 'Vanessa'),
(1053, 97, '_user_email', 'vanessa@handgran.com'),
(1054, 97, '_server_remote_addr', '168.181.49.181'),
(1055, 97, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=202&action=edit'),
(1056, 98, 'post_id', '202'),
(1057, 98, 'post_type', 'post'),
(1058, 98, 'post_title', 'OAC &#8211; Observação e Abordagem de Comportamentos em Segurança – Por que implantar?'),
(1059, 98, 'post_prev_thumb_id', '232'),
(1060, 98, 'post_prev_thumb_title', 'blog-oac'),
(1061, 98, 'post_new_thumb_id', '234'),
(1062, 98, 'post_new_thumb_title', 'blog-oac-1200&#215;430'),
(1063, 98, '_message_key', 'post_updated'),
(1064, 98, '_user_id', '2'),
(1065, 98, '_user_login', 'Vanessa'),
(1066, 98, '_user_email', 'vanessa@handgran.com'),
(1067, 98, '_server_remote_addr', '168.181.49.181'),
(1068, 98, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=202&action=edit'),
(1069, 99, 'post_type', 'attachment'),
(1070, 99, 'attachment_id', '235'),
(1071, 99, 'attachment_title', 'blog-fatores-humanos-1200&#215;430'),
(1072, 99, 'attachment_filename', 'blog-fatores-humanos-1200x430.jpg'),
(1073, 99, 'attachment_mime', 'image/jpeg'),
(1074, 99, 'attachment_filesize', '69588'),
(1075, 99, '_message_key', 'attachment_created'),
(1076, 99, '_user_id', '2'),
(1077, 99, '_user_login', 'Vanessa'),
(1078, 99, '_user_email', 'vanessa@handgran.com'),
(1079, 99, '_server_remote_addr', '168.181.49.181'),
(1080, 99, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1081, 100, 'post_id', '204'),
(1082, 100, 'post_type', 'post'),
(1083, 100, 'post_title', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.'),
(1084, 100, 'post_prev_post_content', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone size-medium wp-image-205" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3-300x185.jpg" alt="" width="300" height="185" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: <span class="SpellE">Reason</span>, 2002\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>'),
(1085, 100, 'post_new_post_content', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone wp-image-205 size-full" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3.jpg" alt="" width="567" height="350" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: <span class="SpellE">Reason</span>, 2002\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>'),
(1086, 100, 'post_prev_thumb_id', '233'),
(1087, 100, 'post_prev_thumb_title', 'blog-fatores-humanos'),
(1088, 100, 'post_new_thumb_id', '235'),
(1089, 100, 'post_new_thumb_title', 'blog-fatores-humanos-1200&#215;430'),
(1090, 100, '_message_key', 'post_updated'),
(1091, 100, '_user_id', '2'),
(1092, 100, '_user_login', 'Vanessa'),
(1093, 100, '_user_email', 'vanessa@handgran.com'),
(1094, 100, '_server_remote_addr', '168.181.49.181'),
(1095, 100, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1096, 101, 'post_id', '204'),
(1097, 101, 'post_type', 'post'),
(1098, 101, 'post_title', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.'),
(1099, 101, '_message_key', 'post_updated'),
(1100, 101, '_user_id', '2'),
(1101, 101, '_user_login', 'Vanessa'),
(1102, 101, '_user_email', 'vanessa@handgran.com'),
(1103, 101, '_server_remote_addr', '168.181.49.181'),
(1104, 101, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1105, 102, 'user_id', '1'),
(1106, 102, 'user_email', 'contato@unika.com.br'),
(1107, 102, 'user_login', 'unika'),
(1108, 102, '_user_id', '1'),
(1109, 102, '_user_login', 'unika'),
(1110, 102, '_user_email', 'contato@unika.com.br'),
(1111, 102, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(1112, 102, '_message_key', 'user_logged_in'),
(1113, 102, '_server_remote_addr', '168.181.49.181'),
(1114, 102, '_server_http_referer', 'http://unika.handgran.com.br/wp-login.php?redirect_to=http%3A%2F%2Funika.handgran.com.br%2Fwp-admin%2F&reauth=1'),
(1115, 103, 'post_id', '204'),
(1116, 103, 'post_type', 'post'),
(1117, 103, 'post_title', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.');
INSERT INTO `uk_simple_history_contexts` (`context_id`, `history_id`, `key`, `value`) VALUES
(1118, 103, 'post_prev_post_content', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone wp-image-205 size-full" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3.jpg" alt="" width="567" height="350" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: <span class="SpellE">Reason</span>, 2002\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>'),
(1119, 103, 'post_new_post_content', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone wp-image-205 size-full" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3.jpg" alt="" width="567" height="350" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: <span class="SpellE">Reason</span>, 2002\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>'),
(1120, 103, '_message_key', 'post_updated'),
(1121, 103, '_user_id', '1'),
(1122, 103, '_user_login', 'unika'),
(1123, 103, '_user_email', 'contato@unika.com.br'),
(1124, 103, '_server_remote_addr', '168.181.49.181'),
(1125, 103, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1126, 104, 'post_id', '204'),
(1127, 104, 'post_type', 'post'),
(1128, 104, 'post_title', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.'),
(1129, 104, 'post_prev_post_content', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone wp-image-205 size-full" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3.jpg" alt="" width="567" height="350" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: <span class="SpellE">Reason</span>, 2002\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>');
INSERT INTO `uk_simple_history_contexts` (`context_id`, `history_id`, `key`, `value`) VALUES
(1130, 104, 'post_new_post_content', '<strong>“Dizer que o acidente é devido à falha humana é tão útil quanto dizer que uma queda é devida à ação da gravidade”. </strong>(Kletz, 2001)\r\n\r\nOs fatores humanos estão presentes em todas as dimensões que se trabalha dentro da cultura organizacional, desde as decisões mais estratégicas às mais operacionais, encontramos o ato de um ser humano. Começar a compreender melhor os comportamentos e ações das pessoas dentro de um sistema complexo de operações é fundamental para que uma empresa possa ter uma atuação verdadeiramente preventiva em acidentes de trabalho.\r\n\r\nHá muito tempo estuda-se sobre os fatores humanos e sua influência nos ambientes de trabalho e mais especificamente sobre os “erros” cometidos durante a execução do trabalho que podem levar a acidentes de diferentes proporções. A teoria que até os dias de hoje melhor responde às questões sobre o Fatores humanos na segurança do trabalho e também melhor se aplica aos contextos industriais, é a teoria do Queijo Suíço de James Reason – 1990.\r\n\r\nNão parece uma grande novidade, pois todos que trabalham na área de segurança de alguma maneira já tiveram algum contato com esta teoria. No entanto, quando bem estudada, compreendida e aplicada, pode gerar resultados muito significativos, haja visto que os modelos mais atuais de análise e investigação de acidentes e de Gestão de Consequências têm se utilizado da teoria do Reason como base.\r\n\r\nA teoria do Queijo Suíço parte do pressuposto que os seres humanos falham e que, portanto, os erros são esperados, mesmo nas melhores organizações. Os erros são considerados mais do que causas, tendo sua origem em fatores sistêmicos que estão acima da natureza do ser humano.\r\n\r\nPortanto, a ideia central desta teoria é a de que as defesas, barreiras e salvaguardas dos perigos precisam funcionar como a chave para a segurança das pessoas. Sistemas de alta tecnologia têm muitas camadas defensivas, dentre elas as de engenharia, (dispositivos automatizados, bloqueios, manutenção) as defesas que estão nas pessoas, e ainda, as que dependem de procedimentos e controles administrativos.\r\n\r\nO objetivo destes sistemas é proteger as pessoas e o patrimônio dos perigos do ambiente. A maior parte das defesas funcionam bem, mas as organizações são sistemas complexos em constante mudança, o que pode gerar possíveis falhas nestas defesas, levando à ocorrência de um evento perigoso. Estas “fraquezas” nas defesas podem surgir por duas razões: falhas ativas e falhas/ condições latentes – conforme pode-se observar na figura abaixo:\r\n\r\n&nbsp;\r\n\r\n<img class="alignnone wp-image-205 size-full" src="http://unika.handgran.com.br/wp-content/uploads/2018/01/Untitled-3.jpg" alt="" width="567" height="350" />\r\n\r\nFonte: Reason, 1990 – Swiss Cheese Model\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> são representadas pelos atos inseguros cometidos pelas pessoas que estão em contato direto com o sistema, podendo assumir diferentes formas: deslizes, lapsos, erros ou violações. Geralmente as falhas ativas tem um impacto de curta duração sobre o sistema.\r\n\r\nJá as <strong>falhas ou condições latentes</strong> são representadas por patologias intrínsecas do sistema, e surgem a partir de decisões dos projetistas, construtores, elaboradores de procedimentos e do nível gerencial mais alto. Tais decisões podem se constituir de erros ou não. Toda decisão estratégica pode potencialmente introduzir um elemento disfuncional no sistema.\r\n\r\nAs condições latentes têm dois tipos de efeitos adversos: podem contribuir para o erro no local de trabalho, como por exemplo: pressão de tempo, sobrecarga de trabalho, equipamentos inadequados, fadiga, inexperiência; e podem criar “fraquezas” duradouras nas defesas como por exemplo: alarmes e indicadores não confiáveis, procedimentos não exequíveis, deficiências projetuais e construtivas, etc. As condições latentes, assim como o próprio nome sugere, podem permanecer dormentes no sistema por anos antes que se combinem com as falhas ativas provocando um acidente de trabalho.\r\n\r\n&nbsp;\r\n<table class="MsoTableGridLight" style="border-collapse: collapse; border: none; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-yfti-tbllook: 1184; mso-padding-alt: 0cm 5.4pt 0cm 5.4pt;" border="1" cellspacing="0" cellpadding="0">\r\n<tbody>\r\n<tr style="mso-yfti-irow: 0; mso-yfti-firstrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas Ativas</span></b></p>\r\n</td>\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-left: none; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" valign="top" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; text-align: center; line-height: 115%;" align="center"><b style="mso-bidi-font-weight: normal;"><span style="font-family: ''Arial'',sans-serif;">Falhas/ Condições Latentes</span></b></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 1;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto da ação adversa\r\ntem efeito imediato.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">O impacto de uma ação/\r\ndecisão adversa pode se manter latente na organização durante um bom tempo\r\nsem ser lesivo.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 2;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por aqueles que\r\nestão na linha de frente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Cometidas por pessoas que\r\nocupam a alta hierarquia da empresa e se relacionam com produção,\r\nregulamentação e assuntos governamentais.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 3;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Tendem a ser uma ação\r\núnica com um efeito bem específico. </span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Uma única ação pode\r\ncontribuir para um número grande de eventos adversos.</span></p>\r\n</td>\r\n</tr>\r\n<tr style="mso-yfti-irow: 4; mso-yfti-lastrow: yes;">\r\n<td style="width: 212.35pt; border: solid #BFBFBF 1.0pt; mso-border-themecolor: background1; mso-border-themeshade: 191; border-top: none; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif;">Podem criar um mal-estar\r\nmomentâneo e abalo no clima de trabalho dependendo de como a falha ativa for\r\ntratada no ambiente.</span></p>\r\n</td>\r\n<td style="width: 212.35pt; border-top: none; border-left: none; border-bottom: solid #BFBFBF 1.0pt; mso-border-bottom-themecolor: background1; mso-border-bottom-themeshade: 191; border-right: solid #BFBFBF 1.0pt; mso-border-right-themecolor: background1; mso-border-right-themeshade: 191; mso-border-top-alt: solid #BFBFBF .5pt; mso-border-top-themecolor: background1; mso-border-top-themeshade: 191; mso-border-left-alt: solid #BFBFBF .5pt; mso-border-left-themecolor: background1; mso-border-left-themeshade: 191; mso-border-alt: solid #BFBFBF .5pt; mso-border-themecolor: background1; mso-border-themeshade: 191; padding: 0cm 5.4pt 0cm 5.4pt;" width="283">\r\n<p class="MsoNormal" style="margin-bottom: .0001pt; line-height: 115%;"><span style="font-family: ''Arial'',sans-serif; mso-fareast-font-family: ''Times New Roman''; mso-fareast-theme-font: minor-fareast;">Podem favorecer a\r\ncriação de um ambiente tolerante a erros e violações e ainda agravar\r\nconsequências de atos inseguros pelos efeitos sobre medidas de proteção/\r\nbarreira do sistema organizacional.</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\nFonte: Reason, 2002\r\n\r\n&nbsp;\r\n\r\nAs <strong>falhas ativas</strong> não podem ser previstas facilmente, mas as <strong>condições latentes</strong> podem ser identificadas e corrigidas antes de um evento adverso. E a compreensão destas dimensões é o que possibilita a uma empresa realizar um gerenciamento proativo ao invés de reativo.\r\n\r\nPortanto, compreender os Fatores humanos de uma perspectiva ampliada dentro da sua organização pode ser a chave para uma atuação mais preventiva sobre o comportamento de líderes e liderados, evitando os acidentes de trabalho.\r\n\r\n&nbsp;\r\n\r\nKarla Maria Mikoski\r\n\r\nPsicóloga - CRP 08/09399\r\n\r\n<em>www.unikapsicologia.com.br</em>\r\n\r\n<em>contato@unikapsicologia.com.br</em>'),
(1131, 104, '_message_key', 'post_updated'),
(1132, 104, '_user_id', '1'),
(1133, 104, '_user_login', 'unika'),
(1134, 104, '_user_email', 'contato@unika.com.br'),
(1135, 104, '_server_remote_addr', '168.181.49.181'),
(1136, 104, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1137, 105, 'post_id', '94'),
(1138, 105, 'post_type', 'destaque'),
(1139, 105, 'post_title', 'Palestras'),
(1140, 105, 'post_prev_post_content', 'Oferecemos soluções para preservar a vida nos ambientes de\r\ntrabalho por meio de intervenções em comportamento e cultura.'),
(1141, 105, 'post_new_post_content', 'Oferecemos soluções para preservar a vida nos ambientes de trabalho por meio de intervenções em comportamento e cultura.'),
(1142, 105, '_message_key', 'post_updated'),
(1143, 105, '_user_id', '1'),
(1144, 105, '_user_login', 'unika'),
(1145, 105, '_user_email', 'contato@unika.com.br'),
(1146, 105, '_server_remote_addr', '168.181.49.179'),
(1147, 105, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=94&action=edit'),
(1148, 106, 'post_id', '93'),
(1149, 106, 'post_type', 'destaque'),
(1150, 106, 'post_title', 'Workshops'),
(1151, 106, '_message_key', 'post_updated'),
(1152, 106, '_user_id', '1'),
(1153, 106, '_user_login', 'unika'),
(1154, 106, '_user_email', 'contato@unika.com.br'),
(1155, 106, '_server_remote_addr', '168.181.49.179'),
(1156, 106, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=93&action=edit'),
(1157, 107, 'post_id', '92'),
(1158, 107, 'post_type', 'destaque'),
(1159, 107, 'post_title', 'Treinamentos'),
(1160, 107, '_message_key', 'post_updated'),
(1161, 107, '_user_id', '1'),
(1162, 107, '_user_login', 'unika'),
(1163, 107, '_user_email', 'contato@unika.com.br'),
(1164, 107, '_server_remote_addr', '168.181.49.179'),
(1165, 107, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=92&action=edit'),
(1166, 108, 'post_id', '91'),
(1167, 108, 'post_type', 'destaque'),
(1168, 108, 'post_title', 'Ferramentas de Segurança Comportamental'),
(1169, 108, 'post_prev_post_content', 'Oferecemos soluções para preservar a vida nos ambientes de\r\ntrabalho por meio de intervenções em comportamento e cultura.'),
(1170, 108, 'post_new_post_content', 'Oferecemos soluções para preservar a vida nos ambientes de trabalho por meio de intervenções em comportamento e cultura.'),
(1171, 108, '_message_key', 'post_updated'),
(1172, 108, '_user_id', '1'),
(1173, 108, '_user_login', 'unika'),
(1174, 108, '_user_email', 'contato@unika.com.br'),
(1175, 108, '_server_remote_addr', '168.181.49.179'),
(1176, 108, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=91&action=edit'),
(1177, 109, 'post_id', '87'),
(1178, 109, 'post_type', 'destaque'),
(1179, 109, 'post_title', 'Planejamento Estratégico de Segurança'),
(1180, 109, '_message_key', 'post_updated'),
(1181, 109, '_user_id', '1'),
(1182, 109, '_user_login', 'unika'),
(1183, 109, '_user_email', 'contato@unika.com.br'),
(1184, 109, '_server_remote_addr', '168.181.49.179'),
(1185, 109, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=87&action=edit'),
(1186, 110, 'post_id', '95'),
(1187, 110, 'post_type', 'destaque'),
(1188, 110, 'post_title', 'Diagnóstico de Cultura de Segurança'),
(1189, 110, '_message_key', 'post_updated'),
(1190, 110, '_user_id', '1'),
(1191, 110, '_user_login', 'unika'),
(1192, 110, '_user_email', 'contato@unika.com.br'),
(1193, 110, '_server_remote_addr', '168.181.49.179'),
(1194, 110, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=95&action=edit'),
(1195, 111, 'post_id', '91'),
(1196, 111, 'post_type', 'destaque'),
(1197, 111, 'post_title', 'Ferramentas de Segurança Comportamental'),
(1198, 111, 'post_prev_post_content', 'Oferecemos soluções para preservar a vida nos ambientes de trabalho por meio de intervenções em comportamento e cultura.'),
(1199, 111, 'post_new_post_content', 'Conheça os diversos serviços que podemos oferecer para implantar ou aprimorar a execução de ferramentas de segurança comportamental'),
(1200, 111, '_message_key', 'post_updated'),
(1201, 111, '_user_id', '1'),
(1202, 111, '_user_login', 'unika'),
(1203, 111, '_user_email', 'contato@unika.com.br'),
(1204, 111, '_server_remote_addr', '168.181.49.179'),
(1205, 111, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=91&action=edit'),
(1206, 112, 'menu_id', '2'),
(1207, 112, 'menu_name', 'Menu Principal'),
(1208, 112, 'menu_items_added', '0'),
(1209, 112, 'menu_items_removed', '1'),
(1210, 112, '_message_key', 'edited_menu'),
(1211, 112, '_user_id', '1'),
(1212, 112, '_user_login', 'unika'),
(1213, 112, '_user_email', 'contato@unika.com.br'),
(1214, 112, '_server_remote_addr', '168.181.49.158'),
(1215, 112, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/nav-menus.php'),
(1216, 113, 'term_id', '2'),
(1217, 113, 'from_term_name', 'Menu Principal'),
(1218, 113, 'from_term_taxonomy', 'nav_menu'),
(1219, 113, 'from_term_slug', 'menu-principal'),
(1220, 113, 'from_term_description', ''),
(1221, 113, 'to_term_name', 'Menu Principal'),
(1222, 113, 'to_term_taxonomy', 'nav_menu'),
(1223, 113, 'to_term_slug', 'null'),
(1224, 113, 'to_term_description', ''),
(1225, 113, '_message_key', 'edited_term'),
(1226, 113, '_user_id', '1'),
(1227, 113, '_user_login', 'unika'),
(1228, 113, '_user_email', 'contato@unika.com.br'),
(1229, 113, '_server_remote_addr', '168.181.49.158'),
(1230, 113, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/nav-menus.php'),
(1231, 114, 'post_type', 'attachment'),
(1232, 114, 'attachment_id', '240'),
(1233, 114, 'attachment_title', 'unikapattern'),
(1234, 114, 'attachment_filename', 'unikapattern.png'),
(1235, 114, 'attachment_mime', 'image/png'),
(1236, 114, 'attachment_filesize', '19194'),
(1237, 114, '_message_key', 'attachment_created'),
(1238, 114, '_user_id', '1'),
(1239, 114, '_user_login', 'unika'),
(1240, 114, '_user_email', 'contato@unika.com.br'),
(1241, 114, '_server_remote_addr', '168.181.49.158'),
(1242, 114, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/admin.php?page=Unika&tab=1'),
(1243, 115, 'post_type', 'attachment'),
(1244, 115, 'attachment_id', '241'),
(1245, 115, 'attachment_title', 'unikapatternbody-1'),
(1246, 115, 'attachment_filename', 'unikapatternbody-1.png'),
(1247, 115, 'attachment_mime', 'image/png'),
(1248, 115, 'attachment_filesize', '19790'),
(1249, 115, '_message_key', 'attachment_created'),
(1250, 115, '_user_id', '1'),
(1251, 115, '_user_login', 'unika'),
(1252, 115, '_user_email', 'contato@unika.com.br'),
(1253, 115, '_server_remote_addr', '168.181.49.158'),
(1254, 115, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/admin.php?page=Unika&tab=1'),
(1255, 116, 'user_id', '2'),
(1256, 116, 'user_email', 'vanessa@handgran.com'),
(1257, 116, 'user_login', 'Vanessa'),
(1258, 116, '_user_id', '2'),
(1259, 116, '_user_login', 'Vanessa'),
(1260, 116, '_user_email', 'vanessa@handgran.com'),
(1261, 116, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(1262, 116, '_message_key', 'user_logged_in'),
(1263, 116, '_server_remote_addr', '168.181.49.158'),
(1264, 116, '_server_http_referer', 'http://unika.handgran.com.br/wp-login.php?redirect_to=http%3A%2F%2Funika.handgran.com.br%2Fwp-admin%2Fadmin.php%3Fpage%3DUnika%26tab%3D1&reauth=1'),
(1265, 117, 'post_type', 'attachment'),
(1266, 117, 'attachment_id', '242'),
(1267, 117, 'attachment_title', 'blog-fatores-humanos-novo'),
(1268, 117, 'attachment_filename', 'blog-fatores-humanos-novo.jpg'),
(1269, 117, 'attachment_mime', 'image/jpeg'),
(1270, 117, 'attachment_filesize', '202372'),
(1271, 117, '_message_key', 'attachment_created'),
(1272, 117, '_user_id', '2'),
(1273, 117, '_user_login', 'Vanessa'),
(1274, 117, '_user_email', 'vanessa@handgran.com'),
(1275, 117, '_server_remote_addr', '168.181.49.158'),
(1276, 117, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1277, 118, 'post_id', '204'),
(1278, 118, 'post_type', 'post'),
(1279, 118, 'post_title', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.'),
(1280, 118, 'post_prev_thumb_id', '235'),
(1281, 118, 'post_prev_thumb_title', 'blog-fatores-humanos-1200&#215;430'),
(1282, 118, 'post_new_thumb_id', '242'),
(1283, 118, 'post_new_thumb_title', 'blog-fatores-humanos-novo'),
(1284, 118, '_message_key', 'post_updated'),
(1285, 118, '_user_id', '2'),
(1286, 118, '_user_login', 'Vanessa'),
(1287, 118, '_user_email', 'vanessa@handgran.com'),
(1288, 118, '_server_remote_addr', '168.181.49.158'),
(1289, 118, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1290, 119, 'post_type', 'attachment'),
(1291, 119, 'attachment_id', '242'),
(1292, 119, 'attachment_title', 'blog-fatores-humanos-novo'),
(1293, 119, 'attachment_filename', 'blog-fatores-humanos-novo.jpg'),
(1294, 119, 'attachment_mime', 'image/jpeg'),
(1295, 119, '_message_key', 'attachment_deleted'),
(1296, 119, '_user_id', '2'),
(1297, 119, '_user_login', 'Vanessa'),
(1298, 119, '_user_email', 'vanessa@handgran.com'),
(1299, 119, '_server_remote_addr', '168.181.49.158'),
(1300, 119, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1301, 120, 'post_type', 'attachment'),
(1302, 120, 'attachment_id', '235'),
(1303, 120, 'attachment_title', 'blog-fatores-humanos-1200&#215;430'),
(1304, 120, 'attachment_filename', 'blog-fatores-humanos-1200x430.jpg'),
(1305, 120, 'attachment_mime', 'image/jpeg'),
(1306, 120, '_message_key', 'attachment_deleted'),
(1307, 120, '_user_id', '2'),
(1308, 120, '_user_login', 'Vanessa'),
(1309, 120, '_user_email', 'vanessa@handgran.com'),
(1310, 120, '_server_remote_addr', '168.181.49.158'),
(1311, 120, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1312, 121, 'post_type', 'attachment'),
(1313, 121, 'attachment_id', '233'),
(1314, 121, 'attachment_title', 'blog-fatores-humanos'),
(1315, 121, 'attachment_filename', 'blog-fatores-humanos.jpg'),
(1316, 121, 'attachment_mime', 'image/jpeg'),
(1317, 121, '_message_key', 'attachment_deleted'),
(1318, 121, '_user_id', '2'),
(1319, 121, '_user_login', 'Vanessa'),
(1320, 121, '_user_email', 'vanessa@handgran.com'),
(1321, 121, '_server_remote_addr', '168.181.49.158'),
(1322, 121, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1323, 122, 'post_type', 'attachment'),
(1324, 122, 'attachment_id', '232'),
(1325, 122, 'attachment_title', 'blog-oac'),
(1326, 122, 'attachment_filename', 'blog-oac.jpg'),
(1327, 122, 'attachment_mime', 'image/jpeg'),
(1328, 122, '_message_key', 'attachment_deleted'),
(1329, 122, '_user_id', '2'),
(1330, 122, '_user_login', 'Vanessa'),
(1331, 122, '_user_email', 'vanessa@handgran.com'),
(1332, 122, '_server_remote_addr', '168.181.49.158'),
(1333, 122, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1334, 123, 'post_type', 'attachment'),
(1335, 123, 'attachment_id', '231'),
(1336, 123, 'attachment_title', 'Construction deal'),
(1337, 123, 'attachment_filename', 'Construction-deal-512903318_5760x3840.jpeg'),
(1338, 123, 'attachment_mime', 'image/jpeg'),
(1339, 123, '_message_key', 'attachment_deleted'),
(1340, 123, '_user_id', '2'),
(1341, 123, '_user_login', 'Vanessa'),
(1342, 123, '_user_email', 'vanessa@handgran.com'),
(1343, 123, '_server_remote_addr', '168.181.49.158'),
(1344, 123, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1345, 124, 'post_type', 'attachment'),
(1346, 124, 'attachment_id', '230'),
(1347, 124, 'attachment_title', 'Sem-Título-1'),
(1348, 124, 'attachment_filename', 'Sem-Título-1.jpg'),
(1349, 124, 'attachment_mime', 'image/jpeg'),
(1350, 124, '_message_key', 'attachment_deleted'),
(1351, 124, '_user_id', '2'),
(1352, 124, '_user_login', 'Vanessa'),
(1353, 124, '_user_email', 'vanessa@handgran.com'),
(1354, 124, '_server_remote_addr', '168.181.49.158'),
(1355, 124, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1356, 125, 'post_type', 'attachment'),
(1357, 125, 'attachment_id', '244'),
(1358, 125, 'attachment_title', 'fatores-humanos-21-04'),
(1359, 125, 'attachment_filename', 'fatores-humanos-21-04.png'),
(1360, 125, 'attachment_mime', 'image/png'),
(1361, 125, 'attachment_filesize', '592371'),
(1362, 125, '_message_key', 'attachment_created'),
(1363, 125, '_user_id', '2'),
(1364, 125, '_user_login', 'Vanessa'),
(1365, 125, '_user_email', 'vanessa@handgran.com'),
(1366, 125, '_server_remote_addr', '168.181.49.158'),
(1367, 125, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1368, 126, 'post_id', '204'),
(1369, 126, 'post_type', 'post'),
(1370, 126, 'post_title', 'O que ninguém contou a você sobre a influência dos fatores humanos nos acidentes de trabalho.'),
(1371, 126, 'post_new_thumb_id', '244'),
(1372, 126, 'post_new_thumb_title', 'fatores-humanos-21-04'),
(1373, 126, '_message_key', 'post_updated'),
(1374, 126, '_user_id', '2'),
(1375, 126, '_user_login', 'Vanessa'),
(1376, 126, '_user_email', 'vanessa@handgran.com'),
(1377, 126, '_server_remote_addr', '168.181.49.158'),
(1378, 126, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=204&action=edit'),
(1379, 127, 'post_type', 'attachment'),
(1380, 127, 'attachment_id', '234'),
(1381, 127, 'attachment_title', 'blog-oac-1200&#215;430'),
(1382, 127, 'attachment_filename', 'blog-oac-1200x430.jpg'),
(1383, 127, 'attachment_mime', 'image/jpeg'),
(1384, 127, '_message_key', 'attachment_deleted'),
(1385, 127, '_user_id', '2'),
(1386, 127, '_user_login', 'Vanessa'),
(1387, 127, '_user_email', 'vanessa@handgran.com'),
(1388, 127, '_server_remote_addr', '168.181.49.158'),
(1389, 127, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=202&action=edit'),
(1390, 128, 'post_type', 'attachment'),
(1391, 128, 'attachment_id', '245'),
(1392, 128, 'attachment_title', 'oac-nova'),
(1393, 128, 'attachment_filename', 'oac-nova.png'),
(1394, 128, 'attachment_mime', 'image/png'),
(1395, 128, 'attachment_filesize', '431380'),
(1396, 128, '_message_key', 'attachment_created'),
(1397, 128, '_user_id', '2'),
(1398, 128, '_user_login', 'Vanessa'),
(1399, 128, '_user_email', 'vanessa@handgran.com'),
(1400, 128, '_server_remote_addr', '168.181.49.158'),
(1401, 128, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=202&action=edit'),
(1402, 129, 'post_id', '202'),
(1403, 129, 'post_type', 'post'),
(1404, 129, 'post_title', 'OAC &#8211; Observação e Abordagem de Comportamentos em Segurança – Por que implantar?'),
(1405, 129, 'post_new_thumb_id', '245'),
(1406, 129, 'post_new_thumb_title', 'oac-nova'),
(1407, 129, '_message_key', 'post_updated'),
(1408, 129, '_user_id', '2'),
(1409, 129, '_user_login', 'Vanessa'),
(1410, 129, '_user_email', 'vanessa@handgran.com'),
(1411, 129, '_server_remote_addr', '168.181.49.158'),
(1412, 129, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=202&action=edit'),
(1413, 130, 'post_type', 'attachment'),
(1414, 130, 'attachment_id', '222'),
(1415, 130, 'attachment_title', 'foto-blog-evolução-da-cultura'),
(1416, 130, 'attachment_filename', 'foto-blog-evolução-da-cultura.png'),
(1417, 130, 'attachment_mime', 'image/png'),
(1418, 130, '_message_key', 'attachment_deleted'),
(1419, 130, '_user_id', '2'),
(1420, 130, '_user_login', 'Vanessa'),
(1421, 130, '_user_email', 'vanessa@handgran.com'),
(1422, 130, '_server_remote_addr', '168.181.49.158'),
(1423, 130, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=194&action=edit'),
(1424, 131, 'post_type', 'attachment'),
(1425, 131, 'attachment_id', '247'),
(1426, 131, 'attachment_title', 'foto-blog-evolução-da-cultura-nova'),
(1427, 131, 'attachment_filename', 'foto-blog-evolução-da-cultura-nova.png'),
(1428, 131, 'attachment_mime', 'image/png'),
(1429, 131, 'attachment_filesize', '821172'),
(1430, 131, '_message_key', 'attachment_created'),
(1431, 131, '_user_id', '2'),
(1432, 131, '_user_login', 'Vanessa'),
(1433, 131, '_user_email', 'vanessa@handgran.com'),
(1434, 131, '_server_remote_addr', '168.181.49.158'),
(1435, 131, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=194&action=edit'),
(1436, 132, 'post_id', '194'),
(1437, 132, 'post_type', 'post'),
(1438, 132, 'post_title', 'Evolução da Cultura de Segurança'),
(1439, 132, 'post_new_thumb_id', '247'),
(1440, 132, 'post_new_thumb_title', 'foto-blog-evolução-da-cultura-nova'),
(1441, 132, '_message_key', 'post_updated'),
(1442, 132, '_user_id', '2'),
(1443, 132, '_user_login', 'Vanessa'),
(1444, 132, '_user_email', 'vanessa@handgran.com'),
(1445, 132, '_server_remote_addr', '168.181.49.158'),
(1446, 132, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=194&action=edit'),
(1447, 133, 'post_type', 'attachment'),
(1448, 133, 'attachment_id', '249'),
(1449, 133, 'attachment_title', 'blog-nova'),
(1450, 133, 'attachment_filename', 'blog-nova.png'),
(1451, 133, 'attachment_mime', 'image/png'),
(1452, 133, 'attachment_filesize', '1401735'),
(1453, 133, '_message_key', 'attachment_created'),
(1454, 133, '_user_id', '2'),
(1455, 133, '_user_login', 'Vanessa'),
(1456, 133, '_user_email', 'vanessa@handgran.com'),
(1457, 133, '_server_remote_addr', '168.181.49.158'),
(1458, 133, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=173&action=edit'),
(1459, 134, 'post_id', '173'),
(1460, 134, 'post_type', 'post'),
(1461, 134, 'post_title', 'Comportamento humano: O que você precisa saber para evitar  acidentes do trabalho'),
(1462, 134, 'post_prev_thumb_id', '225'),
(1463, 134, 'post_prev_thumb_title', 'Three construction helmets hanging on a hat-rack'),
(1464, 134, 'post_new_thumb_id', '249'),
(1465, 134, 'post_new_thumb_title', 'blog-nova'),
(1466, 134, '_message_key', 'post_updated'),
(1467, 134, '_user_id', '2'),
(1468, 134, '_user_login', 'Vanessa'),
(1469, 134, '_user_email', 'vanessa@handgran.com'),
(1470, 134, '_server_remote_addr', '168.181.49.158'),
(1471, 134, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=173&action=edit'),
(1472, 135, 'post_type', 'attachment'),
(1473, 135, 'attachment_id', '192'),
(1474, 135, 'attachment_title', 'dialogo'),
(1475, 135, 'attachment_filename', 'dialogo.jpg'),
(1476, 135, 'attachment_mime', 'image/jpeg'),
(1477, 135, '_message_key', 'attachment_deleted'),
(1478, 135, '_user_id', '2'),
(1479, 135, '_user_login', 'Vanessa'),
(1480, 135, '_user_email', 'vanessa@handgran.com'),
(1481, 135, '_server_remote_addr', '168.181.49.158'),
(1482, 135, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=191&action=edit'),
(1483, 136, 'post_type', 'attachment'),
(1484, 136, 'attachment_id', '226'),
(1485, 136, 'attachment_title', '3'),
(1486, 136, 'attachment_filename', '3.jpg'),
(1487, 136, 'attachment_mime', 'image/jpeg'),
(1488, 136, '_message_key', 'attachment_deleted'),
(1489, 136, '_user_id', '2'),
(1490, 136, '_user_login', 'Vanessa'),
(1491, 136, '_user_email', 'vanessa@handgran.com'),
(1492, 136, '_server_remote_addr', '168.181.49.158'),
(1493, 136, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=191&action=edit'),
(1494, 137, 'post_type', 'attachment'),
(1495, 137, 'attachment_id', '179'),
(1496, 137, 'attachment_title', 'Three construction helmets hanging on a hat-rack'),
(1497, 137, 'attachment_filename', 'Three-construction-helmets-hanging-on-a-hat-rack-519864743_4322x2881.jpeg'),
(1498, 137, 'attachment_mime', 'image/jpeg'),
(1499, 137, '_message_key', 'attachment_deleted'),
(1500, 137, '_user_id', '2'),
(1501, 137, '_user_login', 'Vanessa'),
(1502, 137, '_user_email', 'vanessa@handgran.com'),
(1503, 137, '_server_remote_addr', '168.181.49.158'),
(1504, 137, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=191&action=edit'),
(1505, 138, 'post_type', 'attachment'),
(1506, 138, 'attachment_id', '225'),
(1507, 138, 'attachment_title', 'Three construction helmets hanging on a hat-rack'),
(1508, 138, 'attachment_filename', '2.jpg'),
(1509, 138, 'attachment_mime', 'image/jpeg'),
(1510, 138, '_message_key', 'attachment_deleted'),
(1511, 138, '_user_id', '2'),
(1512, 138, '_user_login', 'Vanessa'),
(1513, 138, '_user_email', 'vanessa@handgran.com'),
(1514, 138, '_server_remote_addr', '168.181.49.158'),
(1515, 138, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=191&action=edit'),
(1516, 139, 'post_type', 'attachment'),
(1517, 139, 'attachment_id', '251'),
(1518, 139, 'attachment_title', 'dialogo-nova'),
(1519, 139, 'attachment_filename', 'dialogo-nova.png'),
(1520, 139, 'attachment_mime', 'image/png'),
(1521, 139, 'attachment_filesize', '347720'),
(1522, 139, '_message_key', 'attachment_created'),
(1523, 139, '_user_id', '2'),
(1524, 139, '_user_login', 'Vanessa'),
(1525, 139, '_user_email', 'vanessa@handgran.com'),
(1526, 139, '_server_remote_addr', '168.181.49.158'),
(1527, 139, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=191&action=edit'),
(1528, 140, 'post_id', '191'),
(1529, 140, 'post_type', 'post'),
(1530, 140, 'post_title', 'Preciso escolher um tema para o DDS &#8211; Diálogos Diários de Segurança no Trabalho. O que devo levar em consideração?'),
(1531, 140, 'post_new_thumb_id', '251'),
(1532, 140, 'post_new_thumb_title', 'dialogo-nova'),
(1533, 140, '_message_key', 'post_updated'),
(1534, 140, '_user_id', '2'),
(1535, 140, '_user_login', 'Vanessa'),
(1536, 140, '_user_email', 'vanessa@handgran.com'),
(1537, 140, '_server_remote_addr', '168.181.49.158'),
(1538, 140, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/post.php?post=191&action=edit'),
(1539, 141, 'user_id', '1'),
(1540, 141, 'user_email', 'contato@unika.com.br'),
(1541, 141, 'user_login', 'unika'),
(1542, 141, '_user_id', '1'),
(1543, 141, '_user_login', 'unika'),
(1544, 141, '_user_email', 'contato@unika.com.br'),
(1545, 141, 'server_http_user_agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36'),
(1546, 141, '_message_key', 'user_logged_in'),
(1547, 141, '_server_remote_addr', '168.181.49.158'),
(1548, 141, '_server_http_referer', 'http://unika.handgran.com.br/wp-login.php?redirect_to=http%3A%2F%2Funika.handgran.com.br%2Fwp-admin%2F&reauth=1'),
(1549, 142, 'plugin_name', 'Contact Form 7'),
(1550, 142, 'plugin_current_version', '4.9.1'),
(1551, 142, 'plugin_new_version', '5.0.1'),
(1552, 142, '_message_key', 'plugin_update_available'),
(1553, 142, '_server_remote_addr', '50.116.86.118'),
(1554, 142, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1519735701.1586880683898925781250'),
(1555, 143, 'plugin_name', 'Disqus Comment System'),
(1556, 143, 'plugin_current_version', '2.87'),
(1557, 143, 'plugin_new_version', '3.0.13'),
(1558, 143, '_message_key', 'plugin_update_available'),
(1559, 143, '_server_remote_addr', '50.116.86.118'),
(1560, 143, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1519735701.1586880683898925781250'),
(1561, 144, 'plugin_name', 'Meta Box'),
(1562, 144, 'plugin_current_version', '4.12.6'),
(1563, 144, 'plugin_new_version', '4.13.4'),
(1564, 144, '_message_key', 'plugin_update_available'),
(1565, 144, '_server_remote_addr', '50.116.86.118'),
(1566, 144, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1519843264.9399709701538085937500'),
(1567, 145, 'plugin_name', 'Disqus Comment System'),
(1568, 145, 'plugin_current_version', '2.87'),
(1569, 145, 'plugin_new_version', '3.0.14'),
(1570, 145, '_message_key', 'plugin_update_available'),
(1571, 145, '_server_remote_addr', '50.116.86.118'),
(1572, 145, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1519921071.0065209865570068359375'),
(1573, 146, 'user_id', '1'),
(1574, 146, 'user_email', 'contato@unika.com.br'),
(1575, 146, 'user_login', 'unika'),
(1576, 146, '_user_id', '1'),
(1577, 146, '_user_login', 'unika'),
(1578, 146, '_user_email', 'contato@unika.com.br'),
(1579, 146, 'server_http_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'),
(1580, 146, '_message_key', 'user_logged_in'),
(1581, 146, '_server_remote_addr', '187.23.74.32'),
(1582, 146, '_server_http_referer', 'http://unika.handgran.com.br/wp-login.php?redirect_to=http%3A%2F%2Funika.handgran.com.br%2Fwp-admin%2F&reauth=1'),
(1583, 147, 'edited_user_id', '3'),
(1584, 147, 'edited_user_email', 'karla@unika.com.br'),
(1585, 147, 'edited_user_login', 'Karla'),
(1586, 147, 'server_http_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'),
(1587, 147, 'user_prev_description', ''),
(1588, 147, 'user_new_description', 'Karla Maria Mikoski\r\n\r\nPsicóloga – CRP 08/09399\r\n\r\nwww.unikapsicologia.com.br\r\n\r\ncontato@unikapsicologia.com.br'),
(1589, 147, 'user_prev_locale', ''),
(1590, 147, 'user_new_locale', 'site-default'),
(1591, 147, '_message_key', 'user_updated_profile'),
(1592, 147, '_user_id', '1'),
(1593, 147, '_user_login', 'unika'),
(1594, 147, '_user_email', 'contato@unika.com.br'),
(1595, 147, '_server_remote_addr', '187.23.74.32'),
(1596, 147, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(1597, 148, 'edited_user_id', '1'),
(1598, 148, 'edited_user_email', 'contato@unika.com.br'),
(1599, 148, 'edited_user_login', 'unika'),
(1600, 148, 'server_http_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'),
(1601, 148, 'user_prev_description', ''),
(1602, 148, 'user_new_description', 'Karla Maria Mikoski\r\n\r\nPsicóloga – CRP 08/09399\r\n\r\nwww.unikapsicologia.com.br\r\n\r\ncontato@unikapsicologia.com.br'),
(1603, 148, 'user_prev_locale', ''),
(1604, 148, 'user_new_locale', 'site-default'),
(1605, 148, '_message_key', 'user_updated_profile'),
(1606, 148, '_user_id', '1'),
(1607, 148, '_user_login', 'unika'),
(1608, 148, '_user_email', 'contato@unika.com.br'),
(1609, 148, '_server_remote_addr', '187.23.74.32'),
(1610, 148, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/profile.php?wp_http_referer=%2Fwp-admin%2Fusers.php'),
(1611, 149, 'edited_user_id', '2'),
(1612, 149, 'edited_user_email', 'vanessa@handgran.com'),
(1613, 149, 'edited_user_login', 'Vanessa'),
(1614, 149, 'server_http_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'),
(1615, 149, 'user_prev_description', ''),
(1616, 149, 'user_new_description', 'Karla Maria Mikoski\r\n\r\nPsicóloga – CRP 08/09399\r\n\r\nwww.unikapsicologia.com.br\r\n\r\ncontato@unikapsicologia.com.br'),
(1617, 149, 'user_prev_locale', ''),
(1618, 149, 'user_new_locale', 'site-default'),
(1619, 149, '_message_key', 'user_updated_profile'),
(1620, 149, '_user_id', '1'),
(1621, 149, '_user_login', 'unika'),
(1622, 149, '_user_email', 'contato@unika.com.br'),
(1623, 149, '_server_remote_addr', '187.23.74.32'),
(1624, 149, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=2&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(1625, 150, 'edited_user_id', '2'),
(1626, 150, 'edited_user_email', 'vanessa@handgran.com'),
(1627, 150, 'edited_user_login', 'Vanessa'),
(1628, 150, 'server_http_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'),
(1629, 150, 'user_prev_description', 'Karla Maria Mikoski\r\n\r\nPsicóloga – CRP 08/09399\r\n\r\nwww.unikapsicologia.com.br\r\n\r\ncontato@unikapsicologia.com.br'),
(1630, 150, 'user_new_description', 'Karla Maria Mikoski<br>\r\n\r\nPsicóloga – CRP 08/09399<br>\r\n\r\nwww.unikapsicologia.com.br<br>\r\n\r\ncontato@unikapsicologia.com.br<br>'),
(1631, 150, 'user_prev_locale', ''),
(1632, 150, 'user_new_locale', 'site-default'),
(1633, 150, '_message_key', 'user_updated_profile'),
(1634, 150, '_user_id', '1'),
(1635, 150, '_user_login', 'unika'),
(1636, 150, '_user_email', 'contato@unika.com.br'),
(1637, 150, '_server_remote_addr', '187.23.74.32'),
(1638, 150, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=2&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(1639, 151, 'edited_user_id', '1'),
(1640, 151, 'edited_user_email', 'contato@unika.com.br'),
(1641, 151, 'edited_user_login', 'unika'),
(1642, 151, 'server_http_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'),
(1643, 151, 'user_prev_description', 'Karla Maria Mikoski\r\n\r\nPsicóloga – CRP 08/09399\r\n\r\nwww.unikapsicologia.com.br\r\n\r\ncontato@unikapsicologia.com.br'),
(1644, 151, 'user_new_description', 'Karla Maria Mikoski<br>\r\n\r\nPsicóloga – CRP 08/09399<br>\r\n\r\nwww.unikapsicologia.com.br<br>\r\n\r\ncontato@unikapsicologia.com.br<br>'),
(1645, 151, 'user_prev_locale', ''),
(1646, 151, 'user_new_locale', 'site-default'),
(1647, 151, '_message_key', 'user_updated_profile'),
(1648, 151, '_user_id', '1'),
(1649, 151, '_user_login', 'unika'),
(1650, 151, '_user_email', 'contato@unika.com.br'),
(1651, 151, '_server_remote_addr', '187.23.74.32'),
(1652, 151, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/profile.php?wp_http_referer=%2Fwp-admin%2Fusers.php'),
(1653, 152, 'edited_user_id', '3'),
(1654, 152, 'edited_user_email', 'karla@unika.com.br'),
(1655, 152, 'edited_user_login', 'Karla'),
(1656, 152, 'server_http_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'),
(1657, 152, 'user_prev_description', 'Karla Maria Mikoski\r\n\r\nPsicóloga – CRP 08/09399\r\n\r\nwww.unikapsicologia.com.br\r\n\r\ncontato@unikapsicologia.com.br'),
(1658, 152, 'user_new_description', 'Karla Maria Mikoski<br>\r\n\r\nPsicóloga – CRP 08/09399<br>\r\n\r\nwww.unikapsicologia.com.br<br>\r\n\r\ncontato@unikapsicologia.com.br<br>'),
(1659, 152, 'user_prev_locale', ''),
(1660, 152, 'user_new_locale', 'site-default'),
(1661, 152, '_message_key', 'user_updated_profile'),
(1662, 152, '_user_id', '1'),
(1663, 152, '_user_login', 'unika'),
(1664, 152, '_user_email', 'contato@unika.com.br'),
(1665, 152, '_server_remote_addr', '187.23.74.32'),
(1666, 152, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(1667, 153, 'edited_user_id', '3'),
(1668, 153, 'edited_user_email', 'karla@unika.com.br'),
(1669, 153, 'edited_user_login', 'Karla'),
(1670, 153, 'server_http_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'),
(1671, 153, 'user_prev_description', 'Karla Maria Mikoski\r\n\r\nPsicóloga – CRP 08/09399\r\n\r\nwww.unikapsicologia.com.br\r\n\r\ncontato@unikapsicologia.com.br'),
(1672, 153, 'user_new_description', 'Karla Maria Mikoski <br>\r\n\r\nPsicóloga – CRP 08/09399<br>\r\n\r\nwww.unikapsicologia.com.br<br>\r\n\r\ncontato@unikapsicologia.com.br<br>'),
(1673, 153, 'user_prev_locale', ''),
(1674, 153, 'user_new_locale', 'site-default'),
(1675, 153, '_message_key', 'user_updated_profile'),
(1676, 153, '_user_id', '1'),
(1677, 153, '_user_login', 'unika'),
(1678, 153, '_user_email', 'contato@unika.com.br'),
(1679, 153, '_server_remote_addr', '187.23.74.32'),
(1680, 153, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(1681, 154, 'edited_user_id', '3'),
(1682, 154, 'edited_user_email', 'karla@unika.com.br'),
(1683, 154, 'edited_user_login', 'Karla'),
(1684, 154, 'server_http_user_agent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'),
(1685, 154, 'user_prev_locale', ''),
(1686, 154, 'user_new_locale', 'site-default'),
(1687, 154, '_message_key', 'user_updated_profile'),
(1688, 154, '_user_id', '1'),
(1689, 154, '_user_login', 'unika'),
(1690, 154, '_user_email', 'contato@unika.com.br'),
(1691, 154, '_server_remote_addr', '187.23.74.32'),
(1692, 154, '_server_http_referer', 'http://unika.handgran.com.br/wp-admin/user-edit.php?user_id=3&wp_http_referer=%2Fwp-admin%2Fusers.php'),
(1693, 155, 'plugin_name', 'Disqus Comment System'),
(1694, 155, 'plugin_current_version', '2.87'),
(1695, 155, 'plugin_new_version', '3.0.15'),
(1696, 155, '_message_key', 'plugin_update_available'),
(1697, 155, '_server_remote_addr', '50.116.86.118'),
(1698, 155, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1520075040.1866869926452636718750'),
(1699, 156, 'plugin_name', 'Meta Box'),
(1700, 156, 'plugin_current_version', '4.12.6'),
(1701, 156, 'plugin_new_version', '4.14.0'),
(1702, 156, '_message_key', 'plugin_update_available'),
(1703, 156, '_server_remote_addr', '50.116.86.118'),
(1704, 156, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1520355435.9374639987945556640625'),
(1705, 157, 'plugin_name', 'Yoast SEO'),
(1706, 157, 'plugin_current_version', '5.9'),
(1707, 157, 'plugin_new_version', '7.0.1'),
(1708, 157, '_message_key', 'plugin_update_available'),
(1709, 157, '_server_remote_addr', '50.116.86.118'),
(1710, 157, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1520355435.9374639987945556640625'),
(1711, 158, 'plugin_name', 'Yoast SEO'),
(1712, 158, 'plugin_current_version', '5.9'),
(1713, 158, 'plugin_new_version', '7.0.2'),
(1714, 158, '_message_key', 'plugin_update_available'),
(1715, 158, '_server_remote_addr', '50.116.86.118'),
(1716, 158, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1520622225.2087540626525878906250'),
(1717, 159, 'plugin_name', 'MailPoet 2'),
(1718, 159, 'plugin_current_version', '2.8.1'),
(1719, 159, 'plugin_new_version', '2.8.2'),
(1720, 159, '_message_key', 'plugin_update_available'),
(1721, 159, '_server_remote_addr', '50.116.86.118'),
(1722, 159, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1521475980.0735468864440917968750'),
(1723, 160, 'plugin_name', 'Yoast SEO'),
(1724, 160, 'plugin_current_version', '5.9'),
(1725, 160, 'plugin_new_version', '7.0.3'),
(1726, 160, '_message_key', 'plugin_update_available'),
(1727, 160, '_server_remote_addr', '50.116.86.118'),
(1728, 160, '_server_http_referer', 'http://unika.handgran.com.br/wp-cron.php?doing_wp_cron=1521475980.0735468864440917968750');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_termmeta`
--

CREATE TABLE IF NOT EXISTS `uk_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_terms`
--

CREATE TABLE IF NOT EXISTS `uk_terms` (
  `term_id` bigint(20) unsigned NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_terms`
--

INSERT INTO `uk_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem Categoria', 'sem-categoria', 0),
(2, 'Menu Principal', 'menu-principal', 0),
(3, 'Relacionados', 'relacionados', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_term_relationships`
--

CREATE TABLE IF NOT EXISTS `uk_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_term_relationships`
--

INSERT INTO `uk_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(82, 2, 0),
(81, 2, 0),
(80, 2, 0),
(79, 2, 0),
(173, 1, 0),
(191, 1, 0),
(194, 1, 0),
(199, 1, 0),
(202, 1, 0),
(204, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_term_taxonomy`
--

CREATE TABLE IF NOT EXISTS `uk_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_term_taxonomy`
--

INSERT INTO `uk_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 6),
(2, 2, 'nav_menu', '', 0, 4),
(3, 3, 'category', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_usermeta`
--

CREATE TABLE IF NOT EXISTS `uk_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=MyISAM AUTO_INCREMENT=142 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_usermeta`
--

INSERT INTO `uk_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'Unika'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', 'Karla Maria Mikoski\r\n\r\nPsicóloga – CRP 08/09399\r\n\r\nwww.unikapsicologia.com.br\r\n\r\ncontato@unikapsicologia.com.br'),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'comment_shortcuts', 'false'),
(7, 1, 'admin_color', 'fresh'),
(8, 1, 'use_ssl', '0'),
(9, 1, 'show_admin_bar_front', 'true'),
(10, 1, 'locale', ''),
(11, 1, 'uk_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(12, 1, 'uk_user_level', '10'),
(13, 1, 'dismissed_wp_pointers', ''),
(14, 1, 'show_welcome_panel', '0'),
(138, 1, 'session_tokens', 'a:1:{s:64:"10a856e196424bd005efda68128379e38d75b3fb0a00b9653da5e8ddf4ddaf1b";a:4:{s:10:"expiration";i:1520096464;s:2:"ip";s:12:"187.23.74.32";s:2:"ua";s:121:"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36";s:5:"login";i:1519923664;}}'),
(55, 1, 'syntax_highlighting', 'true'),
(61, 2, 'last_name', ''),
(62, 2, 'description', 'Karla Maria Mikoski\r\n\r\nPsicóloga – CRP 08/09399\r\n\r\nwww.unikapsicologia.com.br\r\n\r\ncontato@unikapsicologia.com.br'),
(63, 2, 'rich_editing', 'true'),
(64, 2, 'syntax_highlighting', 'true'),
(16, 1, 'uk_dashboard_quick_press_last_post_id', '253'),
(17, 1, 'community-events-location', 'a:1:{s:2:"ip";s:11:"187.23.74.0";}'),
(18, 1, 'closedpostboxes_dashboard', 'a:1:{i:0;s:24:"wpseo-dashboard-overview";}'),
(19, 1, 'metaboxhidden_dashboard', 'a:4:{i:0;s:19:"dashboard_right_now";i:1;s:18:"dashboard_activity";i:2;s:21:"dashboard_quick_press";i:3;s:17:"dashboard_primary";}'),
(20, 1, 'uk_yoast_notifications', 'a:4:{i:0;a:2:{s:7:"message";s:836:"Percebemos que você já está usando o Yoast SEO há algum tempo; esperamos que esteja gostando! Nós adoraríamos se você pudesse <a href="https://yoa.st/rate-yoast-seo?utm_content=5.9">nos classificar com 5 estrelas no WordPress.org</a>!\n\nSe estiver enfrentando problemas, <a href="https://yoa.st/bugreport?utm_content=5.9">envie um relatório de erro</a> e faremos tudo o que pudermos para ajudar você.\n\nA propósito, sabia que também temos um <a href=''https://yoa.st/premium-notification?utm_content=5.9''>plugin Premium</a>? Ele tem recursos avançados, como um gerenciador de redirecionamentos e suporte para múltiplas palavras-chave em foco. Também inclui suporte pessoal 24/7.\n\n<a class="button" href="http://unika.handgran.com.br/wp-admin/?page=wpseo_dashboard&yoast_dismiss=upsell">Não mostre mais essa notificação</a>";s:7:"options";a:8:{s:4:"type";s:7:"warning";s:2:"id";s:19:"wpseo-upsell-notice";s:5:"nonce";N;s:8:"priority";d:0.8000000000000000444089209850062616169452667236328125;s:9:"data_json";a:0:{}s:13:"dismissal_key";N;s:12:"capabilities";s:20:"wpseo_manage_options";s:16:"capability_check";s:3:"all";}}i:1;a:2:{s:7:"message";s:192:"Não deixe escapar seus erros de rastreamento: <a href="http://unika.handgran.com.br/wp-admin/admin.php?page=wpseo_search_console&tab=settings">conecte-se com o Google Search Console aqui</a>.";s:7:"options";a:8:{s:4:"type";s:7:"warning";s:2:"id";s:17:"wpseo-dismiss-gsc";s:5:"nonce";N;s:8:"priority";d:0.5;s:9:"data_json";a:0:{}s:13:"dismissal_key";N;s:12:"capabilities";s:20:"wpseo_manage_options";s:16:"capability_check";s:3:"all";}}i:2;a:2:{s:7:"message";s:328:"Você ainda usa o slogan padrão do WordPress, provavelmente deixar em branco é melhor. <a href="http://unika.handgran.com.br/wp-admin/customize.php?url=http%3A%2F%2Funika.handgran.com.br%2Fwp-admin%2Fuser-edit.php%3Fuser_id%3D3%26updated%3D1%26wp_http_referer%3Dwp-adminusers.php">You pode corrigir isso no personalizador</a>.";s:7:"options";a:8:{s:4:"type";s:5:"error";s:2:"id";s:28:"wpseo-dismiss-tagline-notice";s:5:"nonce";N;s:8:"priority";d:0.5;s:9:"data_json";a:0:{}s:13:"dismissal_key";N;s:12:"capabilities";s:20:"wpseo_manage_options";s:16:"capability_check";s:3:"all";}}i:3;a:2:{s:7:"message";s:266:"<strong>Problema grave de SEO: Você está bloqueando o acesso dos robôs.</strong> Você deve <a href="http://unika.handgran.com.br/wp-admin/options-reading.php">acessar as Configurações de leitura</a> e desmarcar a opção "Visibilidade nos mecanismos de busca".";s:7:"options";a:8:{s:4:"type";s:5:"error";s:2:"id";s:32:"wpseo-dismiss-blog-public-notice";s:5:"nonce";N;s:8:"priority";i:1;s:9:"data_json";a:0:{}s:13:"dismissal_key";N;s:12:"capabilities";s:20:"wpseo_manage_options";s:16:"capability_check";s:3:"all";}}}'),
(65, 2, 'comment_shortcuts', 'false'),
(21, 1, 'last_login_time', '2018-03-01 14:01:05'),
(22, 1, 'uk_user-settings', 'editor_expand=on&libraryContent=browse&editor=tinymce&hidetb=0'),
(23, 1, 'uk_user-settings-time', '1519069194'),
(24, 1, 'closedpostboxes_servico', 'a:1:{i:0;s:10:"wpseo_meta";}'),
(25, 1, 'metaboxhidden_servico', 'a:1:{i:0;s:7:"slugdiv";}'),
(26, 1, 'closedpostboxes_parceiros', 'a:1:{i:0;s:10:"wpseo_meta";}'),
(27, 1, 'metaboxhidden_parceiros', 'a:1:{i:0;s:7:"slugdiv";}'),
(28, 1, 'closedpostboxes_depoimento', 'a:0:{}'),
(29, 1, 'metaboxhidden_depoimento', 'a:1:{i:0;s:7:"slugdiv";}'),
(30, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:"link-target";i:1;s:11:"css-classes";i:2;s:3:"xfn";i:3;s:11:"description";i:4;s:15:"title-attribute";}'),
(31, 1, 'metaboxhidden_nav-menus', 'a:6:{i:0;s:22:"add-post-type-destaque";i:1;s:21:"add-post-type-servico";i:2;s:24:"add-post-type-depoimento";i:3;s:23:"add-post-type-parceiros";i:4;s:20:"add-post-type-equipe";i:5;s:12:"add-post_tag";}'),
(32, 1, 'closedpostboxes_page', 'a:1:{i:0;s:10:"wpseo_meta";}'),
(33, 1, 'metaboxhidden_page', 'a:4:{i:0;s:10:"postcustom";i:1;s:16:"commentstatusdiv";i:2;s:7:"slugdiv";i:3;s:9:"authordiv";}'),
(34, 1, 'closedpostboxes_destaque', 'a:1:{i:0;s:10:"wpseo_meta";}'),
(35, 1, 'metaboxhidden_destaque', 'a:1:{i:0;s:7:"slugdiv";}'),
(36, 1, 'meta-box-order_destaque', 'a:3:{s:4:"side";s:22:"submitdiv,postimagediv";s:6:"normal";s:35:"metaboxDescaques,wpseo_meta,slugdiv";s:8:"advanced";s:0:"";}'),
(37, 1, 'screen_layout_destaque', '2'),
(39, 1, 'closedpostboxes_equipe', 'a:1:{i:0;s:10:"wpseo_meta";}'),
(38, 1, 'wysija_pref', 'YTowOnt9'),
(40, 1, 'metaboxhidden_equipe', 'a:1:{i:0;s:7:"slugdiv";}'),
(126, 1, 'wp_user_avatars_rating', 'G'),
(127, 2, 'wp_user_avatars', 'a:12:{s:4:"full";s:84:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795.png";i:192;s:92:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795-192x192.png";i:96;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795-96x96.png";i:500;s:92:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795-500x500.png";i:250;s:92:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795-250x250.png";i:64;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795-64x64.png";i:32;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795-32x32.png";i:128;s:92:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795-128x128.png";i:52;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795-52x52.png";i:26;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795-26x26.png";i:48;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795-48x48.png";i:24;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_2_1518721795-24x24.png";}'),
(128, 2, 'wp_user_avatars_rating', 'G'),
(129, 2, 'wpseo_title', ''),
(130, 2, 'wpseo_metadesc', ''),
(131, 2, 'wpseo_metakey', ''),
(132, 2, 'wpseo_excludeauthorsitemap', ''),
(133, 2, 'wpseo_content_analysis_disable', ''),
(134, 2, 'wpseo_keyword_analysis_disable', ''),
(135, 2, 'googleplus', ''),
(136, 2, 'twitter', ''),
(137, 2, 'facebook', ''),
(43, 1, '_yoast_wpseo_profile_updated', '1519926321'),
(44, 1, 'wpseo_title', ''),
(45, 1, 'wpseo_metadesc', ''),
(46, 1, 'wpseo_metakey', ''),
(47, 1, 'wpseo_excludeauthorsitemap', ''),
(48, 1, 'wpseo_content_analysis_disable', ''),
(49, 1, 'wpseo_keyword_analysis_disable', ''),
(50, 1, 'googleplus', ''),
(51, 1, 'twitter', ''),
(52, 1, 'facebook', ''),
(125, 1, 'wp_user_avatars', 'a:12:{s:4:"full";s:84:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750.png";i:128;s:92:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750-128x128.png";i:64;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750-64x64.png";i:52;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750-52x52.png";i:26;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750-26x26.png";i:192;s:92:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750-192x192.png";i:96;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750-96x96.png";i:500;s:92:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750-500x500.png";i:250;s:92:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750-250x250.png";i:32;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750-32x32.png";i:48;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750-48x48.png";i:24;s:90:"http://unika.handgran.com.br/wp-content/uploads/2018/02/avatar_user_1_1518721750-24x24.png";}'),
(59, 2, 'nickname', 'Vanessa'),
(94, 3, 'uk_user_level', '10'),
(95, 3, '_yoast_wpseo_profile_updated', '1519926476'),
(96, 3, 'dismissed_wp_pointers', ''),
(93, 3, 'uk_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(56, 1, 'meta-box-order_depoimento', 'a:3:{s:4:"side";s:22:"submitdiv,postimagediv";s:6:"normal";s:36:"metaboxDepoimento,wpseo_meta,slugdiv";s:8:"advanced";s:0:"";}'),
(57, 1, 'screen_layout_depoimento', '2'),
(60, 2, 'first_name', ''),
(58, 1, 'nav_menu_recently_edited', '2'),
(83, 3, 'first_name', 'Karla Mikoski'),
(84, 3, 'last_name', ''),
(85, 3, 'description', 'Karla Maria Mikoski \r\n\r\nPsicóloga – CRP 08/09399\r\n\r\nwww.unikapsicologia.com.br\r\n\r\ncontato@unikapsicologia.com.br'),
(86, 3, 'rich_editing', 'true'),
(87, 3, 'syntax_highlighting', 'true'),
(88, 3, 'comment_shortcuts', 'false'),
(89, 3, 'admin_color', 'fresh'),
(90, 3, 'use_ssl', '0'),
(91, 3, 'show_admin_bar_front', 'true'),
(92, 3, 'locale', ''),
(66, 2, 'admin_color', 'fresh'),
(67, 2, 'use_ssl', '0'),
(68, 2, 'show_admin_bar_front', 'true'),
(69, 2, 'locale', ''),
(70, 2, 'uk_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(71, 2, 'uk_user_level', '10'),
(72, 2, '_yoast_wpseo_profile_updated', '1519926318'),
(73, 2, 'dismissed_wp_pointers', ''),
(81, 1, 'meta-box-order_dashboard', 'a:4:{s:6:"normal";s:70:"dashboard_right_now,dashboard_activity,simple_history_dashboard_widget";s:4:"side";s:64:"dashboard_quick_press,dashboard_primary,wpseo-dashboard-overview";s:7:"column3";s:0:"";s:7:"column4";s:0:"";}'),
(75, 2, 'last_login_time', '2018-02-21 10:37:42'),
(76, 2, 'uk_dashboard_quick_press_last_post_id', '227'),
(77, 2, 'uk_yoast_notifications', 'a:4:{i:0;a:2:{s:7:"message";s:836:"Percebemos que você já está usando o Yoast SEO há algum tempo; esperamos que esteja gostando! Nós adoraríamos se você pudesse <a href="https://yoa.st/rate-yoast-seo?utm_content=5.9">nos classificar com 5 estrelas no WordPress.org</a>!\n\nSe estiver enfrentando problemas, <a href="https://yoa.st/bugreport?utm_content=5.9">envie um relatório de erro</a> e faremos tudo o que pudermos para ajudar você.\n\nA propósito, sabia que também temos um <a href=''https://yoa.st/premium-notification?utm_content=5.9''>plugin Premium</a>? Ele tem recursos avançados, como um gerenciador de redirecionamentos e suporte para múltiplas palavras-chave em foco. Também inclui suporte pessoal 24/7.\n\n<a class="button" href="http://unika.handgran.com.br/wp-admin/?page=wpseo_dashboard&yoast_dismiss=upsell">Não mostre mais essa notificação</a>";s:7:"options";a:8:{s:4:"type";s:7:"warning";s:2:"id";s:19:"wpseo-upsell-notice";s:5:"nonce";N;s:8:"priority";d:0.8000000000000000444089209850062616169452667236328125;s:9:"data_json";a:0:{}s:13:"dismissal_key";N;s:12:"capabilities";s:20:"wpseo_manage_options";s:16:"capability_check";s:3:"all";}}i:1;a:2:{s:7:"message";s:192:"Não deixe escapar seus erros de rastreamento: <a href="http://unika.handgran.com.br/wp-admin/admin.php?page=wpseo_search_console&tab=settings">conecte-se com o Google Search Console aqui</a>.";s:7:"options";a:8:{s:4:"type";s:7:"warning";s:2:"id";s:17:"wpseo-dismiss-gsc";s:5:"nonce";N;s:8:"priority";d:0.5;s:9:"data_json";a:0:{}s:13:"dismissal_key";N;s:12:"capabilities";s:20:"wpseo_manage_options";s:16:"capability_check";s:3:"all";}}i:2;a:2:{s:7:"message";s:257:"Você ainda usa o slogan padrão do WordPress, provavelmente deixar em branco é melhor. <a href="http://unika.handgran.com.br/wp-admin/customize.php?url=http%3A%2F%2Funika.handgran.com.br%2Fwp-admin%2Fpost.php">You pode corrigir isso no personalizador</a>.";s:7:"options";a:8:{s:4:"type";s:5:"error";s:2:"id";s:28:"wpseo-dismiss-tagline-notice";s:5:"nonce";N;s:8:"priority";d:0.5;s:9:"data_json";a:0:{}s:13:"dismissal_key";N;s:12:"capabilities";s:20:"wpseo_manage_options";s:16:"capability_check";s:3:"all";}}i:3;a:2:{s:7:"message";s:266:"<strong>Problema grave de SEO: Você está bloqueando o acesso dos robôs.</strong> Você deve <a href="http://unika.handgran.com.br/wp-admin/options-reading.php">acessar as Configurações de leitura</a> e desmarcar a opção "Visibilidade nos mecanismos de busca".";s:7:"options";a:8:{s:4:"type";s:5:"error";s:2:"id";s:32:"wpseo-dismiss-blog-public-notice";s:5:"nonce";N;s:8:"priority";i:1;s:9:"data_json";a:0:{}s:13:"dismissal_key";N;s:12:"capabilities";s:20:"wpseo_manage_options";s:16:"capability_check";s:3:"all";}}}'),
(78, 2, 'community-events-location', 'a:1:{s:2:"ip";s:12:"168.181.49.0";}'),
(79, 2, 'closedpostboxes_dashboard', 'a:0:{}'),
(80, 2, 'metaboxhidden_dashboard', 'a:6:{i:0;s:19:"dashboard_right_now";i:1;s:18:"dashboard_activity";i:2;s:31:"simple_history_dashboard_widget";i:3;s:24:"wpseo-dashboard-overview";i:4;s:21:"dashboard_quick_press";i:5;s:17:"dashboard_primary";}'),
(82, 3, 'nickname', 'Karla'),
(99, 3, 'wpseo_title', ''),
(100, 3, 'wpseo_metadesc', ''),
(101, 3, 'wpseo_metakey', ''),
(102, 3, 'wpseo_excludeauthorsitemap', ''),
(103, 3, 'wpseo_content_analysis_disable', ''),
(104, 3, 'wpseo_keyword_analysis_disable', ''),
(105, 3, 'googleplus', ''),
(106, 3, 'twitter', ''),
(107, 3, 'facebook', ''),
(112, 3, 'sabox_social_links', 'a:0:{}'),
(113, 3, 'sabox-profile-image', 'http://unika.handgran.com.br/wp-content/uploads/2017/10/karla@2x.png'),
(123, 3, 'wp_user_avatars', 'a:11:{s:8:"media_id";i:110;s:7:"site_id";i:1;s:4:"full";s:68:"http://unika.handgran.com.br/wp-content/uploads/2017/10/karla@2x.png";i:180;s:76:"http://unika.handgran.com.br/wp-content/uploads/2017/10/karla@2x-180x180.png";i:90;s:74:"http://unika.handgran.com.br/wp-content/uploads/2017/10/karla@2x-90x90.png";i:192;s:76:"http://unika.handgran.com.br/wp-content/uploads/2017/10/karla@2x-192x192.png";i:96;s:74:"http://unika.handgran.com.br/wp-content/uploads/2017/10/karla@2x-96x96.png";i:500;s:76:"http://unika.handgran.com.br/wp-content/uploads/2017/10/karla@2x-500x500.png";i:250;s:76:"http://unika.handgran.com.br/wp-content/uploads/2017/10/karla@2x-250x250.png";i:64;s:74:"http://unika.handgran.com.br/wp-content/uploads/2017/10/karla@2x-64x64.png";i:32;s:74:"http://unika.handgran.com.br/wp-content/uploads/2017/10/karla@2x-32x32.png";}'),
(124, 3, 'wp_user_avatars_rating', 'G'),
(139, 2, 'session_tokens', 'a:2:{s:64:"f9a456083f5f1ebfd7b4b1bb0873a43c2cba84d59ee37d560eb0fe82031780ff";a:4:{s:10:"expiration";i:1519240722;s:2:"ip";s:14:"168.181.49.181";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36";s:5:"login";i:1519067922;}s:64:"465726929adfdd1bc58b48cb9f297627d3f3e1634fad98c7fc8a04358a4137ff";a:4:{s:10:"expiration";i:1519393062;s:2:"ip";s:14:"168.181.49.158";s:2:"ua";s:115:"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.167 Safari/537.36";s:5:"login";i:1519220262;}}'),
(140, 2, 'uk_user-settings', 'libraryContent=browse'),
(141, 2, 'uk_user-settings-time', '1519067953');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_users`
--

CREATE TABLE IF NOT EXISTS `uk_users` (
  `ID` bigint(20) unsigned NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_users`
--

INSERT INTO `uk_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'unika', '$P$BilRk36k4SxrvtFfeeQkjd2S33pFxx0', 'unika', 'contato@unika.com.br', '', '2017-10-29 15:38:23', '', 0, 'Unika'),
(2, 'Vanessa', '$P$Bb6CYxXHFYZSiQEJ8yeUYy/MfnFAo50', 'vanessa', 'vanessa@handgran.com', '', '2018-01-26 18:17:38', '1516990658:$P$BsBm.3lWacZt3LaXe9eMqIBG06coh1/', 0, 'Vanessa'),
(3, 'Karla', '$P$Bgg2bGd4FlxcksKeJBrno7Cy9QCZne0', 'karla', 'karla@unika.com.br', '', '2018-02-15 18:20:16', '1518718816:$P$B7hU/hQ/9rgMMGghWwfKRRhPgMTX/H/', 0, 'Karla Mikoski');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_campaign`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_campaign` (
  `campaign_id` int(10) unsigned NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `description` text
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `uk_wysija_campaign`
--

INSERT INTO `uk_wysija_campaign` (`campaign_id`, `name`, `description`) VALUES
(1, 'Guia de Usuário de 5 Minutos', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_campaign_list`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_campaign_list` (
  `list_id` int(10) unsigned NOT NULL,
  `campaign_id` int(10) unsigned NOT NULL,
  `filter` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_custom_field`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_custom_field` (
  `id` mediumint(9) NOT NULL,
  `name` tinytext NOT NULL,
  `type` tinytext NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `settings` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_email`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_email` (
  `email_id` int(10) unsigned NOT NULL,
  `campaign_id` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(250) NOT NULL DEFAULT '',
  `body` longtext,
  `created_at` int(10) unsigned DEFAULT NULL,
  `modified_at` int(10) unsigned DEFAULT NULL,
  `sent_at` int(10) unsigned DEFAULT NULL,
  `from_email` varchar(250) DEFAULT NULL,
  `from_name` varchar(250) DEFAULT NULL,
  `replyto_email` varchar(250) DEFAULT NULL,
  `replyto_name` varchar(250) DEFAULT NULL,
  `attachments` text,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `number_sent` int(10) unsigned NOT NULL DEFAULT '0',
  `number_opened` int(10) unsigned NOT NULL DEFAULT '0',
  `number_clicked` int(10) unsigned NOT NULL DEFAULT '0',
  `number_unsub` int(10) unsigned NOT NULL DEFAULT '0',
  `number_bounce` int(10) unsigned NOT NULL DEFAULT '0',
  `number_forward` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text,
  `wj_data` longtext,
  `wj_styles` longtext
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `uk_wysija_email`
--

INSERT INTO `uk_wysija_email` (`email_id`, `campaign_id`, `subject`, `body`, `created_at`, `modified_at`, `sent_at`, `from_email`, `from_name`, `replyto_email`, `replyto_name`, `attachments`, `status`, `type`, `number_sent`, `number_opened`, `number_clicked`, `number_unsub`, `number_bounce`, `number_forward`, `params`, `wj_data`, `wj_styles`) VALUES
(1, 1, 'Guia de Usuário de 5 Minutos', '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml"  >\n<head>\n    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />\n    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>\n    <title>Guia de Usuário de 5 Minutos</title>\n    <style type="text/css">body {\n        width:100% !important;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        margin:0;\n        padding:0;\n    }\n\n    body,table,td,p,a,li,blockquote{\n        -ms-text-size-adjust:100%;\n        -webkit-text-size-adjust:100%;\n    }\n\n    .ReadMsgBody{\n        width:100%;\n    }.ExternalClass {width:100%;}.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important; background:#e8e8e8;}img {\n        outline:none;\n        text-decoration:none;\n        -ms-interpolation-mode: bicubic;\n    }\n    a img {border:none;}\n    .image_fix {display:block;}p {\n        font-family: "Arial";\n        font-size: 16px;\n        line-height: 150%;\n        margin: 1em 0;\n        padding: 0;\n    }h1,h2,h3,h4,h5,h6{\n        margin:0;\n        padding:0;\n    }h1 {\n        color:#000000 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:40px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h2 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:30px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }h3 {\n        color:#424242 !important;\n        display:block;\n        font-family:Trebuchet MS;\n        font-size:24px;\n        font-style:normal;\n        font-weight:normal;\n        line-height:125%;\n        letter-spacing:normal;\n        margin:0;\n        \n        text-align:left;\n    }table td {border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;}table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }a {\n        color:#4a91b0;\n        word-wrap:break-word;\n    }\n    #outlook a {padding:0;}\n    .yshortcuts { color:#4a91b0; }\n\n    #wysija_wrapper {\n        background:#e8e8e8;\n        color:#000000;\n        font-family:"Arial";\n        font-size:16px;\n        -webkit-text-size-adjust:100%;\n        -ms-text-size-adjust:100%;\n        \n    }\n\n    .wysija_header_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_block {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        background:#ffffff;\n    }\n\n    .wysija_footer_container {\n        mso-border-right-alt: 0;\n        mso-border-left-alt: 0;\n        mso-border-top-alt: 0;\n        mso-border-bottom-alt: 0;\n        \n    }\n\n    .wysija_viewbrowser_container, .wysija_viewbrowser_container a {\n        font-family: "Arial" !important;\n        font-size: 12px !important;\n        color: #000000 !important;\n    }\n    .wysija_unsubscribe_container, .wysija_unsubscribe_container a {\n        text-align:center;\n        color: #000000 !important;\n        font-size:12px;\n    }\n    .wysija_viewbrowser_container a, .wysija_unsubscribe_container a {\n        text-decoration:underline;\n    }\n    .wysija_list_item {\n        margin:0;\n    }@media only screen and (max-device-width: 480px), screen and (max-width: 480px) {a[href^="tel"], a[href^="sms"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }body, table, td, p, a, li, blockquote { -webkit-text-size-adjust:none !important; }body{ width:100% !important; min-width:100% !important; }\n    }@media only screen and (min-device-width: 768px) and (max-device-width: 1024px), screen and (min-width: 768px) and (max-width: 1024px) {a[href^="tel"],\n        a[href^="sms"] {\n            text-decoration: none;\n            color: #4a91b0;pointer-events: none;\n            cursor: default;\n        }\n\n        .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {\n            text-decoration: default;\n            color: #4a91b0 !important;\n            pointer-events: auto;\n            cursor: default;\n        }\n    }\n\n    @media only screen and (-webkit-min-device-pixel-ratio: 2) {\n    }@media only screen and (-webkit-device-pixel-ratio:.75){}\n    @media only screen and (-webkit-device-pixel-ratio:1){}\n    @media only screen and (-webkit-device-pixel-ratio:1.5){}</style><!--[if IEMobile 7]>\n<style type="text/css">\n\n</style>\n<![endif]--><!--[if gte mso 9]>\n<style type="text/css">.wysija_image_container {\n        padding-top:0 !important;\n    }\n    .wysija_image_placeholder {\n        mso-text-raise:0;\n        mso-table-lspace:0;\n        mso-table-rspace:0;\n        margin-bottom: 0 !important;\n    }\n    .wysija_block .wysija_image_placeholder {\n        margin:2px 1px 0 1px !important;\n    }\n    p {\n        line-height: 110% !important;\n    }\n    h1, h2, h3 {\n        line-height: 110% !important;\n        margin:0 !important;\n        padding: 0 !important;\n    }\n</style>\n<![endif]-->\n\n<!--[if gte mso 15]>\n<style type="text/css">table { font-size:1px; mso-line-height-alt:0; line-height:0; mso-margin-top-alt:0; }\n    tr { font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px; }\n</style>\n<![endif]-->\n\n</head>\n<body bgcolor="#e8e8e8" yahoo="fix">\n    <span style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding-bottom:0;padding-left:0;padding-right:0;padding-top:0;display:block;background:#e8e8e8;">\n    <table width="100%" cellpadding="0" cellspacing="0" border="0" id="wysija_wrapper">\n        <tr>\n            <td valign="top" align="center">\n                <table width="600" cellpadding="0" cellspacing="0" border="0" align="center">\n                    \n                    <tr>\n                        <td width="600" style="min-width:600px;" valign="top" align="center"   >\n                            <p class="wysija_viewbrowser_container" style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;" >Problemas de visualização? <a style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;" href="[view_in_browser_link]" target="_blank">Veja esta newsletter em seu navegador.</a></p>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width="600" style="min-width:600px;" valign="top" align="center">\n                            \n<table class="wysija_header" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">\n <tr>\n <td height="1" align="center" class="wysija_header_container" style="font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;" >\n \n <img width="600" height="72" src="http://unika.handgran.com.br/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/header.png" border="0" alt="" class="image_fix" style="width:600px; height:72px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width="600" style="min-width:600px;" valign="top" align="left">\n                            \n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n \n \n <td class="wysija_content_container left" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" align="left" >\n \n <div class="wysija_text_container"><h2 style="font-family: ''Trebuchet MS'', ''Lucida Grande'', ''Lucida Sans Unicode'', ''Lucida Sans'', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;"><strong>Passo 1:</strong> ei, clique neste texto!</h2><p style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;">Para editar, simplesmente clique neste bloco de texto.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr style="font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;">\n <td width="100%" valign="middle" class="wysija_divider_container" style="height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;" align="left">\n <div align="center">\n <img src="http://unika.handgran.com.br/wp-content/uploads/wysija/dividers/solid.jpg" border="0" width="564" height="1" alt="---" class="image_fix" style="width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n \n \n <td class="wysija_content_container left" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" align="left" >\n \n <div class="wysija_text_container"><h2 style="font-family: ''Trebuchet MS'', ''Lucida Grande'', ''Lucida Sans Unicode'', ''Lucida Sans'', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;"><strong>Passo 2:</strong> brinque com esta imagem</h2></div>\n </td>\n \n </tr>\n</table>\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n \n \n <td class="wysija_content_container left" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" align="left" >\n \n \n \n <table style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;" width="1%" height="190" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr style="font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;">\n <td class="wysija_image_container left" style="border: 0;border-collapse: collapse;border: 1px solid #ffffff;display: block;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 10px;padding-bottom: 0;padding-left: 0;" width="1%" height="190" valign="top">\n <div align="left" class="wysija_image_placeholder left" style="height:190px;width:281px;border: 0;display: block;margin-top: 0;margin-right: 10px;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;" >\n \n <img width="281" height="190" src="http://unika.handgran.com.br/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/pigeon.png" border="0" alt="" class="image_fix" style="width:281px; height:190px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </div>\n </td>\n </tr>\n </table>\n\n <div class="wysija_text_container"><p style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;">Posicione o seu mouse acima da imagem à esquerda.</p></div>\n </td>\n \n </tr>\n</table>\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr style="font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;">\n <td width="100%" valign="middle" class="wysija_divider_container" style="height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;" align="left">\n <div align="center">\n <img src="http://unika.handgran.com.br/wp-content/uploads/wysija/dividers/solid.jpg" border="0" width="564" height="1" alt="---" class="image_fix" style="width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n \n \n <td class="wysija_content_container left" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" align="left" >\n \n <div class="wysija_text_container"><h2 style="font-family: ''Trebuchet MS'', ''Lucida Grande'', ''Lucida Sans Unicode'', ''Lucida Sans'', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;"><strong>Passo 3:</strong> solte conteúdo aqui</h2><p style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;">Arraste e solte <strong>texto, posts, divisores.</strong> Veja no lado direito!</p><p style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;">Você pode até criar <strong>compartilhamentos sociais</strong> como estes:</p></div>\n </td>\n \n </tr>\n</table>\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n <td class="wysija_gallery_container" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" >\n <table class="wysija_gallery_table center" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;text-align: center;margin-top: 0;margin-right: auto;margin-bottom: 0;margin-left: auto;" width="184" cellspacing="0" cellpadding="0" border="0" align="center">\n <tr>\n \n \n <td class="wysija_cell_container" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;" width="61" height="32" valign="top">\n <div align="center">\n <a style="color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;" href="http://www.facebook.com/mailpoetplugin"><img src="http://unika.handgran.com.br/wp-content/uploads/wysija/bookmarks/medium/02/facebook.png" border="0" alt="Facebook" style="width:32px; height:32px;" /></a>\n </div>\n </td>\n \n \n \n <td class="wysija_cell_container" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;" width="61" height="32" valign="top">\n <div align="center">\n <a style="color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;" href="http://www.twitter.com/mail_poet"><img src="http://unika.handgran.com.br/wp-content/uploads/wysija/bookmarks/medium/02/twitter.png" border="0" alt="Twitter" style="width:32px; height:32px;" /></a>\n </div>\n </td>\n \n \n \n <td class="wysija_cell_container" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 1px solid #ffffff;" width="61" height="32" valign="top">\n <div align="center">\n <a style="color: #4a91b0;color: #4a91b0 !important;background-color: #ffffff;border: 0;word-wrap: break-word;" href="https://plus.google.com/+Mailpoet"><img src="http://unika.handgran.com.br/wp-content/uploads/wysija/bookmarks/medium/02/google.png" border="0" alt="Google" style="width:32px; height:32px;" /></a>\n </div>\n </td>\n \n \n </tr>\n </table>\n </td>\n </tr>\n</table>\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr style="font-size:1px; mso-line-height-alt:0; mso-margin-top-alt:1px;">\n <td width="100%" valign="middle" class="wysija_divider_container" style="height:1px;background-color: #ffffff;border: 0;padding-top: 15px;padding-right: 17px;padding-bottom: 15px;padding-left: 17px;" align="left">\n <div align="center">\n <img src="http://unika.handgran.com.br/wp-content/uploads/wysija/dividers/solid.jpg" border="0" width="564" height="1" alt="---" class="image_fix" style="width:564px; height:1px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </div>\n </td>\n </tr>\n</table>\n\n\n<table class="wysija_block" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="left">\n <tr>\n \n \n <td class="wysija_content_container left" style="border: 0;border-collapse: collapse;background-color: #ffffff;border: 0;padding-top: 10px;padding-right: 17px;padding-bottom: 10px;padding-left: 17px;" align="left" >\n \n <div class="wysija_text_container"><h2 style="font-family: ''Trebuchet MS'', ''Lucida Grande'', ''Lucida Sans Unicode'', ''Lucida Sans'', Tahoma, sans-serif;font-size: 30px;color: #424242;color: #424242 !important;background-color: #ffffff;border: 0;font-weight: normal;font-style: normal;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 125%;margin-top: 0;margin-right: 0;margin-bottom: 0;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;"><strong>Passo 4:</strong> e o rodapé?</h2><p style="font-family: Arial, ''Helvetica Neue'', Helvetica, sans-serif;font-size: 16px;color: #000000;color: #000000 !important;background-color: #ffffff;border: 0;letter-spacing: normal;mso-line-height-rule: exactly;-mso-line-height-rule: exactly;line-height: 150%;margin-top: 1em;margin-right: 0;margin-bottom: 1em;margin-left: 0;padding-top: 0;padding-right: 0;padding-bottom: 0;padding-left: 0;vertical-align: top;word-wrap: break-word;">Modifique o conteúdo do rodapé na <strong>página de opções</strong> do MailPoet.</p></div>\n </td>\n \n </tr>\n</table>\n                        </td>\n                    </tr>\n                    <tr>\n                        <td width="600" style="min-width:600px;" valign="top" align="center"   >\n                            \n<table class="wysija_footer" style="border: 0;border-collapse: collapse;mso-table-lspace: 0pt; mso-table-rspace: 0pt;clear: both;border: 0;min-width: 100%;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center">\n <tr>\n <td height="1" align="center" class="wysija_footer_container" style="font-size:1px;line-height:1%;mso-line-height-rule:exactly;border: 0;min-width: 100%;background-color: #e8e8e8;border: 0;" >\n \n <img width="600" height="46" src="http://unika.handgran.com.br/wp-content/plugins/wysija-newsletters/img/default-newsletter/newsletter/footer.png" border="0" alt="" class="image_fix" style="width:600px; height:46px;text-decoration: none;outline: 0;border: 0;display: block;-ms-interpolation-mode: bicubic;" />\n </td>\n </tr>\n</table>\n                        </td>\n                    </tr>\n                    \n                    <tr>\n                        <td width="600" style="min-width:600px;" valign="top" align="center"  >\n                            <p class="wysija_unsubscribe_container" style="font-family: Verdana, Geneva, sans-serif;font-size: 12px;color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;text-align: center;padding-top: 8px;padding-right: 8px;padding-bottom: 8px;padding-left: 8px;" ><a style="color: #000000;color: #000000 !important;background-color: #e8e8e8;border: 0;" href="[unsubscribe_link]" target="_blank">Cancelar Assinatura</a><br /><br /></p>\n                        </td>\n                    </tr>\n                    \n                </table>\n            </td>\n        </tr>\n    </table>\n    </span>\n</body>\n</html>', 1509458682, 1509458682, NULL, 'info@localhost', 'unika', 'info@localhost', 'unika', NULL, 0, 1, 0, 0, 0, 0, 0, 0, 'YToxOntzOjE0OiJxdWlja3NlbGVjdGlvbiI7YToxOntzOjY6IndwLTMwMSI7YTo1OntzOjEwOiJpZGVudGlmaWVyIjtzOjY6IndwLTMwMSI7czo1OiJ3aWR0aCI7aToyODE7czo2OiJoZWlnaHQiO2k6MTkwO3M6MzoidXJsIjtzOjExNDoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy91bmlrYS93cC1jb250ZW50L3BsdWdpbnMvd3lzaWphLW5ld3NsZXR0ZXJzL2ltZy9kZWZhdWx0LW5ld3NsZXR0ZXIvbmV3c2xldHRlci9waWdlb24ucG5nIjtzOjk6InRodW1iX3VybCI7czoxMjI6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvdW5pa2Evd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvcGlnZW9uLTE1MHgxNTAucG5nIjt9fX0=', 'YTo0OntzOjc6InZlcnNpb24iO3M6NjoiMi43LjE0IjtzOjY6ImhlYWRlciI7YTo1OntzOjQ6InRleHQiO047czo1OiJpbWFnZSI7YTo1OntzOjM6InNyYyI7czoxMTQ6Imh0dHA6Ly9sb2NhbGhvc3QvcHJvamV0b3MvdW5pa2Evd3AtY29udGVudC9wbHVnaW5zL3d5c2lqYS1uZXdzbGV0dGVycy9pbWcvZGVmYXVsdC1uZXdzbGV0dGVyL25ld3NsZXR0ZXIvaGVhZGVyLnBuZyI7czo1OiJ3aWR0aCI7aTo2MDA7czo2OiJoZWlnaHQiO2k6NzI7czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo2OiJjZW50ZXIiO3M6Njoic3RhdGljIjtiOjA7czo0OiJ0eXBlIjtzOjY6ImhlYWRlciI7fXM6NDoiYm9keSI7YTo5OntzOjc6ImJsb2NrLTEiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MTYwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNVG84TDNOMGNtOXVaejRnWldrc0lHTnNhWEYxWlNCdVpYTjBaU0IwWlhoMGJ5RThMMmd5UGp4d1BsQmhjbUVnWldScGRHRnlMQ0J6YVcxd2JHVnpiV1Z1ZEdVZ1kyeHBjWFZsSUc1bGMzUmxJR0pzYjJOdklHUmxJSFJsZUhSdkxqd3ZjRDQ9Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6MTtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stMiI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjI7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjc2OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL3VuaWthL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTMiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6ODA6IlBHZ3lQanh6ZEhKdmJtYytVR0Z6YzI4Z01qbzhMM04wY205dVp6NGdZbkpwYm5GMVpTQmpiMjBnWlhOMFlTQnBiV0ZuWlcwOEwyZ3lQZz09Ijt9czo1OiJpbWFnZSI7TjtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6MztzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stNCI7YTo2OntzOjQ6InRleHQiO2E6MTp7czo1OiJ2YWx1ZSI7czo3NjoiUEhBK1VHOXphV05wYjI1bElHOGdjMlYxSUcxdmRYTmxJR0ZqYVcxaElHUmhJR2x0WVdkbGJTRERvQ0JsYzNGMVpYSmtZUzQ4TDNBKyI7fXM6NToiaW1hZ2UiO2E6NTp7czozOiJzcmMiO3M6MTE0OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL3VuaWthL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL3BpZ2Vvbi5wbmciO3M6NToid2lkdGgiO2k6MjgxO3M6NjoiaGVpZ2h0IjtpOjE5MDtzOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO31zOjk6ImFsaWdubWVudCI7czo0OiJsZWZ0IjtzOjY6InN0YXRpYyI7YjowO3M6ODoicG9zaXRpb24iO2k6NDtzOjQ6InR5cGUiO3M6NzoiY29udGVudCI7fXM6NzoiYmxvY2stNSI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjU7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjc2OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL3VuaWthL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTYiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MzAwOiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdNem84TDNOMGNtOXVaejRnYzI5c2RHVWdZMjl1ZEdYRHVtUnZJR0Z4ZFdrOEwyZ3lQanh3UGtGeWNtRnpkR1VnWlNCemIyeDBaU0E4YzNSeWIyNW5QblJsZUhSdkxDQndiM04wY3l3Z1pHbDJhWE52Y21Wekxqd3ZjM1J5YjI1blBpQldaV3BoSUc1dklHeGhaRzhnWkdseVpXbDBieUU4TDNBK1BIQStWbTlqdzZvZ2NHOWtaU0JoZE1PcElHTnlhV0Z5SUR4emRISnZibWMrWTI5dGNHRnlkR2xzYUdGdFpXNTBiM01nYzI5amFXRnBjend2YzNSeWIyNW5QaUJqYjIxdklHVnpkR1Z6T2p3dmNEND0iO31zOjU6ImltYWdlIjtOO3M6OToiYWxpZ25tZW50IjtzOjQ6ImxlZnQiO3M6Njoic3RhdGljIjtiOjA7czo4OiJwb3NpdGlvbiI7aTo2O3M6NDoidHlwZSI7czo3OiJjb250ZW50Ijt9czo3OiJibG9jay03IjthOjU6e3M6NToid2lkdGgiO2k6MTg0O3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo1OiJpdGVtcyI7YTozOntpOjA7YTo3OntzOjM6InVybCI7czozODoiaHR0cDovL3d3dy5mYWNlYm9vay5jb20vbWFpbHBvZXRwbHVnaW4iO3M6MzoiYWx0IjtzOjg6IkZhY2Vib29rIjtzOjk6ImNlbGxXaWR0aCI7aTo2MTtzOjEwOiJjZWxsSGVpZ2h0IjtpOjMyO3M6Mzoic3JjIjtzOjkwOiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL3VuaWthL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvYm9va21hcmtzL21lZGl1bS8wMi9mYWNlYm9vay5wbmciO3M6NToid2lkdGgiO2k6MzI7czo2OiJoZWlnaHQiO2k6MzI7fWk6MTthOjc6e3M6MzoidXJsIjtzOjMyOiJodHRwOi8vd3d3LnR3aXR0ZXIuY29tL21haWxfcG9ldCI7czozOiJhbHQiO3M6NzoiVHdpdHRlciI7czo5OiJjZWxsV2lkdGgiO2k6NjE7czoxMDoiY2VsbEhlaWdodCI7aTozMjtzOjM6InNyYyI7czo4OToiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy91bmlrYS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvdHdpdHRlci5wbmciO3M6NToid2lkdGgiO2k6MzI7czo2OiJoZWlnaHQiO2k6MzI7fWk6MjthOjc6e3M6MzoidXJsIjtzOjMzOiJodHRwczovL3BsdXMuZ29vZ2xlLmNvbS8rTWFpbHBvZXQiO3M6MzoiYWx0IjtzOjY6Ikdvb2dsZSI7czo5OiJjZWxsV2lkdGgiO2k6NjE7czoxMDoiY2VsbEhlaWdodCI7aTozMjtzOjM6InNyYyI7czo4ODoiaHR0cDovL2xvY2FsaG9zdC9wcm9qZXRvcy91bmlrYS93cC1jb250ZW50L3VwbG9hZHMvd3lzaWphL2Jvb2ttYXJrcy9tZWRpdW0vMDIvZ29vZ2xlLnBuZyI7czo1OiJ3aWR0aCI7aTozMjtzOjY6ImhlaWdodCI7aTozMjt9fXM6ODoicG9zaXRpb24iO2k6NztzOjQ6InR5cGUiO3M6NzoiZ2FsbGVyeSI7fXM6NzoiYmxvY2stOCI7YTo1OntzOjg6InBvc2l0aW9uIjtpOjg7czo0OiJ0eXBlIjtzOjc6ImRpdmlkZXIiO3M6Mzoic3JjIjtzOjc2OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL3VuaWthL3dwLWNvbnRlbnQvdXBsb2Fkcy93eXNpamEvZGl2aWRlcnMvc29saWQuanBnIjtzOjU6IndpZHRoIjtpOjU2NDtzOjY6ImhlaWdodCI7aToxO31zOjc6ImJsb2NrLTkiO2E6Njp7czo0OiJ0ZXh0IjthOjE6e3M6NToidmFsdWUiO3M6MTg4OiJQR2d5UGp4emRISnZibWMrVUdGemMyOGdORG84TDNOMGNtOXVaejRnWlNCdklISnZaR0Z3dzZrL1BDOW9NajQ4Y0Q1TmIyUnBabWx4ZFdVZ2J5QmpiMjUwWmNPNlpHOGdaRzhnY205a1lYRERxU0J1WVNBOGMzUnliMjVuUG5ERG9XZHBibUVnWkdVZ2IzRERwOE8xWlhNOEwzTjBjbTl1Wno0Z1pHOGdUV0ZwYkZCdlpYUXVQQzl3UGc9PSI7fXM6NToiaW1hZ2UiO047czo5OiJhbGlnbm1lbnQiO3M6NDoibGVmdCI7czo2OiJzdGF0aWMiO2I6MDtzOjg6InBvc2l0aW9uIjtpOjk7czo0OiJ0eXBlIjtzOjc6ImNvbnRlbnQiO319czo2OiJmb290ZXIiO2E6NTp7czo0OiJ0ZXh0IjtOO3M6NToiaW1hZ2UiO2E6NTp7czozOiJzcmMiO3M6MTE0OiJodHRwOi8vbG9jYWxob3N0L3Byb2pldG9zL3VuaWthL3dwLWNvbnRlbnQvcGx1Z2lucy93eXNpamEtbmV3c2xldHRlcnMvaW1nL2RlZmF1bHQtbmV3c2xldHRlci9uZXdzbGV0dGVyL2Zvb3Rlci5wbmciO3M6NToid2lkdGgiO2k6NjAwO3M6NjoiaGVpZ2h0IjtpOjQ2O3M6OToiYWxpZ25tZW50IjtzOjY6ImNlbnRlciI7czo2OiJzdGF0aWMiO2I6MDt9czo5OiJhbGlnbm1lbnQiO3M6NjoiY2VudGVyIjtzOjY6InN0YXRpYyI7YjowO3M6NDoidHlwZSI7czo2OiJmb290ZXIiO319', 'YToxMDp7czo0OiJodG1sIjthOjE6e3M6MTA6ImJhY2tncm91bmQiO3M6NjoiZThlOGU4Ijt9czo2OiJoZWFkZXIiO2E6MTp7czoxMDoiYmFja2dyb3VuZCI7czo2OiJlOGU4ZTgiO31zOjQ6ImJvZHkiO2E6NDp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjU6IkFyaWFsIjtzOjQ6InNpemUiO2k6MTY7czoxMDoiYmFja2dyb3VuZCI7czo2OiJmZmZmZmYiO31zOjY6ImZvb3RlciI7YToxOntzOjEwOiJiYWNrZ3JvdW5kIjtzOjY6ImU4ZThlOCI7fXM6MjoiaDEiO2E6Mzp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO3M6NjoiZmFtaWx5IjtzOjEyOiJUcmVidWNoZXQgTVMiO3M6NDoic2l6ZSI7aTo0MDt9czoyOiJoMiI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjQyNDI0MiI7czo2OiJmYW1pbHkiO3M6MTI6IlRyZWJ1Y2hldCBNUyI7czo0OiJzaXplIjtpOjMwO31zOjI6ImgzIjthOjM6e3M6NToiY29sb3IiO3M6NjoiNDI0MjQyIjtzOjY6ImZhbWlseSI7czoxMjoiVHJlYnVjaGV0IE1TIjtzOjQ6InNpemUiO2k6MjQ7fXM6MToiYSI7YToyOntzOjU6ImNvbG9yIjtzOjY6IjRhOTFiMCI7czo5OiJ1bmRlcmxpbmUiO2I6MDt9czoxMToidW5zdWJzY3JpYmUiO2E6MTp7czo1OiJjb2xvciI7czo2OiIwMDAwMDAiO31zOjExOiJ2aWV3YnJvd3NlciI7YTozOntzOjU6ImNvbG9yIjtzOjY6IjAwMDAwMCI7czo2OiJmYW1pbHkiO3M6NToiQXJpYWwiO3M6NDoic2l6ZSI7aToxMjt9fQ=='),
(2, 0, 'Confirme sua assinatura em Unika', 'Olá!\n\nLegal! Você se inscreveu como assinante em nosso site.\nPrecisamos que você ative sua assinatura para a(s) lista(s) [lists_to_confirm] clicando no link abaixo: \n\n[activation_link]Clique aqui para confirmar sua assinatura[/activation_link]\n\nObrigado,\n\nA equipe!\n', 1509458684, 1509458684, NULL, 'info@localhost', 'unika', 'info@localhost', 'unika', NULL, 99, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_email_user_stat`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_email_user_stat` (
  `user_id` int(10) unsigned NOT NULL,
  `email_id` int(10) unsigned NOT NULL,
  `sent_at` int(10) unsigned NOT NULL,
  `opened_at` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_email_user_url`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_email_user_url` (
  `email_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `url_id` int(10) unsigned NOT NULL,
  `clicked_at` int(10) unsigned DEFAULT NULL,
  `number_clicked` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_form`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_form` (
  `form_id` int(10) unsigned NOT NULL,
  `name` tinytext CHARACTER SET utf8 COLLATE utf8_bin,
  `data` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `styles` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `subscribed` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `uk_wysija_form`
--

INSERT INTO `uk_wysija_form` (`form_id`, `name`, `data`, `styles`, `subscribed`) VALUES
(1, 'Assine nossa Newsletter', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjQ6e3M6MTA6Im9uX3N1Y2Nlc3MiO3M6NzoibWVzc2FnZSI7czoxNToic3VjY2Vzc19tZXNzYWdlIjtzOjczOiJWZWphIHN1YSBjYWl4YSBkZSBlbnRyYWRhIG91IHBhc3RhIGRlIHNwYW0gcGFyYSBjb25maXJtYXIgc3VhIGFzc2luYXR1cmEuIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjEiO31zOjE3OiJsaXN0c19zZWxlY3RlZF9ieSI7czo1OiJhZG1pbiI7fXM6NDoiYm9keSI7YToyOntpOjA7YTo0OntzOjQ6Im5hbWUiO3M6NToiRW1haWwiO3M6NDoidHlwZSI7czo1OiJpbnB1dCI7czo1OiJmaWVsZCI7czo1OiJlbWFpbCI7czo2OiJwYXJhbXMiO2E6Mjp7czo1OiJsYWJlbCI7czo1OiJFbWFpbCI7czo4OiJyZXF1aXJlZCI7YjoxO319aToxO2E6NDp7czo0OiJuYW1lIjtzOjY6IkVudmlhciI7czo0OiJ0eXBlIjtzOjY6InN1Ym1pdCI7czo1OiJmaWVsZCI7czo2OiJzdWJtaXQiO3M6NjoicGFyYW1zIjthOjE6e3M6NToibGFiZWwiO3M6ODoiQXNzaW5hciEiO319fXM6NzoiZm9ybV9pZCI7aToxO30=', NULL, 0),
(2, 'Assinates Unika', 'YTo0OntzOjc6InZlcnNpb24iO3M6MzoiMC40IjtzOjg6InNldHRpbmdzIjthOjU6e3M6NzoiZm9ybV9pZCI7czoxOiIyIjtzOjU6Imxpc3RzIjthOjE6e2k6MDtzOjE6IjEiO31zOjEwOiJvbl9zdWNjZXNzIjtzOjc6Im1lc3NhZ2UiO3M6MTU6InN1Y2Nlc3NfbWVzc2FnZSI7czo3MzoiVmVqYSBzdWEgY2FpeGEgZGUgZW50cmFkYSBvdSBwYXN0YSBkZSBzcGFtIHBhcmEgY29uZmlybWFyIHN1YSBhc3NpbmF0dXJhLiI7czoxNzoibGlzdHNfc2VsZWN0ZWRfYnkiO3M6NToiYWRtaW4iO31zOjQ6ImJvZHkiO2E6Mjp7czo3OiJibG9jay0xIjthOjU6e3M6NjoicGFyYW1zIjthOjM6e3M6NToibGFiZWwiO3M6MTM6InlvdXIgZW1haWwuLi4iO3M6MTI6ImxhYmVsX3dpdGhpbiI7czoxOiIxIjtzOjg6InJlcXVpcmVkIjtzOjE6IjEiO31zOjg6InBvc2l0aW9uIjtpOjE7czo0OiJ0eXBlIjtzOjU6ImlucHV0IjtzOjU6ImZpZWxkIjtzOjU6ImVtYWlsIjtzOjQ6Im5hbWUiO3M6NToiRW1haWwiO31zOjc6ImJsb2NrLTIiO2E6NTp7czo2OiJwYXJhbXMiO2E6MTp7czo1OiJsYWJlbCI7czo2OiJFbnZpYXIiO31zOjg6InBvc2l0aW9uIjtpOjI7czo0OiJ0eXBlIjtzOjY6InN1Ym1pdCI7czo1OiJmaWVsZCI7czo2OiJzdWJtaXQiO3M6NDoibmFtZSI7czo2OiJFbnZpYXIiO319czo3OiJmb3JtX2lkIjtpOjI7fQ==', NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_list`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_list` (
  `list_id` int(10) unsigned NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `namekey` varchar(255) DEFAULT NULL,
  `description` text,
  `unsub_mail_id` int(10) unsigned NOT NULL DEFAULT '0',
  `welcome_mail_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_enabled` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `is_public` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created_at` int(10) unsigned DEFAULT NULL,
  `ordering` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `uk_wysija_list`
--

INSERT INTO `uk_wysija_list` (`list_id`, `name`, `namekey`, `description`, `unsub_mail_id`, `welcome_mail_id`, `is_enabled`, `is_public`, `created_at`, `ordering`) VALUES
(1, 'Minha primeira lista', 'minha-primeira-lista', 'A lista criada automaticamente na instalação do MailPoet.', 0, 0, 1, 1, 1509458682, 0),
(2, 'Usuários do WordPress', 'users', 'A lista criada automaticamente na importação dos assinantes do plugin : "WordPress', 0, 0, 0, 0, 1509458683, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_queue`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_queue` (
  `user_id` int(10) unsigned NOT NULL,
  `email_id` int(10) unsigned NOT NULL,
  `send_at` int(10) unsigned NOT NULL DEFAULT '0',
  `priority` tinyint(4) NOT NULL DEFAULT '0',
  `number_try` tinyint(3) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_url`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_url` (
  `url_id` int(10) unsigned NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `url` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_url_mail`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_url_mail` (
  `email_id` int(11) NOT NULL,
  `url_id` int(10) unsigned NOT NULL,
  `unique_clicked` int(10) unsigned NOT NULL DEFAULT '0',
  `total_clicked` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_user`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_user` (
  `user_id` int(10) unsigned NOT NULL,
  `wpuser_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL DEFAULT '',
  `lastname` varchar(255) NOT NULL DEFAULT '',
  `ip` varchar(100) NOT NULL,
  `confirmed_ip` varchar(100) NOT NULL DEFAULT '0',
  `confirmed_at` int(10) unsigned DEFAULT NULL,
  `last_opened` int(10) unsigned DEFAULT NULL,
  `last_clicked` int(10) unsigned DEFAULT NULL,
  `keyuser` varchar(255) NOT NULL DEFAULT '',
  `created_at` int(10) unsigned DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `domain` varchar(255) DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `uk_wysija_user`
--

INSERT INTO `uk_wysija_user` (`user_id`, `wpuser_id`, `email`, `firstname`, `lastname`, `ip`, `confirmed_ip`, `confirmed_at`, `last_opened`, `last_clicked`, `keyuser`, `created_at`, `status`, `domain`) VALUES
(1, 1, 'contato@unika.com.br', 'Unika', '', '', '0', NULL, NULL, NULL, 'a61e16c768ed63001ea2f40b168d04b1', 1509458684, 1, 'unika.com.br'),
(2, 2, 'vanessa@handgran.com', 'Vanessa', '', '168.181.49.179', '0', NULL, NULL, NULL, '90629a6185a8167e5a595ac335367330', 1516990658, 1, 'handgran.com'),
(3, 3, 'karla@unika.com.br', 'Karla Mikoski', '', '168.181.49.144', '0', NULL, NULL, NULL, '16463330ede6aa37968d14acc26cc5f6', 1518718816, 1, 'unika.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_user_field`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_user_field` (
  `field_id` int(10) unsigned NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `column_name` varchar(250) NOT NULL DEFAULT '',
  `type` tinyint(3) unsigned DEFAULT '0',
  `values` text,
  `default` varchar(250) NOT NULL DEFAULT '',
  `is_required` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `error_message` varchar(250) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `uk_wysija_user_field`
--

INSERT INTO `uk_wysija_user_field` (`field_id`, `name`, `column_name`, `type`, `values`, `default`, `is_required`, `error_message`) VALUES
(1, 'Nome', 'firstname', 0, NULL, '', 0, 'Por favor, digite seu nome'),
(2, 'Sobrenome', 'lastname', 0, NULL, '', 0, 'Por favor, digite seu sobrenome');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_user_history`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_user_history` (
  `history_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `email_id` int(10) unsigned DEFAULT '0',
  `type` varchar(250) NOT NULL DEFAULT '',
  `details` text,
  `executed_at` int(10) unsigned DEFAULT NULL,
  `executed_by` int(10) unsigned DEFAULT NULL,
  `source` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_wysija_user_list`
--

CREATE TABLE IF NOT EXISTS `uk_wysija_user_list` (
  `list_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `sub_date` int(10) unsigned DEFAULT '0',
  `unsub_date` int(10) unsigned DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `uk_wysija_user_list`
--

INSERT INTO `uk_wysija_user_list` (`list_id`, `user_id`, `sub_date`, `unsub_date`) VALUES
(1, 1, 1509458682, 0),
(2, 1, 1509458683, 0),
(2, 2, 1516990658, 0),
(2, 3, 1518718816, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_yoast_seo_links`
--

CREATE TABLE IF NOT EXISTS `uk_yoast_seo_links` (
  `id` bigint(20) unsigned NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_id` bigint(20) unsigned NOT NULL,
  `target_post_id` bigint(20) unsigned NOT NULL,
  `type` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_yoast_seo_links`
--

INSERT INTO `uk_yoast_seo_links` (`id`, `url`, `post_id`, `target_post_id`, `type`) VALUES
(8, 'http://www.energyinst.org/heartsandminds', 194, 0, 'external');

-- --------------------------------------------------------

--
-- Estrutura da tabela `uk_yoast_seo_meta`
--

CREATE TABLE IF NOT EXISTS `uk_yoast_seo_meta` (
  `object_id` bigint(20) unsigned NOT NULL,
  `internal_link_count` int(10) unsigned DEFAULT NULL,
  `incoming_link_count` int(10) unsigned DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `uk_yoast_seo_meta`
--

INSERT INTO `uk_yoast_seo_meta` (`object_id`, `internal_link_count`, `incoming_link_count`) VALUES
(8, 0, 0),
(9, 0, 0),
(10, 0, 0),
(11, 0, 0),
(13, 0, 0),
(15, 0, 0),
(17, 0, 0),
(19, 0, 0),
(21, 0, 0),
(22, 0, 0),
(23, 0, 0),
(24, 0, 0),
(25, 0, 0),
(26, 0, 0),
(27, 0, 0),
(28, 0, 0),
(29, 0, 0),
(30, 0, 0),
(31, 0, 0),
(32, 0, 0),
(33, 0, 0),
(34, 0, 0),
(39, 0, 0),
(40, 0, 0),
(41, 0, 0),
(42, 0, 0),
(44, 0, 0),
(52, 0, 0),
(53, 0, 0),
(54, 0, 0),
(55, 0, 0),
(56, 0, 0),
(57, 0, 0),
(1, 0, 0),
(60, 0, 0),
(63, 0, 0),
(2, 0, 0),
(71, 0, 0),
(73, 0, 0),
(75, 0, 0),
(77, 0, 0),
(83, 0, 0),
(85, 0, 0),
(86, 0, 0),
(87, 0, 0),
(91, 0, 0),
(92, 0, 0),
(93, 0, 0),
(94, 0, 0),
(95, 0, 0),
(96, 0, 0),
(97, 0, 0),
(98, 0, 0),
(99, 0, 0),
(100, 0, 0),
(101, 0, 0),
(102, 0, 0),
(106, 0, 0),
(107, 0, 0),
(112, 0, 0),
(113, 0, 0),
(114, 0, 0),
(115, 0, 0),
(117, 0, 0),
(119, 0, 0),
(121, 0, 0),
(124, 0, 0),
(126, 0, 0),
(127, 0, 0),
(129, 0, 0),
(131, 0, 0),
(133, 0, 0),
(70, 0, 0),
(69, 0, 0),
(67, 0, 0),
(3, 0, 0),
(146, 0, 0),
(150, 0, 0),
(171, 0, 0),
(173, 0, 0),
(177, 0, 0),
(176, 0, 0),
(191, 0, 0),
(194, 0, 0),
(199, 0, 0),
(202, 0, 0),
(204, 0, 0),
(211, 0, 0),
(212, 0, 0),
(213, 0, 0),
(214, 0, 0),
(145, 0, 0),
(66, 0, 0),
(68, 0, 0),
(216, 0, 0),
(215, 0, 0),
(227, 0, 0),
(122, 0, 0),
(120, 0, 0),
(116, 0, 0),
(65, 0, 0),
(118, 0, 0),
(62, 0, 0),
(58, 0, 0),
(237, 0, 0),
(135, 0, 0),
(218, 0, 0),
(242, 0, 0),
(235, 0, 0),
(233, 0, 0),
(232, 0, 0),
(231, 0, 0),
(230, 0, 0),
(234, 0, 0),
(222, 0, 0),
(192, 0, 0),
(226, 0, 0),
(179, 0, 0),
(225, 0, 0),
(228, 0, 0),
(253, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `uk_aiowps_events`
--
ALTER TABLE `uk_aiowps_events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uk_aiowps_failed_logins`
--
ALTER TABLE `uk_aiowps_failed_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uk_aiowps_global_meta`
--
ALTER TABLE `uk_aiowps_global_meta`
  ADD PRIMARY KEY (`meta_id`);

--
-- Indexes for table `uk_aiowps_login_activity`
--
ALTER TABLE `uk_aiowps_login_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uk_aiowps_login_lockdown`
--
ALTER TABLE `uk_aiowps_login_lockdown`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uk_aiowps_permanent_block`
--
ALTER TABLE `uk_aiowps_permanent_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uk_commentmeta`
--
ALTER TABLE `uk_commentmeta`
  ADD PRIMARY KEY (`meta_id`), ADD KEY `comment_id` (`comment_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `uk_comments`
--
ALTER TABLE `uk_comments`
  ADD PRIMARY KEY (`comment_ID`), ADD KEY `comment_post_ID` (`comment_post_ID`), ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`), ADD KEY `comment_date_gmt` (`comment_date_gmt`), ADD KEY `comment_parent` (`comment_parent`), ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `uk_links`
--
ALTER TABLE `uk_links`
  ADD PRIMARY KEY (`link_id`), ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `uk_options`
--
ALTER TABLE `uk_options`
  ADD PRIMARY KEY (`option_id`), ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `uk_postmeta`
--
ALTER TABLE `uk_postmeta`
  ADD PRIMARY KEY (`meta_id`), ADD KEY `post_id` (`post_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `uk_posts`
--
ALTER TABLE `uk_posts`
  ADD PRIMARY KEY (`ID`), ADD KEY `post_name` (`post_name`(191)), ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`), ADD KEY `post_parent` (`post_parent`), ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `uk_simple_history`
--
ALTER TABLE `uk_simple_history`
  ADD PRIMARY KEY (`id`), ADD KEY `date` (`date`), ADD KEY `loggerdate` (`logger`,`date`);

--
-- Indexes for table `uk_simple_history_contexts`
--
ALTER TABLE `uk_simple_history_contexts`
  ADD PRIMARY KEY (`context_id`), ADD KEY `history_id` (`history_id`), ADD KEY `key` (`key`);

--
-- Indexes for table `uk_termmeta`
--
ALTER TABLE `uk_termmeta`
  ADD PRIMARY KEY (`meta_id`), ADD KEY `term_id` (`term_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `uk_terms`
--
ALTER TABLE `uk_terms`
  ADD PRIMARY KEY (`term_id`), ADD KEY `slug` (`slug`(191)), ADD KEY `name` (`name`(191));

--
-- Indexes for table `uk_term_relationships`
--
ALTER TABLE `uk_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`), ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `uk_term_taxonomy`
--
ALTER TABLE `uk_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`), ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`), ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `uk_usermeta`
--
ALTER TABLE `uk_usermeta`
  ADD PRIMARY KEY (`umeta_id`), ADD KEY `user_id` (`user_id`), ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `uk_users`
--
ALTER TABLE `uk_users`
  ADD PRIMARY KEY (`ID`), ADD KEY `user_login_key` (`user_login`), ADD KEY `user_nicename` (`user_nicename`), ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `uk_wysija_campaign`
--
ALTER TABLE `uk_wysija_campaign`
  ADD PRIMARY KEY (`campaign_id`);

--
-- Indexes for table `uk_wysija_campaign_list`
--
ALTER TABLE `uk_wysija_campaign_list`
  ADD PRIMARY KEY (`list_id`,`campaign_id`);

--
-- Indexes for table `uk_wysija_custom_field`
--
ALTER TABLE `uk_wysija_custom_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uk_wysija_email`
--
ALTER TABLE `uk_wysija_email`
  ADD PRIMARY KEY (`email_id`);

--
-- Indexes for table `uk_wysija_email_user_stat`
--
ALTER TABLE `uk_wysija_email_user_stat`
  ADD PRIMARY KEY (`user_id`,`email_id`);

--
-- Indexes for table `uk_wysija_email_user_url`
--
ALTER TABLE `uk_wysija_email_user_url`
  ADD PRIMARY KEY (`user_id`,`email_id`,`url_id`);

--
-- Indexes for table `uk_wysija_form`
--
ALTER TABLE `uk_wysija_form`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `uk_wysija_list`
--
ALTER TABLE `uk_wysija_list`
  ADD PRIMARY KEY (`list_id`);

--
-- Indexes for table `uk_wysija_queue`
--
ALTER TABLE `uk_wysija_queue`
  ADD PRIMARY KEY (`user_id`,`email_id`), ADD KEY `SENT_AT_INDEX` (`send_at`);

--
-- Indexes for table `uk_wysija_url`
--
ALTER TABLE `uk_wysija_url`
  ADD PRIMARY KEY (`url_id`);

--
-- Indexes for table `uk_wysija_url_mail`
--
ALTER TABLE `uk_wysija_url_mail`
  ADD PRIMARY KEY (`email_id`,`url_id`);

--
-- Indexes for table `uk_wysija_user`
--
ALTER TABLE `uk_wysija_user`
  ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `EMAIL_UNIQUE` (`email`);

--
-- Indexes for table `uk_wysija_user_field`
--
ALTER TABLE `uk_wysija_user_field`
  ADD PRIMARY KEY (`field_id`);

--
-- Indexes for table `uk_wysija_user_history`
--
ALTER TABLE `uk_wysija_user_history`
  ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `uk_wysija_user_list`
--
ALTER TABLE `uk_wysija_user_list`
  ADD PRIMARY KEY (`list_id`,`user_id`);

--
-- Indexes for table `uk_yoast_seo_links`
--
ALTER TABLE `uk_yoast_seo_links`
  ADD PRIMARY KEY (`id`), ADD KEY `link_direction` (`post_id`,`type`);

--
-- Indexes for table `uk_yoast_seo_meta`
--
ALTER TABLE `uk_yoast_seo_meta`
  ADD UNIQUE KEY `object_id` (`object_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `uk_aiowps_events`
--
ALTER TABLE `uk_aiowps_events`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uk_aiowps_failed_logins`
--
ALTER TABLE `uk_aiowps_failed_logins`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `uk_aiowps_global_meta`
--
ALTER TABLE `uk_aiowps_global_meta`
  MODIFY `meta_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uk_aiowps_login_activity`
--
ALTER TABLE `uk_aiowps_login_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `uk_aiowps_login_lockdown`
--
ALTER TABLE `uk_aiowps_login_lockdown`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uk_aiowps_permanent_block`
--
ALTER TABLE `uk_aiowps_permanent_block`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uk_commentmeta`
--
ALTER TABLE `uk_commentmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uk_comments`
--
ALTER TABLE `uk_comments`
  MODIFY `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `uk_links`
--
ALTER TABLE `uk_links`
  MODIFY `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uk_options`
--
ALTER TABLE `uk_options`
  MODIFY `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1193;
--
-- AUTO_INCREMENT for table `uk_postmeta`
--
ALTER TABLE `uk_postmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=777;
--
-- AUTO_INCREMENT for table `uk_posts`
--
ALTER TABLE `uk_posts`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=254;
--
-- AUTO_INCREMENT for table `uk_simple_history`
--
ALTER TABLE `uk_simple_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=161;
--
-- AUTO_INCREMENT for table `uk_simple_history_contexts`
--
ALTER TABLE `uk_simple_history_contexts`
  MODIFY `context_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1729;
--
-- AUTO_INCREMENT for table `uk_termmeta`
--
ALTER TABLE `uk_termmeta`
  MODIFY `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uk_terms`
--
ALTER TABLE `uk_terms`
  MODIFY `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `uk_term_taxonomy`
--
ALTER TABLE `uk_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `uk_usermeta`
--
ALTER TABLE `uk_usermeta`
  MODIFY `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT for table `uk_users`
--
ALTER TABLE `uk_users`
  MODIFY `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `uk_wysija_campaign`
--
ALTER TABLE `uk_wysija_campaign`
  MODIFY `campaign_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `uk_wysija_custom_field`
--
ALTER TABLE `uk_wysija_custom_field`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uk_wysija_email`
--
ALTER TABLE `uk_wysija_email`
  MODIFY `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `uk_wysija_form`
--
ALTER TABLE `uk_wysija_form`
  MODIFY `form_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `uk_wysija_list`
--
ALTER TABLE `uk_wysija_list`
  MODIFY `list_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `uk_wysija_url`
--
ALTER TABLE `uk_wysija_url`
  MODIFY `url_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uk_wysija_url_mail`
--
ALTER TABLE `uk_wysija_url_mail`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uk_wysija_user`
--
ALTER TABLE `uk_wysija_user`
  MODIFY `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `uk_wysija_user_field`
--
ALTER TABLE `uk_wysija_user_field`
  MODIFY `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `uk_wysija_user_history`
--
ALTER TABLE `uk_wysija_user_history`
  MODIFY `history_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `uk_yoast_seo_links`
--
ALTER TABLE `uk_yoast_seo_links`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
